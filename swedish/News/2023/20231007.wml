#use wml::debian::translation-check translation="b827d01d6caf2b68e865bd2c4ff78fab56ab8eb4"
<define-tag pagetitle>Uppdaterad Debian 12; 12.2 utgiven</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin andra uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction amd64-microcode "Update included microcode, including fixes for <q>AMD Inception</q> on AMD Zen4 processors [CVE-2023-20569]">
<correction arctica-greeter "Support configuring the onscreen keyboard theme via ArcticaGreeter's gsettings; use <q>Compact</q> OSK layout (instead of Small) which includes special keys such as German Umlauts; fix display of authentication failure messages; use active theme rather than emerald">
<correction autofs "Fix regression determining reachability on dual-stack hosts">
<correction base-files "Update for the 12.2 point release">
<correction batik "Fix Server Side Request Forgery issues [CVE-2022-44729 CVE-2022-44730]">
<correction boxer-data "No longer install https-everywhere for Firefox">
<correction brltty "xbrlapi: Do not try to start brltty with ba+a2 when unavailable; fix cursor routing and braille panning in Orca when xbrlapi is installed but the a2 screen driver is not">
<correction ca-certificates-java "Work around unconfigured JRE during new installations">
<correction cairosvg "Handle data: URLs in safe mode">
<correction calibre "Fix export feature">
<correction clamav "New upstream stable release; security fixes [CVE-2023-20197 CVE-2023-20212]">
<correction cryptmount "Avoid memory initialisation issues in command line parser">
<correction cups "Fix heap-based buffer overflow issue [CVE-2023-4504]; fix unauthenticated access issue [CVE-2023-32360]">
<correction curl "Build with OpenLDAP to correct improper fetch of binary LDAP attributes; fix excessive memory consumption issue [CVE-2023-38039]">
<correction cyrus-imapd "Ensure mailboxes are not lost on upgrades from bullseye">
<correction dar "Fix issues with creating isolated catalogs when dar was built using a recent gcc version">
<correction dbus "New upstream stable release; fix a dbus-daemon crash during policy reload if a connection belongs to a user account that has been deleted, or if a Name Service Switch plugin is broken, on kernels not supporting SO_PEERGROUPS; report the error correctly if getting the groups of a uid fails; dbus-user-session: Copy XDG_CURRENT_DESKTOP to activation environment">
<correction debian-archive-keyring "Clean up leftover keyrings in trusted.gpg.d">
<correction debian-edu-doc "Update Debian Edu Bookworm manual">
<correction debian-edu-install "New upstream release; adjust D-I auto-partitioning sizes">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-13; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-parl "Rebuild with newer boxer-data; no longer depend on webext-https-everywhere">
<correction debianutils "Fix duplicate entries in /etc/shells; manage /bin/sh in the state file; fix canonicalization of shells in aliased locations">
<correction dgit "Use the old /updates security map only for buster; prevent pushing older versions than are already in the archive">
<correction dhcpcd5 "Ease upgrades with leftovers from wheezy; drop deprecated ntpd integration; fix version in cleanup script">
<correction dpdk "New upstream stable release">
<correction dput-ng "Update permitted upload targets; fix failure to build from source">
<correction efibootguard "Fix Insufficient or missing validation and sanitization of input from untrustworthy bootloader environment files [CVE-2023-39950]">
<correction electrum "Fix a Lightning security issue">
<correction filezilla "Fix builds for 32-bit architectures; fix crash when removing filetypes from list">
<correction firewalld "Don't mix IPv4 and IPv6 addresses in a single nftables rule">
<correction flann "Drop extra -llz4 from flann.pc">
<correction foot "Ignore XTGETTCAP queries with invalid hex encodings">
<correction freedombox "Use n= in apt preferences for smooth upgrades">
<correction freeradius "Ensure TLS-Client-Cert-Common-Name contains correct data">
<correction ghostscript "Fix buffer overflow issue [CVE-2023-38559]; try and secure the IJS server startup [CVE-2023-43115]">
<correction gitit "Rebuild against new pandoc">
<correction gjs "Avoid infinite loops of idle callbacks if an idle handler is called during GC">
<correction glibc "Fix the value of F_GETLK/F_SETLK/F_SETLKW with __USE_FILE_OFFSET64 on ppc64el; fix a stack read overflow in getaddrinfo in no-aaaa mode [CVE-2023-4527]; fix use after free in getcanonname [CVE-2023-4806 CVE-2023-5156]; fix _dl_find_object to return correct values even during early startup">
<correction gosa-plugins-netgroups "Silence deprecation warnings in web interface">
<correction gosa-plugins-systems "Fix management of DHCP/DNS entries in default theme; fix adding (standalone) <q>Network printer</q> systems; fix generation of target DNs for various system types; fix icon rendering in DHCP servlet; enforce unqualified hostname for workstations">
<correction gtk+3.0 "New upstream stable release; fix several crashes; show more information in the <q>inspector</q> debugging interface; silence GFileInfo warnings if used with a backported version of GLib; use a light colour for the caret in dark themes, making it much easier to see in some apps, in particular Evince">
<correction gtk4 "Fix truncation in places sidebar with large text accessibility setting">
<correction haskell-hakyll "Rebuild against new pandoc">
<correction highway "Fix support for armhf systems lacking NEON">
<correction hnswlib "Fix double free in init_index when the M argument is a large integer [CVE-2023-37365]">
<correction horizon "Fix open redirect issue [CVE-2022-45582]">
<correction icingaweb2 "Suppress undesirable deprecation notices">
<correction imlib2 "Fix preservation of alpha channel flag">
<correction indent "Fix out of buffer read; fix buffer overwrite [CVE-2023-40305]">
<correction inetutils "Check return values when dropping privileges [CVE-2023-40303]">
<correction inn2 "Fix nnrpd hangs when compression is enabled; add support for high-precision syslog timestamps; make inn-{radius,secrets}.conf not world readable">
<correction jekyll "Support YAML aliases">
<correction kernelshark "Fix segfault in libshark-tepdata; fix capturing when target directory contains a space">
<correction krb5 "Fix freeing of uninitialised pointer [CVE-2023-36054]">
<correction lemonldap-ng "Apply login control to auth-slave requests; fix open redirection due to incorrect escape handling; fix open redirection when OIDC RP has no redirect URIs; fix Server Side Request Forgery issue [CVE-2023-44469]">
<correction libapache-mod-jk "Remove implicit mapping functionality, which could lead to unintended exposure of the status worker and/or bypass of security constraints [CVE-2023-41081]">
<correction libclamunrar "New upstream stable release">
<correction libmatemixer "Fix heap corruptions / application crashes when removing audio devices">
<correction libpam-mklocaluser "pam-auth-update: ensure the module is ordered before other session type modules">
<correction libxnvctrl "New source package split from nvidia-settings">
<correction linux "New upstream stable release">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction llvm-defaults "Fix /usr/include/lld symlink; add Breaks against not co-installable packages for smoother upgrades from bullseye">
<correction ltsp "Avoid using mv on init symlink">
<correction lxc "Fix nftables syntax for IPv6 NAT">
<correction lxcfs "Fix CPU reporting within an arm32 container with large numbers of CPUs">
<correction marco "Only enable compositing if it is available">
<correction mariadb "New upstream bugfix release">
<correction mate-notification-daemon "Fix two memory leaks">
<correction mgba "Fix broken audio in libretro core; fix crash on hardware incapable of OpenGL 3.2">
<correction modsecurity "Fix denial of service issue [CVE-2023-38285]">
<correction monitoring-plugins "check_disk: avoid mounting when searching for matching mount points, resolving a regression in speed from bullseye">
<correction mozjs102 "New upstream stable release; fix <q>incorrect value used during WASM compilation</q> [CVE-2023-4046], potential use after free issue [CVE-2023-37202], memory safety issues [CVE-2023-37211 CVE-2023-34416]">
<correction mutt "New upstream stable release">
<correction nco "Re-enable udunits2 support">
<correction nftables "Fix incorrect bytecode generation hit with new kernel check that rejects adding rules to bound chains">
<correction node-dottie "Security fix (prototype pollution) [CVE-2023-26132]">
<correction nvidia-settings "New upstream bugfix release">
<correction nvidia-settings-tesla "New upstream bugfix release">
<correction nx-libs "Fix missing symlink /usr/share/nx/fonts; fix manpage">
<correction open-ath9k-htc-firmware "Load correct firmware">
<correction openbsd-inetd "Fix memory handling issues">
<correction openrefine "Fix arbitrary code execution issue [CVE-2023-37476]">
<correction openscap "Fix dependencies of openscap-utils and python3-openscap">
<correction openssh "Fix remote code execution issue via a forwarded agent socket [CVE-2023-38408]">
<correction openssl "New upstream stable release; security fixes [CVE-2023-2975 CVE-2023-3446 CVE-2023-3817]">
<correction pam "Fix pam-auth-update --disable; update Turkish translation">
<correction pandoc "Fix arbitrary file write issue [CVE-2023-35936]">
<correction plasma-framework "Fix plasmashell crashes">
<correction plasma-workspace "Fix crash in krunner">
<correction python-git "Fix remote code execution issue [CVE-2023-40267], blind local file inclusion issue [CVE-2023-41040]">
<correction pywinrm "Fix compatibility with Python 3.11">
<correction qemu "Update to upstream 7.2.5 tree; ui/vnc-clipboard: fix infinite loop in inflate_buffer [CVE-2023-3255]; fix NULL pointer dereference issue [CVE-2023-3354]; fix buffer overflow issue [CVE-2023-3180]">
<correction qtlocation-opensource-src "Fix freeze when loading map tiles">
<correction rar "Upstream bugfix release [CVE-2023-40477]">
<correction reprepro "Fix race condition when using external decompressors">
<correction rmlint "Fix error in other packages caused by invalid python package version; fix GUI startup failure with recent python3.11">
<correction roundcube "New upstream stable release; fix OAuth2 authentication; fix cross site scripting issues [CVE-2023-43770]">
<correction runit-services "dhclient: don't hardcode use of eth1">
<correction samba "New upstream stable release">
<correction sitesummary "New upstream release; fix installation of sitesummary-maintenance CRON/systemd-timerd script; fix insecure temporary file and directory creation">
<correction slbackup-php "Bug fixes: log remote commands to stderr; disable SSH known hosts files; PHP 8 compatibility">
<correction spamprobe "Fix crashes parsing JPEG attachments">
<correction stunnel4 "Fix handling of a peer closing TLS connection without proper shutdown messaging">
<correction systemd "New upstream stable release; fix minor security issue in arm64 and riscv64 systemd-boot (EFI) with device tree blobs loading">
<correction testng7 "Backport to stable for future openjdk-17 builds">
<correction timg "Fix buffer overflow vulnerability [CVE-2023-40968]">
<correction transmission "Replace openssl3 compat patch to fix memory leak">
<correction unbound "Fix error log flooding when using DNS over TLS with openssl 3.0">
<correction unrar-nonfree "Fix remote code execution issue [CVE-2023-40477]">
<correction vorta "Handle ctime and mtime changes in diffs">
<correction vte2.91 "Invalidate ring view more often when necessary, fixing various assertion failures during event handling">
<correction x2goserver "x2goruncommand: add support for KDE Plasma 5; x2gostartagent: prevent logfile corruption; keystrokes.cfg: sync with nx-libs; fix encoding of Finnish translation">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2023 5454 kanboard>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5458 openjdk-17>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5460 curl>
<dsa 2023 5462 linux-signed-amd64>
<dsa 2023 5462 linux-signed-arm64>
<dsa 2023 5462 linux-signed-i386>
<dsa 2023 5462 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5466 ntpsec>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5469 thunderbird>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5477 samba>
<dsa 2023 5479 chromium>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5487 chromium>
<dsa 2023 5488 thunderbird>
<dsa 2023 5491 chromium>
<dsa 2023 5492 linux-signed-amd64>
<dsa 2023 5492 linux-signed-arm64>
<dsa 2023 5492 linux-signed-i386>
<dsa 2023 5492 linux>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5496 firefox-esr>
<dsa 2023 5497 libwebp>
<dsa 2023 5498 thunderbird>
<dsa 2023 5501 gnome-shell>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction https-everywhere "föråldrat, stora webbläsare erbjuder inbyggt stöd">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


