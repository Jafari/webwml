# Status: published
# $Rev$
#use wml::debian::translation-check translation="2a1830b31e7529340bf703cd96b2b22677f850df"
<define-tag pagetitle>Säkerhetsstöd för Bullseye har överlämnats till LTS-gruppen</define-tag>
<define-tag release_date>2024-08-14</define-tag>
#use wml::debian::news

<p>
Den 14 augusti 2024, tre år efter dess ursprungliga utgåva, upphör 
det regelbundna säkerhetsstödet för Debian 11, alias <q>Bullseye</q>. Debians
<a href="https://wiki.debian.org/LTS/">grupp för långtidsstöd (LTS - Long Term Support)</a>
kommer att ta över säkerhetsstödet från säkerhetsgruppen och utgåvegruppen.
</p>

<h2>Information för användare</h2>

<p>
Bullseye LTS kommer att ges stöd från 15 augusti 2024 till 31 augusti 2026.
</p>

<p>
När det är möjligt uppmuntras användare att uppgradera sina maskiner till
Debian 12, alias <q>Bookworm</q>, den nuvarande stabila utgåvan av Debian.
För att göra livscykeln för Debianutgåvor lättare att komma ihåg har de
relaterade Debiangrupperna kommit överens om följande schema: tre års
regelbundet stöd plus två års långtidsstöd. Debian 12 kommer att få sitt
reguljära stöd till 10 juni 2026 och långtidsstöd till 30 juni 2028, tre
respektive fem år efter ursprungliga utgåvan.
</p>

<p>
Användare som måste hållas sig till Debian 11 kan hitta relevant information om
Debians långtidsstöd på
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
Viktig information och förändringar rörande Bullseye LTS specifikt kan hittas
på
<a href="https://wiki.debian.org/LTS/Bullseye">LTS/Bullseye</a>.
</p>

<p>
Debina 11 LTS-användare är inbjudna till att prenumerera på
<a href="https://lists.debian.org/debian-lts-announce/">sändlistan för
tillkännagivanden</a> för att få notifikationer om säkerhetsuppdateringar,
eller för att följa de senaste bulletinerna genom webbsidan för
<a href="https://www.debian.org/lts/security/">LTS Säkerhetsinformation</a>.
</p>

<p>
Några paket täcks inte av Bullseyes LTS-stöd. Paket utan stöd som är
installerade i en användares maskin kan identifieras genom att installera
paketet
<a href="https://tracker.debian.org/pkg/debian-security-support">debian-security-support</a>.
Om debian-security-support detekterar ett paket utan stöd som är kritiskt för
dig, var vänlig kontakta 
<strong>debian-lts@lists.debian.org</strong>.
</p>

<p>
Debian och dess LTS-grupp skulle vilja tacka alla bidragande användare,
utvecklare, sponsorer och andra Debian-grupper som gör det möjligt att
förlänga livslängden för tidigare stabila utgåvor, och som har gjort
Buster LTS till en framgång.
</p>

<p>
Om du förlitar dig på Debian LTS, vänligen överväg
<a href="https://wiki.debian.org/LTS/Development">gå med i teamet</a>,
tillhandahåll patchar, testning eller
<a href="https://wiki.debian.org/LTS/Funding">finansiera insatserna</a>.
</p>


<h2>Om Debian</h2>

<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella 
	operativsystemet</q>.
</p>

<h2>Kontaktinformation</h2>

<p>
	För ytterligare information, vänligen besök Debians webbplats på
	<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på 
	engelska) till &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

