#use wml::debian::translation-check translation="08d95547b797f8dd3e3b1575b759cba78e627b32"
<define-tag pagetitle>Debianprojektet sörjer förlusten av Peter De Schrijver</define-tag>
<define-tag release_date>2024-07-25</define-tag>
#use wml::debian::news

<p>
   Debianprojektet sörjer vår medutvecklare och vän, Peter De Schrijvers
   plötsliga bortgång.
</p>

<p>
   Många av oss kände Peter som en väldigt hjälpsam och hängiven person och vi
   uppskattade hans bidrag till vårt projekt och Linuxgemenskapen.
</p>

<p>
   Peter var ett bekant ansikte på många konferenser och möten runt om i världen.
</p>

<p>
   Peter var mycket uppskattad för sin tekniska expertis inom problemlösning
   och för sin vilja att dela med sig av den kunskapen. På frågan "vad jobbar
   du med?", tog Peter ofta tid att förklara något som du tyckte var extremt
   komplicerat på ett förståeligt sätt, eller visade dig personligen sin höga
   skicklighet i agerande på sådana uppgifter som att översätta en
   disassemblerad binär till C-källkod.
</p>

<p>
   Peters arbete, ideal och minne lämnar ett anmärkningsvärt arv och en förlust
   som märks runt om i världen, inte bara i de många gemenskaper som han
   interagerade med utan i de han inspirerade och berörde också.
</p>

<p>
   Våra tankar finns med hans familj.
</p>

<h2>Om Debian</h2>

<p>
	Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
	ger frivilligt av sin tid och insats för att producera det helt fria
	operativsystemet Debian.
</p>


<h2>Kontaktinformation</h2>

<p>
   För ytterligare information, vänligen besök Debians webbplats på
   <a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på engelska)
   till &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
