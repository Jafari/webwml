#use wml::debian::template title="Installera Debian via Internet" BARETITLE=true
#use wml::debian::translation-check translation="75e33068da4f64cce68c5fda8f15b7d6cd7179ea"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>
Om du har en permanent anslutning till Internet kan du installera Debian
med hjälp av den.
Du kommer till att börja med att bara hämta den lilla del av Debian som krävs
för att starta installationsproceduren, och sedan installera det du vill ha i
tillägg inifrån installationsprogrammet.
</p>
<p>Denna metod för installation av Debian förutsätter en fungerande internetanslutning
<em>under</em> installationen. Jämfört med andra metoder laddar du ner
mindre data eftersom processen skräddarsys efter dina behov. Ethernet- och
trådlösa anslutningar stöds.</p>

<div class="line">
<div class="item col50">

<h2>Liten cd eller usb-minnen</h2>
<p>
  Följande är avbildningsfiler. För ytterligare detaljer, se: <a
  href="../CD/netinst/">Nätverksinstallation från en minimal CD</a>
</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<div class="line">
<div class="item col50">

<h2>Minimal cd, flexibla usb-minnen, osv.</h2>

<p>
  <strong>För avancerade användare:</strong>:
  Du kan hämta ett antal små avbildningar lämpade för
  usb-pinnar och liknande enheter, skriva dem till media och sedan påbörja
  installationen genom att starta maskinen från dessa.
</p>

<p>
  Hur installation från de minsta medieformaten går till skiljer sig mellan
  arkitekturerna.
</p>


<p>
  Se
  <a href="$(HOME)/releases/stable/installmanual">installationsmanualen
  för din arkitektur</a>, speciellt kapitlet <q>Få tag på
  installationsmedia</q>, för ytterligare information.
</p>

<p>
  Se följande för länkar till tillgängliga avbildningsfiler (se
  MANIFEST-filen för information):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<br/>

<h2>Starta från nätverk</h2>

<p>
  <strong>För avancerade användare</strong>:
  Du sätter upp en TFTP- och en DHCP- (eller BOOTP- eller RARP-) -server
  som tillhandahåller installationsmedia till maskiner på ditt lokala
  nätverk.
  Om BIOS på klientdatorn stöder det kan du starta Debians
  installationssystem från nätverket (med PXE och TFTP) och sedan
  fortsätta med att installera resten av Debian från nätverket.
</p>

<p>
  Det är inte alla maskiner som stöder nätverksstart.
  På grund av det merarbete som behövs rekommenderas inte detta sätt
  att installera till nybörjaranvändare.
</p>
  
<p>
  Se
  <a href="$(HOME)/releases/stable/installmanual">installationsmanualen
  för din arkitektur</a>, speciellt kapitlet <q>Förbered filerna för
  nätverksuppstart via TFTP</q>, för ytterligare information.
</p>

<p>
  Se följande för länkar till tillgängliga avbildningsfiler (se
  MANIFEST-filen för information):
</p>

<stable-netboot-images />
</div>
</div>

