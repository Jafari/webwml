#use wml::debian::cdimage title="Hämta Debian-USB-/cd-/-dvd-avbildningar via http/ftp" BARETITLE=true
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Följande Debianavbildningar finns att hämta direkt:</p>

<ul>
 <li>
  <a href="#stable">Officiella USB-/cd-/dvd-avbildningar för den
  stabila utgåvan (<q lang="en">stable</q>)</a>.
 </li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Officiella
  USB/CD/DVD-avbildningar av <q>uttestnings</q>-distributionen (<em>genereras
  varje vecka</em>)</a></li>

</ul>

<p>
Se även:
</p>

<ul>
 <li>
  En fullständig
  <a href="#mirrors">förteckning över <tt>debian-cd/</tt>-speglar</a>.
 </li>

 <li>
  För <q>nätverksinstallation</q>, se sidan om
  <a href="../netinst/">nätverksinstallationer</a>.
 </li>

 <li>
  För avbildningar av <q>uttestningsutgåvan</q> se
  <a href="$(DEVEL)/debian-installer/">Debian-Installer-sidan</a>.
 </li>
</ul>

<hr />

<h2><a name="stable">Officiella usb/cd/dvd-avbildningar för den <q>stabila</q>
utgåvan</a></h2>

<p>
För att installera Debian på en maskin som saknar Internetförbindelse kan du
använda cd/usb- (700 Mbyte styck) eller dvd/usb-avbildningar (4,7 Gbyte styck).
Hämta den första cd/usb- eller dvd/usb-avbildningsfilen, skriv den med en
usb-/cd-/dvd-inspelare och starta om datorn från den.
</p>

<p>
Den <strong>första</strong> usb:n/cd:n/dvd:n innehåller alla filer som behövs för
att installera ett vanligt Debiansystem.<br />
</p>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>

<p>
Länkarna nedan pekar på avbildningsfiler som är upp till 700 Mbyte stora,
vilket gör att de passar att skriva till vanliga cd-r(w)-media:
</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>

<p>
Länkarna nedan pekar på avbildningsfiler som är upp till 4,7 Gbyte stora,
vilket gör att de passar att skriva till vanliga dvd-r/dvd+r och liknande
media:
</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Se till att läsa dokumentationen innan du installerar.
<strong>Om du bara tänker läsa ett dokument</strong> innan du installerar,
se vår
<a href="$(HOME)/releases/stable/amd64/apa">installationshjälp</a>, en snabb
genomgång av installationsprocessen.
Dessutom finns följande nyttiga dokumentation:
</p>

<ul>
 <li>
  <a href="$(HOME)/releases/stable/installmanual">Installationsguide</a>,
  de detaljerade installationsinstruktionerna.
 </li>
 <li>
  <a href="https://wiki.debian.org/DebianInstaller">Dokumentation för
  Debian-installer</a>, bland annat vanliga frågor med svar.
 </li>
 <li>
  <a href="$(HOME)/releases/stable/debian-installer/#errata">Errata för
  Debian-installer</a>, en förteckning över kända problem i
  installationsprogrammet.
 </li>
</ul>

<hr />

<h2><a name="mirrors">Registrerade speglar av
<q>debian-cd</q>-arkivet</a></h2>

<p>Observera att <strong>vissa speglar inte är à jour</strong> &ndash;
den aktuella utgåvan av de "stabila" USB/CD/DVD-avbildningarna är <strong><current-cd-release></strong>
</p>

<p>
<strong>Om du är osäker bör du använda
<a href="https://cdimage.debian.org/debian-cd/">den primära servern för
cd-avbildningar</a> i Sverige</strong>.</p>

<p>Vill du erbjuda Debian-cd-avbildningar på din spegel?
I så fall, se
<a href="../mirroring/">instruktionerna för att skapa en
cd-avbildningsspegel</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
