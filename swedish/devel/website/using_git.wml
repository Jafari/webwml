#use wml::debian::template title="Använda Git för att jobba på Debianwebbplatsen" MAINPAGE="true"
#use wml::debian::translation-check translation="68ac2b55da5ec0c0222f324e928679e27e5de908"
#use wml::debian::toc

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Jobba på Gitförrådet</a></li>
<li><a href="#write-access">Skrivåtkomst till Gitförrådet</a></li>
<li><a href="#notifications">Få aviseringar</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a> är ett <a
href="https://en.wikipedia.org/wiki/Version_control">versionshanteringssystem</a>
som hjälper dig att koordinera ditt arbete mellan flera utvecklare. Alla användare
kan ha en lokal kopia av ett huvudförråd. Den lokala kopian kan finnas på samma
maskin eller utspritt över hela världen. Utvecklare kan sedan modifiera sin
lokala kopia och skicka in sina ändringar till huvudförrådet när de är klara.</p>
</aside>

<h2><a id="work-on-repository">Jobba på Gitförrådet</a></h2>

<p>
Låt oss börja på en gång - i denna sektion kommer du lära dig hur du skapar en
lokal kopia av huvudförrådet, hur du håller förrådet uppdaterat och hur du
skickar in ditt arbete. Vi kommer även att förklara hur du jobbar på
översättningar.
</p>

<h3><a name="get-local-repo-copy">Hämta en lokal kopia</a></h3>

<p>
Det första du måste göra att installera Git. Sedan - konfigurera Git och mata
in ditt namn och din e-postadress. Om du är nybörjare när det gäller Git så
är det troligen en god idé att läsa den allmänna Gitdokumentationen först.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a
href="https://git-scm.com/doc">Gitdokumentation</a></button></p>

<p>
Nästa steg är att klona förrådet (med andra ord: göra en lokal kopia av det).
Det finns två sätt att göra detta:
</p>

<ul>
  <li>Registrera ett konto på <url https://salsa.debian.org/> och aktivera
  SSH-åtkomst genom att ladda upp en publik SSH-nyckel till ditt salsakonto.
  Se <a
  href="https://salsa.debian.org/help/user/ssh.md">Salsas hjälpsidor</a> för
  ytterligare detaljer. Sedan kan du klona <code>webwml</code>-förrådet
  med hjälp av följande kommando:
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>Ett alternativ är att klona förrådet med hjälp av HTTPS-protokollet.
  Kom ihåg att detta kommer att skapa det lokala förrådet, men du kan inte
  skicka tillbaks förändringar direkt på detta sätt:
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>Tips:</strong> Att klona hela <code>webwml</code>-förrådet kräver en
hämtning på runt 1.3 GB data vilket kan vara för mycket om du är på en långsam
eller instabil internetanslutning. Därför finns det en möjlighet att definiera
ett minimalt djup för en mindre initial hämtning:
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Efter att du har hämtat en användbar (grund) kopia av förrådet kan du göra
din lokala kopia djupare och eventuellt konvertera den till en fullständigt
lokalt förråd:</p>

<pre>
  git fetch --deepen=1000 # fördjupa förrådet med ytterligare 1000 commits
  git fetch --unshallow   # hämta alla saknade commits, och konvertera förrådet till ett fullständigt
</pre>

<p>Du kan skapa en checkout för endast en undergrupp av sidorna:</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Skapa filen <code>.git/info/sparse-checkout</code> i <code>webwml</code>-mappen
  för att definiera innehållet som du vill checka ut. Till exempel,
  om du bara vill hämta root-filerna, engelska, katalanska och spanska översättningarna,
  ser filen ut så här:
    <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
    </pre></li>
  <li>Slutligen kan du checka ut förrådet: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Skicka in lokala ändringar</a></h3>

<h4><a name="keep-local-repo-up-to-date">Håll ditt lokala förråd uppdaterat</a></h4>

<p>Med några få dagars mellanrum (och åtminstone innan du påbörjar
redigeringsarbete!) kommer du att vilja köra</p>

<pre>
  git pull
</pre>

<p>för att få alla filer som har ändrats från förrådet.</p>

<p>
Det rekommenderas starkt att hålla din lokala git-arbetskatalog ren innan
du utför en <code>git pull</code> och arbetar med denna. Om du har icke uppskickade
ändringar eller lokala commits som inte finns i fjärrförrådet på aktuell
gren kommer <code>git pull</code> att automatiskt skapa merge commits eller
kanske till och med misslyckas på grund av konflikter. Vänligen överväg att
hålla ofullständigt arbete i en annan gren eller genom att använda kommandon
som <code>git stash</code>.
</p>

<p>Notera att git är ett distribuerat (och inte centraliserat)
versionshanteringssystem. Detta betyder att när du committar ändringar så kommer
dessa endast att lagras i ditt lokala förråd. För att dela dessa med andra
så måste du pusha dina ändringar till det centraliserade förrådet på salsa.</p>

<h4><a name="example-edit-english-file">Exempel: exempel på en ändring i en engelsk fil</a></h4>

<p>
Ett exempel på hur man redigerar engelska filer i webbplatsens källkodsförråd
följer. Efter du har hämtat en <a href="#get-local-repo-copy">lokal kopia</a> på
förrådet mha <code>git clone</code> och innan du påbörjar redigeringsarbetet, kör följande kommando:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Nu kan du börja redigera och göra ändringar till filerna.</li>
  <li>När du är klar, committa din ändringar till ditt lokala förråd:
    <pre>
    git add path/to/file(s)
    git commit -m "Ditt ändringsmeddelande"
    </pre></li>

  <li>Om du har <a href="#write-access">obegränsad skrivåtkomst</a> till fjärrförrådet <code>webwml</code>, kan
  du nu skicka dina förändringar direkt till salsaförrådet: <code>git push</code></li>
  <li>Om du inte har direkt skrivåtkomst till <code>webwml</code>-förrådet, skicka
  dina ändringar med hjälp av en <a href="#write-access-via-merge-request">merge
  request</a> eller kontakta andra utvecklare för att få hjälp.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Gitdokumentation</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Stänga Debian-felrapporter med Git-commits</a></h4>

<p>
Om du inkluderar <code>Closes: #</code><var>nnnnnn</var> i ditt
commitloggmeddelande, så kommer felnummer <code>#</code><var>nnnnnn</var> att
stängas automatiskt när du pushar dina ändringar. Den exakta formen för detta
är samma som
<a href="$(DOC)/debian-policy/ch-source.html#id24">i Debians policy</a>.</p>

<h4><a name="links-using-http-https">Länka med hjälpa av HTTP/HTTPS</a></h4>

<p>
Många Debianwebbplatser stödjer SSL/TLS, var vänlig använd HTTPS-länkar
där det är möjligt och förnuftigt.
Några Debian/DebConf/SPI/osv webbplats har inte SSL-stöd eller är
endast signerade av SPI och inte av någon SSL CA som är betrodd av
webbläsare utanför Debian, så därmed bör vi undvika att länka till
https:-versioner av dessa webbplatser så att personer som inte använder
Debian får fel som de inte förstår sig på.</p>
<p>
Gitförrådet kommer att avvisa commits som innehåller rena HTTP-länkar för
Debianwebbplatser som stödjer HTTPS eller innehåller HTTPS-länkar för
Debian/DebConf/SPI-webbplatserna som är kända att inte stödja HTTPS eller
använder certifikat som endast är signerade av SPI.</p>

<h3><a name="translation-work">Jobba på översättningar</a></h3>

<p>Översättningar skall alltid hållas uppdaterade och up-to-date med dess
engelska motsvarighet. Huvudet <code>translation-check</code> i översatta filer används
för att spåra vilka versioner av engelska filer som den aktuella översättningen
baseras på. Om du ändrar översatta filer, kan du vara tvungen att uppdatera
<code>translation-check</code>-huvudet för att matcha git commithashen i motsvarande
ändring i den Engelska filen. Du kan hitta denna hash med följande kommando:</p>

<pre>
  git log sökväg/till/engelsk/fil
</pre>

<p>Om du gör en ny översättning av en fil, vänligen använd <code>copypage.pl</code>-skriptet
så kommer det att göra en ny mall för ditt språk, och inkludera ett korrekt
översättningshuvud.</p>

<h4><a name="translation-smart-change">Översättningsändringar med smart_change.pl</a></h4>

<p><code>smart_change.pl</code> är ett skript som är designat för att göra
det lättare att uppdatera original-filer och dess översättningar tillsammans.
Det finns två sätt att göra detta, beroende på vilken typ av ändringar du gör.</p>

<p>
Här följer hur du använder <code>smart_change.pl</code> och hur du bara uppdaterar
<code>translation-check</code>-huvudet när du jobbar på filer manuellt:
</p>

<ol>
  <li>Gör ändringarna till originalfil(erna), och committa dina ändringar</li>
  <li>Uppdatera översättningar</li>
  <li>Kör <code>smart_change.pl -c COMMIT_HASH</code> (använd commit-hashen för ändringarna i original-filen).
  Detta kommer att snappa upp förändringarna och uppdatera huvuden i
  de översatta filerna.</li>
  <li>Granska ändringarna (exempelvis med <code>git diff</code>)</li>
  <li>Committa översättningsändringarna.</li>
</ol>

<p>Eller, om du använder smart_change med ett reguljärt uttryck för att göra
flera ändringar över filer i ett pass:</p>

<ol>
  <li>Kör <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>Granska förändringarna (t.ex. med <code>git diff</code>)
  <li>Committa original-filerna</li>
  <li>Kör <code>smart_change.pl origfile1 origfile2</code>
    (vilket är <strong>utan reguljära uttrycket</strong> den här gången); det kommer nu
	 att endast uppdatera huvudena i de översatta filerna</li>
  <li>Slutligen, committa översättningsändringarna</li>
</ol>

<p>Detta är mer komplicerat än tidigare (det kräver två commits), men
oundvikligt på grund av hur git commit-hashen fungerar.</p>

<h2><a id="write-access">Skrivåtkomst till Gitförrådet</a></h2>

<p>
Källkoden till Debians webbsajt hanteras i Git och hittas på
<url https://salsa.debian.org/webmaster-team/webwml/>.
Som standard har inte gäster rättigheter att skicka commits till
källkodsförrådet. Om du vill bidra till Debians webbplats behöver du någon
sorts tillåtelse att få skrivåtkomst till förrådet.
</p>

<h3><a name="write-access-unlimited">Obegränsade skrivrättigheter</a></h3>

<p>
Om du behöver obegränsade skrivrättigheter till förrådet (om du exempelvis är
på väg att bli en frekvent bidragslämnare), var vänlig efterfråga detta
via webbgränssnittet på <url https://salsa.debian.org/webmaster-team/webwml/>
efter att du har loggat in på salsa-plattformen.</p>

<p>
Om du är ny på utveckling av Debian's webbsida och inte har någon tidigare
erfarenhet, var vänlig kontakta även <a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a> med en introduktion om dig själv innan du
efterfrågar obegränsade skrivrättigheter. Vänligen
skriv något användbart i din introduktion, så som vilket språk du vill jobba
med eller vilken del av webbplatsen som du kommer att jobba på och vem
som kommer att gå i god för dig.
</p>


<h3><a name="write-access-via-merge-request">Merge Requests</a></h3>

<p>
Om du inte har intentionen att få obegränsade skrivrättigheter till förrådet
eller inte har möjligheten till detta, så kan du alltid skicka Merge Requests
och låta andra utvecklare undersöka och acceptera ditt arbete. Var vänlig
skicka Merge Requests enligt standardförfarandet som det tillhandahålls av
Salsa Gitlab-plattformen via dess webbgränssnitt och läs följande två dokument:
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Arbetsflöde vid projektförgreningar</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">När du arbetar i en förgrening</a></li>
</ul>

<p>
Vänligen notera att Merge Requests inte övervakas av alla webbplatsutvecklare
och kan därmed kanske inte behandlas i tid. Om du inte är säker på om ditt
bidrag skulle accepteras, vänligen skicka e-post till sändlistan
<a href="https://lists.debian.org/debian-www/">debian-www</a> och
efterfråga en granskning.
</p>

<h2><a id="notifications">Få aviseringar</a></h2>

<p>
Om du arbetar på Debians webbplats, så vill du antagligen veta vad som sker
i förrådet <code>webwml</code>. Det finns två sätt att hålla sig uppdaterad:
commitaviseringar och aviseringar för merge requests.</p>

<h3><a name="commit-notifications">Commit-aviseringar</a></h3>

<p>
Vi har satt upp konfigurationen av projektet <code>webwml</code> i Salsa så att commits
visas i IRC-kanalen #debian-www.</p>


<p>Om du vill få aviseringar via e-post när det finns commits i <code>webwml</code>-förrådet
vänligen prenumerera på pseudopaketet <code>www.debian.org</code> via
<a href="https://tracker.debian.org/">tracker.debian.org</a> och aktivera nyckelordet <code>vcs</code> där, genom att följa
dessa steg (en gång endast):</p>

<ol>
  <li>Öppna en webbläsare och gå till <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>Prenumerera på pseudopaketet <code>www.debian.org</code>. (Du kan autentisera
      via SSO eller registrera en e-post och lösenord, om du inte redan har
      använt tracker.debian.org för andra ändamål).</li>
  <li>Gå till <url https://tracker.debian.org/accounts/subscriptions/>, och sedan till <q>modify
      keywords</q>, kryssa i <code>vcs</code> (om den inte redan är ikryssad) och spara.</li>
  <li>Från och med nu kommer du att få e-post när någon committar till
      <code>webwml</code>-förrådet. </li>
</ol>

<h3><a name="merge-request-notifications">Få Merge-Request-aviseringar</a></h3>

<p>
Om du vill få aviseringar via e-post när det finns nya Merge Requests skickade
till <code>webwml</code>-förrådet på Salsa Gitlab-plattformen, kan du
konfigurera dina notifierings-inställningar för <code>webwml</code>-förrådet via webgränssnittet, och följa dessa steg:
</p>

<ol>
   <li>Logga in på ditt Salsa-konto och gå till projekt-sidan.</li>
   <li>Klicka på ringklocke-ikonen på översidan av projekt-sidan.</li>
   <li>Välj notifieringsnivån som du önskar.</li>
</ol>
