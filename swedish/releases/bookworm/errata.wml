#use wml::debian::template title="Debian 12 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ba68acca0765fe2fd2a7fc056b6c929651151770"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Kända problem</toc-add-entry>
<toc-add-entry name="security">Säkerhetsproblem</toc-add-entry>

<p>Debians säkerhetsgrupp ger ut uppdateringar till paket i den stabila utgåvan
där de har identifierat problem relaterade till säkerhet. Vänligen se
<a href="$(HOME)/security/">säkerhetssidorna</a> för information om
potentiella säkerhetproblem som har identifierats i <q>Bookworm</q>.</p>

<p>Om du använder APT, lägg till följande rad i <tt>/etc/apt/sources.list</tt>
för att få åtkomst till de senaste säkerhetsuppdateringarna:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free-firmware non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktutgåvor</toc-add-entry>

<p>Ibland, när det gäller kritiska problem eller säkerhetsuppdateringar,
uppdateras utgåvedistributionen. Generellt indikeras detta som punktutgåvor.
</p>

<ul>
  <li>Den första punktutgåvan, 12.1, släpptes
      <a href="$(HOME)/News/2023/20230722">den 22 juli, 2023</a>.</li>
  <li>Den andra punktutgåvan, 12.2, släpptes
      <a href="$(HOME)/News/2023/20231007">den 7 oktober, 2023</a>.</li>
  <li>Den tredje punktutgåvan, 12.3, släpptes
      <a href="$(HOME)/News/2023/2023120902">den 9 december, 2023</a>.</li>
  <li>Den fjärde punktutgåvan, 12.4, släpptes
     <a href="$(HOME)/News/2023/20231210">den 10 december, 2023</a>.</li>
  <li>Den femte punktutgåvan, 12.5, släpptes
     <a href="$(HOME)/News/2024/20240210">den 10 februari, 2024</a>.</li>
  <li>Den sjätte punktutgåvan, 12.6, släpptes
     <a href="$(HOME)/News/2024/20240629">den 29 juni, 2024</a>.</li>
  <li>Den sjunde punktutgåvan, 12.7, släpptes
     <a href="$(HOME)/News/2024/20240831">den 31 augusti, 2024</a>.</li>
  <li>Den åttonde punktutgåvan 12.8, släpptes
     <a href="$(HOME)/News/2024/20241109">den 9 november, 2024</a>.</li>
 
</ul>



<ifeq <current_release_bookworm> 12.0 "


<p>Det finns inga punktutgåvor för Debian 12 ännu.</p>" "

<p>Se <a
href="http://deb.debian.org/debian/dists/bookworm/ChangeLog">\
förändringsloggen</a>
för detaljer om förändringar mellan 12 och <current_release_bookworm/>.</p>"/>


<p>Rättningar till den utgivna stabila utgåvan går ofta genom en
utökad testningsperiod innan de accepteras i arkivet. Dock, så finns dessa
rättningar tillgängliga i mappen
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> i alla Debianarkivspeglingar.</p>

<p>Om du använder APT för att uppdatera dina paket kan du installera de
föreslagna uppdateringarna genom att lägga till följande rad i
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# föreslagna tillägg för en punktutgåva till Debian 12
  deb http://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>

<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
För mer information om kända problem och uppdateringar till
installationssystemet, se
<a href="debian-installer/">debian-installer</a>-sidan.
</p>
