#use wml::debian::projectnews::header PUBDATE="2023-08-05" SUMMARY="Bienvenue au premier numéro des Debian Project Bits !, Journée Debian, DebConf23, Information sur les publications de Debian, Paquets populaires, Demandes d'aide !"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="b2fda4608c91330e5cebab041c68b73660028d68" maintainer="Jean-Pierre Giraud"

# Status: [frozen]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<h2>Bienvenue au premier numéro des Debian Project Bits !</h2>

<shortintro issue="premier"/>

<p>Tous ceux qui se rappellent la gazette hebdomadaire de Debian (Debian
Weekly News — DwN) reconnaîtront ici certaines des sections qui nous ont
inspirés.</p>

<p>Les billets Debian Project Bits permettront une publication plus
rapide de certaines nouvelles du projet à un rythme mensuel. Le service
<a href="https://micronews.debian.org/">Debian Micronews</a>
continuera à diffuser des brèves et les
<a href="https://www.debian.org/News/weekly/">Nouvelles du projet Debian</a>
(Debian Project News — DPN) restent la lettre
d'information de la communauté Debian, qui pourrait se transformer en
format d'archive semestriel.
</p>

<h2>Nouvelles</h2>

<p><b>Journée Debian</b></p>

<p>Le projet Debian a été <a href="https://wiki.debian.org/DebianHistory">fondé officiellement</a>
par Ian Murdock le 16 août 1993, depuis cette date, nous avons chaque année
fêté l’anniversaire de cette naissance par des célébrations partout dans le
monde. Nous serions ravis si vous vous joigniez à nos festivités dans
cette année particulière où nous avons le privilège de fêter les <b>30 ans</b>
de Debian !
</p>

<p>Organisez ou participez à un événement local pour fêter la
<a href="https://wiki.debian.org/fr/DebianDay">Journée Debian</a>. Nous vous
invitons à préparer votre propre événement : que ce soit une chasse aux bogues,
une séance de signatures de clé, une rencontre ou n'importe quel type d'événement
social qu'il soit petit ou grand. Et prenez bien soin de vérifier la page du
wiki Debian concernant les
<a href="https://wiki.debian.org/Teams/DPL/Reimbursement">remboursements</a>
si vous avez besoin de ressources.</p>

<p>Partagez les informations sur ces journées et ces événements, vos idées ou
notes avec nous et le reste de la communauté avec le tag <b>#debianday</b> qui
sera utilisé sur la plupart des
<a href="https://wiki.debian.org/Teams/DebianSocial">plateformes de média social</a>.
À plus tard !</p>

<p><b>Événements à venir</b></p>

<p><b>Debian 30 anos</b></p>

<p>La <a href="https://debianbrasil.org.br/">communauté Debian du Brésil</a>
organise l'événement
<a href="https://debianbrasil.gitlab.io/debian30anos/">Debian 30 anos</a> pour
célébrer le 30ᵉ anniversaire du projet Debian.</p>

<p>Du 14 au 18 août, entre 19 et 22 heures (UTC -03:00) des contributeurs
interviendront en portugais avec une diffusion en direct sur le canal
<a href="https://www.youtube.com/DebianBrasilOficial">YouTube Debian Brasil</a>.</p>

<p><b>DebConf23 : DebCamp et Conférence des développeurs Debian</b></p>

<p>Le DebCamp des développeurs Debian 2023 et la Conférence Debian
(<a href="https://debconf23.debconf.org/">DebConf23</a>) se tiendront cette
année à l'Infopark de
<a href="https://debconf23.debconf.org/about/kochi/">Kochi, Inde</a>. Un
DebCamp est prévu du 3 septembre au 9 septembre 2023, immédiatement suivi par
la DebConf de plus grande ampleur du 10 au 17 septembre 2023.
</p>

<p>Si vous avez prévu d'assister à la conférence cette année, c'est maintenant
le moment de vous assurer de tenir prêts vos documents de voyage,
<a href="https://lists.debian.org/debconf-announce/2023/07/msg00001.html">information sur les visas</a>,
les demandes de bourses, les documents et tout le nécessaire. Pour plus
d'informations contactez : &lt;debconf@debconf.org&gt;.</p>

<p><b>MiniDebConf à Cambridge 2023</b></p>

<p>Une
<a href="https://lists.debian.org/debian-devel-announce/2023/07/msg00002.html">MiniDebConf</a>
se tiendra à Cambridge, Royaume-Uni, hébergée par Arm pendant quatre jours en
novembre : deux jours pour un mini-DebCamp (jeudi 23 et vendredi 24), avec des
espaces consacrés au développement, aux rencontres et aux réunions d'équipe,
puis deux jours pour la MiniDebConf proprement dite (samedi 25 et dimanche 26)
avec un espace pouvant rassembler jusqu'à 80 personnes pour des communications
plus générales.</p>

<p><b>Comptes-rendus</b></p>

<p>Durant les derniers mois, la communauté Debian a organisé quelques
<a href="https://wiki.debian.org/fr/BSP">Chasses aux bogues</a>:</p>
<ul>
<li><a href="https://wiki.debian.org/BSP/2022/11/nl/Tilburg">Tilburg</a>, Pays-Bas, octobre 2022 ;</li>
<li><a href="https://wiki.debian.org/BSP/2023/01/ch/St-Cergue">St-Cergue</a>, St-Cergue [16], Suisse, janvier 2023 ;</li>
<li><a href="https://wiki.debian.org/BSP/2023/02/ca/Montreal">Montréal</a>, Canada, février 2023.</li>
</ul>

<p>En janvier, Debian India a hébergé la
<a href="https://wiki.debian.org/DebianIndia/MiniDebConfTamilNadu2023">MiniDebConf Tamil Nadu</a>
à Viluppuram, Tamil Nadu, Inde (samedi 28 — dimanche 29). Le mois suivant, la
<a href="https://wiki.debian.org/DebianEvents/pt/2023/MiniDebConfLisbon">MiniDebConf Portugal 2023</a>
s'est tenue à Lisbonne (du 12 au 16 février 2023).</p>

<p>Ces événements, considérés comme des <b>succès éclatants</b> par certains de
leurs participants, démontrent la vitalité de la communauté.</p>

<p><b>La communauté Debian Brazil au Campus Party Brazil 2023</b></p>

<p>Une nouvelle édition du <a href="https://brasil.campus-party.org/cpbr15/">Campus Party Brazil</a>
s'est tenue dans la ville de São Paulo du 25 au 30 juillet. Et une fois de
plus, la communauté Debian Brazil s'était mobilisée. Durant ces journées, des
activités se sont déroulées dans l'espace mis à disposition :</p>
<ul>
<li>cadeaux aux participants (autocollants, tasses, tours de cou) ;</li>
<li>atelier pour montrer comment participer à l'équipe de traduction ;</li>
<li>atelier d’empaquetage ;</li>
<li>séance de signatures de clé ;</li>
<li>informations sur le projet.</li>
</ul>

<p>Plus d'informations et quelques photos sont disponibles dans le
<a href="https://debianbrasil.org.br/blog/debian-brasil-campusparty-sp-2023-report/">compte-rendu des organisateurs</a>.</p>

<p><b>MiniDebConf Brasília 2023</b></p>

<p>Du 25 au 27 mai, Brasilia a accueilli la
<a href="https://brasilia.mini.debconf.org">MiniDebConf Brasília 2023</a>.
Cette conférence a rassemblé un certain nombre d'activités telles que des
communications, des ateliers, des rencontres, des chasses aux bogues (BSP),
une séance de signatures de clé, des activités sociales, de la programmation,
destinées à rassembler la communauté et à célébrer le plus grand projet du
logiciel libre : Debian.</p>

<p>Pour plus d'information, consultez le
<a href="https://debianbrasil.org.br/blog/minidebconf-brasilia-2023-a-brief-report/">compte-rendu complet</a>
rédigé par les organisateurs.</p>

<p><b>Réunion Debian Hambourg 2023</b></p>

<p>Cette année, la
<a href="https://wiki.debian.org/DebianEvents/de/2023/DebianReunionHamburg">Réunion Debian Hambourg</a>
a eu lieu du 23 au 30 mai 2023 débutant par quatre jours de « hacking » suivis
par deux jours de communication, puis à nouveau deux jours de « hacking ». Comme
d'habitude les participants, plus de quarante-cinq venus d'Allemagne, Tchéquie,
France, Slovaquie et Suisse, étaient contents de se rencontrer, de coder et de
discuter ensemble, et bien plus encore. Si vous avez raté la diffusion en
direct, les
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/Debian-Reunion-Hamburg/">enregistrements vidéo</a>
sont disponibles.</p>

<p><b>Ateliers traduction de l'équipe pt_BR</b></p>

<p>L'équipe de traduction brésilienne, debian-l10n-portuguese, a tenu son
premier atelier de 2023 en février avec de bons résultats. L'atelier était
destiné aux débutants et a travaillé sur
<a href="https://ddtp.debian.org/ddtss">DDTP/DDTSS</a>.</p>

<p>Pour plus d'information, veuillez consultez le
<a href="https://debianbrasil.org.br/blog/first-2023-translation-workshop-from-the-pt-BR-team/">compte-rendu complet</a>
publié par les organisateurs.</p>

<p>Et le 13 juin, un autre atelier s'est tenu pour traduire
<a href="https://debian-handbook.info">Le manuel de l'administrateur Debian</a>.
L'objectif principal était de montrer aux débutants comment collaborer à la
traduction de cet ouvrage important qui existe depuis 2004. Les traductions du
manuel sont hébergées sur
<a href="https://hosted.weblate.org/projects/debian-handbook/#languages">Weblate</a>.</p>


<h2>Publications</h2>

<p><b>Publication stable</b></p>

<p>Debian 12 <a href="https://wiki.debian.org/DebianBookworm">Bookworm</a> a été
publié le <a href="https://www.debian.org/News/2023/20230610">10 juin 2023</a>.
Cette nouvelle version devient la version stable de Debian et a changé le
statut de la version Debian 11
<a href="https://wiki.debian.org/DebianBullseye">Bullseye</a> à celui de version
<a href="https://wiki.debian.org/DebianOldStable">oldstable</a>. La communauté
Debian a célébré la publication par vingt-trois
<a href="https://wiki.debian.org/ReleasePartyBookworm">fêtes célébrant cette sortie</a>
à travers le monde.</p>

<p>La première version intermédiaire de Bookworm,
<a href="https://wiki.debian.org/DebianReleases">12.1</a>, qui fournit
divers correctifs de bogues pour 88 paquets ainsi que des mises à jour de
la documentation et de l'installateur, est disponible depuis le
<a href="https://www.debian.org/News/2023/20230722">22 juillet 2023</a>.</p>

<p><b>Prise en charge de RISC-V</b></p>

<p>L'architecture <a href="https://wiki.debian.org/RISC-V">riscv64</a> a été
ajoutée récemment aux architectures officielles de Debian pour la prise en
charge du matériel <a href="https://riscv.org">RISC-V</a> 64 bits petit-boutien
exécutant le noyau Linux. La prise en charge complète du processeur riscv64 est
attendue dans Debian 13 Trixie. L'équipe a récemment communiqué sur les mises à
jour du bootstrap, du démon de construction, de porterbox et sur les progrès du
développement dans le message
<a href="https://lists.debian.org/debian-devel-announce/2023/07/msg00003.html">Bits from the Debian riscv64 porters</a>.</p>

<p><b>non-free-firmware</b></p>
<p>L'archive de Debian 12 Bookworm inclut maintenant non-free-firmware,
veuillez vous assurer de mettre à jour le fichier sources.list d'apt si
votre système requiert ces composants pour fonctionner. Si votre ancien
sources.list incluait non-free, il peut être supprimé sans risque.</p>

<p><b>sources.list d'apt</b></p>
<p>L'archive Debian renferme plusieurs composants :</p>
<ul>
<li><a href="http://www.debian.org/doc/debian-policy/ch-archive#s-main">main</a>
contient les paquets conformes aux
<a href="https://www.debian.org/social_contract#guidelines">DFSG</a>, (principes
du logiciel libre selon Debian), qui ne dépendent pas pour fonctionner de
logiciels extérieurs à cette section ;</li>
<li><a href="http://www.debian.org/doc/debian-policy/ch-archive#s-contrib">contrib</a>
contient des paquets qui renferment des logiciels conformes aux DFSG, mais ont
des dépendances extérieures à la section main ;</li>
<li><a href="http://www.debian.org/doc/debian-policy/ch-archive#s-non-free">non-free</a>
contient des logiciels qui ne se conforment pas à la DFSG ;</li>
<li>non-free-firmware contient des microprogrammes, qui autrement ne font pas
partie du système Debian, pour utiliser un matériel qui a besoin de ces
microprogrammes.</li>
</ul>

<p>Exemple de fichier sources.list :</p>

<code>deb http://deb.debian.org/debian bookworm main<br/>
deb-src http://deb.debian.org/debian bookworm main</code>

<code>deb http://deb.debian.org/debian-security/ bookworm-security main<br/>
deb-src http://deb.debian.org/debian-security/ bookworm-security main</code>

<code>deb http://deb.debian.org/debian bookworm-updates main<br/>
deb-src http://deb.debian.org/debian bookworm-updates main</code>

<p>Exemple utilisant les composants :</p>

<code>deb http://deb.debian.org/debian bookworm main non-free-firmware<br/>
deb-src http://deb.debian.org/debian bookworm main non-free-firmware</code>

<code>deb http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware<br/>
deb-src http://deb.debian.org/debian-security/ bookworm-security main non-free-firmware</code>
    
<code>deb http://deb.debian.org/debian bookworm-updates main non-free-firmware<br/>
deb-src http://deb.debian.org/debian bookworm-updates main non-free-firmware</code>

<p>Pour davantage d'informations et de directives pour une configuration
correcte du fichier source.list d'apt, veuillez consulter la page du wiki
<a href="https://wiki.debian.org/SourcesList">Configuring Apt Sources</a>.</p>


<p><b>Dans Debian</b></p>

<p><b>Nouveaux membres de Debian</b></p>


<p>Bienvenue aux nouveaux membres du projet Debian :</p>

<ul>
<li>Marius Gripsgard \(mariogrip)</li>
<li>Mohammed Bilal \(rmb)</li>
<li>Emmanuel Arias \(amanu)</li>
<li>Robin Gustafsson \(rgson)</li>
<li>Lukas Märdian \(slyon)</li>
<li>David da Silva Polverari \(polverari)</li>
</ul>

<p>Pour en savoir plus sur nos nouveaux membres ou sur n'importe quel
développeur ou développeuse Debian, recherchez-les sur la page
<a href="https://nm.debian.org/public/people/">Debian People list</a>.</p>


<toc-add-entry name="security">Annonces de sécurité</toc-add-entry>


<p>L'équipe de sécurité de Debian publie chaque jour les annonces du moment.
Certaines des annonces publiées récemment concernent ces paquets :</p>

<ul>
<li><a href="https://www.debian.org/security/2023/dsa-5435-2">trafficserver</a>
Plusieurs vulnérabilités ont été découvertes dans Traffic Server d'Apache, un
serveur mandataire inverse et de redirection, qui pouvaient avoir pour
conséquences la divulgation d'informations ou un déni de service.</li>

<li><a href="https://www.debian.org/security/2023/dsa-5438">asterisk</a>
Un défaut a été découvert dans Asterisk, un autocommutateur téléphonique
privé (PBX) au code source ouvert. Une vulnérabilité de dépassement de
tampon affecte les utilisateurs qui utilisent le résolveur DNS PJSIP.
Cette vulnérabilité est liée au CVE-2022-24793, à la différence que ce
problème réside dans l'analyse de l'enregistrement de requête
« parse_query() » tandis que le problème dans CVE-2022-24793 est dans
« parse_rr() X. Un contournement consiste à désactiver la résolution DNS dans
la configuration de PJSIP (en réglant « nameserver_count » à zéro) ou à utiliser
à la place une implémentation externe de résolveur.</li>

<li><a href="https://www.debian.org/security/2023/dsa-5442">flask</a>
Dans certaines conditions le cadriciel web Flask peut divulguer un cookie de
session.</li>

<li><a href="https://www.debian.org/security/2023/dsa-5456">chromium</a>
Plusieurs problèmes de sécurité ont été découverts dans Chromium, qui pouvaient
avoir pour conséquences l'exécution de code arbitraire, un déni de service ou
la divulgation d'informations.</li>
</ul>

<p><b>Divers</b></p>

<p><b>Paquets populaires</b></p>

<ul>
<li><a href="https://packages.debian.org/stable/gpgv">gpgv — GNU privacy guard
outil de vérification de signature</a>.
99 053 installations. gpgv est réellement une version allégée de gnupg
qui est seulement capable de vérifier des signatures. Il est légèrement
plus petit que le programme complet gnupg et utilise une autre méthode
(plus simple) pour vérifier que les clés publiques utilisées pour la
signature sont des clés de confiance. Il ne dispose pas de fichiers de
configuration et seules quelques fonctions sont implémentées.</li>

<li><a href="https://packages.debian.org/stable/dmsetup">dmsetup — bibliothèque
d'accès en espace utilisateur au gestionnaire de périphériques du noyau
Linux.</a>. 77 769 installations. Linux Kernel Device Mapper (gestionnaire de
périphériques du noyau Linux) est la mise en œuvre, par l'équipe LVM (Linux
Logical Volume Management, gestion de volumes logiques Linux), d'un pilote
d'espace noyau qui traite la gestion des volumes, tout en conservant la
connaissance sous-jacente des couches de périphérique dans l'espace utilisateur.
Cela est utile, non seulement pour LVM, mais aussi le raid logiciel et d'autres
pilotes qui créent des périphériques « virtuels » en mode bloc.</li>

<li><a href="https://packages.debian.org/stable/sensible-utils">sensible-utils
- utilitaires pratiques pour la sélection d'alternatives.</a>. 96 001
utilisateurs par jour. Ce paquet fournit un certain nombre de petits utilitaires
auxquels certains programmes font appel pour sélectionner et exécuter un
navigateur, un éditeur ou un lecteur de fichier (« pager ») approprié. Ces
utilitaires incluent : sensible-browser, sensible-editor et sensible-pager.</li>

<li><a href="https://packages.debian.org/stable/popularity-contest">popularity-contest -
vote automatique pour vos paquets préférés</a>.
90 758 utilisateurs par jour. Le paquet popularity-contest met en place
une tâche cron qui envoie périodiquement et de façon anonyme aux
développeurs Debian des statistiques à propos des paquets Debian les
plus utilisés sur ce système. Cette information aide Debian à prendre
des décisions sur des sujets comme les paquets devant se trouver sur le
premier CD Debian. Ces données permettent également à Debian d'améliorer
les prochaines versions de la distribution, afin que les paquets les
plus populaires soient installés automatiquement à chaque nouvelle
installation.</li>
</ul>


<p><b>Nouveaux paquets dignes d'intérêt dans unstable</b></p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/libsimgrid3.34">Boîte à
outils pour une simulation évolutive des applications distribuées</a> est une
boîte à outils qui fournit les fonctions principales pour la simulation
d'applications distribuées dans des environnements distribués hétérogènes.
SimGrid peut être utilisé comme simulateur de grille, simulateur P2P,
simulateur de nuage, simulateur MPI ou un mélange de tout cela. Le cas d'usage
typique de SimGrid inclut l'évaluation heuristique, le prototypage
d'application, le développement et la mise au point de l'application réelle.
Ce paquet fournit les bibliothèques dynamiques et l'exécutable.</li>

<li><a href="https://packages.debian.org/unstable/main/ldraw-mklist">Programme mklist de LDraw</a>
Les programmes de CAD et de rendu 3D utilisant la bibliothèque de composants
LDraw de pièces LEGO reposent sur un fichier appelé parts.lst contenant une
liste de tous les composants disponibles. Le programme ldraw-mklist est utilisé
pour générer cette liste à partir d'un répertoire de compsants LDraw.</li>

<li><a href="https://packages.debian.org/unstable/main/ola-rdm-tests">Open
Lighting Architecture — Tests de répondeur RDM</a> La norme DMX512 pour Digital
MultipleX est utilisée pour les réseaux de communication numérique utiles
habituellement pour contrôler l'éclairage et les effets de scènes. Le protocole
Remote Device Management est une extension de DMX512 permettant la communication
bidirectionnelle entre des périphériques compatibles RDM sans perturber d’autres
périphériques sur la même connexion. L'Open Lighting Architecture (OLA) fournit
une infrastructure de greffons pour distribuer les signaux de contrôle DMX512.
Le paquet ola-rdm-tests fournit une manière automatique pour vérifier la
conformité au protocole des périphériques RDM.</li>

<li><a href="https://packages.debian.org/unstable/main/parsec-service">parsec-service</a>
Parsec est une couche d'abstraction qui peut être utilisée pour interagir avec
des fonctionnalités de sécurité basées sur le matériel telles que le Hardware
Security Module (HSM), le Trusted Platform Module (TPM), de même que les
services logiciels isolés et basés sur les microprogrammes. Le composant
principal de Parsec est le service de sécurité fourni par ce paquet. Le
service est un processus en arrière-plan qui est exécuté sur la plateforme hôte
et fournit une connectivité avec les fonctions sécurisées de cet hôte, exposant
une API indépendante de la plateforme qui peut être exploitée dans divers
langages de programmation en utilisant une bibliothèque client. Pour une
bibliothèque client implémentée en Rust voir le paquet
librust-parsec-interface-dev.</li>

<li><a href="https://packages.debian.org/unstable/main/ripcalc">Outil simple de
recherche et calcul de réseau</a>
Traitement et recherche avec ripalc d'adresses de réseaux en ligne de commande
ou avec des fichiers CSV. La sortie possède divers formats personnalisables.</li>

<li><a href="https://packages.debian.org/unstable/main/xmrig">Mineur haute
performance, à source libre pour processeurs et processeurs graphiques et vérificateur RandomX</a>
XMRig est un mineur haute performance, à source libre et multiplateforme pour
processeurs et processeurs graphiques unifiés RandomX, KawPow, CryptoNight et
GhostRider ainsi qu'un vérificateur RandomX.</li>

<li><a href="https://packages.debian.org/unstable/main/librust-gping-dev">Ping,
mais avec un graphe — code source en Rust</a> Ce paquet fournit le source
du « crate » gping de Rust, empaqueté par debcargo pour une utilisation avec
cargo et dh-cargo.</li>
</ul>


<p><b>Il était une fois dans Debian</b></p>

<ul>
<li>Le 31 juillet 2014, le comité technique choisit <a href="https://lists.debian.org/debian-devel-announce/2014/08/msg00000.html">libjpeg-turbo comme décodeur JPEG par défaut</a>.</li>

<li>Le 1er août 2010, la <a href="https://debconf10.debconf.org/">DebConf10 débute à New-York, USA.</a>.</li>

<li>Le 5 août 2007, le statut de
<a href="https://www.debian.org/vote/2007/vote_003/">Mainteneur Debian approuvé par vote</a>.</li>

<li>Le 25 août 2009, Jeff Chimene a rapporté le
<a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=540000/">bogue n° 540000 sur le paquet live-initramfs</a>.</li>
</ul>


<h2>Demandes d'aide !</h2>

<p><b>L'équipe publicity demande des volontaires et de l'aide !</b></p>

<p>L'équipe en charge de la publicité vous demande de l'aide, à vous nos
lecteurs, nos développeurs et à toutes les personnes intéressées pour
contribuer à la réussite des nouvelles de Debian. Nous vous demandons de
nous soumettre des sujets susceptibles d'intéresser la communauté et
sollicitons votre assistance pour la traduction des nouvelles dans une
autre langue (la vôtre !) et aussi pour les relectures nécessaires pour
amender notre travail avant publication. Si vous pouvez donner un peu de
votre temps pour aider notre équipe qui fait tout son possible pour nous
tenir tous informés, nous avons besoin de vous. Veuillez consulter la
<a href="https://wiki.debian.org/ProjectNews/HowToContribute">page de contribution</a>
pour trouver des explications sur la façon de participer et vous pouvez nous
joindre sur le canal IRC #debian-publicity sur <a href="https://oftc.net/">OFTC.net</a>,
sur notre <a href="mailto:debian-publicity@lists.debian.org">liste de diffusion publique</a>,
ou par courriel à <a href="mailto:press@debian.org">press@debian.org</a> pour
des questions difficiles ou privées.

<toc-add-entry name="continuedpb">Continuer à lire les Debian Project Bits</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">S'inscrire ou se désinscrire</a> de la liste de diffusion Debian News</p>

#use wml::debian::projectnews::footer editor="l'équipe en charge de la publicité avec des contributions de Jean-Pierre Giraud, Joost van Baal-Ilić, Carlos Henrique Lima Melara, Donald Norwood, Paulo Henrique de Lima Santana" translator="Jean-Pierre Giraud, l\'équipe francophone de traduction"
