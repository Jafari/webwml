#use wml::debian::translation-check translation="1c49456c19b64e777ff4c1303f2d1e5402da4775" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.4</define-tag>
<define-tag release_date>2023-12-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Veuillez noter que ce document a été mis à jour le mieux possible pour tenir
compte du remplacement de Debian 12.3 par Debian 12.4. Ces changements sont
survenus à la suite de l'annonce, à la dernière minute, du bogue
<a href=https://bugs.debian.org/1057843>nº 1057843</a> concernant des problèmes
avec linux-image-6.1.0-14 (6.1.64-1).
</p>

<p>
Debian 12.4 est publiée avec le noyau linux-image-6.1.0-15 (6.1.66-1), en même
temps que quelques autres corrections de bogues.
</p>

<p>
Le projet Debian a l'honneur d'annoncer la quatrième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction adequate "Test de non correspondance taille/symboles évité sur les architectures où les symboles de table n'incluent pas de longueur particulière ; désactivation des avertissements d'obsolescence pour l'utilisation de smartmatch, given, dans Perl 5.38 ; correction des avertissements pour les comparaisons de versions à propos de <q>smartmatch being experimental</q>">
<correction amanda "Correction d'une élévation locale de privilèges [CVE-2023-30577]">
<correction arctica-greeter "Logo éloigné du bord lors de l'accueil">
<correction awstats "Demande mise à niveau due au nettoyage de la configuration de logrotate évitée">
<correction axis "Filtrage des protocoles non pris en charge dans la classe client ServiceFactory [CVE-2023-40743]">
<correction base-files "Mise à jour pour cette version">
<correction ca-certificates-java "Suppression de dépendances circulaires">
<correction calibre "Correction de plantage dans Get Books lors de la régénération des fichiers UIC">
<correction crun "Correction des conteneurs avec systemd comme système d'initialisation lors de l'utilisation des versions les plus récentes du noyau">
<correction cups "Prise en charge du fait que sur certaines imprimantes le choix d'impression en couleur de l'option ColorModel est CMYK et non RGB">
<correction dav4tbsync "Nouvelle version amont rétablissant la compatibilité avec les versions récentes de Thunderbird">
<correction debian-edu-artwork "Fourniture d'un thème graphique basé sur Emerald pour Debian Edu 12">
<correction debian-edu-config "Nouvelle version amont stable ; correction de la configuration et modification des mots de passe de LDAP">
<correction debian-edu-doc "Mise à jour de la documentation incluse et des traductions">
<correction debian-edu-fai "Nouvelle version amont stable">
<correction debian-edu-router "Correction de la génération de dnsmasq conf pour les réseaux sur VLAN ; génération des règles de filtrage UIF pour SSH uniquement si l'interface <q>Uplink</q> est définie ; mise à jour des traductions">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 6.1.0-15 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debootstrap "Rétroportage des modifications de la prise en charge de merged-/usr à partir de Trixie : implémentation de merged-/usr par fusion post-installation, merged-/usr par défaut pour les suites plus récentes que Bookworm dans tous les profils">
<correction devscripts "Debchange : mise à jour pour les distributions Debian actuelles">
<correction dhcpcd5 "Remplacement de Breaks/Replaces dhcpcd5 par Conflicts">
<correction di-netboot-assistant "Correction de la prise en charge de l'image ISO d'installation autonome pour Bookworm">
<correction distro-info "Mise à jour des tests pour distro-info-data 0.58+deb12u1 ajustant la date de fin de vie de Debian 7">
<correction distro-info-data "Ajout d'Ubuntu 24.04 LTS Noble Numbat ; Correction de plusieurs dates de fin de vie">
<correction eas4tbsync "Nouvelle version amont rétablissant la compatibilité avec les versions récentes de Thunderbird">
<correction exfatprogs "Correction de problèmes d'accès mémoire hors limites [CVE-2023-45897]">
<correction exim4 "Correction de problèmes de sécurité liés au protocole mandataire [CVE-2023-42117] et aux recherches DNSDB [CVE-2023-42119] ; ajout de la sécurisation des recherches SPF ; substitutions d'UTF-16 à partir de ${utf8clean:...} ; correction de plantage avec <q>tls_dhparam = none</q> ; correction de l'expansion de $recipients lors de l'utilisation dans ${run...} ; correction de la date d'expiration des certificats SSL auto-générés ; correction de plantage induit par certaines combinaisons de chaînes de longueur zéro et ${tr...}">
<correction fonts-noto-color-emoji "Ajout de la prise en charge d'Unicode 15.1">
<correction gimp "Ajout de Conflicts et Replaces : gimp-dds pour supprimer les anciennes versions de ce greffon fourni par gimp lui-même depuis la version 2.10.10">
<correction gnome-characters "Ajout de la prise en charge d'Unicode 15.1">
<correction gnome-session "Ouverture des fichiers texte dans gnome-text-editor si gedit n'est pas installé">
<correction gnome-shell "Nouvelle version amont stable ; abandon des notifications par la touche Retour Arrière en plus de la touche Suppr permis ; correction de l'affichage dupliqué de périphériques lors d'une reconnexion à PulseAudio ; correction d'un possible plantage par utilisation de mémoire après libération au redémarrage de PulseAudio/Pipewire ; rapport aux outils d'accessibilité évité comme objet de leur propre parent des curseurs dans les réglages rapides (volume, etc.)  ; alignement des fenêtres d'affichage défilantes à la grille de pixels pour éviter un sautillement visible durant le défilement">
<correction gnutls28 "Correction d'un problème d'attaque par canal auxiliaire temporelle [CVE-2023-5981]">
<correction gosa "Nouvelle version amont stable">
<correction gosa-plugins-sudo "Correction d'une variable non initialisée">
<correction hash-slinger "Correction de la génération des enregistrements TLSA">
<correction intel-graphics-compiler "Correction de la compatibilité avec la version intel-vc-intrinsics de stable">
<correction iotop-c "Correction de la logique dans l'option <q>only</q> ; correction d'une boucle occupée lorsque la touche ESC est pressée ; correction du rendu de graphiques ASCII">
<correction jdupes "Mise à jour des demandes pour aider à éviter des choix qui pouvaient mener à une perte de données inattendue">
<correction lastpass-cli "Nouvelle version amont stable ; mise à jour des hachages de certificat ; ajout de la prise en charge de la lecture des URL chiffrées">
<correction libapache2-mod-python "Assurance que les versions de binNMU sont compatibles avec PEP-440">
<correction libde265 "Correction d'un problème de violation de segmentation [CVE-2023-27102], de problèmes de dépassement de tampon [CVE-2023-27103 CVE-2023-47471], d'un problème dépassement de tampon [CVE-2023-43887]">
<correction libervia-backend "Correction d'un échec de démarrage sans configuration préexistante ; exec avec un chemin dans le fichier du service dbus ; correction de dépendances à python3-txdbus/python3-dbus">
<correction libmateweather "Localisations : ajout de San Miguel de Tucuman (Argentine) ; mise à jour des zones de prévision pour Chicago ; correction de l'URL du serveur de données ; correction de certains noms de localisation">
<correction libsolv "Activation de la prise en charge de la compression zstd">
<correction linux "Mise à jour vers la version amont stable 6.1.66 ; passage de l'ABI à la version 15 ; [rt] mise à jour vers la version 6.1.59-rt16 ; activation de 86_PLATFORM_DRIVERS_HP ; nvmet : les NQN passés dans la commande connect terminés par un caractère null [CVE-2023-6121]">
<correction linux-signed-amd64 "Mise à jour vers la version amont stable 6.1.66 ; passage de l'ABI à la version 15 ; [rt] mise à jour vers la version 6.1.59-rt16 ; activation de 86_PLATFORM_DRIVERS_HP ; nvmet : les NQN passés dans la commande connect terminés par un caractère null [CVE-2023-6121]">
<correction linux-signed-arm64 "Mise à jour vers la version amont stable 6.1.66 ; passage de l'ABI à la version 15 ; [rt] mise à jour vers la version 6.1.59-rt16 ; activation de 86_PLATFORM_DRIVERS_HP ; nvmet : les NQN passés dans la commande connect terminés par un caractère null [CVE-2023-6121]">
<correction linux-signed-i386 "Mise à jour vers la version amont stable 6.1.66 ; passage de l'ABI à la version 15 ; [rt] mise à jour vers la version 6.1.59-rt16 ; activation de 86_PLATFORM_DRIVERS_HP ; nvmet : les NQN passés dans la commande connect terminés par un caractère NULL [CVE-2023-6121]">
<correction llvm-toolchain-16 "Nouveau paquet rétroporté pour prendre en charge les constructions des nouvelles versions de chromium">
<correction lxc "Correction de la création de copies éphémères">
<correction mda-lv2 "Correction de l’emplacement d’installation du greffon de LV2">
<correction midge "Retrait de fichiers d'exemple non libres">
<correction minizip "Correction de problèmes de dépassements d'entier et de tas [CVE-2023-45853]">
<correction mrtg "Gestion du fichier de configuration déplacé ; mise à jour des traductions">
<correction mutter "Nouvelle version amont stable ; correction de la capacité de déplacer les fenêtres libdecor par leur barre de titre sur les écrans tactiles ; corrections des artefacts de clignotement et de rendu lors de l'utilisation d'un logiciel de rendu ; amélioration des performances de la vue Applications de GNOME Shell en évitant le rafraîchissement des écrans autres que celui qui l'affiche">
<correction nagios-plugins-contrib "Correction de la détection de la version du noyau présente sur le disque">
<correction network-manager-openconnect "Ajout d'un User Agent à Openconnect VPN pour NetworkManager">
<correction node-undici "Suppression des en-têtes cookie et host lors d’une redirection d’origine croisée [CVE-2023-45143]">
<correction nvidia-graphics-drivers "Nouvelle version amont ; correction d'un problème de déréférencement de pointeur NULL [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla "Nouvelle version amont ; correction d'un problème de déréférencement de pointeur NULL [CVE-2023-31022]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont ; correction d'un problème de déréférencement de pointeur NULL [CVE-2023-31022]">
<correction nvidia-open-gpu-kernel-modules "Nouvelle version amont ; correction d'un problème de déréférencement de pointeur NULL [CVE-2023-31022]">
<correction opendkim "Correction de retrait des en-têtes Authentication-Results entrants [CVE-2022-48521]">
<correction openrefine "Correction d'une vulnérabilité d'exécution de code à distance [CVE-2023-41887 CVE-2023-41886]">
<correction opensc "Correction d'un problème de lecture hors limite [CVE-2023-4535], d'un contournement potentiel de PIN [CVE-2023-40660], de problèmes de gestion de la mémoire [CVE-2023-40661]">
<correction oscrypto "Correction d'analyse de version d'OpenSSL ; correction d'autopkgtest">
<correction pcs "Correction de <q>resource move</q>">
<correction perl "Correction d'un problème de dépassement de tampon [CVE-2023-47038]">
<correction php-phpseclib3 "Correction d'un problème de déni de service [CVE-2023-49316]">
<correction postgresql-15 "Nouvelle version amont stable ; correction d'un problème d'injection de code SQL [CVE-2023-39417] ; correction de MERGE pour appliquer correctement la politique de sécurité de ligne [CVE-2023-39418]">
<correction proftpd-dfsg "Correction de la taille des tampons d'échange de clés SSH">
<correction python-cogent "Tests qui requièrent plusieurs processeurs évités uniquement lors d'une exécution sur les systèmes avec un seul processeur">
<correction python3-onelogin-saml2 "Correction des charges utiles de tests expirées">
<correction pyzoltan "Prise en charge de la construction sur les systèmes avec un seul cœur">
<correction qbittorrent "Désactivation de UPnP pour l'interface utilisateur web par défaut dans qbittorrent-nox">
<correction qemu "Mise à jour vers la version amont stable 7.2.7 ; hw/scsi/scsi-disk : interdiction des tailles de bloc inférieures à 512 [CVE-2023-42467]">
<correction qpdf "Correction d'un problème de perte de données avec certaines chaînes octales entre guillemets">
<correction redis "Abandon du drapeau de durcissement ProcSubset=pid de l'unité systemd parce qu'il provoque des plantages">
<correction rust-sd "Assurance que les versions du paquet binaire soient classées correctement par rapport aux versions plus anciennes (où il a été construit à partir d'un paquet source différent)">
<correction sitesummary "Utilisation du minuteur de systemd pour l'exécution de sitesummary-client s'il est disponible">
<correction speech-dispatcher-contrib "Activation de voxin sur armhf et arm64">
<correction spyder "Correction de la configuration automatique de la langue de l'interface">
<correction symfony "Correction d'un problème de fixation de session [CVE-2023-46733] ; ajout d'une absence d'échappement [CVE-2023-46734]">
<correction systemd "Nouvelle version amont stable">
<correction tbsync "Nouvelle version amont rétablissant la compatibilité avec les versions récentes de Thunderbird">
<correction toil "Un seul cœur requis pour les tests">
<correction tzdata "Mise à jour de la liste des secondes intercalaires">
<correction unadf "Correction d'un problème de dépassement de tampon [CVE-2016-1243] ; correction d'un problème d'exécution de code [CVE-2016-1244]">
<correction vips "Correction d'un problème de déréférencement de pointeur NULL [CVE-2023-40032]">
<correction weborf "Correction d'un problème de déni de service">
<correction wormhole-william "Désactivation de tests peu fiables, corrigeant des échecs de construction">
<correction xen "Nouvelle mise à jour amont stable ; correction de plusieurs problèmes de sécurité [CVE-2022-40982 CVE-2023-20569 CVE-2023-20588 CVE-2023-20593 CVE-2023-34320 CVE-2023-34321 CVE-2023-34322 CVE-2023-34323 CVE-2023-34325 CVE-2023-34326 CVE-2023-34327 CVE-2023-34328 CVE-2023-46835 CVE-2023-46836]">
<correction yuzu "Retrait de <q>:native</q> de la dépendance de construction de glslang-tools, corrigeant un échec de construction">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5499 chromium>
<dsa 2023 5506 firefox-esr>
<dsa 2023 5508 chromium>
<dsa 2023 5511 mosquitto>
<dsa 2023 5512 exim4>
<dsa 2023 5513 thunderbird>
<dsa 2023 5514 glibc>
<dsa 2023 5515 chromium>
<dsa 2023 5516 libxpm>
<dsa 2023 5517 libx11>
<dsa 2023 5518 libvpx>
<dsa 2023 5519 grub-efi-amd64-signed>
<dsa 2023 5519 grub-efi-arm64-signed>
<dsa 2023 5519 grub-efi-ia32-signed>
<dsa 2023 5519 grub2>
<dsa 2023 5520 mediawiki>
<dsa 2023 5521 tomcat10>
<dsa 2023 5523 curl>
<dsa 2023 5524 libcue>
<dsa 2023 5525 samba>
<dsa 2023 5526 chromium>
<dsa 2023 5527 webkit2gtk>
<dsa 2023 5528 node-babel7>
<dsa 2023 5529 slurm-wlm-contrib>
<dsa 2023 5529 slurm-wlm>
<dsa 2023 5531 roundcube>
<dsa 2023 5532 openssl>
<dsa 2023 5533 gst-plugins-bad1.0>
<dsa 2023 5534 xorg-server>
<dsa 2023 5535 firefox-esr>
<dsa 2023 5536 chromium>
<dsa 2023 5538 thunderbird>
<dsa 2023 5539 node-browserify-sign>
<dsa 2023 5540 jetty9>
<dsa 2023 5541 request-tracker5>
<dsa 2023 5542 request-tracker4>
<dsa 2023 5543 open-vm-tools>
<dsa 2023 5544 zookeeper>
<dsa 2023 5545 vlc>
<dsa 2023 5546 chromium>
<dsa 2023 5547 pmix>
<dsa 2023 5548 jtreg6>
<dsa 2023 5548 openjdk-17>
<dsa 2023 5549 trafficserver>
<dsa 2023 5550 cacti>
<dsa 2023 5551 chromium>
<dsa 2023 5552 ffmpeg>
<dsa 2023 5553 postgresql-15>
<dsa 2023 5555 openvpn>
<dsa 2023 5556 chromium>
<dsa 2023 5557 webkit2gtk>
<dsa 2023 5558 netty>
<dsa 2023 5559 wireshark>
<dsa 2023 5560 strongswan>
<dsa 2023 5561 firefox-esr>
<dsa 2023 5562 tor>
<dsa 2023 5563 intel-microcode>
<dsa 2023 5564 gimp>
<dsa 2023 5565 gst-plugins-bad1.0>
<dsa 2023 5566 thunderbird>
<dsa 2023 5567 tiff>
<dsa 2023 5568 fastdds>
<dsa 2023 5569 chromium>
<dsa 2023 5570 nghttp2>
<dsa 2023 5571 rabbitmq-server>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction gimp-dds "Plus nécessaire ; intégré à GIMP">

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Mises à jour proposées à la distribution stable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>


<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

