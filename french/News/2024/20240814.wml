#use wml::debian::translation-check translation="2a1830b31e7529340bf703cd96b2b22677f850df" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>La prise en charge de sécurité de Bullseye transmise à l'équipe LTS</define-tag>
<define-tag release_date>2024-08-14</define-tag>
#use wml::debian::news

<p>
Le 14 août 2024, trois ans après sa publication initiale, la prise en
charge de sécurité normale de Debian 11 <q>Bullseye</q> se termine.
L'équipe
<a href="https://wiki.debian.org/LTS/">du suivi à long terme (LTS)</a> de
Debian prendra le relai des équipes de sécurité et de publication.
</p>

<h2>Informations pour les utilisateurs</h2>

<p>
Bullseye LTS sera prise en charge du 15 août 2024 au 31 août 2026.
</p>

<p>
Autant que possible, les utilisateurs sont encouragés à mettre à niveau
leurs machines vers Debian 12, <q>Bookworm</q>, l'actuelle version stable
de Debian. Pour que le cycle de vie des versions de Debian soit plus
simple à suivre, les équipes concernées se sont mises d'accord sur le
programme suivant : trois ans de suivi normal, puis deux ans de suivi à
long terme (LTS). Ainsi, Debian 12 bénéficiera d'un suivi normal jusqu'au
10 juin 2026, puis du suivi à long terme jusqu'au 30 juin 2028,
respectivement trois et cinq ans après la publication initiale.
</p>

<p>
Les utilisateurs qui sont obligés de s'en tenir à Debian 11 trouveront
les informations pertinentes sur la prise en charge à long terme de
Debian sur la page
<a href="https://wiki.debian.org/fr/LTS/Using">LTS/Using</a> du wiki.
Des informations importantes et les modifications concernant
spécifiquement Bullseye LTS se trouvent sur la page
<a href="https://wiki.debian.org/LTS/Bullseye">LTS/Bullseye</a>.
</p>

<p>
Les utilisateurs de Debian 11 LTS sont invités à s'inscrire à la
<a href="https://lists.debian.org/debian-lts-announce/">liste de
diffusion d'annonces</a> pour recevoir celles de mise à jour de
sécurité ou à suivre les dernières annonces sur la page web
<a href="https://www.debian.org/lts/security/">LTS — Informations de sécurité</a>.
</p>

<p>
Quelques paquets ne sont pas couverts par la prise en charge de Bullseye
LTS. Il est possible d'identifier les paquets installés sur les machines
des utilisateurs qui ne sont pas pris en charge en installant le paquet
<a href="https://tracker.debian.org/pkg/debian-security-support">\
debian-security-support</a>.
Si debian-security-support détecte un paquet non pris en charge
d'importance critique pour vous, veuillez contacter
<strong>debian-lts@lists.debian.org</strong>.
</p>

<p>
Debian et l'équipe LTS aimeraient remercier tous ceux, utilisateurs
contributeurs, développeurs, parrains et autres équipes de Debian, qui
rendent possible le prolongement de la vie des anciennes distributions
<q>stable</q> et qui ont fait de <q>Buster</q> LTS un succès.
</p>

<p>
Si vous dépendez de Debian LTS, vous pouvez envisager de
<a href="https://wiki.debian.org/fr/LTS/Development">rejoindre l'équipe</a>,
en fournissant des correctifs, en réalisant des tests ou en
<a href="https://wiki.debian.org/fr/LTS/Funding">finançant l'initiative</a>.
</p>


<h2>À propos de Debian</h2>

<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code
source ouvert. Des milliers de volontaires du monde entier travaillent
ensemble pour créer et maintenir les logiciels Debian.
Traduite en soixante-dix langues et prenant en charge un grand nombre
de types d'ordinateurs, la distribution Debian est conçue
pour être le <q>système d'exploitation universel</q>.
</p>

<h2>Contact</h2>

<p>Pour de plus amples informations, veuillez consulter le site internet
de Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un
courrier électronique à &lt;press@debian.org&gt;.</p>

