#use wml::debian::template title="Site web Debian dans différentes langues" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="f6f1c0bfd748103f4f7718222b2b57401eb27288" maintainer="Jean-Paul Guillonneau"

# Translators:
# Christian Couder, 1999-2006.
# Nicolas Bertolissio, 2007.
# Symmon Paillard, 2008.
# Guillaume Delacour, 2009, 2010.
# David Prévot, 2010-2012, 2014.
# Jean-Paul Guillonneau, 2016-2023.

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Négociation de contenu</a></li>
    <li><a href="#howtoset">Comment définir la langue d’un navigateur web</a></li>
    <li><a href="#override">Comment outrepasser les réglages</a></li>
    <li><a href="#fix">Résolution des problèmes</a></li>
  </ul>
</div>

<h2><a id="intro">Négociation de contenu</a></h2>

<p>
Une équipe de <a href="../devel/website/translating">traducteurs</a>
travaille sur le site web de Debian pour le traduire dans un nombre croissant
de langues différentes. Mais comment fonctionne le changement de langue dans un
navigateur web ? Une norme appelée
<a href="$(HOME)/devel/website/content_negotiation">négociation de contenu</a>
permet aux utilisateurs de définir leur(s) langue(s) préférée(s). La version
affichée est négociée entre le navigateur web et le serveur web : le
navigateur envoie ses préférences au serveur et celui-ci décide quelle version
il envoie en se basant sur les préférences et les versions disponibles.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">En apprendre plus sur W3C</a></button></p>

<p>
Tout le monde n’a pas connaissance de la négociation de contenu, aussi des
liens en bas de chaque page pointent vers les autres versions disponibles.
Veuillez remarquer que la sélection d’une autre langue dans cette liste
n’affecte que la page en cours. Elle ne modifie pas la langue par défaut de
votre navigateur web. Si vous suivez un autre lien vers une page différente,
elle sera visible dans la langue par défaut.
</p>

<p>
Pour modifier la langue par défaut, deux options s’offrent à vous :
</p>

<ul>
  <li><a href="#howtoset">Configurer votre navigateur web</a></li>
  <li><a href="#override">Outrepasser les préférences de langue de votre navigateur</a></li>
</ul>

<p>
Allez directement aux instructions de configuration pour ces navigateurs web :</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> La langue originelle du
site web de Debian est la langue anglaise. Par conséquent, c’est une bonne idée
d’ajouter l’anglais (<code>en</code>) au bas de votre liste de langues. Cela
fonctionne comme solution de repli au cas où la page n’aurait pas encore été
traduite dans aucune des langues préférées.</p>
</aside>

<h2><a id="howtoset">Comment définir la langue d’un navigateur web</a></h2>

<p>
Avant de décrire comment configurer la langue dans les différents navigateurs,
quelques remarques d’ordre général. Premièrement, c’est une bonne idée
d’inclure toutes les langues que vous parlez à la liste des langues préférées.
Par exemple, si vous êtes un locuteur natif français, vous pouvez choisir
<code>fr</code> comme première langue, suivie par anglais avec le code de
langue <code>en</code>.
</p>

<p>
Deuxièmement, dans quelques navigateurs il est possible d’entrer des codes de
langue au lieu de les choisir d’après un menu. Dans ce cas, gardez à l’esprit
que le fait de créer une liste telle que <code>fr, en</code> ne définit pas
de préférence. Ce sont en fait deux options de même priorité et le
serveur web peut décider d’ignorer l’ordre pour choisir une des langues.
Pour indiquer une réelle préférence, il faut utiliser des valeurs dites de
qualité, c’est-à-dire des nombres décimaux entre 0 et 1. Une valeur plus
haute indique une priorité plus grande. Pour reprendre l’exemple avec les
langues française et anglaise, celui-ci peut être modifié de la façon suivante :
</p>

<pre>
fr; q=1.0, en; q=0.5
</pre>

<h3>Attention aux codes de pays</h3>

<p>
Un serveur web qui reçoit une requête pour un document avec comme langue
préférée <code>en-GB, fr</code> ne sert <strong>pas toujours</strong> la
version anglaise avant celle française. Il ne le fera que si une page avec
l’extension de langue <code>en-gb</code> existe. Par contre, il peut
fonctionner inversement : un serveur peut renvoyer une page <code>en-us</code>
si juste <code>en</code> est inclus dans la liste des langues préférées.
</p>

<p>
Par conséquent nous recommandons de ne pas ajouter au code de langue le code à
deux lettres de pays comme <code>en-GB</code> ou <code>en-US</code>, à moins
d’avoir une très bonne raison de le faire. Si vous le faites, assurez-vous
d’ajouter aussi le code de langue sans l’extension : <code>en-GB, en, fr</code>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">Plus d'informations sur la négociation de contenu</a></button></p>

<h3>Instructions pour plusieurs navigateurs web</h3>

<p>
Nous avons compilé une liste de navigateurs web populaires et quelques
instructions sur la façon de modifier la langue préférée pour le contenu web
dans leurs configurations :
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
En haut à droite, ouvrir le menu et cliquer <em>Réglages</em> -&gt;
<em>Avancés</em> -&gt; <em>Langues</em>. Ouvrir le menu <em>Langue</em> pour
voir la liste des langues. Cliquer sur les trois points près d’une entrée pour
modifier l’ordre. D’autres langues peuvent être ajoutées si nécessaires.</li>

        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
Régler la langue par défaut à l’aide de <em>Paramètres</em> -&gt; <em>Langue</em>
modifie également la langue demandée aux sites web. Ce comportement peut être
modifié en définissant avec précision l’<em>en-tête Accept-Language</em> avec
<em>Paramètres</em> -&gt; <em>Gestionnaire d’options</em> -&gt; <em>Protocoles</em>
-&gt; <em>HTTP</em></li>

        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
Ouvrir <em>Préférences</em> dans le menu principal et aller sur <em>Langue</em>.
Des langues peuvent y être ajoutées, supprimées ou ordonnées.</li>

        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
Dans la barre de menu du haut ouvrir <em>Préférences</em>. Faire défiler jusqu’à
<em>Langue et apparence</em> -&gt; <em>Langue</em> dans le panneau <em>Général</em>.
Cliquer sur le bouton <em>Choisir</em> pour définir la langue préférée d’affichage.
Des langues peuvent être ajoutées, supprimées ou ordonnées dans la même boîte de dialogue.</li>

        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
Aller dans <em>Préférences</em> -&gt; <em>Paramètres</em> -&gt; <em>Réseau</em>.
<em>Langues acceptées</em> affiche probablement un * sur la valeur par défaut.
Un clic sur le bouton <em>Locale</em> devrait permettre d’ajouter la langue
préférée. Sinon, elle doit être saisie manuellement.</li>

        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
<em>Édition</em> -&gt; <em>Préférences</em> -&gt; <em>Navigateur</em> -&gt;
<em>Fontes, Langues</em></li>

        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Édition</em> -&gt; <em>Préférences</em> -&gt; <em>Contenu</em> -&gt;
<em>Langues</em> -&gt; <em>Choisir</em></li>

        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Cliquer sur l’icône <em>Outils</em>, sélectionner <em>Options Internet</em>,
aller sur l’onglet <em>Général</em> et cliquer sur le bouton <em>Langues</em>.
Cliquer sur <em>Régler les préférences de langues</em> et des langues peuvent
être ajoutées, supprimées ou ordonnées dans la boîte de dialogue qui suit.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>

        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Éditer le fichier <em>~/.kde/share/config/kio_httprc</em> pour y inclure
une ligne telle que :<br>
<code>Languages=fr;q=1.0, en;q=0.5</code></li>

        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
Éditer le fichier <em>~/.lynxrc</em> pour y inclure une ligne telle que :<br>
<code>preferred_language=fr; q=1.0, en; q=0.5</code><br>
ou ouvrir les réglages du navigateur en pressant la touche [O], aller à
<em>Langue préférée</em> et ajouter les valeurs ci-dessus.</li>

        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Réglages, etc</em>  -&gt; <em>Réglages</em> -&gt; <em>Langues</em>
-&gt; <em>Ajouter des langues</em><br>
Cliquer sur le bouton trois points près de l’entrée langue pour plus d’options
et pour modifier l’ordre.</li>

        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
<em>Réglages</em> -&gt; <em>Navigateur</em> -&gt; <em>Langues</em> -&gt;
<em>Langues préférées</em></li>

        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
Safari utilise les réglages généraux de macOS et iOS. Pour définir la langue
préférée, ouvrir <em>Préférences du système</em> (macOS) ou <em>Réglages</em>
(iOS).</li>

        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
Presser le bouton [O] pour ouvrir le <em>Panneau de réglage d’options</em>,
naviguer vers <em>Réglages réseau</em> -&gt; <em>Accept-Language header</em>.
Presser [Entrée] pour modifier les réglages (par exemple,  <code>fr; q=1.0,
en; q=0.5</code>) et confirmer avec [Entrée]. Descendre jusqu’à [OK] pour
enregistrer les réglages.</li>

        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
Aller à <em>Réglages</em> -&gt; <em>Général</em> -&gt; <em>Langue</em> -&gt;
<em>Langues acceptées</em>, cliquer <em>Ajouter une langue</em> et en choisir
une à partir du menu. Utiliser les flèches pour modifier l’ordre des préférences.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Bien qu’il soit toujours
préférable de choisir la langue préférée dans la configuration du navigateur,
une autre solution permet d’outrepasser les réglages avec un cookie.</p>
</aside>

<h2><a id="override">Comment outrepasser les réglages</a></h2>

<p>
Si pour une quelconque raison, vous ne pouvez pas définir la langue
préférée dans les réglages du navigateur ou de l’environnement, il est possible
d’outrepasser les préférences en utilisant en dernier ressort un cookie. Cliquez
sur un des boutons ci-dessous pour mettre une seule langue au haut de la liste.
</p>

<p>
Veuillez noter que cela définira un
<a href="https://fr.wikipedia.org/wiki/Cookie_(informatique)">cookie</a>. Votre
navigateur le détruira automatiquement si vous ne visitez pas ce site web
pendant un mois. Bien sûr, ce cookie peut être détruit manuellement dans le
navigateur web ou en cliquant sur le bouton <em>Browser default</em>.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">Résolution des problèmes</a></h2>

<p>
Quelquefois le site web de Debian apparaît dans la mauvaise langue malgré tous
les efforts faits pour définir une langue préférée. Notre première suggestion
est de nettoyer le cache local (disque et mémoire) du navigateur avant d’essayer
de recharger le site. Si vous êtes sûr d’avoir correctement <a href="#howtoset">configuré
votre navigateur</a>, alors un cache brisé ou mal configuré peut être
la source du problème. Cela devient un problème sérieux de nos jours, car de
plus en plus de FAI voient la mise en cache comme une manière de faire
décroître leur trafic réseau. Consultez la <a href="#cache">section</a> à propos
des serveurs mandataires même si vous ne pensez pas en utiliser un.
</p>

<p>
<a href="https://manytools.org/http-html-text/browser-language/">Affichez la
liste de langues</a> indiquées comme préférences par votre navigateur.
</p>

<p>
De toutes façons, il est toujours possible qu’un problème existe avec
<a href="https://www.debian.org/">www.debian.org</a>. Bien que seulement une
poignée de problèmes signalés durant ces dernières années aient été provoqués
par un bogue de notre part, cela reste possible. Nous suggérons que
vous examiniez d’abord vos propres réglages et le problème potentiel de cache
avant de <a href="../contact">prendre contact</a> avec nous. Si
<a href="https://www.debian.org/">https://www.debian.org/</a> fonctionne
alors qu’un de nos <a href="https://www.debian.org/mirror/list">miroirs</a>
ne fonctionne pas, veuillez nous faire un rapport pour que nous puissions
contacter le responsable du miroir.
</p>

<h3><a name="cache">Problèmes potentiels avec les serveurs mandataires</a></h3>

<p>
Les serveurs mandataires sont principalement des serveurs qui n’ont pas en
propre de contenu. Ils se situent entre les utilisateurs et les serveurs web
réels, capturent les requêtes de page web puis récupèrent les pages. Après cela,
ils transmettent le contenu vers les navigateurs web des utilisateurs, mais
réalisent une copie en cache utilisée pour des requêtes futures. Cela peut
réduire notablement le trafic réseau quand plusieurs utilisateurs demandent la
même page.
</p>

<p>
Bien que ce soit une bonne idée la plupart du temps, cela cause des ratés quand
le cache est bogué. En particulier, certains serveurs mandataires n’intègrent
pas la négociation de contenu. Cela aboutit à mettre en cache une page et la
servir même si une langue différente est demandée plus tard. La seule solution
est de mettre à niveau ou de remplacer le logiciel de mise en cache.
</p>

<p>
Historiquement, les serveurs mandataires étaient utilisés quand les utilisateurs
configuraient en conséquence leur navigateur web. Ce n’est plus le cas
maintenant. Votre FAI redirige peut-être toutes les requêtes HTTP à travers
un serveur mandataire transparent. Si celui-ci ne gère pas correctement la
négociation de contenu, alors les utilisateurs peuvent recevoir des pages mises
en cache dans la langue non désirée. La seule façon de corriger cela est de le
signaler à votre FAI pour qu’il mette à niveau ou remplace son logiciel.
</p>
