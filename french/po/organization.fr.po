# Translation of organisation web page to French
# Copyright (C) 2002-2013 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the Debian web site.
#
# Denis Barbier, 2002-2004.
# Pierre Machard, 2002, 2004.
# Martin Quinson, 2003.
# Frederic Bothamy, 2004-2007.
# Thomas Huriaux, 2005.
# Mohammed Adnène Trojette, 2005-2007.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008, 2009.
# David Prévot <david@tilapin.org>, 2010-2013.
# Baptiste Jammet <baptiste@mailoo.org>, 2017-2019.
msgid ""
msgstr ""
"Project-Id-Version: debian webwml organisation 0.1\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-03-02 14:57+0100\n"
"Last-Translator: Baptiste Jammet <baptiste@mailoo.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "message de délégation"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "message de nomination"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "délégué"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "déléguée"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "délégué"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "déléguée"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "déléguée"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "déléguées"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "actuel"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membre"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "responsable"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Responsable de la publication stable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "conseiller"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "présidence"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistant"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secrétaire"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "représentant"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "rôle"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"Dans la liste suivante, <q>actuel</q> est utilisé pour les postes qui sont\n"
"provisoires (élus ou nommés avec une date d'échéance donnée)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Directeurs"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:107
msgid "Distribution"
msgstr "Distribution"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:168
msgid "Communication and Outreach"
msgstr "Communication et ouverture"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:171
msgid "Data Protection team"
msgstr "Équipe de protection des données"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:176
msgid "Publicity team"
msgstr "Équipe de publicité"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:232
msgid "Support and Infrastructure"
msgstr "Assistance et infrastructure"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:315
msgid "Membership in other organizations"
msgstr "Participation dans d'autres organisations"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Chef du projet"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comité technique"

#: ../../english/intro/organization.data:102
msgid "Secretary"
msgstr "Secrétaire"

#: ../../english/intro/organization.data:110
msgid "Development Projects"
msgstr "Projets de développement"

#: ../../english/intro/organization.data:111
msgid "FTP Archives"
msgstr "Archives FTP"

#: ../../english/intro/organization.data:113
msgid "FTP Masters"
msgstr "Responsables du FTP"

#: ../../english/intro/organization.data:119
msgid "FTP Assistants"
msgstr "Assistants pour le FTP"

#: ../../english/intro/organization.data:125
msgid "FTP Wizards"
msgstr "Conseillers pour le FTP"

#: ../../english/intro/organization.data:128
msgid "Backports"
msgstr "Rétroportages"

#: ../../english/intro/organization.data:130
msgid "Backports Team"
msgstr "Équipe en charge des rétroportages"

#: ../../english/intro/organization.data:134
msgid "Release Management"
msgstr "Organisation des nouvelles versions"

#: ../../english/intro/organization.data:136
msgid "Release Team"
msgstr "Équipe de publication"

#: ../../english/intro/organization.data:146
msgid "Quality Assurance"
msgstr "Assurance qualité"

#: ../../english/intro/organization.data:147
msgid "Installation System Team"
msgstr "Équipe du système d'installation"

#: ../../english/intro/organization.data:148
msgid "Debian Live Team"
msgstr "Équipe Debian Live"

#: ../../english/intro/organization.data:149
msgid "CD/DVD/USB Images"
msgstr "Images CD/DVD/USB"

#: ../../english/intro/organization.data:151
msgid "Production"
msgstr "Production"

#: ../../english/intro/organization.data:159
msgid "Testing"
msgstr "Test"

#: ../../english/intro/organization.data:161
msgid "Cloud Team"
msgstr "Équipe pour l'informatique dématérialisée"

#: ../../english/intro/organization.data:181
msgid "Press Contact"
msgstr "Contact presse"

#: ../../english/intro/organization.data:183
msgid "Web Pages"
msgstr "Pages web"

#: ../../english/intro/organization.data:191
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:196
msgid "Outreach"
msgstr "Programme d'ouverture"

#: ../../english/intro/organization.data:203
msgid "Community"
msgstr "Équipe en charge de la communauté"

#: ../../english/intro/organization.data:209
msgid ""
"To send a private message to all the members of the Community Team, use the "
"OpenPGP key <a href=\"community-team-pubkey."
"txt\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Pour envoyer un message privé à tous les membres de l'équipe en charge de la "
"communauté, veuillez utiliser la clef OpenPGP <a href=\"community-team-"
"pubkey.txt\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:211
msgid "DebConf Committee"
msgstr "Comité DebConf"

#: ../../english/intro/organization.data:219
msgid "Partner Program"
msgstr "Programme de partenariat"

#: ../../english/intro/organization.data:223
msgid "Hardware Donations Coordination"
msgstr "Coordination des dons de machines"

#: ../../english/intro/organization.data:235
msgid "Bug Tracking System"
msgstr "Système de suivi des bogues"

#: ../../english/intro/organization.data:240
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administration et archives des listes de diffusion"

#: ../../english/intro/organization.data:248
msgid "New Members Front Desk"
msgstr "Secrétariat des nouveaux membres"

#: ../../english/intro/organization.data:257
msgid "Debian Account Managers"
msgstr "Responsables des comptes de Debian"

#: ../../english/intro/organization.data:263
msgid ""
"To send a private message to all DAMs, use the OpenPGP key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Pour envoyer un message privé à tous les gestionnaires de comptes (DAM), "
"veuillez utiliser la clef OpenPGP 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:264
msgid "Keyring Maintainers (OpenPGP)"
msgstr "Responsable du fichier de clefs (OpenPGP)"

#: ../../english/intro/organization.data:268
msgid "Security Team"
msgstr "Équipe de sécurité"

#: ../../english/intro/organization.data:280
msgid "Policy"
msgstr "Charte"

#: ../../english/intro/organization.data:283
msgid "System Administration"
msgstr "Administration système"

#: ../../english/intro/organization.data:284
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"C'est l'adresse à utiliser lorsque vous rencontrez des problèmes sur l'une "
"des machines Debian, en particulier les problèmes de mots de passe ou si "
"vous souhaitez l'installation d'un nouveau paquet."

#: ../../english/intro/organization.data:294
msgid ""
"If you have hardware problems with Debian machines, please see <a "
"href=\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it "
"should contain per-machine administrator information."
msgstr ""
"Si vous avez des problèmes de matériel avec les machines Debian, veuillez "
"consulter la page sur les <a href=\"https://db.debian.org/machines."
"cgi\">machines Debian</a>, elle pourrait contenir des informations fournies "
"par l'administrateur de la machine."

#: ../../english/intro/organization.data:295
msgid "Mirrors"
msgstr "Miroirs"

#: ../../english/intro/organization.data:299
msgid "DNS Maintainer"
msgstr "Responsable DNS"

#: ../../english/intro/organization.data:300
msgid "Package Tracking System"
msgstr "Système de suivi de paquets"

#: ../../english/intro/organization.data:302
msgid "Treasurer"
msgstr "Trésorier"

#: ../../english/intro/organization.data:307
msgid "Salsa administrators"
msgstr "Administration de Salsa"

#: ../../english/intro/organization.data:318
msgid "GNOME Foundation"
msgstr "Fondation GNOME"

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:320
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:322
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:323
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:324
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS : Organization\n"
"      for the Advancement of Structured Information Standards\n"
"      (organisation pour l'avancement des normes informatiques)"

#: ../../english/intro/organization.data:326
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#~ msgid ""
#~ "<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use "
#~ "requests"
#~ msgstr ""
#~ "Demandes d'utilisation de <a name=\"trademark\" href=\"m4_HOME/"
#~ "trademark\">marque déposée</a>"

#~ msgid "Alioth administrators"
#~ msgstr "Administration d'Alioth"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (inactif : non publié avec Squeeze)"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-harcèlement"

#~ msgid "Auditor"
#~ msgstr "Commissaire aux comptes"

#~ msgid "Autobuilding infrastructure"
#~ msgstr "Infrastructure des empaqueteurs automatiques"

#~ msgid "Bits from Debian"
#~ msgstr "Brèves de Debian"

#~ msgid "Buildd administration"
#~ msgstr "Administration des démons de construction"

#~ msgid "CD Vendors Page"
#~ msgstr "Pages des revendeurs de CD"

#~ msgid "Consultants Page"
#~ msgstr "Page des consultants"

#~ msgid "DebConf chairs"
#~ msgstr "Responsables de DebConf"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Responsables du fichier de clefs des mainteneurs Debian (DM)"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Debian Women Project"
#~ msgstr "Projet Debian Women"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian pour l'astronomie"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian pour les enfants de 1 à 99 ans"

#~ msgid "Debian for education"
#~ msgstr "Debian pour l'éducation"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian pour la pratique médicale et la recherche"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian pour les personnes handicapées"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian pour la science et la recherche associée"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian dans les organismes juridiques"

#~ msgid "Documentation"
#~ msgstr "Documentation"

#~ msgid "Embedded systems"
#~ msgstr "Systèmes embarqués"

#~ msgid "Events"
#~ msgstr "Événements"

#~ msgid "Firewalls"
#~ msgstr "Pare-feux"

#~ msgid "Individual Packages"
#~ msgstr "Paquets individuels"

#~ msgid "LDAP Developer Directory Administrator"
#~ msgstr "Administrateur de la base LDAP des développeurs"

#~ msgid "Laptops"
#~ msgstr "Portables"

#~ msgid "Live System Team"
#~ msgstr "Équipe du système autonome (« Live »)"

#~ msgid ""
#~ "OVAL: Open Vulnerability\n"
#~ "      Assessment Language"
#~ msgstr ""
#~ "OVAL : Open Vulnerability\n"
#~ "      Assessment Language\n"
#~ "      (langage libre d'analyse de vulnérabilités)"

#~ msgid "Ports"
#~ msgstr "Portages"

#~ msgid "Publicity"
#~ msgstr "Publicité"

#~ msgid "Release Notes"
#~ msgstr "Notes de publication"

#~ msgid "SchoolForge"
#~ msgstr "SchoolForge"

#~ msgid "Security Audit Project"
#~ msgstr "Projet d'audit de sécurité"

#~ msgid "Special Configurations"
#~ msgstr "Configurations spéciales"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Administrateurs du Summer of Code 2013"

#~ msgid "Testing Security Team"
#~ msgstr "Équipe de sécurité de testing"

#~ msgid "User support"
#~ msgstr "Assistance aux utilisateurs"

#~ msgid "Vendors"
#~ msgstr "Revendeurs"

#~ msgid "Volatile Team"
#~ msgstr "Équipe en charge de volatile"

#~ msgid "Wanna-build team"
#~ msgstr "Équipe en charge de wanna-build"

#~ msgid "Work-Needing and Prospective Packages list"
#~ msgstr "Liste des paquets en souffrance et paquets souhaités"

#~ msgid "current Debian Project Leader"
#~ msgstr "Chef du projet actuel"
