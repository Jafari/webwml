# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: debian webwml security 0.1\n"
"PO-Revision-Date: 2008-07-28 21:29+0200\n"
"Last-Translator: Pierre Machard <pmachard@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "Question <define-tag small_Q>Q. </define-tag>"

#~ msgid ""
#~ "<a href=\\\"undated/\\\">undated</a> security advisories, included for "
#~ "posterity"
#~ msgstr ""
#~ "annonces de sécurités <a href=\\\"undated/\\\">non datées</a>, conservées "
#~ "pour la postérité"

#~ msgid "Affected Packages"
#~ msgstr "Paquets concernés "

#~ msgid "Architecture-independent component:"
#~ msgstr "Composant indépendant de l'architecture&nbsp;:"

#~ msgid "Bug"
#~ msgstr "Bogue"

#~ msgid "BugTraq ID"
#~ msgstr "Identifiant BugTraq"

#~ msgid "CERT Advisories"
#~ msgstr "Alertes du CERT"

#~ msgid "CERT's vulnerabilities, advisories and incident notes:"
#~ msgstr ""
#~ "Les annonces de vulnérabilité et les bulletins d'alerte du CERT&nbsp;:"

#~ msgid "Date Reported"
#~ msgstr "Date du rapport "

#~ msgid "Debian Security"
#~ msgstr "Sécurité Debian"

#~ msgid "Debian Security Advisories"
#~ msgstr "Bulletins d'alerte Debian"

#~ msgid "Debian Security Advisory"
#~ msgstr "Bulletin d'alerte Debian"

#~ msgid "Fixed in"
#~ msgstr "Corrigé dans "

#~ msgid "In Mitre's CVE dictionary:"
#~ msgstr "Dans le dictionnaire CVE du Mitre&nbsp;:"

#~ msgid "In the Bugtraq database (at SecurityFocus):"
#~ msgstr ""
#~ "Dans la base de données de suivi des bogues (chez SecurityFocus)&nbsp;:"

#~ msgid "In the Debian bugtracking system:"
#~ msgstr "Dans le système de suivi des bogues Debian&nbsp;:"

#~ msgid ""
#~ "MD5 checksums of the listed files are available in the <a href=\"<get-var "
#~ "url />\">original advisory</a>."
#~ msgstr ""
#~ "Les sommes MD5 des fichiers indiqués sont disponibles sur la <a "
#~ "href=\"<get-var url />\">page originale de l'alerte de sécurité</a>."

#~ msgid ""
#~ "MD5 checksums of the listed files are available in the <a href=\"<get-var "
#~ "url />\">revised advisory</a>."
#~ msgstr ""
#~ "Les sommes MD5 des fichiers indiqués sont disponibles dans la <a "
#~ "href=\"<get-var url />\">nouvelle annonce de sécurité</a>."

#~ msgid "Mitre CVE dictionary"
#~ msgstr "Dictionnaire CVE du Mitre&nbsp;:"

#~ msgid "More information"
#~ msgstr "Plus de précisions "

#~ msgid "No other external database security references currently available."
#~ msgstr ""
#~ "Aucune référence à une base de données externe en rapport avec la "
#~ "sécurité n'est actuellement disponible."

#~ msgid "Security database references"
#~ msgstr "Références dans la base de données de sécurité "

#~ msgid "Securityfocus Bugtraq database"
#~ msgstr "Base de données du suivi des bogues chez Securityfocus"

#~ msgid "Source:"
#~ msgstr "Source :"

#~ msgid "US-CERT vulnerabilities Notes"
#~ msgstr "Notes d'alerte du CERT US"

#~ msgid "Vulnerable"
#~ msgstr "Vulnérabilité "
