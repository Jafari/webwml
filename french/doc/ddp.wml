#use wml::debian::template title="Le Projet de documentation Debian (DDP)"
#use wml::debian::translation-check translation="76dd78fbaab2ad656d92c187bdfa9a5ad9e0bff0" maintainer="Jean-Pierre Giraud"

# Premier traducteur: Mickael Simon (2001)
# Frédéric Bothamy, 2006-2007
# David Prévot, 2010-2011
# Jean-Paul Guillonneau, 2017-2020

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Le Projet de documentation Debian (DDP) s'occupe de la documentation de Debian, par exemple les manuels pour les utilisateurs et pour les développeurs, divers manuels, les FAQ et les notes de publication. Cette page offre un rapide aperçu des travaux du DDP.</p>
</aside>

<h2><a id="ddp_work">Nos travaux</a></h2>

<p>
<div class="line">
  <div class="item col50">

    <h3>Manuels</h3>
    <ul>
      <li><strong><a href="user-manuals">Manuels pour les utilisateurs</a></strong></li>
      <li><strong><a href="devel-manuals">Manuels pour les développeurs</a></strong></li>
      <li><strong><a href="misc-manuals">Autres manuels</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Politique de documentation</h3>
    <ul>
      <li>Licences de manuel compatibles avec les DFSG.</li>
      <li>Nous utilisons restructuredText ou Docbook XML (obsolète) pour nos documents.</li>
      <li>Toutes les sources sont disponibles sur notre
      <a href="https://salsa.debian.org/ddp-team">dépot Git</a>.</li>
      <li><tt>www.debian.org/doc/&lt;nom_de_manuel&gt;</tt> sera l’URL
       officielle.</li>
      <li>Chaque document devrait être entretenu activement.</li>
    </ul>

    <h3>Accès à Git</h3>
    <ul>
      <li>Comment accéder aux <a href="vcs">dépôt Git du DDP</a></li>
    </ul>

  </div>

</div>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> Communiquez avec l'équipe sur la liste <a href="https://lists.debian.org/debian-doc/">debian-doc</a></button></p>
