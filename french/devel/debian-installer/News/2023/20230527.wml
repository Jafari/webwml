#use wml::debian::translation-check translation="2f6e497d031196a0f2c3d92a8e9bfce55cd8bcbf" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bookworm RC4</define-tag>
<define-tag release_date>2023-05-27</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la quatrième version candidate de l’installateur pour Debian 12 <q>Bookworm</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>cdebconf :
  <ul>
    <li>amélioration de l'affichage de la bannière lorsque la largeur du logo de
      l'installateur est plus petite que la largeur de la fenêtre GTK
      (<a href="https://bugs.debian.org/745359">nº 745359</a>) : au lieu d'une
      mise à l'échelle, élargir la bannière sur le ou les côtés spécifiés dans
      le thème (<a href="https://bugs.debian.org/1036321">nº 1036321</a>).</li>
  </ul>
  </li>
  <li>debian-installer :
  <ul>
    <li>netboot/x86 : correction des couleurs dans les sous-menus de GRUB
      (<a href="https://bugs.debian.org/1036771">nº 1036771</a>) ;</li>
    <li>netboot/x86 : ajout du lien symbolique /splash.png pour que GRUB charge
      l'écran d'accueil (<a href="https://bugs.debian.org/1036215">nº 1036215</a>).</li>
  </ul>
  </li>
  <li>grub2 :
  <ul>
    <li>lors d'une installation aussi dans le chemin d'un média amovible,
      inclusion du binaire mokmanager pertinent
      (<a href="https://bugs.debian.org/1034409">nº 1034409</a>) ;</li>
    <li>permettre plusieurs paramètres d'initrd
      (<a href="https://bugs.debian.org/838177">nº 838177</a>,
      <a href="https://bugs.debian.org/820838">nº 820838</a>).</li>
  </ul>
  </li>
  <li>partman-auto-raid :
  <ul>
    <li>ajout de la prise en charge de LVM-on-LUKS-on-RAID à l'aide du nouveau
      mot clé « crypto-lvm » semblable au mot clé existant « lvm »
      (<a href="https://bugs.debian.org/1036347">nº 1036347</a>).</li>
  </ul>
  </li>
  <li>partman-basicfilesystems :
  <ul>
    <li>installation de dosfstools s'il existe une partition ESP FAT.</li>
  </ul>
  </li>
  <li>rootskel-gtk :
  <ul>
    <li>ajout des métadonnées de la bannière (<a href="https://bugs.debian.org/745359">nº 745359</a>,
      <a href="https://bugs.debian.org/1036321">nº 1036321</a>) ;</li>
    <li>alignement des couleurs de la bannière sombre sur celles du thème
      sombre/à fort contraste ;</li>
    <li>correction de la fenêtre d'affichage du document, évitant des décalages
      d'un pixel.</li>
  </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>finish-install :
  <ul>
    <li>ajout de bochs/cirrus à l'initramfs si une carte est détectée
    (<a href="https://bugs.debian.org/1036788">nº 1036788</a>).</li>
  </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 42 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres médias d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
