#use wml::debian::template title="VCS (Sistema di Controllo Versione) del Progetto di Documentazione Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="e6e987deba52309df5b062b1567c8f952d7bd476"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#access">Accedere ai sorgenti in Git</a></li>
  <li><a href="#obtaining">Ottenere i privilegi di scrittura (Push)</a></li>
  <li><a href="#updates">Meccanismo di aggiornamento automatico</a></li>
</ul>

<aside> <p> <span class="fas fa-caret-right fa-3x"></span> Il <a
href="ddp">Debian Documentation Project (Progetto di documentazione di
Debian)</a> raccoglie tutte le sue pagine web e gran parte dei manuali in <a
href="https://salsa.debian.org">Salsa</a>, l'istanza GitLab di Debian. Chiunque
pu&ograve; scaricare i sorgenti dal servizio Salsa, ma solo i membri del DDP
hanno i necessari diritti di scrittura per poter aggiornare i file. </p>
</aside>

<h2><a id="access">Accedere ai sorgenti in Git</a></h2>

<p>Il Progetto di documentazione di Debian raccoglie tutti i contenuti in Salsa,
l'istanza GitLab di Debian. Per accedere a singoli file, controllare cambiamenti
recenti e visualizzare altre attivit&agrave; del progetto, si prega di visitare
il <a href="https://salsa.debian.org/ddp-team/">repository del DDP</a>.</p>

<p>Per scaricare i manuali completi, l'opzione migliore &egrave; quella di avere
accesso diretto a Salsa. Le prossime sezioni spiegheranno come clonare un
repository Git (in sola lettura o in scrittura) sulla propria macchina e come
aggiornare la propria copia locale. Come prerequisito si prega di installare il
pacchetto <a href="https://packages.debian.org/git">git</a> sulla propria
macchina.</p>

<p style="text-align:center"> <button type="button"> <span class="fas
fa-book-open fa-2x"> </span> <a href="https://wiki.debian.org/Salsa">Leggi la
documentazione di Salsa </a> </button> </p>

<h3>Clonare anonimamente il repository Git (in sola lettura)</h3>

<p>Usare questo comando per scaricare tutti i file relativi a un progetto:</p>

<pre>
git clone https://salsa.debian.org/ddp-team/release-notes.git
</pre>

<!--Questo paragrafo da rivedere: cercare la traduzione di termini tecnici in
italino.-->
<p>Lanciare lo stesso comando per scaricare ogni altro progetto sul proprio
computer. <strong>Suggerimento:</strong> Per ottenere l'URL corretto da passare
al comando <code >git clone</code>, aprire il progetto in un browser, fare click
sul bottone blu <em>Clone with HTTPS</em> e copiare l'URL negli appunti.</p>

<h3>Clonare un repository Git con privilegi di push (lettura e scrittura)</h3>

<p>Prima di avere accesso in lettura e scrittura al server Git, bisogna ottenere
i diritti di scrittura sul repository. Si prega di consultare <a
href="#obtaining">questa sezione</a> per maggiori informazioni riguardanti la
richiesta di permessi di scrittura (push).</p>

<p>Avendo i permessi di scrittura su Salsa, si pu&ograve; utilizzare il seguente
comando per scaricare tutti i file di un progetto:</p>

<pre>
git clone git@salsa.debian.org:ddp-team/release-notes.git
</pre> 

<p>Lanciare lo stesso comando per ogni progetto che si desidera clonare
localmente.</p>

<h3>Scaricare le modifiche dal repository Git remoto</h3>

<p>Per aggiornare la propria copia locale con tutte le modifiche fatte da altri
a un manuale, entrare nella corrispondente sotto cartella e lanciare il seguente
comando: </p>

<pre>
git pull
</pre>

<h2><a id="obtaining">Ottenere i permessi di scrittura (Push)</a></h2>

<p>I permessi di scrittura sono disponibili a chiunque voglia partecipare alla
stesura di manuali, FAQ (domande frequenti), howto, ecc. &Egrave; generalmente
richiesto l'invio di un paio di modifiche. Dopodich&eacute;, seguire questi
passi per inoltrare la richiesta di accesso in scrittura:</p>

<ol>
  <li>Creare un account su <a href="https://salsa.debian.org/">Salsa</a>, se non lo si &egrave; gi&agrave; fatto.</li>
  <li>Andare al <a href="https://salsa.debian.org.ddp-team/">repository del DDP</a> e fare click su <em>Richiedi Accesso</em></li>
  <li>Inviare un'e-mail a <a href="mailto:debian-doc@lists.debian.org">debian-doc@lists.debian.org</a> e comunicarci in che modo si ha contribuito al progetto Debian.</li>
  <li>Una volta che la richiesta sar&agrave; approvata, si far&agrave; parte del team DDP.</li>
</ol>

<h2><a id="updates">Meccanismo di aggiornamento automatico</a></h2>

<p>Tutti i manuali sono pubblicati come pagine web. Queste sono generate
automaticamente su www-master.debian.org durante il periodico processo di
compilazione del sito web, pianificato ogni quattro ore. Durante tale processo,
la versione pi&uacute; aggiornata &egrave; scaricata dall'archivio, ogni manuale
ricompilato e tutti i file trasferiti nella sotto cartella
<code>doc/manuals/</code> del sito web.</p>

<p>I file di documentazione generati dallo script di aggiornamento si trovano
all'indirizzo <a href="manuals/">https://www.debian.org/doc/manuals/</a>.</p>

<p>I file di log generati dal processo di update si trovano all'indirizzo <url
"https://www-master.debian.org/build-logs/webwml/"/> (lo script si chiama
<code>7doc</code> e fa parte del cron job nominato <code>often</code>).</p>
