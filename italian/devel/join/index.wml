#use wml::debian::template title="Come unirsi a Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside><p><span class="fas fa-caret-right fa-3x"></span> Siamo alla continua
ricerca di contributori e volontari. Incoraggiamo la <a
href="$(HOME)/intro/diversity">partecipazione di chiunque</a>. Requisiti:
interesse nel Software Libero e un po' di tempo a disposizione.</p></aside>

<ul class="toc">
  <li><a href="#reading">Letture consigliate</a></li>
  <li><a href="#contributing">Come contribuire</a></li>
  <li><a href="#joining">Come unirsi a noi</a></li>
</ul>

<h2><a id="reading">Letture consigliate</a></h2>

<p>Se non lo si fosse gi&agrave; fatto, si dovrebbero leggere le pagine
raggiungibili dalla <a href="$(HOME)">pagina iniziale del sito Debian</a>.
Questo dar&agrave; maggiori informazioni su chi siamo e quali obiettivi
perseguiamo. In veste di potenziali contributori, si prega di prestare
particolare attenzione alle seguenti pagine:</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">Linee guida Debian per il Software Libero</a></li>
  <li><a href="$(HOME)/social_contract">Il Contratto sociale Debian</a></li>
</ul>

<p>Molte delle nostre comunicazioni passano dalle <a
href="$(HOME)/MailingLists/">liste di messaggi</a>. Per farsi un'idea di come il
progetto Debian funzioni, ci si dovrebbe iscrivere almeno alle liste <a
href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>
e <a href="https://lists.debian.org/debian-news/">debian-news</a>. Entrambe le
liste hanno un ridotto volume di messaggi e documentano cosa sta succedendo
nella comunit&agrave;. Le <a href="https://www.debian.org/News/weekly/">notizie
riguardanti il Progetto Debian</a>, anch'esse pubblicate sulla lista di messaggi
debian-news, riassumono le discussioni pubblicate sui blog e sulle liste di
messaggi Debian, fornendo i relativi collegamenti.</p>

<p>In veste di potenziali sviluppatori, si consideri l'iscrizione alla lista di
messaggi <a href="https://lists.debian.org/debian-mentors/">debian-mentors</a>.
Qui si potranno porre domande riguardanti la pubblicazione di pacchetti,
progetti infratrutturali e questioni legate allo sviluppo. Si noti che tale
lista &egrave; pensata per i contributori e non per gli utenti finali. Altre
liste interessanti sono <a
href="https://lists.debian.org/debian-devel/">debian-devel</a> (argomenti di
natura tecnica e legati allo sviluppo), <a
href="https://lists.debian.org/debian-project/">debian-project</a> (discussioni
riguardanti questioni meno tecniche), <a
href="https://lists.debian.org/debian-release/">debian-release</a> (utile per il
coordinamento dei rilasci di Debian), <a
href="https://lists.debian.org/debian-qa/">debian-qa</a> (lista legata ai
test).</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope
fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Iscrizione alle liste di
messaggi</a></button></p>

<p><strong>Suggerimento:</strong> Volendo ridurre il numero di messaggi
ricevuti, specialmente da liste ad alto traffico, c'&egrave; la
possibilit&agrave; di ricevere e-mail di sintesi invece dei singoli messaggi.
Potete anche visitare gli <a href="https://lists.debian.org/">Archivi delle
liste di messaggi</a> per leggere i messaggi direttamente all'interno del
browser.</p>

<aside><p><span class="fas fa-caret-right fa-3x"></span> Per contribuire non
&egrave; necessario essere uno sviluppatore Debian (Debian Developer, DD). Un
altro sviluppatore Debian pu&ograve; agire in veste di <a
href="newmaint#Sponsor">sponsor</a> e aiutare a integrare il lavoro altrui nel
progetto.</p></aside>

<h2><a id="contributing">Come contribuire</a></h2>

<p>Si &egrave; interessati nella manutenzione di pacchetti? Si potrebbe dare uno
sguardo alla nostra lista di <a href="$(DEVEL)/wnpp/">pacchetti</a> che
necessitano di lavoro o di pacchetti candidati all'inclusione nel sistema. Qui
si troveranno pacchetti che hanno bisogno di un (nuovo) manutentore. Questo non
solo aiuta la nostra distribuzione, ma dar&agrave; anche l'opportunit&agrave;
di imparare dal precedente manutentore.</p>

<p>Qualche altra idea su come contribuire a Debian:</p>

<ul>
  <li>Scrivere la <a href="$(HOME)/doc/">documentazione</a>.</li>
  <li>Manutenere il <a href="$(HOME)/devel/website/">sito di Debian</a>, produrre contenuti, aggiornare o tradurre le parti esistenti.</li>
  <li>Unirsi al nostro <a href="$(HOME)international/">team di traduzioni</a>.</li>
  <li>Migliorare Debian unendosi al <a href="https://qa.debian.org/">Team di test (Debian Quality Assurance)</a>.</li>
</ul>

<p>Naturalmente ci sono molte altre cose che potrebbero aiutare e noi siamo alla
costante ricerca di persone che offrano supporto legale o che si uniscano al <a
href="https://wiki.debian.org/Teams/Publicity">Team di Pubblicit&agrave;</a>.
Diventare un membro di un team Debian &egrave; un ottimo modo per fare un po' di
esperienza prima di iniziare il processo per divenire un <a
href="newmaint">Nuovo Membro</a>. &Egrave; anche un ottimo modo per trovare uno
sponsor per un pacchetto. Perch&eacute; aspettare? Si trovi un team a cui unirsi
subito!</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users
fa-2x"></span> <a href="https://wiki.debian.org/Teams">Lista dei team di
Debian</a></button></p>

<h2><a id="joining">Come unirsi a noi</a></h2>

<p>E se fosse un po' di tempo che si contribuisce al progetto e si volesse
unirsi a Debian in veste pi&ugrave; ufficiale? Esistono due opzioni:</p>

<aside><p><span class="fas fa-caret-right fa-3x"></span> Il ruolo di Manutentore
di Debian (DM) fu introdotto nel 2007. Fino ad allora, l'unico ruolo ufficiale
era lo Sviluppatore Debian (DD). Ci sono due procedimenti separati, uno per ogni
ruolo che si intende ricoprire.</p></aside>

<ul>
  <li><strong>Manutentore Debian (DM):</strong> Questo &egrave; il primo passo -
  in veste di DM si avrebbe la facolt&agrave; di includere nella distribuzione
  Debian pacchetti di vostra propriet&agrave;. A differenza dei Manutentori con
  sponsor, i Manutentori Debian possono manutenere pacchetti senza uno sponsor.
  <br>Maggiori informazioni: <a
  href="https://wiki.debian.org/DebianMaintainer">Debian Maintainer</a></li>
  <li><strong>Sviluppatore Debian (DD):</strong> questo &egrave; il tradizionale
  ruolo di membri con accesso completo. Un DD pu&ograve; partecipare alle
  elezioni. DD contributori possono caricare qualunque pacchetto nell'archivio.
  Prima di divenire DD contributori, si dovrebbero avere esperienze pregresse in
  veste di manutentori di almeno sei mesi (per esempio, aver fatto caricamenti
  di pacchetti in veste di DM, aver lavorato in un team, o aver manutenuto
  pacchetti caricati da sponsor). I DD non contributori hanno gli stessi diritti
  dei DM in tema di caricamento di pacchetti. Prima di chiedere di diventare DD
  non contributori, dovreste avere fatto esperienze significative all'interno
  del progetto. <br>Maggiori informazioni: <a href="newmaint">L'angolo dei nuovi
  membri</a>.</li>
</ul>

<p>Al di l&agrave; del ruolo a cui si abbia deciso di chiedere l'accesso, si
dovrebbe avere un po' di familiarit&agrave; con le procedure di Debian. Per
questo si raccomanda di leggere il <a href="$(DOC)/debian-policy/">Debian Policy
Manual</a> e la <a href="$(DOC)/developers-reference/">Guida dello
Sviluppatore</a>.</p>
