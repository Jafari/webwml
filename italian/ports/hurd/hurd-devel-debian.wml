#use wml::debian::template title="Debian GNU/Hurd &ndash; Sviluppo" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="8f3db59aacd73ab4251427a34f4599208e3700f4" maintainer="Luca Monducci"

<h1>Debian GNU/Hurd</h1>

<h2>Sviluppo della distribuzione</h2>

<h3>Pacchetti per Hurd</h3>
<p>
I pacchetti specifici per Hurd sono manutenuti su <url "https://salsa.debian.org/hurd-team/"/>.
</p>

<h3>Port dei pacchetti Debian</h3>

<p>Chi desidera contribuire al port Debian GNU/Hurd deve aver confidenza
con il sistema di pacchettizzazione Debian. Una volta acquisite queste
conoscenze leggendo la documentazione disponibile e visitando
<a href="$(HOME)/devel/">l'angolo degli sviluppatori</a> si dovrebbe
essere in grado di estrarre i pacchetti sorgente Debian e di creare un
pacchetto binario Debian. Per le persone più pigre è disponibile un
corso rapido:</p>

<h3>Recuperare i sorgenti e creare i pacchetti</h3>

<p>Per recuperare i sorgenti di un pacchetto usare <code>apt
source pacchetto</code>; questo comando esegue anche l'estrazione
dei sorgenti dal file di archivio.</p>

<p>L'estrazione di un pacchetto sorgente Debian richiede il file
<code>pacchetto_versione.dsc</code> e dei file che sono elencati al suo
interno. La directory di compilazione Debian viene creata con il comando
<code>dpkg-source -x pacchetto_versione.dsc</code>.</p>

<p>Il pacchetto viene creato nella directory di compilazione Debian
<code>pacchetto-versione</code> con il comando
<code>dpkg-buildpackage -B "-mMyName &lt;MyEmail&gt;"</code>. È possibile
usare <code>-b</code> al posto di <code>-B</code> per creare un pacchetto
con le sole parti indipendenti dall'architettura (solitamene è inutile
poiché sono già presenti nell'archivio e inoltre la loro compilazione
richiede anche ulteriori dipendenze). Inoltre è possibile
aggiungere <code>-uc</code> per evitare di firmare il pacchetto con la
propria chiave OpenPGP.</p>

<p>La compilazione potrebbe richiedere l'istallazione di altri pacchetti.
Il modo più semplice per farla funzionare è installare tutti i pacchetti
necessari usando il comando <code>apt build-dep pacchetto</code>.</p>

<p>
Può essere comodo usare pbuilder. Prima deve essere compilato con
<code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/
--debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg
--debootstrapopts --extra-suites=unreleased --extrapackages
debian-ports-archive-keyring</code> e poi usato con
<code>pdebuild -- --binary-arch</code>, gestisce il recupero delle
dipendenze per la compilazione, ecc. e salva il risultato in
<code>/var/cache/pbuilder/result</code>.
</p>


<h3>Scegliere un pacchetto</h3>

<p>Quale pacchetto richiede del lavoro? Qualsiasi pacchetto che ancora
non è stato portato e che deve essere portato. Questi pacchetti cambiano
in continuazione quindi il lavoro è principalmente concentrato sui pacchetti
con molte dipendenze inverse, questi pacchetti possono essere individuati
grazie al grafico (aggiornato quotidianamente) delle dipendenze tra pacchetti
<url "https://people.debian.org/~sthibault/hurd-i386/graph-radial.pdf"> oppure usando
l'elenco dei pacchetti più ricercati
<url "https://people.debian.org/~sthibault/hurd-i386/graph-total-top.txt"> (ordinato a
partire dai pacchetti ricercati da più tempo, lo stesso elenco in ordine
inverso <url "https://people.debian.org/~sthibault/hurd-i386/graph-top.txt">).
Solitamente è una buona idea prendere un pacchetto tra quelli marcati
<q>out-of-date</q> (NdT: non aggiornati) in
<url "https://people.debian.org/~sthibault/hurd-i386/out_of_date2.txt"> o in
<url "https://people.debian.org/~sthibault/hurd-i386/out_of_date.txt"/> perché tali
pacchetti erano funzionanti ma adesso non funzionano più, probabilmente a
causa di piccoli problemi. È anche possibile prendere un pacchetto a caso
oppure sceglierne uno dopo aver controllato attentamente i log del processo
autobuilder sulla lista di messaggi debian-hurd-build-logs oppure da
<url "https://people.debian.org/~sthibault/hurd-i386/failed_packages.txt"/>.</p>

<p>
Inoltre, controllare su
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse"/>,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse"/>
e BTS che il lavoro non sia già stato fatto (<url
"https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd"/>),
<url "https://wiki.debian.org/Debian_GNU/Hurd"/> e lo stato attuale dei
pacchetti su buildd.debian.org, per esempio
<url "https://buildd.debian.org/util-linux"/>.
</p>

<p>
È più opportuno spingere affinché la correzione venga fatta direttamente in
upstream, dato che è lì che dovrà essere integrata alla fine. È meglio
discutere direttamente con loro piuttosto che passare tramite il manutentore
del pacchetto Debian. Possiamo facilmente applicare la correzione nella suite
Debian <tt>unreleased</tt> e attendere che la patch venga rilasciata da
upstream e arrivi in Debian.
</p>

<h4>Pacchetti che non devono essere portati</h4>

<p>Alcuni dei seguenti pacchetti, o delle loro parti, potrebbero essere
portabili in futuro ma almeno per ora sono considerati non-portabili.
Questi pacchetti sono marcati come NotForUs nel database buildd.</p>

<ul>
<li><code>base/makedev</code>, perché Hurd dispone della propria
versione di questo script. Il pacchetto sorgente Debian contiene
solo la versione specifica per Linux.</li>

<li><code>base/modconf</code> e <code>base/modutils</code>, perché i
moduli sono un concetto specifico di Linux.</li>

<li><code>base/netbase</code>, perché le parti fornite da questo
pacchetto non disponibili sono altamente specifiche del kernel Linux.
Al suo posto Hurd usa <code>inetutils</code>.</li>

<li><code>base/pcmcia-cs</code>, perché questo pacchetto è specifico
di Linux.</li>

<li><code>base/setserial</code>, perché è specifico per il kernel Linux.
Comunque con il port dei driver a caratteri di Linux su GNU Mach saremo
in grado di usarlo.</li>
</ul>

<h3><a name="porting_issues">Problemi generali del port</a></h3>

<p>
<a href="https://www.gnu.org/software/hurd/hurd/porting/guidelines.html">Un
elenco dei problemi più comuni</a> è disponibile sul sito web originale.
Le seguenti problematiche sono specifiche di Debian.</p>

<p>Prima di tentare di correggere qualcosa, controllare se il port kfreebsd* ha gi&agrave;
la correzione, che ha solo bisogno di essere estesa a hurd-any.</p>

<ul>
<li>
<code>foo : Depends: foo-data (= 1.2.3-1) but it is not going to be installed</code>
<p>
La risposta breve: è fallita la compilazione del pacchetto <code>foo</code>
su hurd-any e deve essere sistemata, controllare il problema nella pagina
di stato su buildd.debian.org.
</p>
<p>
Questo accade quando l'ultima compilazione del pacchetto <code>foo</code> è
fallita ma in passato è riuscita. Con <code>apt-cache policy foo foo-data</code>
è possibile scoprire, per esempio, che sono disponibili le versioni
<code>1.2.3-1</code> di <code>foo</code> e la versione <code>2.0-1</code>
di <code>foo-data</code> che è più recente. Questo accade perché su debian-ports,
i pacchetti che sono indipendenti dall'architettura (arch:all) creati dal
pacchetto sorgente <code>foo</code> (la cui compilazione crea i pacchetti
binari <code>foo</code> e <code>foo-data</code>) sono installati alla nuova
versione <code>foo-data</code> nonostante fallisca la compilazione del
pacchetto binario <code>foo</code> per hurd-any; questo porta ad avere
versioni incompatibili. La soluzione di questo problema richiede che
l'archivio debian-ports utilizzi dak al posto di mini-dak e questo passaggio
è attualmente in corso.
</p>
</li>

<li>
<code>alcuni simboli o pattern sono scomparsi dal file dei simboli</code>
<p>
Alcuni pacchetti gestiscono una lista dei simboli che ci si aspetta che
appaiano nelle librerie. Tale lista viene solitamente usata su un sistema
Linux e quindi potrebbe contenere simboli che potrebbero non avere senso
su sistemi non-Linux (per esempio per una funzionalità specifica di Linux).
È possibile inserire delle condizioni nel file <code>.symbols</code>, per
esempio:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 (arch=linux-any)linuxish_function@Base 1.23
</pre></td></tr></table>

</li>
    <li><code>Dipendenze di libc6 non funzionanti</code>

    <p>
    Alcuni pacchetti usano erroneamente una dipendenza da
    <code>libc6-dev</code>. Questo è sbagliato perché <code>libc6</code>
    è specifica di alcune delle architetture di GNU/Linux. Il pacchetto
    corrispondente per GNU è <code>libc0.3-dev</code> ma su altri Sistemi
    Operativi potrebbe essere diverso. È possibile trovare questo errore
    nel file <code>debian/control</code> del sorgente del pacchetto.
    Le soluzioni tipiche sono includere il riconoscimento del SO tramite
    <code>dpkg-architecture</code> e specificare il soname, o meglio usare
    un OR logico. Per esempio: 
    <code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.
    Il pacchetto virtuale <code>libc-dev</code>
    funziona con qualsiasi soname ma deve essere usato solo come ultima
    risorsa.</p></li>

<li>
<code>undefined reference to snd_*, SND_* undeclared</code>

<p>Alcuni pacchetti utilizzano ALSA anche sulle architetture diverse da
Linux. Il pacchetto oss-libsalsa fornisce l'emulazione via OSS ma è
limitato alla versione 1.0.5 e alcune funzionalità, per esempio
tutte le operazioni del sequencer, non sono disponibili.</p>

<p>Se il pacchetto lo permette, disabilitare il supporto ALSA sulle
architetture <code>!linux-any</code> (per esempio tramite un'opzione
<code>configure</code>) e aggiungere un qualificatore
<code>[linux-any]</code> in <code>Build-Depends</code> di alsa e
aggiungere il contrario in <code>Build-Conflicts</code>, per esempio
<code>Build-Conflicts: libasound2-dev [!linux-any]</code>.</p>
</li>

<li>
<code>dh_install: Cannot find (any matches for) "foo" (tried in ., debian/tmp)</code>
<p>
Questo accade solitamente quando non è stato installato qualcosa perché non
è stato riconosciuto il SO. Alcune volte è solo una sciocchezza (per esempio
una libreria condivisa su GNU/Hurd si crea esattamente come su GNU/Linux) che
deve essere corretta. Altre volte è più veritiero (non si installano i file
di un servizio di systemd); in questo caso è possibile usare dh-exec: impostare
una dipendenza di compilazione <tt>dh-exec</tt>, <tt>chmod +x</tt> sul file
<tt>.install</tt> e inserire in testa alle righe che danno problemi con
<tt>[linux-any]</tt> o <tt>[!hurd-any]</tt>.</p>
</li>
</ul>

<h3> <a name="debian_installer">Hacking con l'installatore Debian</a></h3>

<p>
Il modo più semplice per creare un'immagine ISO è partire da una esistente
tra quelle presenti nella <a href=hurd-cd>pagina con le immagini dei CD
Hurd</a>. Poi è possibile montarla e copiarla:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
mount debian-sid-hurd-i386-NETINST-1.iso /mnt
cp -a /mnt /tmp/myimage
umount /mnt
chmod -R +w /tmp/myimage
</pre></td></tr></table>

<p>
È possibile montare il ram disk iniziale e, per esempio, sostituire il traduttore
con la propria versione:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
gunzip /tmp/myimage/initrd.gz
mount /tmp/myimage/initrd /mnt
cp ~/hurd/rumpdisk/rumpdisk /mnt/hurd/
umount /mnt
gzip /tmp/myimage/initrd
</pre></td></tr></table>

<p>
Adesso ricreare l'immagine con grub-mkrescue:
</p>
<table><tr><td>&nbsp;</td><td class=example><pre>
rm -fr /tmp/myimage/boot/grub/i386-pc
grub-mkrescue -o /tmp/myimage.iso /tmp/myimage
</pre></td></tr></table>
