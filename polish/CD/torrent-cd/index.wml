#use wml::debian::cdimage title="Pobieranie obrazów płyt Debiana przy użyciu BitTorrenta" BARETITLE=true
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"
# oryginalny plik CD/torrent-cd/index
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<p><a href="https://pl.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
to system wymiany plików p2p zoptymalizowany dla dużej liczby pobierających.
Nie obciąża on zbytnio naszych serwerów, ponieważ użytkownicy BitTorrent'a 
wysyłają części plików do innych użytkowników podczas pobierania, powodując
zrównoważenie obciążenia sieci i umożliwiając tym samym szybkie pobieranie.
</p>

<div class="tip">
<p><strong>Pierwsza</strong> płyta CD/DVD zawiera wszystkie pliki potrzebne,
aby zainstalować standardową wersję Debiana.<br />
Aby uniknąć niepotrzebnych pobrań, prosimy <strong>nie</strong> pobierać
pozostałych obrazów płyt CD/DVD, dopóki nie przekonasz się, że potrzebujesz
zawartych na nich pakietów.</p>
</div>

<p>
Będziesz potrzebował klienta sieci BitTorrent, aby pobrać obraz płyty CD/DVD
z dystrybucją Debiana w ten sposób. Dystrybucja Debiana zawiera
<a href="https://packages.debian.org/bittornado">BitTornado</a>,
<a href="https://packages.debian.org/ktorrent">KTorrent</a> i oryginalnego 
<a href="https://packages.debian.org/bittorrent">BitTorrenta</a>.
Inne systemy operacyjne są wspierane przez <a
href="http://www.bittornado.com/download.html">BitTornado</a> i <a
href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>


<h3>Oficjalne torrenty dla wersji <q>stabilnej</q></h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Pamiętaj aby przeczytać dokumentację przed instalacją.
<strong>Podstawowe wiadomości</strong> znajdziesz w naszym
<a href="$(HOME)/releases/stable/i386/apa">Installation Howto</a>, krótkim
przewodniku po procesie instalacji. Inna przydatna dokumentacja:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a>,
    szczegółowo opisany proces instalacji</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Dokumentacja
    Debian-Installer</a>, zawierająca FAQ z najcześciej zadawanymi pytaniami
    wraz z odpowiedziami</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, lista znanych problemów związanych z instalatorem.</li>
</ul>

# <h3>Oficjalne torrenty dla wersji <q>testowej</q></h3>
# 
# <ul>
# 
#   <li><strong>CD</strong>:<br />
#   <full-cd-torrent>
#   </li>
# 
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
# 
# </ul>

<p>
W miarę możliwości pozostaw swojego klienta włączonego, aby przyspieszyć
pobieranie obrazów innym użytkownikom!
</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego
oprogramowania firmware</strong> wraz ze sterownikiem, można użyć jednego z
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archiwów zawierających pakiety z firmware</a> lub pobrać
<strong>nieoficjalny</strong> obraz zawierający <strong>niewolny</strong> firmware.
Instrukcje, jak użyć tych archiwów i podstawowe
informacje na temat ładowania firmware podczas instalacji
są zamieszczone w <a href="../../releases/stable/amd64/ch06s04">Podręczniku Instalacji</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">nieoficjalne
obrazy instalacyjne dla wersji <q>stabilnej</q> z zawartym firmwarem</a>
</p>
</div>