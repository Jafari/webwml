#use wml::debian::template title="Dokumentation" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="212af30808c438c1827b5edf929ea56c7f379f79"
# Translation update: Holger Wansing <linux@wansing-online.de>, 2015-2017.
# Translation update: Holger Wansing <hwansing@mailbox.org>, 2020, 2022, 2023.
# Translation update: Fabian Baumanis <fabian.baumanis@mailbox.org>, 2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Schnellstart</a></li>
  <li><a href="#manuals">Handbücher</a></li>
  <li><a href="#other">Weitere (kürzere) Dokumente</a></li>
</ul>

<p>Zur Erstellung eines qualitativ hochwertigen, freien Betriebssystems gehört ebenso das Schreiben technischer
   Handbücher, die den Betrieb und die Anwendung der Programme beschreiben.
   Das Debian-Projekt unternimmt jegliche erforderliche Anstrengungen, um die Benutzer mit guter Dokumentation
   in einfach zugänglicher Form zu versorgen.
   Diese Seite enthält eine Sammlung verschiedener Links, die zu Installationsanleitung, HOWTOs, FAQs,
   Veröffentlichungshinweisen, unserem Wiki und weiterem führen.
</p>


<h2><a id="quick_start">Schnellstart</a></h2>

<p>Falls Sie neu sind bei Debian, empfehlen wir Ihnen, zuerst die folgenden beiden Anleitungen zu lesen:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Installationsanleitung</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux FAQ</a></li>
</ul>

<p>Sie sollten diese bei Ihrer ersten Debian-Installation griffbereit haben,
   sie werden Ihnen wahrscheinlich viele Fragen beantworten und
   Ihnen beim Arbeiten mit dem Debian-System helfen.
</p>

<p>
Später können Sie sich durch folgende Dokumente arbeiten:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Debian-Referenz</a>: ein prägnantes Benutzerhandbuch mit Fokus
      auf die Shell (Befehlszeile)</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Veröffentlichungshinweise</a>: normalerweise herausgebracht
      im Zuge neuer Debian-Versionen; für Personen, die eine Systemaktualisierung durchführen möchten</li>
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>: das offizielle Debian-Wiki</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">Drucken Sie sich die Debian GNU/Linux-Referenzkarte aus.</a></button></p>

<h2><a id="manuals">Handbücher</a></h2>

<p>Der größte Teil der Dokumentation in Debian wurde zunächst einmal für GNU/Linux geschrieben. Es gibt aber auch Dokumentation,
   die speziell für Debian geschrieben wurde. All diese Dokumente kann man in folgende grundlegende Kategorien unterteilen:
</p>

<ul>
  <li>Anleitungen: diese gleichen Büchern, da sie bestimmte Hauptthemen umfassend beschreiben. Viele der hier aufgelisteten
      Anleitungen sind sowohl als Debian-Paket als auch online verfügbar. Tatsächlich stammen die meisten Anleitungen auf dieser
      Website aus den jeweiligen Debian-Paketen. Wählen Sie die gewünschte Anleitung im nächsten Absatz aus, um den Paketnamen
      oder Links zu Online-Versionen zu erhalten.</li>
  <li>HOWTOs: die <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">Howto-Dokumente</a> beschreiben - wie ihr Name
      schon sagt, <em>wie man etwas macht</em>, d.h. sie bieten detaillierte und praktische Anweisungen darüber, wie eine
      bestimmte Aufgabe angegangen werden sollte.</li>
  <li>FAQs: wir haben mehrere Dokumente erstellt, die <em>häufig gestellte Fragen</em> beantworten. Fragen speziell zu Debian
      werden in der <a href="manuals/debian-faq/">Debian FAQ</a> beantwortet. Es gibt außerdem eine separate
      <a href="../CD/faq/">FAQ für Debian-CD/DVD-Images</a>, die alle Themen bezüglich unserer Installationsmedien
      behandelt.</li> 
  <li>Weitere (kürzere) Dokumente: bitte schauen Sie sich auch die <a href="#other">Liste</a> von Kurzanleitungen an:</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Eine vollständige Auflistung von Debian-Handbüchern und weiterer Dokumentation
         finden Sie auf der Seite des <a href="ddp">Debian-Dokumentation-Projekts</a>. 
         Desweiteren sind einige Handbücher für Debian-GNU/Linux-Benutzer auch als <a href="books">gedruckte Bücher</a> erhältlich.</p>
</aside>

<p>Viele der hier aufgelisteten Anleitungen sind sowohl als Debian-Paket als auch online verfügbar. Tatsächlich stammen die
   meisten Anleitungen auf dieser Website aus den jeweiligen Debian-Paketen. Wählen Sie die gewünschte Anleitung im nächsten Absatz
   aus, um den Paketnamen oder Links zu Online-Versionen zu erhalten.
</p>

<h3>Handbücher speziell für Debian</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">Handbücher für Benutzer</a></h4>
    <ul>
      <li><a href="https://debian-beginners-handbook.tuxfamily.org/index-en.html">Das Debian Bookworm beginner’s handbook</a></li>
      <li><a href="user-manuals#faq">Debian GNU/Linux-FAQ</a></li>
      <li><a href="user-manuals#install">Debian Installationsanleitung</a></li>
      <li><a href="user-manuals#relnotes">Debian Veröffentlichungshinweise</a></li>
      <li><a href="user-manuals#refcard">Debian Referenzkarte</a></li>
      <li><a href="user-manuals#debian-handbook">Das Debian-Administrationshandbuch</a></li>
      <li><a href="user-manuals#quick-reference">Debian-Referenz</a></li>
      <li><a href="user-manuals#securing">Securing-Debian-Handbuch</a></li>
      <li><a href="user-manuals#aptitude">aptitude-Benutzeranleitung</a></li>
      <li><a href="user-manuals#apt-guide">APT-Benutzeranleitung</a></li>
      <li><a href="user-manuals#java-faq">Die Debian Java-FAQ</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Debian-Handbuch für Hamradio-Paketbetreuer</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Handbücher für Entwickler</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Debian Policy-Handbuch</a></li>
      <li><a href="devel-manuals#devref">Debian-Entwicklerreferenz</a></li>
      <li><a href="devel-manuals#debmake-doc">Handbuch für Debian-Paketbetreuer</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Einführung in die Debian-Paketierung</a></li>
      <li><a href="devel-manuals#menu">Debian Menü-System</a></li>
      <li><a href="devel-manuals#d-i-internals">Debian-Installer Internals</a></li>
      <li><a href="devel-manuals#dbconfig-common">Handbuch für Paketbetreuer, die Datenbanken nutzen</a></li>
      <li><a href="devel-manuals#dbapp-policy">Richtlinien für Pakete, in denen Datenbanken genutzt werden</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">Lesen Sie Debians Projekt-Geschichte</a></button></p>

<h2><a id="other">Weitere (kürzere) Dokumente</a></h2>

<p>Die folgenden Dokumente enthalten einige Kurzanleitungen:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Wenn Sie obige Quellen durchsucht und immer noch keine Antworten auf Ihre Fragen
         oder noch keine Lösungen für Ihre Debian-Probleme gefunden haben, besuchen Sie unsere <a href="../support">Unterstützungs-Seite</a>.
</p>
</aside>

  <dt><strong>Handbuchseiten</strong></dt>
    <dd>Traditionell sind alle Unix-Programme in <em>Handbuchseiten</em>
        dokumentiert, das sind Referenzhandbücher, die Sie über den <tt>man</tt>-Befehl
        erreichen können. Sie sind üblicherweise nicht für Anfänger gedacht, beschreiben
        aber alle Eigenschaften und Funktionen des jeweiligen Befehls.
        Eine vollständige Sammlung aller Handbuchseiten in Debian ist online erreichbar
        unter <a href="https://manpages.debian.org/cgi-bin/man.cgi">\
	https://manpages.debian.org/</a>
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info-Dateien</a></strong></dt>
    <dd>Viel GNU-Software wird über <em>Info-Dateien</em> anstatt über
        Handbuchseiten dokumentiert. Diese Dateien enthalten detaillierte
	Informationen über das Programm selbst, Optionen und Beispiele. Diese
        Info-Dateien sind über den <tt>info</tt>-Befehl erreichbar.
    </dd>

  <dt><strong>README-Dateien</strong></dt>
    <dd>Die <em>read me</em>-(lies mich)-Dateien sind einfache Textdateien, die einen
        einzelnen Punkt beschreiben, üblicherweise ein Paket. Sie können viele davon in den
        Verzeichnissen unterhalb von <tt>/usr/share/doc/</tt> auf Ihrem Debian-System
        finden. Zusätzlich zu den README-Dateien enthalten einige dieser Verzeichnisse
        auch Konfigurationsbeispiele. Beachten Sie, dass die Dokumentation für größere Programme
	typischerweise in einem separaten Paket bereitgestellt wird (mit dem gleichen Namen wie das
	Original-Paket, aber mit einem <em>-doc</em> angehängt).
    </dd>

</dl>
</p>
