#use wml::debian::template title="Etwas beitragen: Wie Sie Debian helfen können" MAINPAGE="true"
#use wml::debian::translation-check translation="3a5c68f115956c48aa60c6aa793eb4d802665b23"
# $Id$
# Translator: Helge Kreutzmann <debian@helgefjell.de>
# Updated: Holger Wansing <linux@wansing-online.de>, 2012 - 2018.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019, 2020.
# Translated from scratch: Holger Wansing <hwansing@mailbox.org>, 2023.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">Programmieren und Betreuen von Paketen</a></li>
    <li><a href="#testing">Testen und Fehlersuche</a></li>
    <li><a href="#documenting">Schreiben von Dokumentation und Kategorisieren von Paketen mit Debtags</a></li>
    <li><a href="#translating">Übersetzen</a></li>
    <li><a href="#usersupport">Anderen Benutzern helfen</a></li>
    <li><a href="#events">Veranstaltungen organisieren</a></li>
    <li><a href="#donations">Spenden von Geld, Hardware oder Bandbreite</a></li>
    <li><a href="#usedebian">Debian benutzen</a></li>
    <li><a href="#organizations">Wie Ihre Organisation Debian unterstützen kann</a></li>
  </ul>
</div>

<p>
Debian ist nicht nur ein Betriebssystem, es ist eine Gemeinschaft.
Viele Leute mit vielen unterschiedlichen Fähigkeiten tragen etwas zum Projekt
bei: unsere Software, die künstlerischen Arbeiten, das Wiki, und weitere
Dokumentation sind das Ergebnis von gemeinsamen Bemühungen einer großen
Gruppen von Individuen.
Nicht jeder ist ein Software-Entwickler, und natürlich müssen Sie nicht
wissen, wie man programmiert, um beim Projekt mitmachen zu können.
Es gibt viele verschiedene Möglichkeiten, wie Sie Debian helfen können,
besser zu werden. Wenn Sie bei Debian einsteigen möchten, haben wir hier
einige Vorschläge sowohl für erfahrene wie auch für unerfahrene Benutzer.
</p>

<h2><a id="coding">Programmieren und Betreuen von Paketen</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>
Vielleicht möchten Sie eine Anwendung von grundauf neu schreiben, oder
eine neue Funktionalität in ein vorhandenes Programm implementieren.
Wenn Sie ein Entwickler sind und etwas zu Debian beitragen möchten,
können Sie uns auch helfen, indem Sie Software vorbereiten, so dass
sie einfach unter Debian installiert werden kann, wir nennen das
»Paketieren«. Werfen Sie einen Blick auf die folgende Liste bezüglich
einiger Ideen, wie Sie beginnen können:
</p>

<ul>
  <li>Paketieren Sie Anwendungen, z.B. solche, mit denen Sie viel Erfahrung haben oder
      die Sie als wertvoll für Debian erachten. Für weitere Informationen, wie Sie ein
      Paketbetreuer werden, lesen Sie die Seiten der
      <a href="$(HOME)/devel/">Debian Entwickler-Ecke</a>.</li>
  <li>Helfen Sie bei der Betreuung von vorhandenen Anwendungen, z.B. indem Sie
      Fehlerkorrekturen (Patches) oder zusätzliche Informationen für Fehler in der
      <a href="https://bugs.debian.org/">Fehlerdatenbank</a> einsenden. Alternativ
      können Sie Mitglied eines Teams für bestimmte Pakete oder eines Software-Projekt
      auf <a href="https://salsa.debian.org/">Salsa</a> (unserer GitLab-Instanz)
      werden.</li>
  <li>Unterstützen Sie uns, <a href="$(HOME)/security/">Sicherheitsprobleme</a>
      in Debian <a href="https://security-tracker.debian.org/tracker/data/report">zu verfolgen</a>
      und <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">zu
      beheben</a>.</li>
  <li>Sie können uns auch dabei helfen, <a href="https://wiki.debian.org/Hardening">Pakete</a>,
      <a href="https://wiki.debian.org/Hardening/RepoAndImages">Depots und Images</a>
      sowie <a href="https://wiki.debian.org/Hardening/Goals">weitere Dinge</a>
      gegen böswillige Angriffe abzuhärten.</li>
  <li>Haben Sie Interesse, Debian auf eine Architektur
      <a href="$(HOME)/ports/">zu portieren</a>, mit der Sie Erfahrung haben?
      Sie können eine neue Portierung starten oder etwas zu einer bereits
      vorhandenen beitragen.</li>
  <li>Helfen Sie uns, Debian-bezogene <a href="https://wiki.debian.org/Services">Dienste
      zu verbessern</a> oder neue, von der Gemeinschaft vorgeschlagene oder angefragte
      Dienste <a href="https://wiki.debian.org/Services#wishlist">zu erstellen</a>.</li>
</ul>

<h2><a id="testing">Testen und Fehlersuche</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>
Wie jedes andere Software-Projekt auch, benötigt Debian Benutzer, die das
Betriebssystem und dessen Anwendungen testen. Ein Weg, hier etwas beizutragen ist,
die letzte Version zu installieren und den Entwicklern Rückmeldung zu geben, falls
etwas nicht so läuft, wie es sollte. 
Wir brauchen auch Leute, die unsere Installationsmedien, Secure Boot und den U-Boot
Bootloader auf unterschiedlicher Hardware testen.
</p>

<ul>
  <li>Sie können unsere <a href="https://bugs.debian.org/">Fehlerdatenbank</a>
      nutzen, um jegliches Problem zu berichten, das Sie in Debian finden.
      Stellen Sie aber vorher bitte sicher, dass der Fehler nicht bereits
      berichtet wurde.</li>
  <li>Besuchen Sie die Fehlerdatenbank und versuchen Sie, die Fehler durchzuschauen,
      die für die Pakete, die Sie verwenden, gemeldet wurden. Kontrollieren Sie,
      ob Sie weitere Informationen dazu bereitstellen oder den Fehler reproduzieren
      können.</li>
  <li>Testen Sie den
      <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">Debian Installer
      und die Live-ISO-Images</a>, die
      <a href="https://wiki.debian.org/SecureBoot/Testing">Secure-Boot-Unterstützung</a>,
      <a href="https://wiki.debian.org/LTS/TestSuites">LTS-Updates</a> oder den
      <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a> Bootloader.</li>
</ul>

<h2><a id="documenting">Schreiben von Dokumentation und Kategorisieren von Paketen mit Debtags</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>Falls Sie Probleme in Debian feststellen, aber keinen Programm-Code
schreiben können, um die Probleme zu beheben, wäre es vielleicht auch
eine Möglichkeit für Sie, sich Notizen zu machen und die Lösung aufzuschreiben,
wenn Sie einen Weg gefunden haben.
Auf diese Art können Sie anderen Benutzern helfen, die ähnliche Probleme
haben.
Die komplette Debian-Dokumentation wurde von Mitgliedern der Gemeinschaft
geschrieben und es gibt verschiedene Möglichkeiten, wie Sie helfen können.
</p>

<ul>
  <li>Treten Sie dem <a href="$(HOME)/doc/ddp">Debian-Dokumentations-Projekt</a>
      bei, um bei der offiziellen Debian-Dokumentation zu helfen.</li>
  <li>Beteiligen Sie sich am <a href="https://wiki.debian.org/">Debian Wiki</a>.</li>
  <li>Helfen Sie beim Kategorisieren von Paketen auf der
      <a href="https://debtags.debian.org/">Debtags</a>-Website, damit
      Debian-Benutzer auf einfachem Wege die Software finden können, die sie suchen.</li>
</ul>


<h2><a id="translating">Übersetzen</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
Ihre Muttersprache ist nicht Englisch, aber Sie verstehen es gut genug,
um Software oder Debian-betreffende Informationen wie Webseiten,
Dokumentation usw. übersetzen zu können?
Warum dann nicht einem Übersetzer-Team betreten und Debian-Anwendungen
in Ihre Sprache übersetzen?
Wir suchen auch Leute, die vorhandene Übersetzungen und diesbezügliche
Fehlerberichte kontrollieren, falls nötig.
</p>

# Translators, link directly to your group's pages
<ul>
  <li>Alles, was mit Internationalisierung in Debian zu tun hat, wird auf der
      <a href="https://lists.debian.org/debian-i18n/">i18n-Mailingliste</a> diskutiert.</li>
  <li>Sprechen Sie eine Sprache, die in Debian noch nicht unterstützt wird?
      Dann besuchen Sie die <a href="$(HOME)/international/">Debian International</a>-Seite.</li>
  <li>Das deutsche Übersetzer-Team erreichen Sie über die
      <a href="https://lists.debian.org/debian-l10n-german/">debian-l10n-german</a>-Mailingliste.</li>
  <li>Die Übersichtsseite zur deutschsprachigen Unterstützung in Debian
      finden Sie <a href="$(HOME)/international/german/">hier</a>.</li>
</ul>

<h2><a id="usersupport">Anderen Benutzern helfen</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>
Sie können auch etwas zu Debian beitragen, indem Sie andere Benutzer bei
Problemen unterstützen.
Das Projekt nutzt verschiedene Wege für die Unterstützung von Benutzern,
zum Beispiel Mailinglisten in unterschiedlichen Sprachen, oder IRC-Kanäle.
Weitere Informationen finden Sie auf unserer
<a href="$(HOME)/support">Support-Seite</a>.
</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<ul>
  <li>Das Debian-Projekt unterhält viele verschiedene
      <a href="https://lists.debian.org/">Mailinglisten</a>; einige sind für
      Entwickler gedacht und andere für Benutzer.
      Erfahrene Benutzer können anderen auf den
      <a href="$(HOME)/support#mail_lists">user-Mailinglisten</a> helfen.</li>
  <li>Die user-Mailingliste in Deutsch finden Sie
      <a href="https://lists.debian.org/debian-user-german/">hier</a>.</li>
  <li>Leute aus der ganzen Welt chatten miteinander in Echtzeit über IRC
      (Internet Relay Chat). Besuchen Sie z.B. den Kanal <tt>#debian</tt> auf
      <a href="https://www.oftc.net/">OFTC</a>, um mit anderen Debian-Benutzern
      zu chatten.</li>
  <li>Es gibt auch einen deutschsprachigen Kanal für Debian-Benutzer:
      <tt>#debian.de</tt> auf <a href="https://www.oftc.net/">OFTC</a>.</li>
</ul>

<h2><a id="events">Veranstaltungen organisieren</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>
Abseits der jährlichen Debian-Konferenz (DebConf) gibt es jedes Jahr
etliche kleinere Veranstaltungen und Treffen in verschiedenen Ländern.
An solch einer <a href="$(HOME)/events/">Veranstaltung</a> teilzunehmen
oder bei der Organisation zu helfen ist eine tolle Gelegenheit, um
andere Debian-Benutzer und -Entwickler zu treffen.
</p>

<ul>
  <li>Helfen Sie bei der jährlichen <a href="https://debconf.org/">Debian-Konferenz</a>,
      zum Beispiel durch das Aufnehmen von <a href="https://video.debconf.org/">Videos</a>
      von Diskussionsrunden und Präsentationen, das Begrüßen von Teilnehmern und Unterstützen
      von Rednern, das Organisieren von speziellen Events während der DebConf (wie der
      Cheese-and-Wine-Party), der Hilfe bei Auf- und Abbau, usw.</li>
  <li>Außerdem gibt es immer wieder
      <a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>-Events (lokale Treffen,
      die von Mitgliedern des Debian-Projekts organisiert werden).</li>
  <li>Sie können auch eine <a href="https://wiki.debian.org/LocalGroups">lokale Debian-Gruppe</a>
      in's Leben rufen oder einer beitreten, und bei regelmäßigen Treffen oder anderen
      Aktivitäten mitmachen.</li>
  <li>Achten Sie darauf, ob es andere Events in Ihrer Nähe gibt, wie die
      <a href="https://wiki.debian.org/DebianDay">Debian-Day-Parties</a>,
      <a href="https://wiki.debian.org/ReleaseParty">Release-Parties</a>,
      <a href="https://wiki.debian.org/BSP">Bug-Squashing-Parties</a>,
      <a href="https://wiki.debian.org/Sprints">Entwickler-Sprints</a>
      oder <a href="https://wiki.debian.org/DebianEvents">andere</a>.</li>
</ul>

<h2><a id="donations">Spenden von Geld, Hardware oder Bandbreite</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>
Alle Spenden an das Debian-Projekt werden von unserem Projektleiter
(DPL) verwaltet. Sie ermöglichen uns die Finanzierung von
Hardware, Domains, Verschlüsselungszertitikaten, usw. Wir nutzen auch
Fonds, um DebConf- und MiniDebConf-Events, Entwickler-Sprints, die
Präsenz auf diversen Veranstaltungen sowie weitere Dinge finanziell zu
unterstützen.
</p>

<ul>
  <li>Sie können Geld, Ausrüstung (Hardware) und Dienste an das Debian-Projekt
      <a href="$(HOME)/donations">spenden</a>.</li>
  <li>Wir suchen ständig neue <a href="$(HOME)/mirror/">Spiegel-Server</a> auf
      der ganzen Welt.</li>
  <li>Unsere Portierungen sind darauf angewiesen, dass unser
      <a href="$(HOME)/devel/buildd/">Autobuilder-Netzwerk</a> stabil funktioniert.</li>
</ul>

<h2><a id="usedebian">Debian benutzen und darüber sprechen</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>
Verbreiten Sie Ihr Wissen und erzählen Sie anderen von Debian und der
Debian-Gemeinschaft. Empfehlen Sie das Betriebssystem anderen Benutzern und
zeigen Sie Ihnen, wie man es installiert. Verwenden Sie es einfach und
genießen Sie es -- dies ist vielleicht die einfachste Möglichkeit, dem
Debian-Projekt etwas zurückzugeben.
</p>

<ul>
  <li>Werben Sie für Debian, indem Sie davon erzählen und es anderen Benutzern
      demonstrieren.</li>
  <li>Tragen Sie etwas zu unserer <a href="https://www.debian.org/devel/website/">Website</a>
      bei und helfen Sie uns, das öffentliche Erscheinungsbild von Debian zu verbessern.</li>
  <li>Nehmen Sie <a href="https://wiki.debian.org/ScreenShots">Bildschirmfotos</a> auf
      und <a href="https://screenshots.debian.net/upload">laden</a> Sie diese
      nach <a href="https://screenshots.debian.net/">screenshots.debian.net</a> hoch, so
      dass unsere Benutzer sehen können, wie Software in Debian aussieht, bevor sie sie
      verwenden.</li>
  <li>Sie können die Teilnahme am
      <a href="https://packages.debian.org/popularity-contest">Beliebtheits-Wettbewerb</a>
      aktivieren, damit wir erfahren, welche Pakete beliebt und nützlich für die Leute sind.</li>
</ul>

<h2><a id="organizations">Wie Ihre Organisation Debian unterstützen kann</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
Unabhängig davon, ob Sie für eine Organisation im Bereich Bildung, kommerzieller oder
gemeinnütziger Unternehmen oder einer Behörde tätig sind, gibt es viele Möglichkeiten,
Debian mithilfe von deren Ressourcen zu unterstützen. 
</p>

<ul>
  <li>Zum Beispiel kann Ihre Organisation einfach Geld oder Hardware
      <a href="$(HOME)/donations">spenden</a>.</li>
  <li>Vielleicht möchten Sie unsere Konferenzen
      <a href="https://www.debconf.org/sponsors/">finanziell unterstützen</a>.</li>
  <li>Ihre Organisation könnte Debian-Mitgliedern
      <a href="https://wiki.debian.org/MemberBenefits">Produkte oder Dienste</a> anbieten.</li>
  <li>Wir suchen auch nach Möglichkeiten für
      <a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">kostenloses Hosting</a>. </li>
  <li>Natürlich würden wir es auch ausdrücklich begrüßen, wenn Sie Spiegel-Server für unsere
      <a href="https://www.debian.org/mirror/ftpmirror">Software</a>,
      <a href="https://www.debian.org/CD/mirroring/">Installations-Images</a> oder
      <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">Videos unserer Konferenzen</a>
      aufsetzen könnten.</li>
  <li>Eventuell können Sie überlegen, Debian-<a href="https://www.debian.org/events/merchandise">Merchandise-Artikel</a>,
      <a href="https://www.debian.org/CD/vendors/">Installations-Medien</a> oder
      <a href="https://www.debian.org/distrib/pre-installed">vorinstallierte Systeme</a>
      zu verkaufen.</li>
  <li>Wenn Ihre Organisation für Debian
      <a href="https://www.debian.org/consultants/">Beratung (Consulting)</a> oder
      <a href="https://wiki.debian.org/DebianHosting">Hosting</a> anbietet, lassen Sie es uns wissen.</li>
</ul>

<p>
Wir sind auch daran interessiert, <a href="https://www.debian.org/partners/">Partnerschaften</a>
einzugehen.
Wenn Sie für Debian werben könnten, indem Sie einen
<a href="https://www.debian.org/users/">Erfahrungsbericht verfassen</a>,
es auf den Servern oder Arbeitsplatzrechnern Ihrer Organisation laufen lassen,
oder Ihre Angestellten ermuntern, sich im Debian-Projekt einzubringen,
wäre das fantastisch.
Vielleicht könnten Sie sogar Schulungen über das Debian-Betriebssystem
und dessen Gemeinschaft abhalten, Ihren Mitarbeitern erlauben, während
der Arbeitszeit für Debian zu arbeiten, oder sie zu einer unserer
<a href="$(HOME)/events/">Veranstaltungen</a> schicken.</p>

