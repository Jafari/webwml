#use wml::debian::template title="Weltkarte der Entwickler" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="bd2e76d96db915ccd0dee367a373417db7422565"
# $Id$
# Translator: Alexander Reiter <leckse@tm1.at>
# Updated: Holger Wansing <hwansing@mailbox.org>, 2023.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Wo sind Debian-Entwickler ansässig? Wenn
         ein Entwickler die Koordinaten seines Wohnorts in der Entwicklerdatenbank eingegeben
         hat, wird der Ort auf unserer Weltkarte angezeigt.</p>
</aside>

<p>
Die untenstehende Karte wurde basierend auf einer anonymisierten <a
href="developers.coords">Liste der Entwicklerkoordinaten</a> unter Zuhilfenahme
von <a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a> erzeugt.
</p>

<img src="developers.map.jpeg" alt="World Map">

<h2>Wie Sie Ihre Koordinaten hinzufügen</h2>

<p>
Wenn Sie Ihre Koordinaten der Datenbank hinzufügen möchten, loggen Sie sich in die
<a href="https://db.debian.org">Debian-Entwicklerdatenbank</a> ein und bearbeiten
Sie Ihren Eintrag. Falls Sie die Koordinaten Ihres Wohnort nicht kennen, können
Sie <a href="https://www.openstreetmap.org/">OpenStreetMap</a> verwenden, um sie
herauszufinden. Suchen Sie nach Ihrer Stadt und wählen Sie die Richtungspfeile
neben der Suchleiste (Routenberechnung). Ziehen Sie jetzt den grünen Marker auf
die OSM-Karte. Die Koordinaten erscheinen in der <em>Von</em>-Zeile.
</p>

<p>Das Format für die Koordinaten sollte wie eines der folgenden aussehen:</p>

<dl>
  <dt>Dezimalgrad</dt>
    <dd>Das Format ist <code>+-DDD.DDDDDDDDDDDDDDD</code>. Programme wie xearth
        und viele Websites benutzen es. Die Genauigkeit auf 4 bis 5
        Nachkommastellen beschränkt.</dd>
  <dt>Grad-Minuten (DGM)</dt>
    <dd>Das Format ist <code>+-DDDMM.MMMMMMMMMMMMM</code>. Das ist kein
        arithmetischer Typ, sondern eine kombinierte Anzeige von zwei
        verschiedenen Angaben: Grad und Minuten. Einige tragbare GPS- und
        NMEA-Geräte geben so etwas üblicherweise aus.</dd>
  <dt>Grad-Minuten-Sekunden (DGMS)</dt>
    <dd>Das Format ist <code>+-DDDMMSS.SSSSSSSSSSS</code>. Wie DGM auch ist
        dies kein arithmetischer Typ, sondern eine kombinierte Anzeige
        von drei verschiedenen Angaben: Grad, Minuten und Sekunden.
        Das Format entwickelte sich über Websites, die 3 separate Werte für
        jede Position angaben. Wenn zum Beispiel
        <code>34:50:12.24523 North</code> die gegebene Position ist,
        wäre das im DGMS-Format <code>+0345012.24523</code>.</dd>
</dl>

<p>
<strong>Bitte beachten:</strong> <code>+</code> beim Breitengrad ist Norden,
<code>+</code> beim Längengrad ist Osten. Es ist wichtig, genügend führende
Nullen anzugeben, damit das Format eindeutig bleibt, falls Ihre Position
weniger als 2 Grad vom Nullpunkt abweicht.
</p>
