# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
# Parodper <parodper@gmail.com>, 2022
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-13 09:42+0200\n"
"Last-Translator: Parodper <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0.1\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "no paquete"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "etiquetado"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "con severidade"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "no paquete de fontes"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "en paquetes mantidos por"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "enviados por"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "pertencentes a"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "co estado"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "con correos de"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "informes de erro máis recentes"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "cun título que contén"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "co estado de pendente"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "que contén no remitente"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "que contén en reenviado"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "que contén no propietario"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "co paquete"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "vista antiga"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "en bruto"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "antigüidade"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Repetir xuntanza"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Informes de fallos inversos"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Pendentes inversos"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Severidade inversa"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Evitar informes de fallo que afecten aos paquetes"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Ningún"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testing"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Sen arquivar"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Arquivados"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Arquivados e sen arquivar"
