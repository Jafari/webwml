#use wml::debian::ddp title="SCV (Sistema de Controle de Versão) do projeto de documentação do Debian"
#use wml::debian::toc
#use wml::debian::translation-check translation="e6e987deba52309df5b062b1567c8f952d7bd476"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#access">Acessando os arquivos-fonte no git</a></li>
  <li><a href="#obtaining">Obtendo privilégios de envio</a></li>
  <li><a href="#updates">Mecanismo de atualização automática</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> O <a href="ddp">projeto de
documentação Debian</a> armazena suas páginas web e a maioria dos manuais no
<a href ="https://salsa.debian.org">salsa</a>, instância do GitLab do Debian.
Todas as pessoas podem baixar os arquivos-fonte do serviço salsa, mas apenas
os(as) membros(as) do DDP têm acesso de escrita e podem atualizar os arquivos.</p>
</aside>

<h2><a id="access">Acessando os arquivos-fonte no git</a></h2>

<p>
Você pode usar uma interface web para acessar os arquivos individualmente e ver
as alterações de cada projeto em
<url "https://salsa.debian.org/ddp-team/" />
</p>

<p>
O projeto de documentação Debian armazena todo o conteúdo no salsa, a instância
GitLab do Debian. Para acessar arquivos individuais, verificar mudanças recentes
e atividades do projeto em geral, por favor visite o
<a href="https://salsa.debian.org/ddp-team/">repositório do DDP</a>.
</p>

<p>
Se deseja baixar manuais inteiros de lá, o acesso direto ao salsa provavelmente
é a melhor opção. As próximas seções explicam como clonar um repositório git
(modo somente leitura e modo leitura-gravação) em sua máquina local e como
atualizar sua cópia local. Antes de começar, instale o pacote
<tt><a href="https://packages.debian.org/git">git</a></tt> na sua máquina.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span><a href="https://wiki.debian.org/Salsa">Leia os documentos do salsa</a></button></p>

<h3>Clonando um repositório git anonimamente (modo apenas leitura)</h3>

<p>
Use este comando para baixar todos os arquivos de um projeto:
</p>

<pre>
  git clone https://salsa.debian.org/ddp-team/release-notes.git
</pre>

<p>
Faça o mesmo para cada projeto que deseja baixar para o seu computador.
<strong>Dica:</strong> para encontrar a URL correta para o comando
<code>git clone</code>, abra o projeto em um navegador web, clique no botão azul
<em>Clone</em> e copie a URL <em>Clone with HTTPS</em> (Clonar com HTTPS) para
sua área de transferência.
</p>

<h3>Clonando um repositório git com privilégios de envio (modo leitura e escrita)</h3>

<p>
Antes de poder acessar o servidor git usando esse método, você deve primeiro
obter acesso de escrita no repositório. Por favor consulte
<a href="#obtaining">esta seção</a> para obter mais informações sobre como
solicitar permissão de escrita.
</p>

<p>
Com acesso de gravação ao salsa, você pode usar o seguinte comando para
baixar todos os arquivos de um projeto:
</p>

<pre>
git clone git@salsa.debian.org:ddp-team/release-notes.git
</pre>

<p>
Faça o mesmo para todos os projetos que você deseja clonar localmente.
</p>

<h3>Buscando alterações do repositório git remoto</h3>

<p>
Para atualizar sua cópia local com quaisquer alterações feitas por outras
pessoas, entre no subdiretório do respectivo manual e execute este comando:

<pre>
git pull
</pre>

<toc-add-entry name="obtaining">Obtendo privilégios de envio</toc-add-entry>

<p>
Os privilégios de envio estão disponíveis para qualquer pessoa que deseje
participar da redação de manuais, FAQs, HOWTOs, etc. Geralmente solicitamos que
você primeiro envie alguns patches úteis. Depois disso, siga estas etapas
para solicitar acesso de gravação:
</p>

<ol>
   <li>Crie uma conta no <a href="https://salsa.debian.org/">salsa</a> se ainda
   não o fez.</li>
   <li>Vá para o <a href="https://salsa.debian.org/ddp-team/">repositório do DDP</a>
   e clique em <em>Request Access</em> (Solicitar acesso).</li>
   <li>Envie um e-mail (em inglês) para
   <a href="mailto:debian-doc@lists.debian.org">debian-doc@lists.debian.org</a>
   e conte-nos como você tem contribuído para o projeto Debian.</li>
   <li>Assim que sua solicitação for aprovada, você fará parte da equipe do
   DDP.</li>
</ol>

<h2><a id="updates">Mecanismo de atualização automática</a></h2>

<p>
Todos os manuais são publicados como páginas web. Elas são geradas
automaticamente em www-master.debian.org como parte do processo regular de
reconstrução do site web, que acontece a cada quatro horas. Durante esse
processo, as versões mais recentes do pacote são baixadas do repositório, cada
manual é reconstruído e todos os arquivos são transferidos para o subdiretório
<code>doc/manuals/</code> do site web.
</p>

<p>
Os arquivos de documentação gerados pelo script de atualização podem ser
encontrados em <a href="manuals/">https://www.debian.org/doc/manuals/</a>.</p>

<p>
Os arquivos de log gerados pelo processo de atualização podem ser encontrados
em <url "https://www-master.debian.org/build-logs/webwml/" /> (o script é
denominado <code>7doc</code> e está sendo executado <code>frequentemente</code>
como parte do trabalho do cron).
</p>

# <p>Observe que esse processo gera novamente o diretório
# <code>/doc/manuals/</code>. O conteúdo do diretório <code>/doc/</code> é
# gerado a partir do <a href="/devel/website/desc">webwml</a> ou de outros
# scripts, como os que extraem certos manuais de seus pacotes.</p>
