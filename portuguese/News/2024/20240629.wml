#use wml::debian::translation-check translation="e3699f036461e1416232bc8af1a6f9a475163598"
<define-tag pagetitle>Atualização Debian 12: 12.6 lançado</define-tag>
<define-tag release_date>2024-06-29</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a sexta atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas mais sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
necessário.</p>

<p>Por favor, note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir de
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>


<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções gerais de bugs</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction aide "Fix concurrent reading of extended attributes">
<correction amavisd-new "Handle multiple boundary parameters that contain conflicting values [CVE-2024-28054]; fix race condition in postinst">
<correction archlinux-keyring "Switch to pre-built keyrings; sync with upstream">
<correction base-files "Update for the 12.6 point release">
<correction bash "Rebuild to fix outdated Built-Using">
<correction bioawk "Disable parallel builds to fix random failures">
<correction bluez "Fix remote code execution issues [CVE-2023-27349 CVE-2023-50229 CVE-2023-50230]">
<correction cdo "Disable hirlam-extensions to avoid causing issues with ICON data files">
<correction chkrootkit "Rebuild to fix outdated Built-Using">
<correction cjson "Fix missing NULL checks [CVE-2023-50471 CVE-2023-50472]">
<correction clamav "New upstream stable release; fix possible heap overflow issue [CVE-2024-20290], possible command injection issue [CVE-2024-20328]">
<correction cloud-init "Declare conflicts/replaces on versioned package introduced for bullseye">
<correction comitup "Ensure service is unmasked in post install">
<correction cpu "Provide exactly one definition of globalLdap in LDAP plugin">
<correction crmsh "Create log directory and file on installation">
<correction crowdsec-custom-bouncer "Rebuild to fix outdated Built-Using">
<correction crowdsec-firewall-bouncer "Rebuild against golang-github-google-nftables version with fixed little-endian architecture support">
<correction curl "Do not keep default protocols when deselected [CVE-2024-2004]; fix memory leak [CVE-2024-2398]">
<correction dar "Rebuild to fix outdated Built-Using">
<correction dcmtk "Clean up properly on purge">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-22; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debvm "debvm-create: do install login; bin/debvm-waitssh: make --timeout=N work; bin/debvm-run: allow being run in environments without TERM set; fix resolv.conf in stretch">
<correction dhcpcd5 "privsep: Allow zero length messages through; fix server not being restarted correctly during upgrades">
<correction distro-info-data "Declare intentions for bullseye/bookworm; fix past data; add Ubuntu 24.10">
<correction djangorestframework "Reinstate missing static files">
<correction dm-writeboost "Fix build error with 6.9 kernel and backports">
<correction dns-root-data "Update root hints; update expired security information">
<correction dpdk "New upstream stable release">
<correction ebook-speaker "Support username over 8 characters when enumerating groups">
<correction emacs "Security fixes [CVE-2024-30202 CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]; replace expired package-keyring.gpg with a current version">
<correction extrepo-data "Update repository information">
<correction flatpak "New upstream stable release">
<correction fpga-icestorm "Restore compatibility with yosys">
<correction freetype "Disable COLRv1 support, which was unintentionally enabled by upstream; fix function existence check when calling get_colr_glyph_paint()">
<correction galera-4 "New upstream bugfix release; update upstream release signing key; prevent date-related test failures">
<correction gdk-pixbuf "ANI: Reject files with multiple anih chunks [CVE-2022-48622]; ANI: Reject files with multiple INAM or IART chunks; ANI: Validate anih chunk size">
<correction glewlwyd "Fix potential buffer overflow during FIDO2 credential validation [CVE-2023-49208]; fix open redirection via redirect_uri [CVE-2024-25715]">
<correction glib2.0 "Fix a (rare) memory leak">
<correction glibc "Revert fix to always call destructors in reverse constructor order due to unforeseen application compatibility issues; fix a DTV corruption due to a reuse of a TLS module ID following dlclose with unused TLS">
<correction gnutls28 "Fix certtool crash when verifying a certificate chain with more than 16 certificates [CVE-2024-28835]; fix side-channel in the deterministic ECDSA [CVE-2024-28834]; fix a memory leak; fix two segfault issues">
<correction golang-github-containers-storage "Rebuild for outdated Built-Using">
<correction golang-github-google-nftables "Fix AddSet() function on little-endian architectures">
<correction golang-github-openshift-imagebuilder "Rebuild for outdated Built-Using">
<correction gosu "Rebuild for outdated Built-Using">
<correction gpaste "Fix conflict with older libpgpaste6">
<correction gross "Fix stack-based buffer overflow [CVE-2023-52159]">
<correction hovercraft "Depend on python3-setuptools">
<correction icinga2 "Fix segmentation fault on ppc64el">
<correction igtf-policy-bundle "Address CAB Forum S/MIME policy change; apply accumulated updates to trust anchors">
<correction intel-microcode "Security mitigations [CVE-2023-22655 CVE-2023-28746 CVE-2023-38575 CVE-2023-39368 CVE-2023-43490]; mitigate for INTEL-SA-01051 [CVE-2023-45733], INTEL-SA-01052 [CVE-2023-46103], INTEL-SA-01036 [CVE-2023-45745,  CVE-2023-47855] and unspecified functional issues on various Intel processors">
<correction jose "Fix potential denial-of-service issue [CVE-2023-50967]">
<correction json-smart "Fix excessive recursion leading to stack overflow [CVE-2023-1370]; fix denial of service via crafted request [CVE-2021-31684]">
<correction kio "Fix file loss and potential locking issues on CIFS">
<correction lacme "Fix post-issuance validation logic">
<correction libapache2-mod-auth-openidc "Fix mising input validation leading to DoS [CVE-2024-24814]">
<correction libesmtp "Break and replace older library versions">
<correction libimage-imlib2-perl "Fix package build">
<correction libjwt "Fix timing side channel attack [CVE-2024-25189]">
<correction libkf5ksieve "Prevent leaking passwords into server-side logs">
<correction libmail-dkim-perl "Add dependency on libgetopt-long-descriptive-perl">
<correction libpod "Handle removed containers properly">
<correction libreoffice "Fix backup copy creation for files on mounted samba shares; don't remove libforuilo.so in -core-nogui">
<correction libseccomp "Add support for syscalls up to Linux 6.7">
<correction libtommath "Fix integer overflow [CVE-2023-36328]">
<correction libtool "Conflict with libltdl3-dev; fix check for += operator in func_append">
<correction libxml-stream-perl "Fix compatibility with IO::Socket::SSL &gt;= 2.078">
<correction linux "New upstream stable release; increase ABI to 22">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 22">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 22">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 22">
<correction lua5.4 "debian/version-script: Export additional missing symbols for lua 5.4.4">
<correction lxc-templates "Fix the <q>mirror</q> option of lxc-debian">
<correction mailman3 "Depend alternatively on cron-daemon; fix postgresql:// url in post-installation script">
<correction mksh "Handle merged /usr in /etc/shells; fix crash with nested bashism; fix arguments to the dot command; distinguish unset and empty in `typeset -p`">
<correction mobian-keyring "Update Mobian archive key">
<correction ms-gsl "Mark not_null constructors as noexcept">
<correction nano "Fix format string issues; fix <q>with --cutfromcursor, undoing a justification can eat a line</q>; fix malicious symlink issue; fix example bindings in nanorc">
<correction netcfg "Handle routing for single-address netmasks">
<correction ngircd "Respect <q>SSLConnect</q> option for incoming connections; server certificate validation on server links (S2S-TLS); METADATA: Fix unsetting <q>cloakhost</q>">
<correction node-babel7 "Fix building against nodejs 18.19.0+dfsg-6~deb12u1; add Breaks/Replaces against obsolete node-babel-* packages">
<correction node-undici "Properly export typescript types">
<correction node-v8-compile-cache "Fix tests when a newer nodejs version is used">
<correction node-zx "Fix flaky test">
<correction nodejs "Skip flaky tests for mipsel/mips64el">
<correction nsis "Don't allow unprivileged users to delete the uninstaller directory [CVE-2023-37378]; fix regression in disabling stub relocations; build reproducibly for arm64">
<correction nvidia-graphics-drivers "Restore compatibility with newer Linux kernel builds; take over packages from nvidia-graphics-drivers-tesla; add new nvidia-suspend-common package; relax dh-dkms build-dependency for compatibility with bookworm; new upstream stable release [CVE-2023-0180 CVE-2023-0183 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199 CVE-2023-25515 CVE-2023-25516 CVE-2023-31022 CVE-2024-0074 CVE-2024-0075 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-graphics-drivers-tesla "Restore compatibility with newer Linux kernel builds">
<correction nvidia-graphics-drivers-tesla-470 "Restore compatibility with newer Linux kernel builds; stop building nvidia-cuda-mps; new upstream stable release; security fixes [CVE-2022-42265 CVE-2024-0074 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-modprobe "Prepare to switch to 535 series LTS drivers">
<correction nvidia-open-gpu-kernel-modules "Update to 535 series LTS drivers [CVE-2023-0180 CVE-2023-0183 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199 CVE-2023-25515 CVE-2023-25516 CVE-2023-31022 CVE-2024-0074 CVE-2024-0075 CVE-2024-0078 CVE-2024-0090 CVE-2024-0092]">
<correction nvidia-persistenced "Switch to 535 series LTS drivers; update list of supported drivers">
<correction nvidia-settings "Also build for ppc64el; new upstream LTS release">
<correction nvidia-xconfig "New upstream LTS release">
<correction openrc "Ignore non-executable scripts in /etc/init.d">
<correction openssl "New upstream stable release; fix excessive time taken issues [CVE-2023-5678 CVE-2023-6237], vector register corruption issue on PowerPC [CVE-2023-6129], PKCS12 Decoding crashes [CVE-2024-0727]">
<correction openvpn-dco-dkms "Build for Linux &gt;= 6.5; install compat-include directory; fix refcount imbalance">
<correction orthanc-dicomweb "Rebuild to fix outdated Built-Using">
<correction orthanc-gdcm "Rebuild to fix outdated Built-Using">
<correction orthanc-mysql "Rebuild to fix outdated Built-Using">
<correction orthanc-neuro "Rebuild to fix outdated Built-Using">
<correction orthanc-postgresql "Rebuild to fix outdated Built-Using">
<correction orthanc-python "Rebuild to fix outdated Built-Using">
<correction orthanc-webviewer "Rebuild to fix outdated Built-Using">
<correction orthanc-wsi "Rebuild to fix outdated Built-Using">
<correction ovn "New upstream stable version; fix insufficient validation of incoming BFD packets [CVE-2024-2182]">
<correction pdudaemon "Depend on python3-aiohttp">
<correction php-composer-class-map-generator "Force system dependency loading">
<correction php-composer-pcre "Add missing Breaks+Replaces: on composer (&lt;&lt; 2.2)">
<correction php-composer-xdebug-handler "Force system dependency loading">
<correction php-doctrine-annotations "Force system dependency loading">
<correction php-doctrine-deprecations "Force system dependency loading">
<correction php-doctrine-lexer "Force system dependency loading">
<correction php-phpseclib "Guard isPrime() and randomPrime() for BigInteger [CVE-2024-27354]; limit OID length in ASN1 [CVE-2024-27355]; fix BigInteger getLength(); remove visibitility modifiers from static variables">
<correction php-phpseclib3 "Force system dependency loading; guard isPrime() and randomPrime() for BigInteger [CVE-2024-27354]; limit OID length in ASN1 [CVE-2024-27355]; fix BigInteger getLength()">
<correction php-proxy-manager "Force system dependency loading">
<correction php-symfony-contracts "Force system dependency loading">
<correction php-zend-code "Force system dependency loading">
<correction phpldapadmin "Fix compatbility with PHP 8.1+">
<correction phpseclib "Force system dependency loading; guard isPrime() and randomPrime() for BigInteger [CVE-2024-27354]; limit OID length in ASN1 [CVE-2024-27355]; fix BigInteger getLength()">
<correction postfix "New upstream stable release">
<correction postgresql-15 "New upstream stable release; restrict visibility of pg_stats_ext and pg_stats_ext_exprs entries to the table owner [CVE-2024-4317]">
<correction prometheus-node-exporter-collectors "Do not adversely affect mirror network; fix deadlock with other apt update runs">
<correction pymongo "Fix out-of-bounds read issue [CVE-2024-5629]">
<correction pypy3 "Strip C0 control and space characters in urlsplit [CVE-2023-24329]; avoid bypass of TLS handshake protections on closed sockets [CVE-2023-40217]; tempfile.TemporaryDirectory: fix symlink bug in cleanup [CVE-2023-6597]; protect zipfile from <q>quoted-overlap</q> zipbomb [CVE-2024-0450]">
<correction python-aiosmtpd "Fix SMTP smuggling issue [CVE-2024-27305]; fix STARTTLS unencrypted command injection issue [CVE-2024-34083]">
<correction python-asdf "Remove unnecessary dependency on asdf-unit-schemas">
<correction python-channels-redis "Ensure pools are closed on loop close in core">
<correction python-idna "Fix denial of service issue [CVE-2024-3651]">
<correction python-jwcrypto "Fix denial of service issue [CVE-2024-28102]">
<correction python-xapian-haystack "Drop dependency on django.utils.six">
<correction python3.11 "Fix use-after-free crash when deallocating a frame object; protect zipfile from <q>quoted-overlap</q> zipbomb [CVE-2024-0450]; tempfile.TemporaryDirectory: fix symlink bug in cleanup [CVE-2023-6597]; fix <q>os.path.normpath(): Path truncation at null bytes</q> [CVE-2023-41105]; avoid bypass of TLS handshake protections on closed sockets [CVE-2023-40217]; strip C0 control and space characters in urlsplit [CVE-2023-24329]; avoid a potential null pointer dereference in filleutils">
<correction qemu "New upstream stable release; security fixes [CVE-2024-26327 CVE-2024-26328 CVE-2024-3446 CVE-2024-3447]">
<correction qtbase-opensource-src "Fix regression in patch for CVE-2023-24607; avoid using system CA certificates when not wanted [CVE-2023-34410]; fix buffer overflow [CVE-2023-37369]; fix infinite loop in XML recursive entity expansion [CVE-2023-38197]; fix buffer overflow with crafted KTX image file [CVE-2024-25580]; fix HPack integer overflow check [CVE-2023-51714]">
<correction rails "Declare breaks and replaces on obsolete ruby-arel package">
<correction riseup-vpn "Use system certificate bundle by default, restoring ability to connect to an endpoint using LetsEncrypt certificate">
<correction ruby-aws-partitions "Ensure binary package includes partitions.json and partitions-metadata.json files">
<correction ruby-premailer-rails "Remove build-dependency on obsolete ruby-arel">
<correction rust-cbindgen-web "New source package to support builds of newer Firefox ESR versions">
<correction rustc-web "New source package to support builds of web browsers">
<correction schleuder "Fix argument parsing insufficient validation; fix importing keys from attachments sent by Thunderbird and handle mails without further content; look for keywords only at the start of mail; validate downcased email addresses when checking subscribers; consider From header for finding reply addresses">
<correction sendmail "Fix SMTP smuggling issue [CVE-2023-51765]">
<correction skeema "Rebuild for outdated Built-Using">
<correction skopeo "Rebuild for outdated Built-Using">
<correction software-properties "software-properties-qt: Add Conflicts+Replaces: on software-properties-kde for smoother upgrades from bullseye">
<correction supermin "Rebuild to fix outdated Built-Using">
<correction symfony "Force system dependency loading; DateTypTest: ensure submitted year is accepted choice">
<correction systemd "New upstream stable release; fix denial of service issues [CVE-2023-50387 CVE-2023-50868]; libnss-myhostname.nss: Install after <q>files</q>; libnss-mymachines.nss: Install before <q>resolve</q> and <q>dns</q>">
<correction termshark "Rebuild to fix outdated Built-Using">
<correction tripwire "Rebuild to fix outdated Built-Using">
<correction tryton-client "Only send compressed content in authenticated sessions">
<correction tryton-server "Prevent <q>zip-bomb</q> attacks from unauthenticated sources">
<correction u-boot "Fix orion-timer for booting sheevaplug and related platforms">
<correction uif "Support VLAN interface names">
<correction umoci "Rebuild for outdated Built-Using">
<correction user-mode-linux "Rebuilt to fix outdated Built-Using">
<correction wayfire "Add missing dependencies">
<correction what-is-python "Declare breaks and replaces on python-dev-is-python2; fix version mangling in build rules">
<correction wpa "Fix authentication bypass issue [CVE-2023-52160]">
<correction xscreensaver "Disable warning about old versions">
<correction yapet "Do not call EVP_CIPHER_CTX_set_key_length() in crypt/blowfish and crypt/aes">
<correction zsh "Rebuild to fix outdated Built-Using">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2023 5575 webkit2gtk>
<dsa 2023 5580 webkit2gtk>
<dsa 2023 5589 nodejs>
<dsa 2024 5609 slurm-wlm-contrib>
<dsa 2024 5616 ruby-sanitize>
<dsa 2024 5618 webkit2gtk>
<dsa 2024 5619 libgit2>
<dsa 2024 5620 unbound>
<dsa 2024 5621 bind9>
<dsa 2024 5623 postgresql-15>
<dsa 2024 5624 edk2>
<dsa 2024 5625 engrampa>
<dsa 2024 5626 pdns-recursor>
<dsa 2024 5627 firefox-esr>
<dsa 2024 5628 imagemagick>
<dsa 2024 5630 thunderbird>
<dsa 2024 5631 iwd>
<dsa 2024 5632 composer>
<dsa 2024 5633 knot-resolver>
<dsa 2024 5635 yard>
<dsa 2024 5637 squid>
<dsa 2024 5638 libuv1>
<dsa 2024 5640 openvswitch>
<dsa 2024 5641 fontforge>
<dsa 2024 5642 php-dompdf-svg-lib>
<dsa 2024 5643 firefox-esr>
<dsa 2024 5644 thunderbird>
<dsa 2024 5645 firefox-esr>
<dsa 2024 5646 cacti>
<dsa 2024 5650 util-linux>
<dsa 2024 5651 mediawiki>
<dsa 2024 5653 gtkwave>
<dsa 2024 5655 cockpit>
<dsa 2024 5657 xorg-server>
<dsa 2024 5658 linux-signed-amd64>
<dsa 2024 5658 linux-signed-arm64>
<dsa 2024 5658 linux-signed-i386>
<dsa 2024 5658 linux>
<dsa 2024 5659 trafficserver>
<dsa 2024 5661 php8.2>
<dsa 2024 5662 apache2>
<dsa 2024 5663 firefox-esr>
<dsa 2024 5664 jetty9>
<dsa 2024 5665 tomcat10>
<dsa 2024 5666 flatpak>
<dsa 2024 5669 guix>
<dsa 2024 5670 thunderbird>
<dsa 2024 5672 openjdk-17>
<dsa 2024 5673 glibc>
<dsa 2024 5674 pdns-recursor>
<dsa 2024 5677 ruby3.1>
<dsa 2024 5678 glibc>
<dsa 2024 5679 less>
<dsa 2024 5680 linux-signed-amd64>
<dsa 2024 5680 linux-signed-arm64>
<dsa 2024 5680 linux-signed-i386>
<dsa 2024 5680 linux>
<dsa 2024 5682 glib2.0>
<dsa 2024 5682 gnome-shell>
<dsa 2024 5684 webkit2gtk>
<dsa 2024 5685 wordpress>
<dsa 2024 5686 dav1d>
<dsa 2024 5688 atril>
<dsa 2024 5690 libreoffice>
<dsa 2024 5691 firefox-esr>
<dsa 2024 5692 ghostscript>
<dsa 2024 5693 thunderbird>
<dsa 2024 5695 webkit2gtk>
<dsa 2024 5698 ruby-rack>
<dsa 2024 5699 redmine>
<dsa 2024 5700 python-pymysql>
<dsa 2024 5702 gst-plugins-base1.0>
<dsa 2024 5704 pillow>
<dsa 2024 5705 tinyproxy>
<dsa 2024 5706 libarchive>
<dsa 2024 5707 vlc>
<dsa 2024 5708 cyrus-imapd>
<dsa 2024 5709 firefox-esr>
<dsa 2024 5711 thunderbird>
<dsa 2024 5712 ffmpeg>
<dsa 2024 5713 libndp>
<dsa 2024 5714 roundcube>
<dsa 2024 5715 composer>
<dsa 2024 5717 php8.2>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos por circunstâncias fora de nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction phppgadmin "Security issues; incompatible with bookworm's PostgreSQL version">
<correction pytest-salt-factories "Only needed for salt, which is not part of bookworm">
<correction ruby-arel "Obsolete, integrated into ruby-activerecord, incompatible with ruby-activerecord 6.1.x">
<correction spip "Incompatible with bookworm's PHP version">
<correction vasttrafik-cli "API withdrawn">

</table>

<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da versão estável (stable) em
&lt;debian-release@lists.debian.org&gt;.</p>
