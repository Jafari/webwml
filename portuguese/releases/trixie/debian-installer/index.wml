#use wml::debian::template title="Debian &ldquo;trixie&rdquo; Informações de instalação" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#use wml::debian::translation-check translation="aa916b5ed417b0116ff9fa62a2607e688980d881"

<h1>Instalando o Debian <current_release_trixie></h1>

<if-stable-release release="forky">
<p><strong>O Debian 13 foi substituído pelo
<a href="../../forky/">Debian 14 (<q>forky</q>)</a>. Algumas dessas
imagens de instalação talvez não estejam mais disponíveis, ou talvez não
funcionem mais, e recomenda-se que você instale o forky em vez delas.
</strong></p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>
Para imagens de instalação e documentação sobre como instalar o <q>trixie</q>
(que está atualmente na teste (testing)), veja a
<a href="$(HOME)/devel/debian-installer/">página do Debian-Installer</a>.
</if-stable-release>

<if-stable-release release="trixie">
<p>
<strong>Para instalar o Debian</strong> <current_release_trixie>
(<em>trixie</em>), faça o download de qualquer uma das seguintes imagens
(todas as imagens de CD/DVD i386 e amd64 também podem ser usadas em pendrives):
</p>

<div class="line">
<div class="item col50">
	<p><strong>imagem de CD netinst (geralmente 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>conjuntos completos de CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>conjuntos completos de DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>outras imagens (netboot, pendrive usb, etc.)</strong></p>
<other-images />
</div>
</div>

<p>
<strong>Notas</strong>
</p>
<ul>
    <li>
	Para baixar imagens completas de CD e DVD, recomenda-se usar
	o BitTorrent ou o jigdo.
    </li><li>
	Para arquiteturas pouco comuns, somente um número limitado de imagens
	dos conjuntos de CD e DVD está disponível como um arquivo ISO ou via
	BitTorrent. Os conjuntos completos estão disponíveis somente via jigdo.
    </li><li>
	As imagens multiarquitetura em <em>CD</em> suportam i386/amd64; a
	instalação é similar à instalação de uma imagem netinst de uma única
        arquitetura.
    </li><li>
	As imagens multiarquitetura em <em>DVD</em> suportam i386/amd64; a
	instalação é similar à instalação de uma imagem de CD de uma única
        arquitetura; o DVD também inclui o código-fonte de todos os pacotes
        inclusos.
    </li><li>
	Para a instalação de imagens, arquivos de verificação
	(<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt> e outros) estão disponíveis no
        mesmo diretório das imagens.
    </li>
</ul>


<h1>Documentação</h1>

<p>
<strong>Se você somente lê um documento</strong> antes da instalação, leia nosso
<a href="../i386/apa">Howto de instalação</a>, um rápido
passo a passo do processo de instalação. Outras documentações úteis incluem:
</p>

<ul>
<li><a href="../installmanual">Guia de instalação do Bookworm</a><br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do Debian-Installer</a>
and <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
perguntas e respostas comuns</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki do Debian-Installer</a><br />
documentação mantida pela comunidade</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Esta é uma lista com problemas conhecidos no instalador enviado com o
Debian <current_release_trixie>. Se você experimentou um problema
ao instalar o Debian e não vê seu problema listado aqui, por favor, nos envie um
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">relatório de instalação</a>
descrevendo o problema, ou
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">verifique o wiki</a>
para outros problemas conhecidos.
</p>

## Tradutores: copiem/colem de devel/debian-installer/errata
<h3 id="errata-r0">Errata para a versão 13.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Versões melhoradas do sistema de instalação estão sendo desenvolvidas
para o próximo lançamento do Debian, e podem ser usadas para instalar o
bookworm. Para detalhes, veja
<a href="$(HOME)/devel/debian-installer/">a página do projeto Debian-Installer</a>.
</p>
</if-stable-release>
