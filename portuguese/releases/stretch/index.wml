#use wml::debian::template title="Informações de lançamento do Debian &ldquo;stretch&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="9f9207921c4ea799d5d3daa8a0f4b0faedf2b073"

<p>O Debian <current_release_stretch> foi
lançado <a href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch>.
<ifneq "9.0" "<current_release>"
  "O Debian 9.0 foi incialmente lançado em <:=spokendate('2017-06-17'):>."
/>
O lançamento incluiu muitas mudanças importantes, descritas em
nosso <a href="$(HOME)/News/2017/20170617">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 9 foi substituído pelo
<a href="../buster/">Debian 10 (<q>buster</q>)</a>.
Atualizações de segurança foram descontinuadas em
<:=spokendate('2020-07-06'):>.
</strong></p>

<p><strong>O Stretch também se beneficiou do suporte de longo prazo
(Long Term Support - LTS) até o final de junho de 2022. O LTS era limitado a
i386, amd64, armel, armhf e arm64.
Todas as outras arquiteturas não eram mais suportadas no Stretch.
Para mais informações, por favor consulte a
<a href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
</strong> </p>

<p>Para obter e instalar o Debian, veja a página de informações de instalação e
o guia de instalação. Para atualizar a partir de uma versão mais antiga do
Debian, veja as instruções nas <a href="releasenotes">notas de lançamento</a>.</p>

# Ative o seguinte, quando o período LTS começar.
<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
</ul>

<p>Arquiteturas de computadores suportadas no lançamento inicial do stretch:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/ppc64el/">POWER Processors</a>
<li><a href="../../ports/mips64el/">64-bit MIPS (little endian)</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
