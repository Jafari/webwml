#use wml::debian::template title="Debian GNU/Hurd --- CDs Hurd" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="38b1ccf880dcaca37d7f9783213ea20cb1dc68b8"

<define-tag cdserie>L1</define-tag>
<define-tag cdbasetarball>gnu-2009-10-18.tar.gz</define-tag>
<define-tag cdbasename>debian-<cdserie>-hurd-i386</define-tag>

<h1>Debian GNU/Hurd</h1>

<p>Enquanto muitas pessoas chamam o sistema GNU de GNU/Hurd, isto não é
totalmente verdadeiro. O Hurd é uma série de servidores que rodam no topo do
microkernel GNU Mach. Ambos, o Hurd e o GNU Mach, são parte do projeto
GNU, enquanto que o kernel Linux é um projeto independente.</p>

<p> O método mais fácil (e bem testado) de experimentar o Debian GNU/Hurd é
usar uma máquina virtual via KVM. Algumas imagens pré-instaladas estão
disponíveis em <url "cdimage.debian.org/cdimage/ports/current-hurd-i386/README.txt">,
mas também é possível usar o instalador do Debian para instalar no KVM ou uma
máquina nativa (mas o suporte ao hardware varia, por isso é mais recomendável
tentar com o KVM).
</p>

<h2>Usando o instalador do Debian do CD-ROM de instalação</h2>

<p> Um porte hurd-i386 do instalador padrão do Debian pode ser baixado em
<url "https://cdimage.debian.org/cdimage/ports/stable/hurd-i386/">, e um
pré-lançamento do porte hurd-amd64 pode ser baixada em
<url "https://cdimage.debian.org/cdimage/ports/latest/hurd-amd64/">.
Certifique-se de ler o arquivo README disponível nas imagens iso.
Ele funciona como o porte Linux habitual do instalador do Debian, ou seja,
automaticamente, exceto por alguns detalhes: </p>

<ul>

<li> Certifique-se de ativar o espaço swap, caso contrário, o Mach terá
problemas se você usar toda a memória. </li>

<li> Não monte uma partição separada para <code> /usr </code>, caso contrário,
a inicialização falhará.</li>

<li>
Leia <a href="hurd-install"> as notas sobre instalação manual </a> que
documentam algumas das etapas finais de configuração.
</li>

</ul>

<p> Instruções para gravar CDs a partir das imagens podem ser encontradas nas
<a href="$(HOME)/CD/faq/"> FAQ CDs do Debian </a>. </p>

<h2>Snapshots mais recentes</h2>

<p>Alguns snapshots mais recentes estão disponíveis em
<url "https://cdimage.debian.org/cdimage/ports/latest/hurd-i386/"> para i386 e
<url "https://cdimage.debian.org/cdimage/ports/latest/hurd-amd64/"> para amd64.</p>

<p>Snapshots diários (não testados!) estão disponíveis em
<url "https://people.debian.org/~sthibault/hurd-i386/installer/cdimage/"> para
i386 e <url "https://people.debian.org/~sthibault/hurd-amd64/installer/cdimage/">
para amd64.
Como eles são baseados na distribuição instável (unstable), muitas vezes
eles não podem realmente instalar um sistema, devido a transições em andamento
na instável (unstable), etc. Portanto, realmente use o snapshpt do link acima.
</p>

<h2>Criando o GRUB no disco de inicialização</h2>

<p>
Se você estiver instalando o Hurd sozinho em seu sistema, poderá deixar o
instalador instalar o GRUB. Se você estiver instalando o Hurd com um sistema já
existente, provavelmente desejará ser capaz de escolher entre os dois. Se o seu
sistema existente for Linux, você provavelmente pode simplesmente executar o
update-grub e ele detectará o seu sistema Hurd recém-instalado. Caso contrário,
ou se você não conseguir inicializar o Hurd dessa maneira, poderá usar um disco
de inicialização do GRUB. </p>

<p>
Instale o pacote grub-disk ou grub-rescue-pc, eles contêm uma imagem do GRUB
para disquete. Você pode usar "dd" se estiver trabalhando no GNU/Linux ou
rawrite se estiver trabalhando no MS.
</p>

<p>
Certifique-se de entender os métodos Linux, GRUB e Hurd de nomear unidades e
partições. Você estará usando todos os três e o relacionamento entre eles pode
ser confuso.
</p>

<p>O Hurd usa nomes de partições diferentes do Linux, por isso seja cuidadoso.
Discos IDE estão numerados em ordem, começando por hd0 para o mestre
primário e seu escravo hd1, seguidos pelo mestre secundário hd2 e seu
escravo hd3. Unidades SCSI também são numerados em ordem absoluta. Eles serão
sempre sd0, sd1 e assim por adiante não importando se as duas unidades tem id
SCSI 4 ou 5 ou qualquer outra. A experiência tem mostrado que unidades de CD-ROM
podem ser trabalhosas. Falaremos mais sobre isso adiante.</p>

<p>Partições Linux são sempre chamadas "sn" quando usando o Hurd, onde n é o
número da partição, deste modo, a primeira partição da primeira unidade IDE
será hd0s1, a terceira partição na segunda unidade SCSI será sd1s3 e assim
por diante.</p>

<p>O GRUB1 possui ainda outro sistema de nomeação de partições. Ele chama
partições (hdN,n), mas desta vez, o número do disco e da partição são baseados
em zero, e os discos estão em ordem, todos os IDE primeiro, e depois todos os
SCSI. Desta vez, a primeira partição no primeiro IDE será (hd0,0). O GRUB2 faz o
mesmo, mas o número da partição é um indexado, portanto, nesse caso, será
(hd0,1). Para realmente causar confusão, (hd1,2) poderia se referir à primeira
unidade SCSI se você tiver apenas uma unidade IDE, ou poderia se referir ao
segundo IDE. Então é importante que você já tenha planejado os vários nomes de
suas partições antes de começar.</p>

<p>Aproveite o Hurd.</p>
