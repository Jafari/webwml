#use wml::debian::template title="O que significa livre?" MAINPAGE="true"
#use wml::debian::translation-check translation="8d8a7b1eda812274f83d0370486ddb709d4f7d54"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Livre como em...?</a></li>
    <li><a href="#licenses">Licenças de software</a></li>
    <li><a href="#choose">Como escolher uma licença?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Em fevereiro de 1998, um grupo
se mobilizou para substituir o termo "<a href="https://www.gnu.org/philosophy/free-sw">Software Livre</a>"
por "<a href="https://opensource.org/osd">Software de Código Aberto</a>".
O debate sobre a terminologia reflete as diferenças filosóficas fundamentais,
mas os requisitos práticos, bem como outras coisas discutidas neste site, são
essencialmente os mesmos para o Software Livre e o Software de Código Aberto.</p>
</aside>

<h2><a id="freesoftware">Livre como em...?</a></h2>

<p>
Muitas pessoas novas nesse assunto ficam confusas por causa da palavra "free".
A palavra não é usada da forma que elas esperam – "free" significa "sem custo"
para elas. Se você olhar no dicionário de inglês, ele lista quase vinte
significados para a palavra "free" e apenas uma delas é "sem custo". O resto
se refere à "liberdade" e "falta de restrição". Então, quando falamos de
Free Software (Software Livre), nos referimos à liberdade, não ao pagamento.
</p>

<p>
O software descrito como livre, mas apenas no sentido de não ter que pagar por
ele, dificilmente é livre como um todo. Você pode ser impedido(a) de passá-lo
adiante e certamente será impedido(a) de modificá-lo. O software licenciado
sem custo é normalmente uma arma em uma campanha de marketing para promover
um produto relacionado ou para tirar um competidor menor do mercado. Não
há garantia de que ele permanecerá livre de cobrança.
</p>

<p>
Para os(as) não iniciados(as), o software é gratuito ou não é. No entanto, a
vida real é muito mais complicada do que isso. Para entender o que as
pessoas querem dizer quando falam de software livre, vamos pegar um
pequeno desvio e entrar no mundo das licenças de software.</p>

<h2><a id="licenses">Licenças de software</a></h2>

<p>
Os direitos autorais (copyrights) são um método para proteger os direitos do(a)
criador(a) de certos tipos de trabalhos. Na maioria dos países, software
recém-escrito é automaticamente protegido por direitos autorais. Uma licença é a
maneira dos(as) autores(as) permitirem o uso de sua criação (software nesse caso)
por outras pessoas, de maneiras aceitáveis para eles(as). Cabe ao(à) autor(a)
incluir uma licença que declare como um pedaço do software pode ser usado.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Leia mais sobre direito autoral (copyright)</a></button></p>

<p>
Claro, diferentes circunstâncias exigem diferentes licenças.
As empresas de software estão procurando proteger seus bens, por isso elas
liberam apenas o código compilado que não é legível por humanos. Elas também
colocam muitas restrições para o uso do software. Os(As) autores(as) de software
livre, por outro lado, estão geralmente procurando por regras diferentes, às
vezes até mesmo uma combinação dos seguintes pontos:
</p>

<ul>
  <li>Não é permitido o uso de seu código em software proprietário. Já que
  eles(as) lançam seu software para que todos(as) possam usá-lo, eles(as) não
  querem ver outros(as) o(a) roubando. Nesse caso, o uso do código é visto como
  uma confiança: você pode usá-lo desde que jogue com as mesmas regras.</li>

  <li>A identidade de autoria deve ser protegida. As pessoas têm um grande
  orgulho de seu trabalho e não querem que outros(as) removam seu nome dos
  créditos ou até mesmo que afirmem que foram eles(as) próprios(as) que
  escreveram o software.</li>

  <li>O código-fonte deve ser distribuído. Um dos problemas com a maioria do
  software proprietário é que você não pode consertar erros ou customizá-lo, já
  que o código-fonte não está disponível. Além disso, um fornecedor pode decidir
  parar de dar suporte ao hardware do(a) usuário(a). A distribuição do
  código-fonte, como definida na maioria das licenças livres, protege os(as)
  usuários(as) permitindo-lhes personalizar o software e ajustá-lo às suas
  necessidades.</li>

  <li>Qualquer trabalho que inclua parte do trabalho do(a) autor(a) (também
  chamado de "trabalhos derivados" na discussão de direitos autorais) deve usar
  a mesma licença.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Três das licenças de software
livre mais amplamente utilizadas são a
<a href="https://www.gnu.org/copyleft/gpl.html">Licença Pública Geral GNU (GPL - GNU General Public)</a>,
a <a href="https://opensource.org/blog/license/artistic-2-0">Licença artística (Artistic License)</a>,
e a <a href="https://opensource.org/blog/license/bsd-3-clause">Licença estilo BSD (BSD Style License)</a>.
</aside>

<h2><a id="choose">Como escolher uma licença?</a></h2>

<p>
Às vezes, as pessoas escrevem suas próprias licenças, o que pode ser
problemático, então isso é desaprovado na comunidade de software livre. Muitas
vezes o texto é ambíguo ou as pessoas criam condições que entram em conflito
umas com as outras. Escrever uma licença válida em um tribunal é ainda mais
difícil. Felizmente, existem várias licenças de código aberto disponíveis para
você escolher. Elas têm as seguintes coisas em comum:</p>

<ul>
  <li>Usuários(as) podem instalar o software em quantas máquinas quiserem.</li>
  <li>Qualquer número de pessoas pode usar o software ao mesmo tempo.</li>
  <li>Usuários(as) podem fazer quantas cópias do software desejarem ou
  precisarem, e também podem dar essas cópias para outros(as) usuários(as)
  (redistribuição livre ou aberta).</li>
  <li>Não há restrições para modificação do software (exceto que se deve manter
  certos créditos).</li>
  <li>Usuários(as) podem não apenas distribuir o software, como podem até
  mesmo vendê-lo.</li>
</ul>

<p>
Especialmente o último ponto, que permite que as pessoas vendam o software,
parece ir contra toda a ideia do software livre, mas na verdade é um de seus
pontos fortes. Uma vez que a licença permite a redistribuição gratuita, uma vez
que alguém obtém uma cópia, ele(a) pode distribuí-la também. As pessoas podem
até tentar vendê-lo.</p>

<p>
Embora o software livre não seja totalmente livre de restrições, oferece
aos(as) usuários(as) flexibilidade para fazer o que eles(as) precisam para
realizar seu trabalho. Ao mesmo tempo, protege os direitos do(a)
autor(a) – isso sim é liberdade. Compilamos a
<a href="../social_contract#guidelines">Definição Debian de Software Livre (DFSG - Debian Free Software Guidelines)</a>
para chegarmos a uma definição razoável do que constitui software livre em nossa
opinião. Somente software que está em conformidade com a DFSG é permitido no
repositório principal (main) do Debian.
</p>

