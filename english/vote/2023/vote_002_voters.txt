-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1             agx	Guido Guenther
    2           alexm	Alex Muntada
    3        amacater	Andrew Martin Adrian Cater
    4         anarcat	Antoine Beaupré
    5            andi	Andreas B. Mundt
    6          anibal	Anibal Monsalve Salazar
    7             apo	Markus Koschany
    8              az	Alexander Zangerl
    9        azekulic	Alen Zekulic
   10      babelouest	Nicolas Mora
   11        ballombe	Bill Allombert
   12           bartm	Bart Martens
   13             bas	Bas Zoetekouw
   14          bbaren	Benjamin Barenblat
   15           bdale	Bdale Garbee
   16            benh	Ben Hutchings
   17            beuc	Sylvain Beucler
   18         bgoglin	Brice Goglin
   19         bigeasy	Sebastian Andrzej Siewior
   20           bluca	Luca Boccassi
   21            bzed	Bernd Zeimetz
   22          carnil	Salvatore Bonaccorso
   23          chrism	Christoph Martin
   24           cklin	Chuan-kai Lin
   25           cwryu	Changwoo Ryu
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26          czchen	ChangZhuo Chen
   27            dank	Nick Loren Black
   28        deltaone	Patrick Franz
   29          dererk	Ulises Vitulli
   30          dlange	Daniel Lange
   31             dmn	Damyan Ivanov
   32             dod	Dominique Dumont
   33         dogsleg	Lev Lamberov
   34         donkult	David Kalnischkies
   35          eamanu	Emmanuel Arias
   36          ebourg	Emmanuel Bourg
   37          eevans	Eric Evans
   38          elbrus	Paul Mathijs Gevers
   39        emollier	Étienne Mollier
   40            eric	Eric Dorland
   41           eriol	Daniele Tricoli
   42         florian	Florian Ernst
   43         fpeters	Frederic Peters
   44        francois	Francois Marier
   45         frankie	Francesco Lovergine
   46           fuddl	Bruno Kleinert
   47        georgesk	Georges Khaznadar
   48          gibmat	Mathias Arthur Gibbens
   49             gio	Giovanni Mascellani
   50         giovani	Giovani Augusto Ferreira
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51           gladk	Anton Gladky
   52          gniibe	NIIBE Yutaka
   53          gregoa	Gregor Herrmann
   54         guilhem	Guilhem Moulin
   55         guillem	Guillem Jover
   56           gwolf	Gunnar Wolf
   57            haas	Christoph Haas
   58           hefee	Sandro Knauß
   59         hertzog	Raphaël Hertzog
   60      hlieberman	Harlan Lieberman-Berg
   61             hmc	Hugh McMaster
   62          holger	Holger Levsen
   63      hvhaugwitz	Hannes von Haugwitz
   64             ijc	Ian James Campbell
   65           jandd	Jan Dittberner
   66             jcc	Jonathan Cristopher Carter
   67            jcfp	Jeroen Ploemen
   68             jdg	Julian Gilbey
   69          jlines	John Lines
   70          johfel	Johann Felix Soden
   71          jpuydt	Julien Puydt
   72              js	Jonas Smedegaard
   73           krala	Antonin Kral
   74           lange	Thomas Lange
   75        lavamind	Jerome Charaoui
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76         ldrolez	Ludovic Drolez
   77         lechner	Felix Lechner
   78        lisandro	Lisandro Damián Nicanor Pérez Meyer
   79          ltworf	Salvo Tomaselli
   80           lucab	Luca Bruno
   81           lucas	Lucas Nussbaum
   82           lumin	Mo Zhou
   83         matthew	Matthew Vernon
   84     matthieucan	Matthieu Caneill
   85          mattia	Mattia Rizzolo
   86         mbehrle	Mathias Behrle
   87          merker	Karsten Merker
   88          merkys	Andrius Merkys
   89           metal	Marcelo Jorge Vieira
   90            mika	Michael Prokop
   91           mones	Ricardo Mones Lastra
   92             mrd	Matthew Danish
   93     mtecknology	Michael Lustfield
   94            myon	Christoph Berg
   95             mzf	Francois Mazen
   96         nicolas	Nicolas Boulenguez
   97           noahm	Noah Meyerhans
   98            noel	Noèl Köthe
   99         noodles	Jonathan McDowell
  100           ohura	Makoto OHURA
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101         olebole	Ole Streicher
  102            olly	Olly Betts
  103           osamu	Osamu Aoki
  104            pabs	Paul Wise
  105    paddatrapper	Kyle Robbertze
  106             peb	Pierre-Elliott Bécue
  107             pgt	Pierre Gruet
  108           philh	Philip Hands
  109            piem	Paul Brossier
  110          plessy	Charles Plessy
  111          pmhahn	Philipp Matthias Hahn
  112           pollo	Louis-Philippe Véronneau
  113        porridge	Marcin Owsiany
  114        pronovic	Kenneth J. Pronovici
  115             ras	Russell Stuart
  116         reichel	Joachim Reichel
  117           rinni	Philip Rinn
  118            roam	Peter Pentchev
  119         roberto	Roberto C. Sanchez
  120        roehling	Timo Röhling
  121        rousseau	Ludovic Rousseau
  122             rra	Russ Allbery
  123     rvandegrift	Ross Vandegrift
  124        santiago	Santiago Ruano Rincón
  125             seb	Sebastien Delafond
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126       sebastien	Sébastien Villemot
  127           smurf	Matthias Urlichs
  128         sophieb	Sophie Brun
  129       spwhitton	Sean Whitton
  130          ssgelm	Stephen Gelman
  131        stappers	Geert Stappers
  132  stephanlachnit	Stephan Lachnit
  133       sthibault	Samuel Thibault
  134           sur5r	Jakob Haufe
  135            tach	Taku Yasui
  136          taffit	David Prévot
  137           talau	Marcos Talau
  138             tbm	Martin Michlmayr
  139        terceiro	Antonio Terceiro
  140              tg	Thorsten Glaser
  141             thb	Thaddeus H. Black
  142            thep	Theppitak Karoonboonyanan
  143          tianon	Tianon Gravi
  144        tjhukkan	Teemu Hukkanen
  145            tobi	Tobias Frost
  146           toddy	Tobias Quathamer
  147         tolimar	Alexander Reichle-Schmehl
  148            toni	Toni Mueller
  149         treinen	Ralf Treinen
  150           troyh	Troy Heber
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151            ucko	Aaron M. Ucko
  152       ultrotter	Guido Trotter
  153        umlaeute	IOhannes m zmölnig
  154           urbec	Judit Foglszinger
  155        vdanjean	Vincent Danjean
  156          vvidic	Valentin Vidic
  157          wagner	Hanno Wagner
  158        weinholt	Göran Weinholt
  159          wijnen	Bas Wijnen
  160          wouter	Wouter Verhelst
  161            yadd	Xavier Guimard
  162            zack	Stefano Zacchiroli
  163            zeha	Christian Hofstaedtler
