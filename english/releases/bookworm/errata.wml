#use wml::debian::template title="Debian 12 -- Errata" BARETITLE=true
#use wml::debian::toc

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">Security issues</toc-add-entry>

<p>Debian security team issues updates to packages in the stable release
in which they've identified problems related to security. Please consult the
<a href="$(HOME)/security/">security pages</a> for information about
any security issues identified in <q>bookworm</q>.</p>

<p>If you use APT, add the following line to <tt>/etc/apt/sources.list</tt>
to be able to access the latest security updates:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free non-free-firmware
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Point releases</toc-add-entry>

<p>Sometimes, in the case of several critical problems or security updates, the
released distribution is updated.  Generally, these are indicated as point
releases.</p>

<ul>
  <li>The first point release, 12.1, was released on
      <a href="$(HOME)/News/2023/20230722">July 22, 2023</a>.</li>
  <li>The second point release, 12.2, was released on
      <a href="$(HOME)/News/2023/20231007">October 07, 2023</a>.</li>
  <li>The third point release, 12.3, was delayed on
      <a href="$(HOME)/News/2023/2023120902">December 09, 2023</a>.</li>
  <li>The fourth point release 12.4, was released on
     <a href="$(HOME)/News/2023/20231210">December 10, 2023</a>.</li>
  <li>The fifth point release 12.5, was released on
     <a href="$(HOME)/News/2024/20240210">February 10, 2024</a>.</li>
  <li>The sixth point release 12.6, was released on
     <a href="$(HOME)/News/2024/20240629">June 29, 2024</a>.</li>
  <li>The seventh point release 12.7, was released on
     <a href="$(HOME)/News/2024/20240831">August 31, 2024</a>.</li>
  <li>The eigthth point release 12.8, was released on
     <a href="$(HOME)/News/2024/20241109">November 9, 2024</a>.</li>

</ul>


<ifeq <current_release_bookworm> 12.0 "

<p>There are no point releases for Debian 12 yet.</p>" "

<p>See the <a
href="http://deb.debian.org/debian/dists/bookworm/ChangeLog">\
ChangeLog</a>
for details on changes between 12 and <current_release_bookworm/>.</p>"/>


<p>Fixes to the released stable distribution often go through an
extended testing period before they are accepted into the archive.
However, these fixes are available in the
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> directory of any Debian archive
mirror.</p>

<p>If you use APT to update your packages, you can install
the proposed updates by adding the following line to
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 12 point release
  deb https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installation system</toc-add-entry>

<p>
For information about errata and updates for the installation system, see
the <a href="debian-installer/">installation information</a> page.
</p>
