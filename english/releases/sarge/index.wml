#use wml::debian::template title="Debian &ldquo;sarge&rdquo; Release Information"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian GNU/Linux 3.1 (a.k.a. <em>sarge</em>) was
released on 6th of June, 2005. The new release includes many major
changes, described in 
our <a href="$(HOME)/News/2005/20050606">press release</a> and
the <a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian GNU/Linux 3.1 has been superseded by
<a href="../etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>.
Security updates have been discontinued as of the end of March 2008.
</strong></p>

<p>To obtain and install Debian GNU/Linux 3.1, see
the installation information page and the
Installation Manual. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

<p>The following computer architectures were supported in this release:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>.
</p>
