msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/partners/partners.def:8
msgid "Partner of the Month"
msgstr ""

#: ../../english/partners/partners.def:12
msgid "Alphabetical List of Partners"
msgstr ""

#: ../../english/partners/partners.def:16
msgid "Development Partners"
msgstr ""

#: ../../english/partners/partners.def:20
msgid "Financial Partners"
msgstr ""

#: ../../english/partners/partners.def:24
msgid "Service Partners"
msgstr ""

#: ../../english/partners/partners.def:30
msgid "<a href=\"http://www.1and1.info/\">1&amp;1</a> supports us by providing several servers in their Karlsruhe high-performance datacenter."
msgstr ""

#: ../../english/partners/partners.def:33
msgid "1&amp;1 is one of the biggest Domain Registrars and Web hosting companies worldwide, with offices in Germany, France, Great Britain, Spain and the USA. Most of the over 5 million websites and other services managed by 1&amp;1 are running on a Debian-based environment using several thousand servers."
msgstr ""

#: ../../english/partners/partners.def:40
msgid "<a href=\"https://amperecomputing.com\">Ampere®</a> provides hardware for arm64/armhf/armel build daemons and other Debian services (current Ampere hardware donations are listed in the <a href=\"https://db.debian.org/machines.cgi\">Debian machines</a> page)."
msgstr ""

#: ../../english/partners/partners.def:43
msgid "Ampere is designing the future of hyperscale cloud and edge computing with the world’s first cloud native processor. Built for the cloud with a modern 64-bit Arm server-based architecture, Ampere gives customers the freedom to accelerate the delivery of all cloud computing applications. With industry-leading cloud performance, power efficiency and scalability, Ampere processors are tailored for the continued growth of cloud and edge computing."
msgstr ""

#: ../../english/partners/partners.def:51
msgid "<a href=\"https://www.bytemark.co.uk/r/deb-partners\">Bytemark</a> support us by providing hosting services and sponsorship for the Debian Conference."
msgstr ""

#: ../../english/partners/partners.def:55
msgid "They are a leading Internet Service Provider (ISP) in the United Kingdom and provide scalable, powerful and affordable hosting with lots of <q>geek friendly</q> extras as standard. Debian GNU/Linux is deployed extensively within their network, and comes recommended as the <q>Distribution of Choice</q> to any new customer who's not 100% sure on what to pick. Bytemark are also well known for their expert and friendly support."
msgstr ""

#: ../../english/partners/partners.def:63
msgid "<a href=\"https://www.conova.com\">conova communications GmbH</a> has been supporting the Debian project as a hosting partner for several years. In addition, the IT specialists also host the Debian Bug Squashing Parties in Salzburg/Austria. Debian is also used on many systems used on a daily basis at conova."
msgstr ""

#: ../../english/partners/partners.def:66
msgid "conova operates one of the most modern data centers in all of Europe in Salzburg, Austria. Their services: customized solutions for housing, hosting, managed &amp; cloud services, outsourcing as well as network and security. The company offers space for more than 10,000 servers at the highest level of supply and security on their 2,000 m² of technical area."
msgstr ""

#: ../../english/partners/partners.def:73
msgid "<a href=\"https://www.credativ.com/\">credativ</a> is an independent consulting and services company and since 1999 been offering comprehensive services and technical support for the implementation and operation of open source software in business applications. Our <q>Open Source Support Center</q> is always available, 365 days a year, around the clock. "
msgstr ""

#: ../../english/partners/partners.def:76
msgid "From the very beginning credativ has actively supported the Debian project and will continue to do so in the future. Furthermore many of our consultants are also Debian developers and actively involved in contributing to free software projects worldwide. Further information can be found at <a href=\"https://www.credativ.com/\">https://www.credativ.com/</a>."
msgstr ""

#: ../../english/partners/partners.def:83
msgid "<a href=\"https://www.freexian.com/\">Freexian</a> is a services company specialized in Free Software and in particular Debian GNU/Linux. Most of Freexian's employees are well-known contributors in the free software community, a choice that is integral to Freexian's business model."
msgstr ""

#: ../../english/partners/partners.def:86
msgid " Freexian is a major contributor to the Debian LTS team. Freexian assist in extending the security support of stable releases to 5 years and extends security support for old Debian releases up to 10 years for Debian releases beyond the normal support period advertised by Debian."
msgstr ""

#: ../../english/partners/partners.def:93
msgid "<a href=\"https://www.fastly.com/\">Fastly</a> provides Debian with content delivery network (CDN) services and is helping us deliver packages to users through <a href=\"https://deb.debian.org/\">deb.debian.org</a> and <a href=\"http://security.debian.org\">security.debian.org</a>."
msgstr ""

#: ../../english/partners/partners.def:96
msgid "Fastly’s edge cloud platform provides advanced application delivery and cloud security for the world’s most popular online destinations. Fastly works with the best of the Internet, serving 14 trillion requests each month, more than 10 percent of all internet requests."
msgstr ""

#: ../../english/partners/partners.def:103
msgid "<a href=\"https://www.gandi.net\">Gandi</a> is Debian's DNS registrar and provides hosting and discounts to Debian developers and maintainers, supporting Debian France since 2014 and Debian Worldwide via SPI since 2022."
msgstr ""

#: ../../english/partners/partners.def:106
msgid "Gandi is a French registrar which is deeply engaged in supporting Free Open Source Software and other ethic projects since 2006."
msgstr ""

#: ../../english/partners/partners.def:114
msgid "<a href=\"https://www.google.com\">Google</a> sponsors parts of Salsa's continuous integration infrastructure within Google Cloud Platform."
msgstr ""

#: ../../english/partners/partners.def:117
msgid "Google is one of the largest technology companies in the world, providing a wide range of Internet-related services and products as online advertising technologies, search, cloud computing, software, and hardware."
msgstr ""

#: ../../english/partners/partners.def:123
msgid "<a href=\"https://www.hetzner.com\">Hetzner</a> provides the Debian project with hosting services."
msgstr ""

#: ../../english/partners/partners.def:126
msgid "Hetzner Online is a professional web hosting provider and experienced data center operator. Since 1997 the company has provided private and business clients with high-performance hosting products as well as the necessary infrastructure for the efficient operation of websites. A combination of stable technology, attractive pricing and flexible support and services has enabled Hetzner Online to continuously strengthen its market position both nationally and internationally. The company owns several data centers in Germany and Finland."
msgstr ""

#: ../../english/partners/partners.def:133
msgid "<a href=\"http://www.hpe.com/engage/opensource\">Hewlett Packard Enterprise (HPE)</a> provides hardware for port development, Debian mirrors, and other Debian services (current HPE hardware donations are listed in the <a href=\"https://db.debian.org/machines.cgi\">Debian machines</a> page)."
msgstr ""

#: ../../english/partners/partners.def:136
msgid "HPE is one of the largest computer companies in the world, providing a wide range of products and services, such as servers, storage, networking, consulting and support, software, and financial services."
msgstr ""

#: ../../english/partners/partners.def:144
msgid "<a href=\"https://www.leaseweb.com/\">LeaseWeb</a> has been one of two partners that provide the infrastructure for the <a href=\"https://snapshot.debian.org\">Debian OS Snapshot Archive</a> since <a href=\"https://www.debian.org/News/2014/20141014\">October 2014</a>, providing 300 Terabytes (TB) of capacity. In 2020 they renewed their support by provisioning new dedicated servers with bigger disk drives, enough to accommodate anticipated growth for years to come."
msgstr ""

#: ../../english/partners/partners.def:147
msgid "LeaseWeb is a global Infrastructure-as-a-Service (IaaS) provider – offering customers on-demand, world-class hosting solutions, from dedicated servers to cloud solutions. You can learn more about LeaseWeb visiting their <a href=\"https://www.leaseweb.com/\">website</a>."
msgstr ""

#: ../../english/partners/partners.def:153
msgid "<a href=\"https://www.lenovo.com/\">Lenovo</a> Some Text "
msgstr ""

#: ../../english/partners/partners.def:156
msgid "Lenovo is a "
msgstr ""

#: ../../english/partners/partners.def:163
msgid "<a href=\"http://www.loongson.cn\">Loongson</a> and Lemote have provided several Loongson-based machines to Debian."
msgstr ""

#: ../../english/partners/partners.def:166
msgid "Loongson processors are a series of MIPS-compatible processors. They have been widely used in many areas, such as desktop, server, embedded application, high-performance computing etc. For more information, contact <a href=\"mailto:info@loongson.cn\">info@loongson.cn</a>."
msgstr ""

#: ../../english/partners/partners.def:173
msgid "<a href=\"http://www.man-da.de/\">man-da.de GmbH</a> is the backbone provider of the Metropolitan Area Network Darmstadt. It is supporting Debian by hosting several debian.org and debian.net servers."
msgstr ""

#: ../../english/partners/partners.def:177
msgid "man-da.de GmbH is operating MANDA, a wide area network in the South Hessen region connecting educational and research organisations to a high speed redundant network ring and providing internet access. The company is owned by TU Darmstadt and University of Applied Sciences Darmstadt and in addition to operating MANDA it is also providing IT consulting and IT services to both universities."
msgstr ""

#: ../../english/partners/partners.def:184
msgid "<a href=\"http://osuosl.org/\">The Oregon State University Open Source Lab</a> provides hosting and administration services to the Debian project."
msgstr ""

#: ../../english/partners/partners.def:188
msgid "The Open Source Lab is a focal point for open source development at Oregon State University and beyond. The OSL provides development, hosting and assorted other services to the Open Source community."
msgstr ""

#: ../../english/partners/partners.def:195
msgid "<a href=\"https://www.raptorcs.com/\">Raptor Computing Systems</a> provides Debian with bare metal access to fully open firmware POWER ISA servers, along with discounted pricing for fully open firmware POWER ISA desktop machines to Debian developers and maintainers."
msgstr ""

#: ../../english/partners/partners.def:199
msgid "Raptor Computing Systems is a hardware ODM specializing in fully owner-controllable computing hardware with 100&percnt; open-source firmware.  Their extensive experience with IBM POWER and OpenPOWER allows them to produce secure desktop and server class systems for general purpose computing applications, as well as custom hardware and FPGA / ASIC designs built around the open POWER ISA."
msgstr ""

#: ../../english/partners/partners.def:206
msgid "nic.at is sponsoring the anycast service <a href=\"https://www.rcodezero.at/\">RcodeZero DNS</a> for Debian as it meets the needs to have geographically disperse locations and support DNSSEC."
msgstr ""

#: ../../english/partners/partners.def:210
msgid "ipcom is a subsidiary of nic.at, the Austrian domain registry. nic.at has been managing the .at-zone since 1998 on a highly professional and reliable level. The RcodeZero Anycast network has been developed by nic.at’s R&amp;D department and has been successfully in use for the .at zone."
msgstr ""

#: ../../english/partners/partners.def:218
msgid "The <a href=\"https://www.sanger.ac.uk/\">Wellcome Sanger Institute</a> provides infrastructure for the <a href=\"https://snapshot.debian.org/\">Debian OS Snapshot Archive</a> since the creation of the service in <a href=\"https://www.debian.org/News/2010/20100412\">April 2010</a>. In 2018 they renewed their support by provisioning a new frontend server and increasing the amount of snapshot storage provided."
msgstr ""

#: ../../english/partners/partners.def:221
msgid "The Wellcome Sanger Institute is one of the world's leading genome centres. Through its ability to conduct research at scale, it is able to engage in bold and long-term exploratory projects that are designed to influence and empower medical science globally. Institute research findings, generated through its own research programmes and through its leading role in international consortia, are being used to develop new diagnostics and treatments for human disease."
msgstr ""

#: ../../english/partners/partners.def:231
msgid "<A HREF=\"http://www.brainfood.com/\">Brainfood</A> (previously called Novare) provides several machines to Debian, as well as hosting and server administration services."
msgstr ""

#: ../../english/partners/partners.def:234
msgid "Brainfood is a technology solutions company whose goal is to work to produce an ideal result for a specific need, whether it's a website or an ongoing, distance-learning intranet application, or custom developed software. &nbsp; For more information, contact <A HREF=\"mailto:debian-contact@brainfood.com\">debian-contact@brainfood.com</A>."
msgstr ""

#: ../../english/partners/partners.def:241
msgid "<a href=\"http://www.hp.com/\">Hewlett-Packard</a> has provided hardware for port development, Debian mirrors, and other Debian services (current HP hardware donations are listed in the <a href=\"https://db.debian.org/machines.cgi\">Debian machines</a> page). For information about HP's involvement in the Linux Open Source community, see <a href=\"http://www.hp.com/products1/linux/\">HP Linux</a>."
msgstr ""

#: ../../english/partners/partners.def:244
msgid "Hewlett-Packard is one of the largest computer companies in the world, providing a wide range of products and services, such as servers, PCs, printers, storage products, network equipment, software, cloud computing solutions, etc."
msgstr ""

#: ../../english/partners/partners.def:251
msgid "<A HREF=\"http://www.linuxcentral.com/\">Linux Central</A> provides us with Debian GNU/Linux CD-ROM sets for trade show type events. These CD's help to promote Debian by getting the system right in the hands of potential and current users."
msgstr ""

#: ../../english/partners/partners.def:255
msgid "Linux Central sells all the latest releases of Linux products such as commercial software, books, hardware, and versions of major Linux distributions. On Linux Central's site you can also find news and basic information about Linux, or sign up for a newsletter to stay updated on all newly released products or daily specials. &nbsp; For more information, contact <A HREF=\"mailto:sales@linuxcentral.com\">sales@linuxcentral.com</A>."
msgstr ""

#: ../../english/partners/partners.def:262
msgid "<a href=\"http://www.mythic-beasts.com\">Mythic Beasts Ltd.</a> supports Debian (and SPI Inc) by providing domain registration facilities.</p><p>They are a UK based web hosting, domain registration and co-location company."
msgstr ""

#: ../../english/partners/partners.def:269
msgid "San Francisco based <a href=\"http://www.nostarch.com/\">No Starch Press</a>, publisher of \"The Finest in Geek Entertainment,\" is co-publisher of the book <a href=\"http://debiansystem.info/\">The Debian System</a>. The company is well known for publishing high-quality titles on a broad range of topics including alternative operating systems, networking, hacking, programming, and LEGO. No Starch Press donates US$1 from the sale of each copy of The Debian System to the Debian Project."
msgstr ""

#: ../../english/partners/partners.def:276
msgid "<a href=\"http://www.opensourcepress.de/\">Open Source Press</a> is the Munich-based publisher of the book <a href=\"http://debiansystem.info/\">The Debian System</a>. For each book sold, the publisher and author together donate 1&euro; to the Debian Project."
msgstr ""

#: ../../english/partners/partners.def:279
msgid "Open Source Press specialises on books around Linux and Open Source Software, written by authors with considerable experience in the fields, and produced in close cooperation with the respective communities. The Open Source Press publications are renowned for their quality and depth."
msgstr ""

#: ../../english/partners/partners.def:287
msgid "<a href=\"http://www.rapidswitch.com/\">RapidSwitch</a> (formerly Black Cat Networks) is a UK ISP specialising in dedicated hosting. It provides dedicated servers, managed servers solutions, colocation services and VPS virtual servers."
msgstr ""

#: ../../english/partners/partners.def:290
msgid "It hosts a Debian development machine, <a href=\"https://db.debian.org/machines.cgi?host=caballero\">caballero</a>."
msgstr ""

#: ../../english/partners/partners.def:298
msgid "<a href=\"http://www.simtec.co.uk\">Simtec</a> has provided several ARM machines which make up the bulk of the Debian ARM infrastructure. In addition Simtec employs several developers who contribute towards the Debian ARM port."
msgstr ""

#: ../../english/partners/partners.def:301
msgid "Simtec is a leading provider of ARM based computing products. We have a range of <a href=\"http://www.simtec.co.uk/products/boards.html\">boards</a> which serve a wide range of uses within the embedded market. We provide a complete range of products and solutions which can be tailored to suit the customers needs from off the shelf integrator solutions right through to fully custom designs. All of our products ship with open source tools and operating systems."
msgstr ""

#: ../../english/partners/partners.def:310
msgid "<a href=\"https://www.stackpath.com/?utm_campaign=Partner%20Display&amp;utm_source=Partner%20Display&amp;utm_medium=Debian\">StackPath</a> provides Debian with content delivery network (CDN) services, allowing high availability of services like <a href=\"https://security-tracker.debian.org/\">the Security Bug Tracker</a> and <a href=\"https://planet.debian.org/\">Planet Debian</a>."
msgstr ""

#: ../../english/partners/partners.def:313
msgid "StackPath is a platform of secure edge services that enables developers to protect, accelerate, and innovate cloud properties ranging from websites to media delivery and IoT services."
msgstr ""

#: ../../english/partners/partners.def:321
msgid "<a href=\"http://www.telegraaf.nl/\">Telegraaf Media ICT BV</a> is supporting the Debian community by donating hardware, rackspace and bandwidth for the Debian server hosting alioth, arch and svn."
msgstr ""

#: ../../english/partners/partners.def:324
msgid "Telegraaf Media ICT BV is running its internet infrastructure (appx. 200 Intel based servers) mainly based on Debian GNU/Linux servers; the sysadmins are highly motivated Debian users and some of them also Debian maintainers."
msgstr ""

#: ../../english/partners/partners.def:327
msgid "Telegraaf Media Group runs some of the most popular Dutch websites (http://www.telegraaf.nl, http://www.speurders.nl and http://www.dft.nl) as well as the two biggest newspapers in The Netherlands."
msgstr ""

#: ../../english/partners/partners.def:334
msgid "<a href=\"https://arc.ubc.ca\">The University of British Columbia (UBC)</a> provides Debian with infrastructure hosting since 2005."
msgstr ""

#: ../../english/partners/partners.def:337
msgid "UBC Advanced Research Computing (ARC) supports the high-performance computing, data management and infrastructure needs of UBC researchers. Working closely with national, regional, and institutional partners, UBC ARC provides consultation, expertise, and access to digital research infrastructure."
msgstr ""

