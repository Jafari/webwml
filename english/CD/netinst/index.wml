#use wml::debian::cdimage title="Network install from a minimal USB, CD"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p>On i386 and amd64 architectures, all USB/CD/DVD images can be
<a href="https://www.debian.org/CD/faq/#write-usb">used on a USB stick</a> too.</div>

<p>A <q>network install</q> or <q>netinst</q> CD is a single CD which enables
you to install the entire operating system. This single CD contains
just the minimal amount of software to install the base system and
fetch the remaining packages over the Internet.</p>

<p><strong>What types of network connections are supported
during installation?</strong>
Various different ways are supported for this, like
Ethernet and WLAN (with some restrictions)</p>

<p>The following minimal bootable CD images are available for
download:</p>

<ul>

  <li>Official <q>netinst</q> images for the <q>stable</q> release &mdash; <a
  href="#netinst-stable">see below</a></li>

  <li>Images for the <q>testing</q> release &mdash; see the <a
  href="$(DEVEL)/debian-installer/">Debian-Installer page</a>.</li>

</ul>


<h2 id="netinst-stable">Official netinst images for the <q>stable</q> release</h2>

<p>This image contains the installer and a
small set of packages which allows the installation of a (very) basic
system.</p>

<div class="line">
<div class="item col50">
<p><strong>netinst CD image</strong></p>
	  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>netinst CD image (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
	  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>For information what these files are and how to use them, please see
the <a href="../faq/">FAQ</a>.</p>

<p>Once you have downloaded the images, be sure to have a look at the
<a href="$(HOME)/releases/stable/installmanual">detailed
information about the installation process</a>.</p>
