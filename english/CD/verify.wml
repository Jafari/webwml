#use wml::debian::cdimage title="Verifying authenticity of Debian images" BARETITLE=true

<p>
Official releases of Debian installation and live images come with
signed checksum files; look for them alongside the images in
the <code>iso-cd</code>, <code>jigdo-dvd</code>, <code>iso-hybrid</code>
etc. directories. These files allow you to check that the images you
download are correct. First of all, the checksum can be used to check
that the images have not been corrupted during download. Secondly, the
signatures on the checksum files allow you to confirm that the images
are the ones created and released by Debian, and have not been
tampered with.
</p>

<p>
To validate the contents of an image file, be sure to use the
appropriate checksum tool. Cryptographically strong checksum
algorithms (SHA256 and SHA512) are available for every releases; you
should use the matching tools
<code>sha256sum</code> or <code>sha512sum</code> to work with these.
</p>

<p>
To ensure that the checksums files themselves are correct, use an OpenPGP
implementation (such as GnuPG, Sequoia-PGP, PGPainless or GopenPGP) to
verify them against the accompanying signature files (e.g.
<code>SHA512SUMS.sign</code>). The keys used for these signatures are
all in the <a href="https://keyring.debian.org">Debian OpenPGP keyring</a>
and the best way to check them is to use that keyring to validate via
the web of trust. To make life easier for people who don't have ready
access to an existing Debian machine, here are details of the keys
that have been used to sign releases in recent years, and links to
download the public keys directly:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
