#use wml::debian::template title="何謂自由（Free）" MAINPAGE="true"
#use wml::debian::translation-check translation="8d8a7b1eda812274f83d0370486ddb709d4f7d54"

# $Id$
# Translator: Franklin <franklin@goodhorse.idv.tw>, 2002/11/19
# Edited by foka, 2002/11/19
# 編註: Franklin 以下的譯文不是逐句直譯。這樣可能會比較通順，但有空時
#       最好再仔細檢查一下，尤其是英文 "free" 與中文「自由」、「免費」
#       之間的微妙關係。 - foka
# 編註: 為了在 Mozilla 裏面效果完美，我加了大量 <span lang="en">...</span>，
#       不知是否多此一舉？但英文字串用了正確的字型，卻真的是好看了。 :-) - foka

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Free 的意思是……？</a></li>
    <li><a href="#licenses">软件许可证</a></li>
    <li><a href="#choose">如何选择许可证？</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 1998 年 2 月，有一個團體建议將<q><a href="https://www.gnu.org/philosophy/free-sw">自由軟體（Free Software）</a></q>這個名稱改為<q><a href="https://opensource.org/osd">開源軟體（Open Source Software）</a></q>。这次术语的选择辩论反映了其内在的理念差异，但是就对软件的实际要求以及本页面讨论的其他内容而言，自由软件和开源软件这两个术语在本质上是相同的。</p>
</aside>


<h2><a id="freesoftware">Free 的意思是……？</a></h2>

<p>
許多人在剛開始接觸自由軟體時都會很困惑，原因是自由軟體中的自由（Free）一詞並不是他
們所期望的那樣。對他們而言<q>自由</q>意味著<q>免費</q>。一本英文字典幾乎能列出
<q>Free</q> 的二十種不同含義。其中只有一個是<q>免費</q>的意思，其餘都與自由和
不強制有關。所以，當我們說<em>自由軟體</em>的時候，我們想傳達的是自由，而不是免費。
</p>

<p>
标榜<q>免费</q>但僅僅意味著您不用付錢就能使用的軟體根本算不上真正的自由。
他們可能會禁止您傳播该软件，而且幾乎可以肯定，他们不会允许您修改该软件。
軟體標榜免費，在市場營銷中通常是作為推銷
相關產品以及打擊競爭對手的一種武器。无法保证它们会永远保持免费。
</p>

<p>
對于新手來說，一個軟體要麼自由，要麼不自由。現實生活要比那個復雜得多。要理
解人們在標榜自由軟件時蘊涵了哪些信息，我們必須繞道去軟體許可証的世界看一看。
</p>

<h2><a id="licenses">软件许可证</a></h2>

<p>
著作權是一種手段，用來保護某些類型工作的[CN:作者:][HKTW:原創者:]的權利。在
大部分國家，您所寫的軟體都是自動受著作權保護的。許可証
是[CN:作者:][HKTW:原創者:]對別人以[CN:作者:][HKTW:原創者:]可以接受的方式
使用他/她的作品（在這裏指軟體）的一种許可。是否包含一個聲明該軟件
使用方式的許可証，這一點取決于[CN:作者:][HKTW:原創者:]。
</p>
# 編註: Copyright = 著作權 or 版權？？ (foka)
# 編注: 版權 - 出版商，著作權 - 作者，開源軟件應該重點保護作者的權利 (s5unty)

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">了解更多关于<q>著作权</q>的内容</a></button></p>

<p>
當然，許可證會視不同的情況而有所不同。軟體公司為了保護它們的資產，通常只會
[CN:發行:][HKTW:釋出:]已編譯過的程式，讓人們無法讀取[CN:源代碼:][HKTW:程式碼:]
，並且在使用上做出很多限制。相对地，自由軟體的作者們通常會提出不同的要求，
有时是以下這些要求的组合：
</p>

<ul>
  <li>不允許將他們的[CN:源代碼:][HKTW:程式碼:]用于[CN:非自由:][HKTW:專屬:]軟體。
既然他們[CN:發布:][HKTW:釋出:]了他們的[CN:源代碼:][HKTW:程式碼:]，让任何人
都可以使用，他们就不希望有人將它佔為己有。在這種情況
下，[CN:源代碼:][HKTW:程式碼:]的使用就是一種信任：您可
以使用這些[CN:源代碼:][HKTW:程式碼:]，只要您遵循相同的規則。</li>
  <li>保護[CN:源代碼:][HKTW:程式碼:]中作者的聲明。這些人對他們做出來的成果感到
十分驕傲，所以不希望有人將他們的名字移除，甚至冒稱是自己寫出來的。</li>
  <li>必须[CN:分发:][HKTW:散布:][CN:源代碼:][HKTW:程式源碼:]。大部分专有軟體
最大的問題就是在於，因為沒有[CN:源代碼:][HKTW:程式源碼:]，您無法修复错误或
进行[CN:定制:][HKTW:客製化:]。而且，商業公司有可能決定
不再[CN:支持:][HKTW:支援:]您使用的硬體。大部分自由軟體的許可證都
要求[CN:分发:][HKTW:散布:][CN:源代碼:][HKTW:程式源碼:]，這樣能保護
使用者[CN:定制:][HKTW:客製化:]该软件以适合自己需要的權利。</li>
  <li>任何包含他們工作成果的軟體（在著作權討論中這被稱為<em>衍生作品</em>）也必
須使用相同的許可證。</li> 
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 三种最常见的自由软件许可证是 <a href="https://www.gnu.org/copyleft/gpl.html">GNU 通用公共许可证（GPL）</a>、<a href="https://opensource.org/blog/license/artistic-2-0">艺术许可证（Artistic License）</a>，以及 <a href="https://opensource.org/blog/license/bsd-3-clause">BSD 许可证</a>。
</aside>

<h2><a id="choose">如何选择许可证？</a></h2>

<p>
有的人會寫他們[CN:自定義:][HKTW:自訂:]的許可證，这可能会引起问题，所以
自由软件社区不喜欢这种行为。有太多人寫出[CN:用詞:][HKTW:用字:]不精確或是条款互相
矛盾的許可證。要寫出一個可以在法庭上有效的許可證就更困難了。所幸的是，有一些已經
寫好的开源软件許可證可供选择。它们有以下共同特点：
</p>

<ul>
  <li>您想在幾台機器上安裝該軟體就可以在幾台機器上安裝該軟體。</li>
  <li>同一時刻可以有任意多的人使用該軟體。</li>
  <li>你想將該軟體復制幾份就可以復制幾份，把它們送給任何你想送的人（自由或開放地
重新[CN:分发:][HKTW:散佈:]）。</li>
  <li>不限制您對該软件的改動（除了保留特定的聲明）。</li>
  <li>用户不仅可以[CN:分发:][HKTW:散布:]该软件，甚至可以[CN:售卖:][HKTW:販賣:]它。</li>
</ul>

<p>
最後一條，說您可以[CN:銷售:][HKTW:販賣:]自由軟體，聽起來似乎跟自由軟體的理
念有衝突，但事實上這是自由軟體的一个重要优势。既然許可證允許軟體的自由[CN:分发
:][HKTW:散布:]，某個人就可以在得到軟體之後自行重新[CN:分发:][HKTW:散布:]，甚至
是嘗試[CN:銷售:][HKTW:販賣:]它們。
</p>

<p>
雖然自由軟體并非完全沒有約束，它仍然提供給使用者很大的彈性完成他們感興趣的工作。
同時，它也保護了作者的權利。這才稱得上是真正的自由。Debian 項目是自由軟體的
鼎力支持者。我们编纂了 <a
href="../social_contract#guidelines">Debian 自由軟體指導方針（DFSG）</a> 來
定義我们眼中的自由軟體。只有遵循這份指導方針的軟體
才能放在 Debian 發行版的 main 軟件倉庫中。
</p>

# 編註:  Franklin 把 Guideline 稱作「綱領」，我也不知道是否比我們原有的
#        「指導方針」好，有待在 mailing list 上討論。 - foka, 2002/11/19
