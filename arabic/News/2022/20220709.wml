#use wml::debian::translation-check translation="4c0e547d89fc6a0f2307d782bbce0a6ac7d74bc2"
<define-tag pagetitle>تحديث دبيان 11: الإصدار 11.4</define-tag>
<define-tag release_date>2022-07-09</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يسعد مشروع دبيان الإعلان عن التحديث الرابع لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أعلنت بشكل منفصل ومشار إليها فقط في هذا الإعلان.
</p>

<p>
يرجى ملاحظة أن هذا التحديث لا يشكّل إصدار جديد لدبيان <release> بل فقط تحديثات لبعض الحزم المضمّنة
وبالتالي ليس بالضرورة رمي الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحزم باستخدام مرآة دبيان محدّثة.
</p>

<p>
الذين يثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحزم،
أغلب التحديثات مضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنيّ إلى هذه المراجعة بتوجيه نظام إدارة الحزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>





<h2>إصلاح العديد من العلاّت</h2>

<p>أضاف هذا التحديث للإصدار المستقر بعض الإصلاحات المهمة للحزم التالية:</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction apache2 "New upstream stable release; fix HTTP request smuggling issue [CVE-2022-26377], out-of-bounds read issues [CVE-2022-28330 CVE-2022-28614 CVE-2022-28615], denial of service issues [CVE-2022-29404 CVE-2022-30522], possible out-of-bounds read issue [CVE-2022-30556], possible IP-based authentication bypass issue [CVE-2022-31813]">
<correction base-files "Update /etc/debian_version for the 11.4 point release">
<correction bash "Fix 1-byte buffer overflow read, causing corrupted multibyte characters in command substitutions">
<correction clamav "New upstream stable release; security fixes [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction clementine "Add missing dependency on libqt5sql5-sqlite">
<correction composer "Fix code injection issue [CVE-2022-24828]; update GitHub token pattern">
<correction cyrus-imapd "Ensure that all mailboxes have a <q>uniqueid</q> field, fixing upgrades to version 3.6">
<correction dbus-broker "Fix buffer overflow issue [CVE-2022-31212]">
<correction debian-edu-config "Accept mail from the local network sent to root@&lt;mynetwork-names&gt;; only create Kerberos host and service principals if they don't yet exist; ensure libsss-sudo is installed on Roaming Workstations; fix naming and visibility of print queues; support krb5i on Diskless Workstations; squid: prefer DNSv4 lookups over DNSv6">
<correction debian-installer "Rebuild against proposed-updates; increase Linux kernel ABI to 16; reinstate some armel netboot targets (openrd)">
<correction debian-installer-netboot-images "Rebuild against proposed-updates; increase Linux kernel ABI to 16; reinstate some armel netboot targets (openrd)">
<correction distro-info-data "Add Ubuntu 22.10, Kinetic Kudu">
<correction docker.io "Order docker.service after containerd.service to fix shutdown of containers; explicitly pass the containerd socket path to dockerd to make sure it doesn't start containerd on its own">
<correction dpkg "dpkg-deb: Fix unexpected end of file conditions on .deb extract; libdpkg: Do not restrict source:* virtual fields to installed packages; Dpkg::Source::Package::V2: Always fix the permissions for upstream tarballs (regression from DSA-5147-1]">
<correction freetype "Fix buffer overflow issue [CVE-2022-27404]; fix crashes [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Fix buffer overflow issues [CVE-2022-25308 CVE-2022-25309]; fix crash [CVE-2022-25310]">
<correction ganeti "New upstream release; fix several upgrade issues; fix live migration with QEMU 4 and <q>security_model</q> of <q>user</q> or <q>pool</q>">
<correction geeqie "Fix Ctrl click inside of a block selection">
<correction gnutls28 "Fix SSSE3 SHA384 miscalculation; fix null pointer deference issue [CVE-2021-4209]">
<correction golang-github-russellhaering-goxmldsig "Fix null pointer dereference caused by crafted XML signatures [CVE-2020-7711]">
<correction grunt "Fix path traversal issue [CVE-2022-0436]">
<correction hdmi2usb-mode-switch "udev: Add a suffix to /dev/video device nodes to disambiguate them; move udev rules to priority 70, to come after 60-persistent-v4l.rules">
<correction hexchat "Add missing dependency on python3-cffi-backend">
<correction htmldoc "Fix infinite loop [CVE-2022-24191], integer overflow issues [CVE-2022-27114] and heap buffer overflow issue [CVE-2022-28085]">
<correction knot-resolver "Fix possible assertion failure in NSEC3 edge-case [CVE-2021-40083]">
<correction libapache2-mod-auth-openidc "New upstream stable release; fix open redirect issue [CVE-2021-39191]; fix crash on reload / restart">
<correction libintl-perl "Really install gettext_xs.pm">
<correction libsdl2 "Avoid out-of-bounds read while loading malformed BMP file [CVE-2021-33657], and during YUV to RGB conversion">
<correction libtgowt "New upstream stable release, to support newer telegram-desktop">
<correction linux "New upstream stable release; increase ABI to 16">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 16">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 16">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 16">
<correction logrotate "Skip locking if state file is world-readable [CVE-2022-1348]; make configuration parsing stricter in order to avoid parsing foreign files such as core dumps">
<correction lxc "Update default GPG key server, fixing creating of containers using the <q>download</q> template">
<correction minidlna "Validate HTTP requests to protect against DNS rebinding attacks [CVE-2022-26505]">
<correction mutt "Fix uudecode buffer overflow issue [CVE-2022-1328]">
<correction nano "Several bug fixes, including fixes for crashes">
<correction needrestart "Make cgroup detection for services and user sessions cgroup v2 aware">
<correction network-manager "New upstream stable release">
<correction nginx "Fix crash when libnginx-mod-http-lua is loaded and init_worker_by_lua* is used; mitigate application layer protocol content confusion attack in the Mail module [CVE-2021-3618]">
<correction node-ejs "Fix server-side template injection issue [CVE-2022-29078]">
<correction node-eventsource "Strip sensitive headers on redirect to different origin [CVE-2022-1650]">
<correction node-got "Don't allow redirection to Unix socket [CVE-2022-33987]">
<correction node-mermaid "Fix cross-site scripting issues [CVE-2021-23648 CVE-2021-43861]">
<correction node-minimist "Fix prototype pollution issue [CVE-2021-44906]">
<correction node-moment "Fix path traversal issue [CVE-2022-24785]">
<correction node-node-forge "Fix signature verification issues [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-raw-body "Fix potential denial of service issue in node-express, by using node-iconv-lite rather than node-iconv">
<correction node-sqlite3 "Fix denial of service issue [CVE-2022-21227]">
<correction node-url-parse "Fix authentication bypass issues [CVE-2022-0686 CVE-2022-0691]">
<correction nvidia-cuda-toolkit "Use OpenJDK8 snapshots for amd64 and ppc64el; check usability of the java binary; nsight-compute: Move the 'sections' folder to a multiarch location; fix nvidia-openjdk-8-jre version ordering">
<correction nvidia-graphics-drivers "New upstream release; switch to upstream 470 tree; fix denial of service issues [CVE-2022-21813 CVE-2022-21814]; fix out-of-bounds write issue [CVE-2022-28181], out-of-bounds read issue [CVE-2022-28183], denial of service issues [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; fix out-of-bound write issues [CVE-2022-28181 CVE-2022-28185]">
<correction nvidia-graphics-drivers-tesla-418 "New upstream stable release">
<correction nvidia-graphics-drivers-tesla-450 "New upstream stable release; fix out-of-bounds write issues [CVE-2022-28181 CVE-2022-28185], denial of service issue [CVE-2022-28192]">
<correction nvidia-graphics-drivers-tesla-460 "New upstream stable release">
<correction nvidia-graphics-drivers-tesla-470 "New package, switching Tesla support to upstream 470 tree; fix out-of-bounds write issue [CVE-2022-28181], out-of-bounds read issue [CVE-2022-28183], denial of service issues [CVE-2022-28184 CVE-2022-28191 CVE-2022-28192]">
<correction nvidia-persistenced "New upstream release; switch to upstream 470 tree">
<correction nvidia-settings "New upstream release; switch to upstream 470 tree">
<correction nvidia-settings-tesla-470 "New package, switching Tesla support to upstream 470 tree">
<correction nvidia-xconfig "New upstream release">
<correction openssh "seccomp: add pselect6_time64 syscall on 32-bit architectures">
<correction orca "Fix usage with webkitgtk 2.36">
<correction php-guzzlehttp-psr7 "Fix improper header parsing [CVE-2022-24775]">
<correction phpmyadmin "Fix some SQL queries generating a server error">
<correction postfix "New upstream stable release; do not override user set default_transport in postinst; if-up.d: do not error out if postfix can't send mail yet">
<correction procmail "Fix null pointer dereference">
<correction python-scrapy "Don't send authentication data with all requests [CVE-2021-41125]; don't expose cookies cross-domain when redirecting [CVE-2022-0577]">
<correction ruby-net-ssh "Fix authentication against systems using OpenSSH 8.8">
<correction runc "Honour seccomp defaultErrnoRet; do not set inheritable capabilities [CVE-2022-29162]">
<correction samba "Fix winbind start failure when <q>allow trusted domains = no</q> is used; fix MIT Kerberos authentication; fix share escape issue via mkdir race condition [CVE-2021-43566]; fix possible serious data corruption issue due to Windows client cache poisoning; fix installation on non-systemd systems">
<correction tcpdump "Update AppArmor profile to allow access to *.cap files, and handle numerical suffix in filenames added by -W">
<correction telegram-desktop "New upstream stable release, restoring functionality">
<correction tigervnc "Fix GNOME desktop start up when using tigervncserver@.service; fix colour display when vncviewer and X11 server use different endianness">
<correction twisted "Fix information disclosure issue with cross-domain redirects [CVE-2022-21712], denial of service issue during SSH handshakes [CVE-2022-21716], HTTP request smuggling issues [CVE-2022-24801]">
<correction tzdata "Update timezone data for Palestine; update leap second list">
<correction ublock-origin "New upstream stable release">
<correction unrar-nonfree "Fix directory traversal issue [CVE-2022-30333]">
<correction usb.ids "New upstream release; update included data">
<correction wireless-regdb "New upstream release; remove diversion added by the installer, ensuring that files from the package are used">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبق لفريق الأمان نشر تنبيه لكل تحديث:
</p>

<table border=0>
<tr><th>معرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2021 4999 asterisk>
<dsa 2021 5026 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5107 php-twig>
<dsa 2022 5108 tiff>
<dsa 2022 5110 chromium>
<dsa 2022 5111 zlib>
<dsa 2022 5112 chromium>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5114 chromium>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5116 wpewebkit>
<dsa 2022 5117 xen>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5120 chromium>
<dsa 2022 5121 chromium>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5124 ffmpeg>
<dsa 2022 5125 chromium>
<dsa 2022 5127 linux-signed-amd64>
<dsa 2022 5127 linux-signed-arm64>
<dsa 2022 5127 linux-signed-i386>
<dsa 2022 5127 linux>
<dsa 2022 5128 openjdk-17>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5130 dpdk>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5133 qemu>
<dsa 2022 5134 chromium>
<dsa 2022 5136 postgresql-13>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5148 chromium>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5155 wpewebkit>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5161 linux-signed-amd64>
<dsa 2022 5161 linux-signed-arm64>
<dsa 2022 5161 linux-signed-i386>
<dsa 2022 5161 linux>
<dsa 2022 5162 containerd>
<dsa 2022 5163 chromium>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5166 slurm-wlm>
<dsa 2022 5167 firejail>
<dsa 2022 5168 chromium>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5174 gnupg2>
</table>


<h2>الحزم المزالة</h2>

<p>
الحزم التالية أزيلت لأسباب خارجة عن سيطرتنا:
</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction elog "Unmaintained; security issues">
<correction python-hbmqtt "Unamintained and broken">

</table>

<h2>مُثبِّت دبيان</h2>
<p>
حدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحزم المغيّرة في هذه المراجعة:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحرة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو إرسال بريد إلكتروني إلى &lt;press@debian.org&gt;
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.
</p>
