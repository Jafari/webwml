#use wml::debian::template title="مدخل إلى دبيان" MAINPAGE="true" FOOTERMAP="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>دبيان هو مجتمع</h2>
      <p>
      آلاف المتطوعين حول العالم يعملون معا على نظام تشغيل دبيان مع إعطاء الأولوية للبرمجيات الحرة والمفتوحة المصدر. انضم لمشروع دبيان.
      </p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">الفاعلون:</a>
          من نحن وماذا نعمل
        </li>
        <li>
          <a href="philosophy">فلسفتنا:</a>
          لماذا وكيف نقوم بهذا العمل
        </li>
        <li>
          <a href="../devel/join/">انخرط:</a>
          كيف يمكن أن تصير مساهما في دبيان
        </li>
        <li>
          <a href="help">ساهم:</a>
          كيف تساعد دبيان
        </li>
        <li>
          <a href="../social_contract">عقد دبيان الاجتماعي:</a>
          أجندتنا الاخلاقية
        </li>
        <li>
          <a href="diversity">Everyone is welcome:</a>
          Debian's Diversity Statement
        </li>
        <li>
          <a href="../code_of_conduct">للمشاركين:</a>
          قواعد دبيان السلوكية
        </li>
        <li>
          <a href="../partners/">الشركاء:</a>
          الشركات والمؤسسات المساندة لمشروع دبيان
        </li>
        <li>
          <a href="../donations">التبرعات:</a>
          كيف تدعم مشروع دبيان
        </li>
        <li>
          <a href="../legal/">المعلومات القانونية:</a>
          الرخص و العلامات التجارية و سياسة الخصوصية و سياسة براءة الاختراع إلخ.
        </li>
        <li>
          <a href="../contact">اتصل بنا:</a>
          كيف تتواصل معنا
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>دبيان هو نظام تشغيل</h2>
      <p>
      دبيان هو نظام تشغيل حُر مطوّر ومصان من طرف مشروع دبيان وهو توزيعة لينكس مع العديد من التطبيقات التي تلائم احتياجات المستخدمين.
      </p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">تنزيل:</a>
          أين تحصل على دبيان
        </li>
        <li>
          <a href="why_debian">لماذا دبيان:</a>
          دوافع اختيار دبيان
        </li>
        <li>
          <a href="../support">الدعم:</a>
          أين تجد المساعدة
        </li>
        <li>
          <a href="../security">الأمان:</a>
          آخر تحديث <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">البرمجيات:</a>
          ابحث وتصفح القائمة الطويلة لبرمجياتنا
        </li>
        <li>
          <a href="../doc">التوثيق:</a>
          كيفية التثبيت و الأسئلة الشائعة و الارشادات و الويكي و المزيد
        </li>
        <li>
          <a href="../bugs">نظام تتبع العلل (BTS):</a>
          كيفية التبليغ عن علة، توثيق BTS
        </li>
        <li>
          <a href="https://lists.debian.org/">القوائم البريدية:</a>
          تجميعة لقوائم دبيان البريدية للمستخدمين و المطورين إلخ.
        </li>
        <li>
          <a href="../blends">الخلطات النقية:</a>
          حزم تعريف خاصة باحتياجات معينة
        </li>
        <li>
          <a href="../devel">ركن المطورين:</a>
          المعلومات التي تهم مطوري دبيان بشكل أساسي
        </li>
        <li>
          <a href="../ports">النُّقول والبُنى:</a>
          دعم دبيان للعديد من بُنى المعالجات المتنوعة
        </li>
        <li>
          <a href="search">البحث:</a>
          معلومات حول كيفية استخدام محرك بحث دبيان
        </li>
        <li>
          <a href="cn">اللغات:</a>
          الإعدادات اللغوية لموقع دبيان
        </li>
      </ul>
    </div>
  </div>

</div>

