#use wml::debian::template title="Что означает Free?" MAINPAGE="true"
#use wml::debian::translation-check translation="8d8a7b1eda812274f83d0370486ddb709d4f7d54" maintainer="Lev Lamberov"

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Free as in...?</a></li>
    <li><a href="#licenses">Лицензии программного обеспечения</a></li>
    <li><a href="#choose">Как выбрать лицензию?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> В феврале 1998 года термин <a href="https://www.gnu.org/philosophy/free-sw">Free Software</a> был заменён на <a href="https://opensource.org/osd">Open Source Software</a>. Этот спор о терминологии отражает подлежащие философские различия между двумя точка зрения, но практические требования к лицензиями на ПО, а также текст на данном сайте по сути представляют одно и то же как для Свободного ПО, так и для ПО с открытым исходным кодом.</p>
</aside>


<h2><a id="freesoftware">Free as in...?</a></h2>

<p>
Многие люди, которые ещё не работали со свободным программным обеспечением озадачены тем,
что означает слово "free". Оно означает не то, чего они ожидают &mdash; "free" означает для них
"бесплатное". Однако если мы обратимся к словарю английского языка, то в нём мы увидим почти двадцать
разных значения для термина "free", и только одно из них означает "бесплатное". Остальные
значения имеют смысл "свобода" и "отсутствие ограничений". Когда мы говорим о
Free Software, мы имеем в виду свободу, а не стоимость.
</p>

<p>
Программное обеспечение, которое является свободным в том смысле, что вы не должны
платить за его использование, едва ли является свободным. Вам могут запретить передавать
его другим и кроме этого, почти наверняка, изменять. Программное обеспечение, которое
лицензировано с нулевой стоимостью, обычно служит оружием для маркетинговых кампаний,
целью которых является продвижение связанного продукта или борьба с конкурентами. Нет
никаких гарантий, что оно в дальнейшем останется бесплатным.
</p>

<p>
Кажется, чего уж проще, или часть программного обеспечения свободна или
нет. Однако, в реальной жизни всё сложнее. Чтобы понять, что разные люди
имеют в виду, когда говорят про свободное программное обеспечение, мы
совершим маленький экскурс в мир лицензий для программного обеспечения.
</p>


<h2><a id="licenses">Лицензии программного обеспечения</a></h2>

<p>
Авторские права (copyright) &mdash; это метод защиты прав того, кто выполнил
определенные типы работ. В большинстве стран, новое программное обеспечение
автоматически защищено авторским правом. Лицензия &mdash; это способ, которым
автор разрешает другим использование своей работы (в нашем случае программного
обеспечения), теми способами, которые он находит приемлемыми. Автор добавляет к программе
лицензию, где описываются то, как может использоваться эта программа.
</p>


<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Ознакомьтесь с дополнительной информацией об авторском праве</a></button></p>

<p>
Разумеется, что различные обстоятельства, требуют разных лицензий.
Компании, которые разрабатывают программное обеспечение, заботятся о защите
своих активов и поэтому они выпускают только скомпилированный код, который
не может прочесть человек. Также они накладывают множество ограничений на использование
их программного обеспечения. С другой стороны, авторы свободного программного
обеспечения по большей части стремятся к другому, что иногда является комбинацией
следующий моментов:
</p>

<ul>
  <li>Не разрешают использовать свой код в закрытом программном обеспечении. Так как они отдают свой код для использования всеми, то они не хотят видеть, что кто-то крадет их код. В этом случае использование кода основано на доверии: вы можете использовать его, если играете по тем же правилам.</li>
  <li>Защищают идентичность авторства. Люди гордятся своей работой и не хотят чтобы кто-либо удалил их имя из кода или надпись, что это писали они.</li>
  <li>Распространяют исходный текст. Одна из проблем с проприетарным программным обеспечением заключается в том, что вы не можете исправить ошибки или что-либо скорректировать, так как исходный код недоступен. Кроме этого, компания может перестать поддерживать то аппаратное обеспечение, с которым вы работаете. Многие свободные лицензии заставляют распространять исходный код. Это защищает пользователя, в том плане, что он может скорректировать программное обеспечение под свои нужды.</li>
  <li>Заставляют использовать ту же самую лицензию для других работ, в которых как часть, используется их работа (такие работы называются <q>производные работы</q> в дискуссиях об авторских правах).</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Тремя наиболее широко используемыми лицензиями свободного программного обеспечения являются <a href="https://www.gnu.org/copyleft/gpl.html">Генеральная общественная лицензия (GNU General Public License) или GPL</a>, <a href="https://opensource.org/blog/license/artistic-2-0">Артистическая лицензия (Artistic License)</a> и <a href="https://opensource.org/blog/license/bsd-3-clause">лицензия в стиле BSD</a>.
</aside>

<h2><a id="choose">Как выбрать лицензию?</a></h2>

<p>
Иногда люди пишут собственные лицензии, которые описывают то, что они хотят. Однако,
при этом возникают некоторые проблемы. Зачастую формулировки, которые используются
в таких лицензиях понимаются неоднозначно или в лицензии указываются условия, которые
противоречат друг другу. Написать же лицензию, которую можно было бы отстаивать в
суде &mdash; это еще более тяжёлое занятие. К счастью, уже есть достаточно большое
количество готовых лицензий, которые, почти наверняка, описывают то, что вы хотите.
</p>

<ul>
  <li>Пользователи могут устанавливать программное обеспечение на столько машин, насколько они хотят.</li>
  <li>В одно и то же время это программное обеспечение может использоваться любым количеством людей.</li>
  <li>Пользователи могут делать столько копий программного обеспечения, сколько они хотят или сколько им нужно, а также могут передавать эти копии другим пользователям (свободно или открытое распространение).</li>
  <li>Нет никаких ограничений на изменение программного обеспечения (за исключением указания авторов).</li>
  <li>Пользователи могут не только распространять программное обеспечение, они даже могут его продавать.</li>
</ul>

<p>
В особенности последний пункт, позволяющий людям продавать программное обеспечение,
как кажется, идёт в разрез в идеей свободного программного обеспечения. Однако в действительности
это одна из сильных сторон этой идеи. Так как лицензия позволяет свободное распространение,
когда кто-то получает копию, он может и распространять её. Люди
даже могут попытаться её продать.
</p>

<p>
Свободное программное обеспечение не полностью свободно от ограничений, но оно
дает пользователю возможность делать то, что ему нужно, чтобы выполнить
работу. При этом, оно защищает права автора. Вот это &mdash; настоящая свобода.
Проект Debian и его участники являются убеждёнными сторонниками
свободного программного обеспечения. Мы создали <a
href="../social_contract#guidelines">Критерии Debian по определению свободного
ПО (DFSG)</a>, чтобы описать то, что, по нашему мнению, представляет
собой свободное программное обеспечение. Только то программное обеспечение, которое соответствует
DFSG разрешается помещать в секцию main дистрибутива Debian.
</p>
