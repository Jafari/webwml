#use wml::debian::cdimage title="Сетевая установка с минимальным USB, компакт-диском"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"

#use wml::debian::translation-check translation="678a572e92767d98d57f67822e00905782bf9637" maintainer="Lev Lamberov"

<div class="tip">
<p>На архитектурах i386 и amd64 все образы USB/CD/DVD можно использовать также для
<a href="https://www.debian.org/CD/faq/#write-usb">USB-носителей</a>.</div>

<p><q>CD для сетевой установки</q>, или <q>netinst</q>&nbsp;&mdash; это один компакт-диск,
позволяющий установить всю операционную систему. Он содержит только
самое необходимое программное обеспечение для того, позволяющее
установить базовую систему и загрузить все остальные пакеты по сети.</p>

<p><strong>Какие типы сетевых соединений поддерживаются
во время установки?</strong>
Поддерживаются различные типы сетевых соединений, например
доступ по локальной сети и WLAN (с некоторыми ограничениями).</p>

<p>Для закачки доступны следующие минимальные образы загрузочных дисков:</p>

<ul>

<li>Официальные образы <q>netinst</q> для <q>стабильного выпуска</q> &mdash; <a
  href="#netinst-stable">смотрите ниже</a></li>

<li>Образы для <q>тестируемого выпуска</q> &mdash; так и
  известные рабочие срезы, смотрите на странице <a
  href="$(DEVEL)/debian-installer/">Debian-Installer</a>.</li>

</ul>


<h2 id="netinst-stable">Официальные образы netinst для <q>стабильного выпуска</q></h2>

<p>Имеют размер до 300&nbsp;МБ, образы содержат программу установки и небольшой
набор пакетов, позволяющих установить только основную систему.</p>

<div class="line">
<div class="item col50">
<p><strong>CD образ netinst</strong></p>
         <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>CD образ netinst (через <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
         <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Информацию об этих файлах и о том, как их использовать, можно
найти в <a href="../faq/">FAQ</a>.</p>

<p>После загрузки образов ознакомьтесь, пожалуйста, с
<a href="$(HOME)/releases/stable/installmanual">подробной
информацией о процессе установки</a>.</p>
