#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664"
<define-tag pagetitle>Обновлённый Debian 12: выпуск 12.5</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о пятом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction apktool "Предотвращение записи в произвольные файлы с вредоносными именами ресурсов [CVE-2024-21633]">
<correction atril "Исправление аварийной остановки при открытии некоторых файлов в формате epub; исправление загрузки указателя для некоторых документов в формате epub; добавление в функцию check_mime_type альтернативного варианта обработки для вредоносных файлов в формате epub; использование libarchive вместо внешней команды для распаковки документов [CVE-2023-51698]">
<correction base-files "Обновление для редакции 12.5">
<correction caja "Исправление артефактов при отрисовке рабочего стола после изменения разрешения экрана; исправление использование формата даты <q>неформальный</q>">
<correction calibre "Исправление ошибки <q>Ввод HTML: по умолчанию не добавлять ресурсы, существующей за пределами иерархии папки с корнем в родительской папке входного HTML-файла</q> [CVE-2023-46303]">
<correction compton "Удаление рекомендации пакета picom">
<correction cryptsetup "cryptsetup-initramfs: добавление поддержки для сжатых модулей ядра; cryptsetup-suspend-wrapper: не выводить сообщение об ошибке при отсутствии каталога /lib/systemd/system-sleep; add_modules(): изменение логики удаление суффикса в соответствии с пакетом initramfs-tools">
<correction debian-edu-artwork "Предоставление темы Emerald на основе темы для Debian Edu 12">
<correction debian-edu-config "Новый выпуск основной ветки разработки">
<correction debian-edu-doc "Обновление поставляемой документации и переводов">
<correction debian-edu-fai "Новый выпуск основной ветки разработки">
<correction debian-edu-install "Новый выпуск основной ветки разработки; исправление адреса репозитория с исправлениями безопасности в sources.list">
<correction debian-installer "Увеличение ABI ядра Linux до 6.1.0-18; повторная сборка с учётом proposed-updates">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debian-ports-archive-keyring "Добавление ключа Debian Ports Archive Automatic Signing Key (2025)">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction dropbear "Исправление <q>атаки terrapin</q> [CVE-2023-48795]">
<correction engrampa "Исправление нескольких утечек памяти; исправление функциональности <q>save as</q> при работе с архивами">
<correction espeak-ng "Исправление переполнения буфера [CVE-2023-49990 CVE-2023-49992 CVE-2023-49993], отрицательного переполнения буфера [CVE-2023-49991], исключения плавающей точки [CVE-2023-49994]">
<correction filezilla "Предотвращение эксплоита <q>Terrapin</q> [CVE-2023-48795]">
<correction fish "Безопасная обработка непечатных символов в кодировке Unicode при передаче подстановки команд [CVE-2023-49284]">
<correction fssync "Отключение нестабильных тестов">
<correction gnutls28 "Исправление ошибки утверждения при проверке цепочки сертификатов с циклом перекрёстных подписей [CVE-2024-0567]; исправление проблемы с таймингом, доступной через сторонний канал [CVE-2024-0553]">
<correction indent "Исправление чтения за пределами буфера [CVE-2024-0911]">
<correction isl "Исправление использования на старых ЦП">
<correction jtreg7 "Новый пакет с исходным кодов для поддержки сборки openjdk-17">
<correction libdatetime-timezone-perl "Обновление поставляемых данных и временных зонах">
<correction libde265 "Исправление переполнения буфера [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libfirefox-marionette-perl "Исправление совместимости с более новыми версиями firefox-esr">
<correction libmateweather "Исправление URL для aviationweather.gov">
<correction libspreadsheet-parsexlsx-perl "Исправление возможной бомбы памяти [CVE-2024-22368]; исправление ошибки с внешней сущностью XML [CVE-2024-23525]">
<correction linux "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 18">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 18">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 18">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки; увеличение ABI до 18">
<correction localslackirc "Отправлять авторизацию и заголовки куки веб-сокету">
<correction mariadb "Новый стабильный выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2023-22084]">
<correction mate-screensaver "Исправление утечек памяти">
<correction mate-settings-daemon "Исправление утечек памяти; ослабление ограничений высоких DPI; исправление обработки множественных событий rfkill">
<correction mate-utils "Исправление различных утечек памяти">
<correction monitoring-plugins "Исправление дополнения check_http при использовании опции <q>--no-body</q> в ситуации, когда ответ основной ветки разделён на части">
<correction needrestart "Исправление регрессии проверки микрокода на ЦП AMD">
<correction netplan.io "Исправление автоматических тестов при использовании новых версий systemd">
<correction nextcloud-desktop "Исправление <q>не удаётся синхронизировать файлы, имена которых содержат специальные символы вида ':'</q>; исправление уведомлений о двухфакторной аутентификации">
<correction node-yarnpkg "Исправление использования с Commander 8">
<correction onionprobe "Исправление инициализации Tor при использовании хешированных паролей">
<correction pipewire "Использование функции malloc_trim() в случаях, когда можно освободить память">
<correction pluma "Исправление утечек памяти; исправление двойной активации расширений">
<correction postfix "Новый стабильный выпуск основной ветки разработки; решение проблемы с подделкой SMTP [CVE-2023-51764]">
<correction proftpd-dfsg "Реализация исправления для атаки Terrapin [CVE-2023-48795]; исправление чтения за пределами выделенного буфера памяти [CVE-2023-51713]">
<correction proftpd-mod-proxy "Реализации исправления для атаки Terrapin [CVE-2023-48795]">
<correction pypdf "Исправление бесконечного цикла [CVE-2023-36464]">
<correction pypdf2 "Исправление бесконечного цикла [CVE-2023-36464]">
<correction pypy3 "Предотвращение ошибки утверждения rpython в JIT, если в цикле не пересекаются диапазоны целых чисел">
<correction qemu "Новый стабильный выпуск основной ветки разработки; virtio-net: корректное копирование заголовка vnet при сбросе TX [CVE-2023-6693]; исправление разыменования null-указателя [CVE-2023-6683]; отмена заплаты, вызывающей регрессии в функциональности suspend / resume">
<correction rpm "Включение движка BerkeleyDB в режиме только для чтения">
<correction rss-glx "Установка хранителей экрана в каталог /usr/libexec/xscreensaver; вызов функции GLFinish() до функции glXSwapBuffers()">
<correction spip "Исправление двух проблем с межсайтовым скриптингом">
<correction swupdate "Предотвращение получения прав суперпользователя через несоответствующий режим сокета">
<correction systemd "Новый выпуск основной ветки разработки; исправление отсутствия проверки в systemd-resolved [CVE-2023-7008]">
<correction tar "Исправление проверки границ массива в декодере base-256 [CVE-2022-48303], обработка префиксов расширенных заголовков [CVE-2023-39804]">
<correction tinyxml "Исправление ошибки утверждения [CVE-2023-34194]">
<correction tzdata "Новый выпуск основной ветки разработки">
<correction usb.ids "Обновление поставляемого списка данных">
<correction usbutils "Исправление ошибки, при которой usb-devices не выводит список всех устройств">
<correction usrmerge "Очистка каталогов с поддержкой двух архитектур, когда она не требуется; не запускать повторно convert-etc-shells на системах, где преобразование уже произведено; обработка смонтированного каталога /lib/modules на Xen-системах; улучшение сообщений об ошибках; добавление конфликтов с указанием версий libc-bin, dhcpcd, libparted1.8-10 и lustre-utils">
<correction wolfssl "Исправление проблемы безопасности, когда клиент не отправляет ни расширения PSK, ни расширения KSE [CVE-2023-3724]">
<correction xen "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2023-46837 CVE-2023-46839 CVE-2023-46840]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5578 ghostscript>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5583 gst-plugins-bad1.0>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5589 node-undici>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5593 linux-signed-amd64>
<dsa 2024 5593 linux-signed-arm64>
<dsa 2024 5593 linux-signed-i386>
<dsa 2024 5593 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5601 php-phpseclib3>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5607 chromium>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5609 slurm-wlm>
<dsa 2024 5610 redis>
<dsa 2024 5611 glibc>
<dsa 2024 5612 chromium>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>



<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
