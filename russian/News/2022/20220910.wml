#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.13</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о тринадцатом обновлении своего
предыдущего стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>После выпуска данной редакции команды безопасности и выпусков Debian более не
будут работать над обновлениями Debian 10. Пользователи, желающие продолжать
получать поддержку безопасности, должны выполнить обновление до Debian 11, либо
обратиться к странице <url "https://wiki.debian.org/LTS">, на которой приводится
подмножество архитектур и пакетов, которые будут поддерживаться в рамках проекта долгосрочной поддержки.
</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное обновление предыдущего стабильного выпуска вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction adminer "Исправление проблемы с перенаправлением, проблем с межсайтовым скриптингом [CVE-2020-35572 CVE-2021-29625]; elasticsearch: не выводить ответ, если код HTTP не 200 [CVE-2021-21311]; предоставление скомпилированной версии и файлов настройки">
<correction apache2 "Исправление отказа в обслуживании [CVE-2022-22719], подделки HTTP-запросов [CVE-2022-22720], переполнения целых чисел issue [CVE-2022-22721], записи за пределы выделенного буфера памяти issue [CVE-2022-23943], подделки HTTP-запросов [CVE-2022-26377], чтения за пределами выделенного буфера памяти [CVE-2022-28614 CVE-2022-28615], отказа в обслуживании [CVE-2022-29404], чтения за пределами выделенного буфера памяти [CVE-2022-30556], возможного обхода аутентификации на основе IP [CVE-2022-31813]">
<correction base-files "Обновление для редакции 10.13">
<correction clamav "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction commons-daemon "Исправление определения JVM">
<correction composer "Исправление введения кода [CVE-2022-24828]; обновления шаблона токена GitHub; использование заголовка Authorization вместо устаревшего параметра запроса access_token">
<correction debian-installer "Повторная сборка с учётом buster-proposed-updates; увеличение номера версии ABI Linux до 4.19.0-21">
<correction debian-installer-netboot-images "Повторная сборка с учётом buster-proposed-updates; увеличение номера версии ABI Linux до 4.19.0-21">
<correction debian-security-support "Обновление статуса безопасности различных пакетов">
<correction debootstrap "Гарантирование возможности создания chroot-окружений без объединённого каталога usr для старых выпусков и сборочных служб">
<correction distro-info-data "Добавление Ubuntu 22.04 LTS, Jammy Jellyfish и Ubuntu 22.10, Kinetic Kudu">
<correction dropbear "Исправление возможной проблемы с упорядочением имён пользователей [CVE-2019-12953]">
<correction eboard "Исправление ошибки сегментирования при выборе движка">
<correction esorex "Исправление ошибок тестирования на armhf и ppc64el, вызванных неправильным использованием libffi">
<correction evemu "Исправление ошибки сборки с новыми версиями ядра">
<correction feature-check "Исправление сравнения некоторых номеров версий">
<correction flac "Исправление записи за пределами выделенного буфера памяти issue [CVE-2021-0561]">
<correction foxtrotgps "Исправление ошибки сборки с новыми версиями imagemagick">
<correction freeradius "Исправление утечки через сторонние каналы, когда 1 в 2048 рукопожатиях завершается ошибкой [CVE-2019-13456], отказа в обслуживании из-за многопоточного доступа к BN_CTX [CVE-2019-17185], аварийной остановки из-за выделения памяти без защиты потоков">
<correction freetype "Исправление переполнения буфера [CVE-2022-27404]; исправление аварийных остановок [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Исправление переполнения буфера [CVE-2022-25308 CVE-2022-25309]; исправление аварийных остановок [CVE-2022-25310]">
<correction ftgl "Не пытаться преобразовывать PNG в EPS для latex, так как в нашей версии imagemagick отключена поддержка EPS в целях обеспечения безопасности">
<correction gif2apng "Исправление переполнений динамической памяти [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction gnucash "Исправление ошибки сборки с новыми версиями tzdata">
<correction gnutls28 "Исправление тестов с OpenSSL 1.1.1e и более новыми версиями">
<correction golang-github-docker-go-connections "Пропуск тестов, использующих устаревшие сертификаты">
<correction golang-github-pkg-term "Исправление сборки на новых ядрах ветки 4.19">
<correction golang-github-russellhaering-goxmldsig "Исправление разыменования NULL-указателя [CVE-2020-7711]">
<correction grub-efi-amd64-signed "Новый выпуск основной ветки разработки">
<correction grub-efi-arm64-signed "Новый выпуск основной ветки разработки">
<correction grub-efi-ia32-signed "Новый выпуск основной ветки разработки">
<correction grub2 "Новый выпуск основной ветки разработки">
<correction htmldoc "Исправление бесконечного цикла [CVE-2022-24191], переполнения целых чисел [CVE-2022-27114] и переполнения динамической памяти [CVE-2022-28085]">
<correction iptables-netflow "Исправление регрессии сборки DKMS, вызванной изменениями в основной ветке ядра 4.19.191">
<correction isync "Исправление переполнений буфера [CVE-2021-3657]">
<correction kannel "Исправление ошибки сборки путём отключения создания документации в формате Postscript">
<correction krb5 "Использование SHA256 в качестве Pkinit CMS Digest">
<correction libapache2-mod-auth-openidc "Улучшение проверки post-logout параметра URL при выходе [CVE-2019-14857]">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libhttp-cookiejar-perl "Исправление ошибки сборки путём увеличения даты устаревания куки для тестирования">
<correction libnet-freedb-perl "Изменение узла по умолчанию с неработающего freedb.freedb.org на gnudb.gnudb.org">
<correction libnet-ssleay-perl "Исправление ошибок тестирования с OpenSSL 1.1.1n">
<correction librose-db-object-perl "Исправление ошибок тестирования после 6/6/2020">
<correction libvirt-php "Исправление ошибки сегментирования в libvirt_node_get_cpu_stats">
<correction llvm-toolchain-13 "Новый пакет с исходным кодом для поддержки сборки новых версий firefox-esr и thunderbird">
<correction minidlna "Проверка HTTP-запросов с целью защиты от переназначения DNS [CVE-2022-26505]">
<correction mokutil "Новая версия основной ветки разработки, позволяющая управлять SBAT">
<correction mutt "Исправление переполнения буфера uudecode [CVE-2022-1328]">
<correction node-ejs "Очистка опций и новых объектов [CVE-2022-29078]">
<correction node-end-of-stream "Временное решение ошибки тестирования">
<correction node-minimist "Исправление загрязнения прототипа [CVE-2021-44906]">
<correction node-node-forge "Исправление проверки подписи [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-require-from-string "Исправление теста с nodejs &gt;= 10.16">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки">
<correction nvidia-graphics-drivers-legacy-390xx "Новый выпуск основной ветки разработки; исправление записи за пределами выделенного буфера памяти [CVE-2022-28181 CVE-2022-28185]; исправления безопасности [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction octavia "Исправление проверок клиентского сертификата [CVE-2019-17134]; правильное определение того, что агент работает в системе Debian; исправление шаблона, создающего проверочный сценарий vrrp; добавление дополнительных зависимостей времени исполнения; поставка дополнительных настроек в пакете с агентом">
<correction orca "Исправление использования с WebKitGTK 2.36">
<correction pacemaker "Обновление версий отношения для исправления обновлений с stretch LTS">
<correction pglogical "Исправление ошибки сборки">
<correction php-guzzlehttp-psr7 "Исправление неправильного грамматического разбора заголовка [CVE-2022-24775]">
<correction postfix "Новый выпуск основной ветки разработки; не изменять установленный пользователем параметр default_transport; if-up.d: не выводить ошибку, если postfix пока не может отправить почту; исправление дублирующих записей bounce_notice_recipient в выводе postconf">
<correction postgresql-common "pg_virtualenv: запись временного файла с паролем до изменения владельца этого файла">
<correction postsrsd "Исправление потенциального отказа в обслуживании, когда Postfix отправляет некоторые длинные поля с данными, такие как объединённые адреса электронной почты [CVE-2021-35525]">
<correction procmail "Исправление разыменования NULL-указателя">
<correction publicsuffix "Обновление поставляемых данных">
<correction python-keystoneauth1 "Обновление тестов с целью исправить ошибку сборки">
<correction python-scrapy "Не отправлять данные аутентификации со всеми запросами [CVE-2021-41125]; не раскрывать междоменные куки при перенаправлении [CVE-2022-0577]">
<correction python-udatetime "Правильная компоновка с библиотекой libm">
<correction qtbase-opensource-src "Исправление setTabOrder для составных виджетов; добавление ограничения раскрытия для XML-сущностей [CVE-2015-9541]">
<correction ruby-activeldap "Добавление отсутствующей зависимости от ruby-builder">
<correction ruby-hiredis "Пропуск некоторых ненадёжных тестов для того, чтобы исправить ошибку сборки">
<correction ruby-http-parser.rb "Исправление ошибки сборки при использовании http-parser, содержащего исправление для CVE-2019-15605">
<correction ruby-riddle "Разрешить использование <q>LOAD DATA LOCAL INFILE</q>">
<correction sctk "Использовать <q>pdftoppm</q> вместо <q>convert</q> для преобразования PDF в JPEG, так как последняя утилита завершает работу с ошибкой из-за изменения политики безопасности ImageMagick">
<correction twisted "Исправление неправильного метода проверки URI и HTTP [CVE-2019-12387], неправильной проверки сертификата в поддержке XMPP [CVE-2019-12855], отказа в обслуживании в HTTP/2, подделки HTTP-запросов [CVE-2020-10108 CVE-2020-10109 CVE-2022-24801], раскрытия информации при междоменных перенаправлениях [CVE-2022-21712], отказа в обслуживании во время рукопожатия SSH [CVE-2022-21716]">
<correction tzdata "Обновление часовых поясов для Ирана, Чили и Палестины; обновление списка корректировочных секунд">
<correction ublock-origin "Новый стабильный выпуск основной ветки разработки">
<correction unrar-nonfree "Исправление обхода каталога [CVE-2022-30333]">
<correction wireshark "Исправление удалённого выполнения кода [CVE-2021-22191], отказа в обслуживании [CVE-2021-4181 CVE-2021-4184 CVE-2021-4185 CVE-2022-0581 CVE-2022-0582 CVE-2022-0583 CVE-2022-0585 CVE-2022-0586]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 4836 openvswitch>
<dsa 2021 4852 openvswitch>
<dsa 2021 4906 chromium>
<dsa 2021 4911 chromium>
<dsa 2021 4917 chromium>
<dsa 2021 4981 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5077 librecad>
<dsa 2022 5080 snapd>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5108 tiff>
<dsa 2022 5109 faad2>
<dsa 2022 5111 zlib>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5126 ffmpeg>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5135 postgresql-11>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5144 condor>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5167 firejail>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5173 linux-latest>
<dsa 2022 5173 linux-signed-amd64>
<dsa 2022 5173 linux-signed-arm64>
<dsa 2022 5173 linux-signed-i386>
<dsa 2022 5173 linux>
<dsa 2022 5174 gnupg2>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5185 mat2>
<dsa 2022 5186 djangorestframework>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction elog "Не сопровождается; проблемы безопасности">
<correction libnet-amazon-perl "Зависит от удалённого API">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию предыдущего стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий предыдущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Предлагаемые обновления для предыдущего стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Информация о предыдущем стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
