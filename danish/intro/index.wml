#use wml::debian::template title="Introduktion til Debian" MAINPAGE="true" FOOTERMAP="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>Debian er et fællesskab</h2>
      <p>Tusinder af frivillige over hele verden arbejder i fællesskab på 
      styresystemet Debian, hvor fri software og open source-software 
      prioriteres.  Mød Debian-projektet.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">Personer:</a>
          Hvem vi er og hvad vi laver
        </li>
        <li>
          <a href="philosophy">Vores filosofi:</a>
	  Hvorfor vi gør det og hvordan vi gør det
        </li>
        <li>
          <a href="../devel/join/">Bliv involveret:</a>
	  Hvordan man bliver bidragsyder til Debian
        </li>
        <li>
          <a href="help">Bidrag:</a>
	  Hvordan du kan hjælpe Debian
        </li>
        <li>
          <a href="../social_contract">Debians sociale kontrakt:</a>
          Vores moralske agenda
        </li>
        <li>
          <a href="diversity">Alle er velkomne:</a>
          Debians mangfoldighedserklæring
        </li>
        <li>
          <a href="../code_of_conduct">Til deltagere:</a>
          Debians etiske regler
        </li>
        <li>
          <a href="../partners/">Partnere:</a>
          Virksomheder og organisationer som støtter Debian-projektet
        </li>
        <li>
          <a href="../donations">Donationer:</a>
          Hvordan man sponserer Debian-projektet
        </li>
        <li>
          <a href="../legal/">Juridiske problemstillinger:</a>
          Licenser, varemærker, privatlivserklæring, patenterklæring, osv.
        </li>
        <li>
          <a href="../contact">Kontakt:</a>
          Hvordan man kommer i kontakt med os
        </li>
      </ul>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>Debian er et styresystem</h2>
      <p>Debian er et frit styresystem, der udvikles og vedligeholdes af 
      Debian-projektet. En fri Linux-distribution med tusindvis af 
      applikationer, for at opfylde vores brugeres behov.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Download:</a>
          Hvor man får fat i Debian
        </li>
        <li>
          <a href="why_debian">Hvorfor Debian:</a>
	  Årsager til at vælge Debian
        </li>
        <li>
          <a href="../support">Support:</a>
          Hvor man kan finde hjælp
        </li>
        <li>
          <a href="../security">Sikkerhed:</a>
          Seneste opdatering <br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Software:</a>
          Søg og gennemse den lange liste over pakker i Debian
        </li>
        <li>
          <a href="../doc">Dokumentation:</a>
          Installeringshåndbog, ofte stillede spørgsmål, HOWTO'er, Wiki, med mere
        </li>
        <li>
          <a href="../bugs">Fejlsporingssystem (BTS):</a>
          Hvordan man rapporterer en fejl, BTS-dokumentation
        </li>
        <li>
          <a href="https://lists.debian.org/">Postlister:</a>
          Samling af Debians lister til brugere, udviklere, osv.
        </li>
        <li>
          <a href="../blends">Pure Blends:</a>
          Meta-pakker til særlige formål
        </li>
        <li>
          <a href="../devel">Udviklerhjørnet:</a>
          Oplysninger primært af interesse for Debians udviklere
        </li>
        <li>
          <a href="../ports">Tilpasninger/arkitekturer:</a>
          Debians understøttelse af forskellige CPU-arkitekturer
        </li>
        <li>
          <a href="search">Søgning:</a>
          Oplysninger om hvordan Debians søgemaskine anvendes
        </li>
        <li>
          <a href="cn">Sprog:</a>
          Sprogindstillinger på Debians websted
        </li>
      </ul>
    </div>
  </div>

</div>

