#use wml::debian::template title="TODO-lijst voor webpagina's van Debian" BARETITLE=true
#use wml::debian::common_tags
#use wml::debian::toc
#use wml::debian::translation-check translation="e0b12a961558398b5a1d050c5595093b68e6d0b7"

# Opmerking voor vertalers: dit bestand vertalen zou niet nodig moeten zijn,
# tenzij u een soort masochistische psychopaat bent :)

<toc-display/>

<toc-add-entry name="important">Redelijk belangrijke zaken</toc-add-entry>

<p>Bekijk naast de hieronder genoemde items ook de lijst met
<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=www.debian.org">
openstaande bugs voor het pseudopakket www.debian.org</a>.

<h3>/sitemap</h3>

  <p>De sitemap moet worden nagekeken om te zien of alle pagina's erin staan, en
  de verschillende secties moeten worden gereorganiseerd.</p>
  <p>Misschien moeten we meer nadruk leggen op enkele pagina's door ze in het
  &lt;vet&gt; of &lt;cursief&gt; te plaatsen.</p>
  <p>We moeten afspreken of we een link naar de sitemap op de startpagina willen
  (en op de herschreven pagina's die een gelijkaardig sjabloon gebruiken),
   en waar (bug
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=982344">#982344</a>).

<h3>/giften</h3>

  <p>Een pagina maken om erkenning te geven aan giften.
  Dit idee moet worden besproken.
  Op dit moment wordt aan aanzienlijke giften erkenning te geven door het
  publiceren van een blogpost in
  <a href="https://bits.debian.org">de blog van Debian</a>
  of via een <a href="https://www.debian.org/News">Nieuwsberich</a>.</p>

<h3>/overzettingen/</h3>

  <p>Alle specifieke informatie over het overdragen van Debian naar een bepaalde
  architectuur zouden zich moeten bevinden in de architectuurspecifieke pagina's.
  Eerder een al lang bestaand verlanglijstje dat nooit kan worden ingelost. :)

  <p>De overzettingen die als onbeheerd staan vermeld in het bestand
  port.maintainers en deze aanpassen.

  <p>De pagina's over de overzetting naar sh importeren.

<h3>/intro/</h3>

  <p>Het schrijven van een intro over iets als Debian is geen eenvoudige taak. Men
  moet echt goed nadenken over hoe men alle (onderling verbonden) zaken opdeelt.</p>

  <p>Sommige pagina's zijn onlangs herzien of herschreven (2021), maar sommige
  andere moeten worden herzien (vooral de "over"-pagina, en ook eens kijken naar
  de index van die sectie om te zien of die kan worden
  vereenvoudigd/gereorganiseerd).</p>

<h3>/Cd/verkopers/</h3>

  <p>Alle websites van leveranciers moeten worden gecontroleerd om te zien of ze
  daadwerkelijk bijdragen. Ze moeten ook worden gecontroleerd na elke grote
  release van Debian.</p>

  <p>
  Coördineer dit werk alstublieft met de mensen achter cdvendors@debian.org
  </p>

<h3>/consultants/</h3>

  <p>Consultants moeten van tijd tot tijd gepingd worden om te zien of ze nog
  actief zijn en de lijst actueel te houden.</p>

  <p>
  Coördineer dit werk alstublieft met de mensen achter consultants@debian.org -
  misschien is het ook goed om de <a href="https://lists.debian.org/debian-consultants/">mailinglijst voor Debian consultants</a> in de gaten te houden.
  </p>

<h3>/evenementen/</h3>

  <p>
  De meeste informatie over evenementen staat nu op wikipagina's. Het zou leuk
  zijn om deze sectie samen met de wikipagina's over evenementen te bekijken en
  dienovereenkomstig bij te werken.
  Coördineer alstublieft met het <a href="https://wiki.debian.org/Teams/Events">Evenemententeam</a>
  en het <a href="https://wiki.debian.org/Teams/LocalGroups">Team voor lokale Debian-groepen</a>
  om dit werk te doen.
  </p>

<h3>/doc/boeken</h3>

  <p>Hulp bij het bijhouden van de lijst met boeken van Debian wordt zeer op prijs
  gesteld.
  Neem contact met books@debian.org om te zien hoe u kunt helpen. Er zijn enkele
  openstaande bugs:
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=317140">#317140 De licentie van de boeken van Debian weergeven</a>,
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=363496">#363496 www.debian.org/doc/books moet het publicatiejaar van de boeken vermelden</a>.
  </p>

<h3>/stemmingen/</h3>

  <p>Uitzoeken of we de sjablonen basic+votebar zouden moeten behouden,
  of eerder gewoon een enkel sjabloon ("stempagina" of iets dergelijks) gebruiken.
  Er is een openstaande bug:
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=364913">#364913 -
  Pagina's over stemmingen zouden gebouwd moeten worden met meer sjablonen
  (makkelijker te vertalen/onderhouden)</a>.
  </p>

<h3>/beveiliging/ en lts/beveiliging</h3>

  <p>Een groot deel van de webpagina's van Debian zijn beveiligingsadviezen.
  Het zou leuk zijn om te overwegen oudere bestanden (meer dan vijf jaar oud?) die
  geen vertalingen of wijzigingen meer krijgen, te verplaatsen naar statische
  bestanden, zodat ze niet bij elke sjabloonwijziging opnieuw worden opgebouwd.
  Dit idee moet met het beveiligingsteam worden besproken.
  </p>

<h3>/intro/organisatie</h3>

  <p>Ontbrekende informatie over vertegenwoordigers van Debian op andere plaatsen
  verzamelen, de huidige controleren/onderhouden.</p>

  <p>Regelmatig controleren of de lijst met teams en leden actueel is (vooral die
  welke niet gedelegeerd zijn)..

<h3>/devel/website/</h3>

  <p>Alle resterende beste huidige praktijken coderen.

<toc-add-entry name="cn">Problemen met onderhandelen over webinhoud</toc-add-entry>

  <p>Het systeem voor het onderhandelen over inhoud heeft verschillende gebreken
  waardoor sommige mensen misschien ontmoedigd raken over onze site. We kunnen
  hier echter niet veel aan doen. Het meeste wordt veroorzaakt door clients die
  niet-RFC strings in de Accept-Language header sturen, waardoor Apache door het
  lint gaat en een aantal van zijn onlogische methodes toepast om het kleinste
  beschikbare bestand aan te bieden.

  <p>Als er een "*" in de Accept-Language-header staat, wordt de eerste
  beschikbare pagina weergegeven, en dat is hoogstwaarschijnlijk niet Engels, maar
  eerder Arabisch of Chinees. Dit is vooral problematisch wanneer de taalvoorkeur
  van de gebruiker een tweedelige taalcode is (zoals "en-ca" of "nl-BE") gevolgd
  door een "*".

  <p>Apache 1.3 houdt zich hier aan de RFC. De code van Apache 2.0 zal uitgaan van
  en respectievelijk nl, maar dit zal een lage prioriteit krijgen en dus zal dit
  waarschijnlijk niet helpen bij bijvoorbeeld "en-ca, fr-ca, *".

  <p>Ook, als er een bestand met een onbekende taal bestaat, d.w.z. een
  niet-herkend foo.*.html-bestand zonder AddLanguage- of
  LanguagePriority-instelling, en wanneer de client een Accept-Language
  verzendt welke alleen niet-beschikbare talen bevat, zal het eerstgenoemde
  bestand aangeboden worden.

  <p>Dit gebeurde het vaakst met de pagina /releases/potato/i386/install, omdat
  daar een Slowaakse variant van is en we die taal niet hadden ingesteld in Apache
  omdat er geen websitevertaling in het Slowaaks is. We hebben het probleem
  opgelost door sk op te nemen in de Apache configuratiebestanden, maar zoals
  gewoonlijk verspreiden aanpassingen zich niet snel naar alle spiegelservers.


<toc-add-entry name="mirroring">Problemen bij het spiegelen</toc-add-entry>

  <p>Sites worden geacht het bestand mirror/timestamps/&lt;host&gt; aan te maken.
  Jay heeft scripts gemaakt die voor elke spiegelserver de datum in dit bestand
  controleren, zodat we kunnen weten wanneer een spiegelserver verouderd is,
  zie mirror/timestamps/*.py.

  <p>We moeten het aantal webspiegelservers in Europa verminderen en het aantal
  spiegelservers op andere, minder verbonden continenten verhogen.

  <p>Alle spiegelservers gepusht maken (indien mogelijk vanuit www-master)
  is ook een doelstelling.

  <p>Ervoor zorgen dat elke spiegelserver de juiste Apache-configuratie heeft is
  lastig, maar er lijkt geen manier te zijn om dat knelpunt te omzeilen. Phil
  Hands stelde voor om het "AddLanguage"-materiaal in een bestand te zetten dat
  met wget kan opgehaald worden en dat we een Perl-script zouden maken dat
  automatisch mensen hun Apache-configuratiebestanden zou bijwerken.

  <P>Alle links in het archief moeten de gebruiker toelaten om de downloadsite te
  selecteren. De hoofdspiegelserverlijst kan worden gebruikt om de lijst met
  spiegelservers up-to-date te houden (misschien zelfs met behulp van een script).
  Zie de bestanden
  <code>webwml/english/mirror/Mirrors.masterlist</code> en
  <code>webwml/english/mirror/mirror_list.pl</code>.

  <P><em>De spiegelserverlijst wordt bijgehouden door de mensen op
  mirrors@debian.org.</em>

<toc-add-entry name="wrongurls">Verkeerde URL's</toc-add-entry>

  <p>Links naar externe pagina's moeten worden gecontroleerd of ze nog correct
  zijn. James Treacy heeft hiervoor een klein Python-script geschreven. Frank
  Lichtenheld onderhoudt het script momenteel. De (dagelijks bijgewerkte)
  resultaten zijn te vinden op
  <url "https://www-master.debian.org/build-logs/urlcheck/" />.
  Defecte moeten worden verwijderd. Dit is meer een permanente taak.</p>

<toc-add-entry name="misc">Diverse verzoeken</toc-add-entry>

  <p><strong>Ga hiermee aan de slag als u wil.</strong></p>

  <P>Als we cgi.debian.org op een minder gebruikte en snellere machine hadden,
  zouden we meer dynamische inhoud op de webpagina's kunnen hebben.

  <p>Javier stelde voor om DDP-pagina's automatisch rekening te laten houden met
  vertalingen.

  <p>Joey zei:
<blockquote>
<p>Op de beveiligingspagina's zou ik liever een opmerking zien zoals:</p>

<p>
  Houd er rekening mee dat we niet kunnen garanderen dat een indringer geen
  toegang krijgt tot onze servers, aangezien deze met internet zijn verbonden.
  In een dergelijk geval zou een kwaadaardige derde partij de uploads naar
  security.debian.org kunnen wijzigen en webpagina's kunnen wijzigen die
  MD5-controlegetallen bevatten. Wij doen echter ons best om dit te voorkomen.
  Houd er rekening mee dat er geen 100% beveiliging bestaat, maar alleen
  verbeteringen ten opzichte van de huidige situatie.
</p>
</blockquote>

<p>Joey zou het waarschijnlijk anders moeten formuleren. :)</p>

  <p>Zou toegevoegd moeten worden aan /security/faq.</p>

  <p>"Vernon Balbert" &lt;vbalbert@mediaone.net&gt; stelde voor dat we duidelijk
  zouden maken welke kernelversie wordt gebruikt in de nieuwste versie van Debian.

  <p>Matti Airas &lt;mairas@iki.fi&gt; stelde voor om "taalselectie te doen met
  Apache mod_rewrite. In plaats van expliciet naar
  https://www.debian.org/intro/about.en.html (of een spiegelserver) te gaan, zou
  ik naar https://www.debian.org/en/intro/about kunnen gaan, hetgeen mod_rewrite
  dan zou kunnen vervangen door de juiste url." Merk op dat dit extra aanpassingen
  zou vereisen om relatieve links niet onklaar te maken.</p>


  <p>Chris Tillman stelde voor:</p>

<blockquote>
<p>Er is vaak verwarring over welke machines Debian ondersteunt en welke nog niet.
Er is een verbijsterende reeks machines, om nog maar te zwijgen over
netwerkkaarten, beeldschermen, muizen, videokaarten, geluidskaarten, poorttypen,
enzovoort die een individueel systeem gecombineerd kunnen worden. Veel van de
pagina's over architecturen waarop Debian werkt zijn verouderd.

<p>Debian ondersteunt *veel* systemen. Ik denk dat het zinvol zou zijn om te
proberen een lijst te maken over welke specifiek worden ondersteund. Het zou ook
erg leuk zijn om te weten welke niet ondersteund worden, zowel voor nieuwe
gebruikers als voor ontwikkelaars als een todo-lijst.

<p>Ik denk dat de eenvoudigste manier om dit te bereiken zou zijn om een webpagina
aan te bieden waar mensen hun systeemcomponenten kunnen invoeren, bij voorkeur
gekozen uit een lijst van bekende componenten met ook een categorie 'andere'. Vervolgens zouden ze ook een duimpje omhoog of een duimpje omlaag kunnen geven aan Debian voor die architectuur. Ook eventueel over specifieke problemen.

<p>Vervolgens kan het systeem, na het indienen van de hardwarespecificaties door
de gebruiker, een (gedateerde) lijst tonen met eerdere ervaringen van gebruikers
met die componenten.

<p>We zouden ook verwijzingen naar deze pagina moeten opnemen in de
installatiedocumentatie, FAQ's en waarschijnlijk zelfs een link op de voorpagina
van Debian.
</blockquote>

<toc-add-entry name="packages">packages.debian.org</toc-add-entry>

  <p>U kunt een actuele <a
  href="https://salsa.debian.org/webmaster-team/packages/blob/master/TODO">TODO-lijst</a> vinden in de <a
  href="https://salsa.debian.org/webmaster-team/packages/">Git
  opslagplaats</a>.</p>

  <p><i>De scripts worden momenteel onderhouden door Frank 'djpig' Lichtenheld en
  Martin 'Joey' Schulze.</i></p>

<toc-add-entry name="bugs">Bugrapporten</toc-add-entry>

<p><a href="https://bugs.debian.org/www.debian.org">De lijst met onze
bugrapporten</a>.

<hr>

<p>Stuur al de rest naar
<a href="mailto:debian-www@lists.debian.org">onze mailinglijst</a>.
