#use wml::debian::template title="Zich bij Debian aansluiten" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> We zijn altijd op zoek naar nieuwe medewerkers en vrijwilligers. We moedigen <a href="$(HOME)/intro/diversity">deelname door iedereen</a> sterk aan. Vereisten: interesse in vrije software en wat vrije tijd.</p>
</aside>

<ul class="toc">
<li><a href="#reading">Lezen</a></li>
<li><a href="#contributing">Bijdragen</a></li>
<li><a href="#joining">Aansluiten</a></li>
</ul>


<h2><a id="reading">Lezen</a></h2>

<p>
Als u dat nog niet gedaan heeft, zou u de sites moeten lezen waarnaar verwezen
wordt op de <a href="$(HOME)">startpagina van Debian</a>. Dit zal u helpen om
een beter begrip te krijgen van wie we zijn en wat we proberen te bereiken.
Besteed als potentiële medewerker van Debian vooral aandacht aan deze twee
pagina's:
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">De Debian Richtlijnen voor Vrije Software (DFSG)</a></li>
  <li><a href="$(HOME)/social_contract">Het Debian Sociale Contract</a></li>
</ul>

<p>
Veel van onze communicatie vindt plaats op de
<a href="$(HOME)/MailingLists/">mailinglijsten</a> van Debian. Als u een idee
wilt krijgen van de interne werking van het Debian-project, zou u zich op
zijn minst moeten abonneren op <a
href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>
en <a href="https://lists.debian.org/debian-news/">debian-news</a>. Beide
lijsten hebben een zeer klein volume en documenteren wat gaande is in de
gemeenschap. Het
<a href="https://www.debian.org/News/weekly/">Debian projectnieuws</a>,
dat ook gepubliceerd wordt op de debian-news mailinglijst, vat de recente
discussies samen uit mailinglijsten en blogs die
met Debian verband houden, en geeft links ernaartoe.
</p>

<p>
Als toekomstige ontwikkelaar zou u ook moeten intekenen op <a
href="https://lists.debian.org/debian-mentors/">debian-mentors</a>. Hier kunt u
vragen stellen over verpakkingswerk en infrastructuurprojecten en over andere
aan de werkzaamheden van ontwikkelaars gerelateerde zaken. Houd er rekening mee
dat deze lijst bedoeld is voor nieuwe medewerkers, niet voor gebruikers.
Andere interessante lijsten zijn <a
href="https://lists.debian.org/debian-devel/">debian-devel</a>
(technische kwesties in verband met de werkzaamheden van ontwikkelaars), <a
href="https://lists.debian.org/debian-project/">debian-project</a>
(discussies over niet-technische kwesties in het project), <a
href="https://lists.debian.org/debian-release/">debian-release</a>
(coördinatie van Debian-releases), <a
href="https://lists.debian.org/debian-qa/">debian-qa</a>
(kwaliteitszorg).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Intekenen op een mailinglijst</a></button></p>

<p>
<strong>Tip:</strong> Voor het geval u het aantal e-mails dat u ontvangt wilt
verminderen, vooral van lijsten met veel verkeer, bieden we e-mailoverzichten
aan in de vorm van een samenvattende e-mail ter vervanging van de individuele
berichten. U kunt ook de
<a href="https://lists.debian.org/">Mailinglijstarchieven</a> bezoeken om de
berichten van onze lijsten in een webbrowser te lezen.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> U hoeft geen officiële Debian-ontwikkelaar (DD) te zijn om bij te dragen. In plaats daarvan kan een bestaande DD optreden als <a href="newmaint#Sponsor">sponsor</a> en uw werk in het project helpen integreren.</p>
</aside>

<h2><a id="contributing">Bijdragen</a></h2>

<p>
Bent u geïnteresseerd in het onderhouden van pakketten? Bekijk dan eens onze
lijst van <a href="$(DEVEL)/wnpp/">Pakketten waaraan werk is en toekomstige
pakketten</a>. Hier vindt u pakketten die een (nieuwe) beheerder nodig hebben.
Een onbeheerd pakket overnemen is de beste manier om als pakketbeheerder
van start te gaan. Niet enkel helpt dit onze distributie vooruit, het geeft u
ook de kans om te leren van de vorige pakketbeheerder.
</p>

<p>
Hier zijn enkele andere ideeën over hoe u kunt bijdragen aan Debian::
</p>

<ul>
  <li>Ons helpen onze <a href="$(HOME)/doc/">documentatie</a> te schrijven.</li>
  <li>Helpen bij het onderhouden van de <a href="$(HOME)/devel/website/">website van Debian</a>, inhoud produceren, bestaande tekst bewerken of vertalen.</li>
  <li>Meewerken met ons <a href="$(HOME)/international/">vertaalteam</a>.</li>
  <li>Debian verbeteren en meewerken met het <a href="https://qa.debian.org/">Debian kwaliteitszorgteam</a> (Debian Quality Assurance - QA).</li>
</ul>

<p>
Natuurlijk zijn er nog tal van andere dingen die u zou kunnen doen en we zijn
altijd op zoek naar mensen die juridische ondersteuning willen bieden of ons
<a href="https://wiki.debian.org/Teams/Publicity">Debian publiciteitsteam</a>
willen komen versterken.
Lid worden van een van de teams van Debian is een uitstekende manier om wat
ervaring op te doen voor u start met de procedure voor
<a href="newmaint">nieuwe leden</a>. Het is ook een goed startpunt als u op
zoek bent naar een sponsor voor uw pakketten. Dus, ga op zoek naar een team en
begin er meteen aan!
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">Lijst met teams in Debian</a></button></p>


<h2><a id="joining">Aansluiten</a></h2>

<p>
U hebt al een tijdje bijgedragen aan het Debian-project en u wilt zich
aansluiten bij Debian in een meer officiële rol? Er zijn in principe twee
opties:
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> De rol van Debian onderhouder (Debian Maintainer - DM) werd in 2007 geïntroduceerd. Tot dan toe was Debian ontwikkelaar (Debian Developer - DD) de enige officiële rol. Er zijn momenteel twee onafhankelijke procedures om uzelf kandidaat te stellen voor een van beide rollen.</p>
</aside>

<ul>
  <li><strong>Onderhouder van Debian (Debian Maintainer - DM):</strong> Dit is
  de eerste stap – als DM kunt u zelf uw eigen pakketten uploaden naar het
  archief van Debian (met enkele beperkingen). In tegenstelling tot gesponsorde
  onderhouders (of gesponsorde pakketbeheerders), kunnen Debian onderhouders
  pakketten onderhouden zonder sponsor. <br>Meer informatie op de:
  <a href="https://wiki.debian.org/DebianMaintainer">Debian Maintainer
  Wiki</a></li>
  <li><strong>Ontwikkelaar van Debian (Debian Developer - DD):</strong> Dit is
  de traditionele rol in Debian bij een volledig lidmaatschap. Een DD kan
  deelnemen aan verkiezingen in Debian. DD's met uploadbevoegdheid kunnen om
  het even welk pakket uploaden naar het archief. Voor u een DD met
  uploadbevoegdheid wordt, moet u minstens gedurende zes maanden een staat van
  dienst hebben als onderhouder van pakketten.
  (bijvoorbeeld, pakketten uploaden als DM, werken binnen een team of
  pakketten onderhouden die door een sponsor geüpload worden). DD's zonder
  uploadbevoegdheden hebben dezelfde verpakkingsbevoegdheid als
  onderhouders van Debian (DM). Voordat u zich kandidaat stelt voor de rol van DD
  zonder uploadbevoegdheid moet u een zichtbare en significante staat van
  dienst hebben als medewerker binnen het project. <br>Meer informatie:
  <a href="newmaint">New Members Corner</a> (Hoekje voor nieuwe leden)</li>
</ul>

<p>
Voor welke rol u zich ook kandidaat wilt stellen, u dient vertrouwd te zijn met de
werkwijzen van Debian. Daarom raden wij ten zeerste aan om het
<a href="$(DOC)/debian-policy/">Debian beleidshandboek</a> te lezen,
evenals de <a
href="$(DOC)/developers-reference/">Referentiehandleiding voor ontwikkelaars</a>.
</p>

