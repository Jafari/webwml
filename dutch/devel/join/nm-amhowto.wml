#use wml::debian::template title="Mini-HOWTO voor beheerders van kandidaat-leden van Debian"
#use wml::debian::translation-check translation="c3a3b8986d2ffef95ef7bb8f7d99f36678ff0e8f"

<p>
<b>Opmerking:</b> De wiki-pagina <a href="https://wiki.debian.org/FrontDesk/AMTutorial">AM Tutorial</a> is actueler dan deze pagina.
</p>

<h2>Documentatie en infrastructuur voor kandidatuurbeheerders</h2>

<p>De basisinformatie die een kandidatuurbeheerder nodig heeft, staat hier, in het <a href="newmaint">hoekje voor nieuwe leden</a>. Kijk rond tot je vertrouwd bent met het proces en alle vereisten voor kandidaten.</p>

<p>Er zijn drie belangrijke e-mailadressen voor kandidatuurbeheerders:</p>

<dl>
 <dt>De mailinglijst voor nieuwe leden: <a href="https://lists.debian.org/debian-newmaint">
  debian-newmaint@lists.debian.org</a></dt>
  <dd>Deze mailinglijst behandelt alle aspecten van het proces voor nieuwe leden en wordt gebruikt door de groep voor nieuwe leden (frontdesk, kandidatuurbeheerders, Debian accountbeheerder) en anderen om administratieve kwesties en het proces voor nieuwe leden te bespreken. Als u vragen hebt over het proces voor nieuwe leden (NM-proces), kunt u daar om hulp vragen. Houd er rekening mee dat de lijst openbaar wordt gearchiveerd, dus vragen van zeer persoonlijke aard mogen daar niet worden besproken. In plaats daarvan kunt u het privé vragen aan de frontdesk.</dd>

 <dt>Frontdesk voor nieuwe leden: nm@debian.org</dt>
  <dd>Naar hier worden de initiële kandidatuurstellingen, pleitbezorgingsberichten en definitieve rapporten over kandidaten verzonden. Alle persoonlijke vragen over individuele kandidatien die niet geschikt zijn voor een openbaar forum, moeten ook hier worden gesteld.</dd>

 <dt>Debian accountbeheerders (DAMs): da-manager@debian.org</dt>
  <dd>Normaal gesproken is dit adres alleen van belang voor het indienen van het definitieve rapport over de kandidaten. De DAM's zijn verantwoordelijk voor het aanmaken van nieuwe accounts op de Debian-machines en het toevoegen van de OpenPGP-sleutels van de nieuwe leden aan de sleutelring. Ze mogen ook de uiteindelijke beslissing nemen over elke kandidatuurstelling als de officiële gemachtigden van de Debian projectleider.
</dd>
</dl>

<p>De coördinatie van het NM-proces gebeurt op <url "https://nm.debian.org/"/>, waar een website een interface biedt met een database die alle belangrijke informatie over NM-kandidaatstellingen bevat. Kandidaten kunnen de site gebruiken om de status van hun kandidatuur te volgen en kandidatuurbeheerders kunnen de site gebruiken om hun werk te organiseren.</p>

<p>Als kandidatuurbeheerder kunt u inloggen via een beveiligde https-verbinding, maar houd er rekening mee dat het wachtwoord dat wordt gebruikt op nm.debian.org <em>niet</em> het wachtwoord is dat wordt gebruikt voor uw normale Debian-account (tenzij u het wijzigt om het te doen overeenkomen, maar dat is uw zaak). U kunt vastleggen wat u met een kandidaat heeft gedaan en hoeveel kandidaten u tegelijkertijd wilt behandelen.</p>

<p>Om iemand in de wacht te zetten, moet u naar de statuspagina van de kandidaat gaan nadat u bent ingelogd en het keuzerondje "Nee" aanvinken voor "AM keurt rapport goed en dient het in". U moet ook een regel in het AM-commentaarveld zetten om te beschrijven waarom u dit hebt gedaan.
</p>

<p>De rest van de pagina's spreken voor zich. Er zijn enkele statistieken over alle kandidatuurbeheerders beschikbaar, u kunt een ongesorteerde lijst van alle kandidaten bekijken en uw AM-profiel wijzigen.
</p>

<h2>Opmerkingen over de NM-controles</h2>

<p>Aangezien de NM-georiënteerde documentatie al voldoende informatie geeft over de vereisten van de controles, wordt dat hier niet herhaald. Als u niet zeker weet hoe u een kandidaat moet beheren, gebruik dan de uitstekende sjablonen van Joerg Jaspert's <a href="https://salsa.debian.org/nm-team/nm-templates">nm-templates</a>-project. Vragen kunnen gesteld worden op debian-newmaint@l.d.o of gestuurd worden naar de frontdesk.</p>

<h3>Een kandidatuur in de wacht zetten</h3>

<p>Kandidaten die niet genoeg tijd kunnen of willen investeren in de controles voor nieuwe leden om deze binnen een redelijke termijn (&sim; 6 tot 9 maanden) af te ronden, moeten in de wacht worden gezet. Dat is geen probleem of een beoordeling van de vaardigheden van de kandidaat, maar een eenvoudige reactie op het gebrek aan tijd. Veel mensen willen lid worden van Debian, dus zouden kandidaten geen AM-plekken mogen blokkeren.</p>

<p>U zou de mogelijkheid om een kandidaat in de wacht te zetten ter sprake moeten brengen wanneer u het gevoel krijgt dat er geen vooruitgang boekt wordt, ofwel omdat de kandidaat niet antwoordt of omdat zijn enige antwoord is "Ja, ik zal het binnenkort doen". Benadruk dat het geen probleem is om terug uit de wacht te komen als men terug meer beschikbare tijd heeft.</p>

<h3>Andere belangrijke opmerkingen</h3>

<ul>
 <li>Kandidaten dienen een korte biografie te verstrekken die kan worden verzonden naar debian-project@lists.debian.org in de regelmatig gepubliceerde e-mail over "Nieuwe leden". Dat is erg handig om nieuwe leden voor te stellen aan het project. Houd er rekening mee dat de kandidaat <em>moet</em> akkoord gaan met de publicatie van deze biografie.</li>

 <li>Verzamel wat achtergrondinformatie over de kandidaat en zet die in het rapport &mdash; gebruik uw favoriete zoekmachine, private e-mailarchieven, het BTS en alle andere middelen die in u opkomen. Soms is de procedure voor nieuwe leden te kort om een goed beeld te krijgen van de persoonlijkheid van de kandidaat, dus probeer te achterhalen wat hij of zij in het verleden heeft gedaan.</li>

 <li>Vraag sponsors en andere mensen die nauw met de kandidaat hebben samengewerkt om korte verklaringen over hem af te leggen. Aangezien steeds meer pakketten in teamverband worden beheerd, kunt u bijna altijd wel iemand vinden die u meer over een kandidaat kan vertellen. Neem deze verklaringen op in het rapport.</li>

 <li>Wanneer u de prestatie van een kandidaat pakketbeheerder controleert, moet u zich ervan bewust zijn dat één klein pakket in het archief niet voldoende is om te voldoen aan het vaardigheden-gedeelte van de T&amp;S-controles. Het pakket moet meer dan één upload hebben gehad, enkele gebruikers (controleer popcon) en enkele (indien mogelijk gesloten) bugrapporten. Dit is belangrijk om te zien hoe een kandidaat omgaat met gebruikers.</li>
</ul>
