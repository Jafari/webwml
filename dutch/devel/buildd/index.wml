#use wml::debian::template title="Autobuilder-netwerk"
#use wml::debian::translation-check translation="e6a1462539e3f8feecbd6e88728cbae06f920c59"

<p>Het autobuilder-netwerk is een door Debian ontwikkeld systeem dat zorg
draagt voor het compileren van pakketten voor alle architecturen die <a
href="$(HOME)/ports/">Debian op dit moment ondersteunt</a>. Dit netwerk bestaat
uit verschillende machines die een specifiek software-pakket gebruiken wat
<em>buildd</em> heet, om pakketten uit het Debian-archief op te pikken en ze
opnieuw te bouwen voor de doelarchitectuur.</p>

<h2>Waarom is het autobuilder-netwerk nodig?</h2>

<p>Het autobuilder-netwerk biedt een veilige functionaliteit voor het bouwen van
pakketten voor alle ondersteunde architecturen.
De Debian-distributie ondersteunt <a href="$(HOME)/ports/">een flink aantal
architecturen</a>, en de pakketbeheerders hebben vaak geen toegang tot alle
machines met de benodigde architecturen. Anderzijds vereist Debian nu dat gewone
binaire pakketten worden gegenereerd vanuit de broncode in een gecontroleerde
bouwomgeving (via de <a href="https://wiki.debian.org/SourceOnlyUpload">eis om
alleen broncode te uploaden</a>) om de introductie te voorkomen van kwaadwillig
gemaakte binaire pakketten door toedoen van menselijke ontwikkelaars. Het
autobuilder-netwerk neemt de broncode van het pakket en bouwt automatisch binaire
pakketten, één keer voor elke ondersteunde hardware-architectuur. Fouten worden
bijgehouden in de autobuilder-database.</p>

<p>Het autobuilder-netwerk verlicht ook het werk van ontwikkelaars die <a
href="https://www.ports.debian.org/">Debian geschikt maken voor andere architecturen</a>, zogenaamde ports.
Toen Debian/m86k (de eerste niet-Intel architectuur) werd opgestart moesten de
ontwikkelaars voor deze architectuur, de zogenaamde porters, zelf in de gaten houden of er nieuwe
versies van pakketten beschikbaar kwamen en deze opnieuw bouwen om de
Intel-distributie bij te houden. Dit werd allemaal handmatig gedaan. De
ontwikkelaars keken op de upload-mailinglijst naar nieuwe pakketten en
compileerden sommige. De noodzakelijke coördinatie om te voorkomen dat
pakketten twee keer gecompileerd werden door verschillende personen werd gedaan
via een eigen mailinglijst. Het is duidelijk dat met deze procedure makkelijk
fouten gemaakt worden, terwijl ze ook veel tijd vraagt. Toch was dit voor een
lange tijd de normale manier om niet-i386 distributies actueel te houden.</p>

<p>Het "build daemon"-systeem automatiseert het grootste gedeelte van dit
proces.  Het bestaat uit een set scripts (geschreven in Perl en Python) die
mettertijd ontwikkeld zijn om porters te helpen met verschillende taken. Ze zijn
uiteindelijk geëvolueerd tot een systeem dat in staat is om Debian-distributies
bijna volautomatisch up-to-date te houden. De beveiligingsupdates worden op
dezelfde machines gecompileerd zodat deze snel beschikbaar zijn.</p>

<h2>Hoe werkt buildd?</h2>

<p><em>Buildd</em> is de naam die gewoonlijk gegeven wordt aan de
software die gebruikt wordt door het autobuilder-netwerk, maar ze
bestaat eigenlijk uit verschillende onderdelen:</p>

<dl>
<dt>wanna-build</dt>
<dd>
Een hulpmiddel dat pakket-(her)compilaties helpt coördineren via een database
die een lijst van pakketten en hun status bijhoudt. Er is één centrale database
per architectuur die per pakket de status, versie en nog wat informatie
bijhoudt. Deze krijgt de Sources- en Packages-bestanden aangeleverd vanaf de
verschillende pakketarchieven van Debian (bv. ftp-master en security-master).
</dd>
<dt><a href="https://packages.debian.org/buildd">buildd</a></dt>
<dd>
Een achtergronddienst die periodiek de database onderhouden door
<em>wanna-build</em> nakijkt en <em>sbuild</em> aanroept om de pakketten te
compileren. Nadat het compilatielogboek is goedgekeurd door de buildd-beheerder
uploadt hij het pakket naar het juiste archief.
</dd>
<dt><a href="https://packages.debian.org/sbuild">sbuild</a></dt>
<dd>
Is verantwoordelijk voor de daadwerkelijke compilatie van pakketten in
geïsoleerde chroot-omgevingen. Er wordt gezorgd dat alle bron-vereisten
in de chroot zijn geïnstalleerd voor de compilatie begint. Vervolgens worden de
standaard Debian-gereedschappen aangeroepen voor het compilatieproces.
Logbestanden worden naar de <a
href="https://buildd.debian.org">buildlog-database</a> gestuurd.
</dd>
</dl>

<p>Al deze componenten <a href="operation">werken samen</a> om het
builder-netwerk te laten werken.</p>

<h2>Wat moet een Debian-ontwikkelaar doen?</h2>

<p>Eigenlijk hoeft een gemiddelde Debian-ontwikkelaar het buildd-netwerk niet
expliciet te gebruiken. Elke keer hij een pakket naar het Debian-archief
uploadt (met een binaire versie voor een bepaalde architectuur) zal het
toegevoegd worden aan de database voor alle architecturen (in de status
<em>Needs-Build</em>). Build-machines zullen de database bevragen voor
pakketten in deze status, en zullen routineus pakketten van die lijst oppikken.
De lijst wordt geprioriteerd op de vorige compilatiestatus
(<em>out-of-date</em> of <em>uncompiled</em>), de prioriteit van het pakket, de
sectie en de pakketnaam. Tot slot worden de prioriteiten nog dynamisch bijgesteld
op basis van wachttijd, dit om te voorkomen dat sommige pakketten achter in de
wachtrij blijven steken.</p>

<p>Als de compilatie succesvol is in alle architecturen, dan hoeft een
pakketbeheerder niets te doen. Al deze binaire pakketten worden geüpload naar
het desbetreffende archief. Als de compilatie niet succesvol is, dan zal het
pakket een speciale status krijgen (<em>Build-Attempted</em> voor
compilatiepogingen die nog niet zijn nagekeken, <em>Failed</em> indien
nagekeken en als bug gemeld bij het pakket of <em>Dep-Wait</em> als ze
bron-vereisten hebben die niet beschikbaar zijn).
De autobuilder-beheerders zullen pakketten die niet compileren nakijken en
informatie sturen naar de pakketbeheerder, gewoonlijk via het openen van een
bug in het Bug Tracking System, het bugvolgsysteem.</p>

<p>Soms duurt het lang om een pakket te compileren voor een gegeven
architectuur, en houdt dat de migratie naar <a
href="$(HOME)/devel/testing">testing</a> van dat pakket tegen.  Indien een
pakket een belangrijke overgang tegenhoudt dan zal de prioriteit meestal op
verzoek van het Release-team worden bijgesteld. Andere verzoeken worden niet
geaccepteerd omdat de langere wachttijd in de rij automatisch tot een hogere
prioriteit leidt.</p>

<p>U kunt de status van de verschillende compilatiepogingen van de pakketten
die onder het beheer van een bepaalde pakketbeheerder vallen, bekijken via de
<a href="https://buildd.debian.org/status/">buildd-logs</a>. Er is ook een link
naar deze logbestanden vanuit het overzicht van de beheerders van pakketten.</p>

<p>Voor meer informatie over de verschillende statussen waarin een
pakket zich kan bevinden, kunt u <a
href="wanna-build-states">wanna-build-states</a> lezen.</p>

<h2>Waar kan ik meer informatie vinden?</h2>

<p>Uiteraard zijn zowel de documentatie als de broncode voor deze
verschillende hulpmiddelen de beste manier om uit te vinden hoe het
buildd-netwerk werkt. Daarnaast bevat de sectie <a
href="$(HOME)/doc/manuals/developers-reference/pkgs.html#porting">Porting
and being ported</a> van de <a
href="$(HOME)/doc/manuals/developers-reference/">Debian Developers
Reference</a> aanvullende informatie over hoe het werkt, terwijl ze ook
wat informatie bevat over
<a href="$(HOME)/doc/manuals/developers-reference/tools.html#tools-builders">\
pakketcompilatie-</a> en
<a href="$(HOME)/doc/manuals/developers-reference/tools.html#tools-porting">\
porteer-hulpmiddelen</a> die gebruikt worden in het proces van zowel het
opzetten als het onderhouden van het buildd-netwerk.</p>

<p>Er zijn een aantal statistieken van het autobuilder-netwerk
beschikbaar op <a href="https://buildd.debian.org/stats/">de buildd
stats pagina</a>.</p>

<h2>Hoe kan ik mijn eigen auto-builder node opzetten?</h2>

<p>Er kunnen verschillende redenen zijn waarom een ontwikkelaar (of
gebruiker) een autobuilder zou willen opzetten:</p>

<ul>
<li>Om te helpen in de ontwikkeling van een port naar een gegeven
architectuur (wanneer autobuilders nodig zijn)</li>
<li>Om de impact van een gegeven compiler-optimalisatie of patch na te
gaan door een grote groep pakketten te hercompileren.</li>
<li>Om hulpmiddelen uit te voeren die pakketten controleren op bekende
vergissingen en die gedraaid moeten worden in gecompileerde pakket-mappen.
Dit is zelfs nodig wanneer men broncode-analyse wil doen, bijvoorbeeld,
als een workaround voor pakketten die <tt>dpatch</tt> gebruiken.</li>
</ul>

<p>U kan meer informatie vinden over hoe u <a
href="https://wiki.debian.org/BuilddSetup">een autobuilder kunt
opzetten</a>.</p>

<h2>Contact opnemen met de buildd-beheerders</h2>

<p>
Het primaire contactadres van de beheerders van wanna-build is de
<email debian-wb-team@lists.debian.org> (<a href="https://lists.debian.org/debian-wb-team/
">mailinglijst</a>)
</p><p>De beheerders die verantwoordelijk zijn voor de buildd's voor een
bepaalde architectuur kunnen bereikt worden via <email
arch@buildd.debian.org>, bijvoorbeeld <email
i386@buildd.debian.org>.</p>

<hrline />
<p><small>Deze introductie tot het autobuilder-netwerk werd geschreven
op basis van informatie verstrekt door Roman Hodek, Christian T. Steigies,
Wouter Verhelst, Andreas Barth, Francesco Paolo Lovergine, Javier
Fernández-Sanguino Peña en Philipp Kern.</small></p>
