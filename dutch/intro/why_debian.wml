#use wml::debian::template title="Redenen om Debian te gebruiken" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian voor gebruikers</a></li>
    <li><a href="#devel">Debian voor ontwikkelaarsrs</a></li>
    <li><a href="#enterprise">Debian voor bedrijfsomgevingen</a></li>
  </ul>
</div>

<p>
Er zijn veel redenen om te kiezen voor Debian als uw besturingssysteem - als
gebruiker, als ontwikkelaar en zelfs in bedrijfsomgevingen. De meeste
gebruikers waarderen de stabiliteit en de soepele opwaarderingsprocessen voor
zowel de pakketten als de gehele distributie. Debian wordt ook veel gebruikt
door software- en hardwareontwikkelaars omdat het op tal van architecturen en
apparaten werkt, een publiek bugvolgsysteem biedt en andere hulpmiddelen voor
ontwikkelaars. Als u van plan bent Debian in een professionele omgeving te
gebruiken, zijn er extra voordelen zoals LTS-versies (versies die een
langetermijnondersteuning genieten) en cloud-images.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Voor mij is dit het perfecte niveau van gebruiksgemak en stabiliteit. Ik heb in de loop der jaren verschillende distributies gebruikt, maar Debian is de enige die gewoon werkt. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest op Reddit</a></p>
</aside>

<h2><a id="users">Debian voor gebruikers</a></h2>

<dl>
  <dt><strong>Debian is vrije software.</strong></dt>
  <dd>
    Debian is gemaakt van vrije en openbronsoftware en zal altijd 100%
    <a href="free">vrij</a> zijn. Vrij om door iedereen gebruikt, aangepast en verspreid te worden. Dit is de belangrijkste belofte die we aan <a href="../users">onze gebruikers</a> doen. Debian is ook kosteloos.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is stabiel en veilig.</strong></dt>
  <dd>
    Debian is een op Linux gebaseerd besturingssysteem voor een breed scala aan
    apparaten, waaronder laptops, desktops en servers. Voor elk pakket bieden
    we een gefundeerde standaardconfiguratie en voorzien we geregeld in
    veiligheidsupdates gedurende de volledige levensduur van het pakket.
  </dd>
</dl>

<dl>
  <dt><strong>Debian heeft uitgebreide ondersteuning voor hardware.</strong></dt>
  <dd>
    De Linux kernel ondersteunt de meeste hardware, hetgeen betekent dat ook
    Debian deze ondersteunt. Gepatenteerde stuurprogramma's voor hardware zijn
    indien nodig beschikbaar.
  </dd>
</dl>

<dl>
  <dt><strong>Debian biedt een flexibel installatiesysteem.</strong></dt>
  <dd>
    Onze <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">live-cd</a> is er voor iedereen die Debian wenst uit te proberen
alvorens het te installeren. Hij bevat ook het Calamares-installatieprogramma
dat het gemakkelijk maakt om Debian vanaf het live-systeem te installeren. Meer
ervaren gebruikers kunnen het Debian-installatieprogramma gebruiken met meer
opties voor een verfijnde afstelling, inclusief de mogelijkheid om een
hulpmiddel voor geautomatiseerd netwerkinstallatie te gebruiken.
  </dd>
</dl>

<dl>
  <dt><strong>Debian biedt soepele opwaarderingen.</strong></dt>
  <dd>
    Het is gemakkelijk om ons besturingssysteem up-to-date te houden, of u nu
    wilt opwaarderen naar een volledig nieuwe release of slechts een enkel
    pakket wilt bijwerken.
  </dd>
</dl>

<dl>
  <dt><strong>Debian vormt de basis voor veel andere distributies.</strong></dt>
  <dd>
    Veel populaire Linux-distributies, zoals Ubuntu, Knoppix, PureOS
    en Tails zijn gebaseerd op Debian.
    We stellen alle hulpmiddelen ter beschikking zodat iedereen de
    softwarepakketten uit het archief van Debian zo nodig kan aanvullen met
    zijn eigen pakketten.
  </dd>
</dl>

<dl>
  <dt><strong>Het Debian-project is een gemeenschap.</strong></dt>
  <dd>
    Iedereen kan deel uitmaken van onze gemeenschap; u hoeft
    geen ontwikkelaar of systeembeheerder te zijn. Debian heeft een
    <a href="../devel/constitution">democratische beleidsstructuur</a>.
    Vermits alle leden van het Debian-project dezelfde rechten hebben, kan
    Debian niet door één bedrijf gecontroleerd worden. Onze ontwikkelaars komen
    uit meer dan 60 verschillende landen en Debian zelf wordt naar meer dan 80
    talen vertaald.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> De reden achter de status van Debian als besturingssysteem voor ontwikkelaars is het grote aantal pakketten en de software-ondersteuning, die voor ontwikkelaars belangrijk zijn. Het wordt sterk aanbevolen voor gevorderde programmeurs en systeembeheerders. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma op Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian voor ontwikkelaars</a></h2>

<dl>
  <dt><strong>Meerdere hardware-architecturen</strong></dt>
  <dd>
    Debian ondersteunt een <a href="../ports">lange lijst</a> van CPU-architecturen,
    waaronder amd64, i386, meerdere versies van ARM en MIPS, POWER7, POWER8,
    IBM System z en RISC-V. Debian is ook beschikbaar voor niche-architecturen.
  </dd>
</dl>

<dl>
  <dt><strong>IoT en ingebedde apparaten</strong></dt>
  <dd>
    Debian werkt op een breed scala aan apparaten, zoals de
    Raspberry Pi, varianten van QNAP, mobiele apparaten, thuisrouters
    en veel Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Enorm aantal softwarepakketten</strong></dt>
  <dd>
    Debian heeft een groot aantal <a
    href="$(DISTRIB)/packages">pakketten</a> (momenteel in de distributie
    stable: <packages_in_stable> pakketten) die de indeling <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb
    gebruiken</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Verschillende releases</strong></dt>
  <dd>
    Naast onze stabiele release kunt u nieuwere softwareversies installeren
    door gebruik te maken van de releases testing en unstable.
  </dd>
</dl>

<dl>
  <dt><strong>Openbare bugopvolging</strong></dt>
  <dd>
    Ons Debian <a href="../Bugs">bugvolgsysteem</a> (BTS - Bug Tracking System)
    is publiekelijk beschikbaar voor iedereen via een webbrowser. We verbergen
    geen bugs in onze software en u kunt gemakkelijk nieuwe bugrapporten
    indienen of deelnemen aan de discussie.
  </dd>
</dl>

<dl>
  <dt><strong>Debian beleidsrichtlijnen en gereedschap voor ontwikkelaars</strong></dt>
  <dd>
    Debian biedt software van hoge kwaliteit. Om meer te weten over onze
    standaarden kunt u de <a href="../doc/debian-policy/">beleidsrichtlijnen</a>
    lezen die technische vereisten beschrijven waaraan elk pakket
    moet voldoen om opgenomen te worden in de distributie. Bij onze
    strategie voor ononderbroken integratie zijn
    Autopkgtest (voert tests uit op pakketten), Piuparts (test installeren,
    opwaarderen en verwijderen) en Lintian (controleert pakketten op
    inconsistenties en fouten) betrokken.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Debian staat synoniem voor stabiliteit. [...] Beveiliging is een van de belangrijkste kenmerken van Debian. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis op pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian voor bedrijfsomgevingen</a></h2>

<dl>
  <dt><strong>Debian is betrouwbaar.</strong></dt>
  <dd>
    Debian toont dagelijks zijn betrouwbaarheid aan in duizenden
    praktijkscenario's, variërend van laptops voor één gebruiker tot
    deeltjesversnellers, effectenbeurzen en de auto-industrie. Het is ook
    populair in de academische wereld en in de wetenschappelijke en de
    publieke sector.
  </dd>
</dl>

<dl>
  <dt><strong>Debian beschikt over vele experten.</strong></dt>
  <dd>
    Onze pakketbeheerders zorgen niet enkel voor de verpakking van toepassingen
    voor Debian en voor het integreren van nieuwe bovenstroomse versies. Vaak
    zijn ze ook experten op het gebied van de toepassing zelf en dragen
    ze daarom rechtstreeks bij tot de bovenstroomse ontwikkeling ervan.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is veilig.</strong></dt>
  <dd>
    Debian biedt beveiligingsondersteuning voor zijn stabiele releases.
    Veel andere distributies en beveiligingsonderzoekers steunen
    op het beveiligingsvolgsysteem van Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Langetermijnondersteuning</strong></dt>
  <dd>
    De versies van Debian met kosteloze
    <a href="https://wiki.debian.org/LTS">langetermijnondersteuning</a>
    (Long Term Support - LTS) verlengen de levensduur van alle stabiele
    releases van Debian tot minimum 5 jaar. Daarenboven ondersteunt het
    commerciële initiatief voor een
    <a href="https://wiki.debian.org/LTS/Extended">uitgebreide LTS</a>
    een beperkt aantal pakketten nog langer dan 5 jaar.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-images</strong></dt>
  <dd>
    Er zijn officiële cloud-images beschikbaar voor alle belangrijke
    cloudplatformen. We stellen ook de hulpmiddelen en de configuratie
    ter beschikking zodat u uw eigen aangepaste cloud-images kunt bouwen.
    U kunt Debian ook gebruiken in een virtuele machine op de desktop
    of in een container.
  </dd>
</dl>
