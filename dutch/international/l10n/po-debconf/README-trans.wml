#use wml::debian::template title="Lokalisatie (l10n) van Debconf-sjablonen met PO-bestanden &mdash; Tips voor vertalers"
#include "$(ENGLISHDIR)/international/l10n/dtc.def"
#use wml::debian::translation-check translation="1643e05b1b94ef4415d1f8951ebc10f32b6ad816"

<p>
#include "$(ENGLISHDIR)/international/l10n/po-debconf/menu.inc"
</p>

<h2>Algemene opmerkingen voor vertalers.</h2>

<ol>
  <li>
         Voordat u aan een vertaling begint, moet u altijd bij uw
         collega-vertalers op debian-l10n-&lt;<em>taal</em>&gt;@lists.debian.org
         (als een dergelijke lijst bestaat) controleren of niemand momenteel
         aan dezelfde vertaling werkt, en moet u de huidige bugrapporten lezen
         voor het pakket dat u gaat vertalen, om te zien of er al een vertaling
         werd gerapporteerd.
  </li>

  <li>
         Als u een vertaling wilt bijwerken, neem dan altijd contact op met de
         vorige vertaler om dubbel werk te voorkomen. E-mailadressen zijn te
         vinden in het PO-bestand.
  </li>

  <li>
         Om met een nieuwe vertaling te beginnen, kopieert u het bestand
         <tt>templates.pot</tt> naar <tt><em>xx</em>.po</tt>, waarbij
         <em>xx</em> de
         <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">\
         ISO-code</a> voor uw taal is. Specificeer dan in de eerste
         <tt>msgstr</tt> de tekenset die gebruikt wordt voor de vertaalde
         tekstfragmenten, en plaats daar ook alle nuttige informatie.
  </li>
  <li>
         Elke algemene teksteditor kan worden gebruikt om aan PO-bestanden te
         werken, en er bestaan ook enkele specifieke gereedschappen, zoals
         <a href="https://packages.debian.org/unstable/text/gtranslator">\
         gtranslator</a>, dat handig kan zijn als u niet vertrouwd bent met
         PO-bestanden.
         <a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_toc.html">\
         Documentatie</a> over
         <a href="https://packages.debian.org/unstable/devel/gettext">\
         Gettext</a>
         bevat uitleg over de indeling van PO-bestanden en minstens zou u
         de twee volgende onderdelen moeten lezen:
         <a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_35.html#SEC35">\
         Filling in the Header Entry</a> (De koptekst invullen) en
         <a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_9.html#SEC9">\
         The Format of PO Files</a> (De indeling van PO-bestanden).
  </li>

  <li>
         Als u klaar bent met uw vertaling, herlees dan altijd uw bestand
         minstens één keer om alle betekenis-, spellings-, grammatica-,
         typefouten en andere fouten te corrigeren. (Misschien bevat uw
         PO-editor een spellingcontrole of een vertaalhulp zoals acheck).
         Voer daarna het commando
         <pre>
            msgfmt -c -v -o /dev/null <em>UW_TAAL_CODE</em>.po
         </pre>
         uit om te controleren of uw bestand geldig is en probleemloos
         opgenomen kan worden. Afhankelijk van het beleid van uw taalteam, kunt
         u uw vertaling ook naar uw taalspecifieke l10n-lijst sturen zodat ze
         kan nagelezen worden.
  </li>

  <li>
         Wanneer uw vertaling klaar en geldig is, dient u een
         <a href="$(HOME)/Bugs/Reporting">bugrapport</a> in
         tegen het vertaalde pakket, met als ernstgraad
         <a href="$(HOME)/Bugs/Developer#severities">wishlist</a>. Tag uw
         rapport met de <a href="$(HOME)/Bugs/Developer#tags">markeringen</a>
         <tt>l10n</tt> en <tt>patch</tt>>, voeg
         <tt><protect>[INTL:</protect><em>xx</em>]</tt> toe (waarbij
         <em>xx</em> de
         <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">\
         ISO-code</a> voor uw taal is) in het onderwerpveld om verdere
         opzoekingen nog makkelijker te maken, vraag beleefd dat uw vertaling
         wordt opgenomen, geef de pakketbeheerder instructies over wat deze
         moet doen met dit bestand (in het Engels, aangezien dit de voertaal is
         in Debian; bijv.
         <strong>Please copy the attachment into debian/po/nl.po</strong>) en
         vergeet niet uw vertaling toe te voegen. Deze stap kan heel
         eenvoudig worden uitgevoerd met het hulpprogramma
         <a href="https://packages.debian.org/unstable/utils/reportbug">\
         reportbug</a>.
  </li>
</ol>

<h2>Opmerkingen die specifiek zijn voor po-debconf</h2>

<ol>
  <li>
         De naam van het veld wordt weergegeven als commentaar vóór
         <tt>msgstr</tt>.
         <tt>Default</tt>-waarden zijn bijzonder voor debconf, omdat er bij
         het sjabloontype <tt>Select</tt> of <tt>Multiselect</tt>
         <strong>geen</strong> vertaling mag gebeuren, maar hun waarden kunnen
         (in zeldzame gevallen) wel worden gewijzigd naar een andere tekenreeks
         die wordt vermeld in het Engelse veld <tt>Choices</tt>. Om deze reden
         zouden ontwikkelaars <tt>DefaultChoice</tt> moeten gebruiken in plaats
         van <tt>Default</tt> om verwarring te voorkomen.
         <strong>Dit is een uitbreiding van po-debconf.</strong>
         In het gegenereerde sjabloonbestand wordt natuurlijk <tt>Default</tt>
         afgedrukt, aangezien debconf dit veld <tt>DefaultChoice</tt> niet
         definieert.
  </li>

  <li>
         Omdat instructies voor vertalers kunnen worden ingevoegd door
         ontwikkelaars, moet u ervoor zorgen dat het gereedschap dat u gebruikt,
         in staat is om deze commentaren weer te geven (zonder
         onregelmatigheden). Zie bijvoorbeeld instructies om met taalselectie
         om te gaan in het pakket
         <a href="https://packages.debian.org/unstable/misc/geneweb">\
         geneweb</a>.
  </li>

  <li>
         Soms zijn msgids identiek terwijl msgstr verschillende waarden kan
         hebben. Om msgids uniek te maken, kan aan het einde van de tekenreeks
         een speciale tekst worden ingevoegd, meer in het bijzonder wordt elke
         opeenvolging van
         <ul>
             <li>een linker vierkant haakje: <tt>[</tt></li>
             <li>een spatie</li>
             <li>nul of meer lettertekens met uitzondering van een linker
             vierkant haakje, een rechter vierkant haakje en een regeleinde</li>
             <li>een rechter vierkant haakje: <tt>]</tt></li>
         </ul>
         aan het einde van een tekenreeks verwijderd uit
         <tt>msgid</tt>-tekenreeksen.
         Dit is ook van toepassing op <tt>msgstr</tt>-tekenreeksen,
         zodat vertalers een lege tekenreeks kunnen invoegen.
  </li>

  <li>
         Regels worden door debconf afgebroken, behalve als ze beginnen met een
         spatie. Zulke regels worden vaak gebruikt om lijsten op te maken, maar
         elke frontend heeft zijn eigen beperkingen. Het lijkt erop dat
         dergelijke regels niet langer mogen zijn dan 72 tekens om redelijk
         goed op alle frontends te worden weergegeven.
  </li>

  <li>
         Het script <tt>podebconf-display-po</tt> (uit po-debconf &gt;= 0.8.3)
         is in staat om uw vertaling te tonen zoals deze zal worden weergegeven
         door debconf tijdens de configuratie. Voer het commando
         <pre>
            podebconf-display-po -fdialog debian/po/<em>UW_TAAL_CODE</em>.po
         </pre>
         uit om een idee te hebben van hoe uw vertaling eruit zal zien in grote
         lijnen. Maar dit werkt alleen voor basale configuratiebestanden; de
         weergave kan variëren als complexe bewerkingen (zoals intensief
         gebruik van tekstvervanging) worden uitgevoerd. Beschikbare
         debconf-frontends (die kunnen worden geselecteerd met de
         vlag <tt>-f</tt>) worden vermeld in debconf(7).
  </li>
</ol>

#include "$(ENGLISHDIR)/international/l10n/date.gen"
