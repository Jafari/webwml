#use wml::debian::template title="Errata: Linux 2.2.x gebruiken in slink"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<p>

Deze pagina beschrijft bekende problemen met het uitvoeren van de
Linux 2.2.x-kernel in Debian 2.1 (slink). Er wordt van uitgegaan dat u een
volledig up-to-date slink-systeem gebruikt.

<p>

De slink-release is gecertificeerd en officieel getest voor gebruik met de late
2.0.x Linux-kernels. Omdat de tijdelijke kodebevriezing van Debian plaatsvond vóór
de release van Linux 2.2, en de verandering van het hoofdversienummer van de kernel
complexe problemen met zich mee kan brengen, werd besloten om vast te houden aan
de beproefde kernellijn 2.0.

<p>

Debian-releases zijn echter niet noodzakelijkerwijs gebonden aan een kernelversie.
U kunt elke gewenste kernel in Debian gebruiken. We kunnen echter gewoon geen
garanties geven dat alles naar behoren zal functioneren. Als u besluit om over te
stappen naar de Linux 2.2-lijn, en u ondervindt problemen met een pakket, dan hebt
u misschien meer geluk met de <a href="../potato/">potato</a>-versie (Debian 2.2)
van dat pakket.

<p>

Hieronder staan veel links die verwijzen naar versies van pakketten uit potato.
Merk op dat als u deze pakketten op een stabiele machine installeert, u misschien
ook bibliotheken uit potato of andere pakketvereisten zult moeten installeren. In
het bijzonder zult u waarschijnlijk uw libc6-pakket moeten opwaarderen. We stellen
voor om hiervoor <code>apt-get</code> te gebruiken, dat alleen de benodigde
pakketten zal ophalen als het correct gebruikt wordt. Wees echter gewaarschuwd:
hoewel de meeste gebruikers geen problemen ondervinden met het gebruiken van een
gemengd systeem met pakketten uit de stabiele en bevroren releases, kunt u
getroffen worden door voorbijgaande bugs in potato.

<p>

Het pakket <a href="https://packages.debian.org/kernel-source-2.2.1">
kernel-source-2.2.1</a> wordt aangeboden in de distributie om gebruikers te helpen
die de Linux 2.2.x kernels willen gebruiken. Het wordt echter aangeraden om de
standaard kerneldistributiesites, zoals
<a href="https://www.kernel.org/">kernel.org</a>, te raadplegen voor nieuwere
versies van de 2.2.x broncodeboom en aanvullende errata. Er zijn bekende bugs in
2.2.1, en van die versie is bekend dat zij bij sommigen databeschadiging heeft
veroorzaakt. U zou de patches voor de nieuwere 2.2 kernel moeten ophalen en ze
toepassen op de Linux kernel broncodeboom.


<h2>Potentieel problematische pakketten</h2>

<p>

Merk op dat deze lijst onvolledig kan zijn. Dien een bug in tegen www.debian.org
als u andere problemen vindt die niet in de lijst staan. Controleer ook de buglogs
voor het pakket in kwestie; probeer er zeker van te zijn dat het probleem werd
geïntroduceerd in Linux 2.2.

<dl>
	<dt><a href="https://packages.debian.org/sysutils">sysutils</a>
	<dd>
<tt>procinfo</tt> zal niet werken. De versie uit
<a href="https://www.debian.org/Packages/frozen/utils/sysutils.html">
potato</a> lost dit op.

	<dt><a href="https://packages.debian.org/netbase">netbase</a>
	<dd>
In Linux 2.2 moet <tt>ipautofw</tt> vervangen worden door <tt>ipmasqadm</tt>
en <tt>ipfwadm</tt> wordt vervangen door <tt>ipchains</tt>. Het pakket
<a href="https://www.debian.org/Packages/frozen/base/netbase.html">
netbase</a> uit potato bevat een verpakkingsscript
<tt>ipfwadm-wrapper</tt> om de transitie te vergemakkelijken.
<p>
<tt>ifconfig</tt> zal geen aliasinterfaces tonen, en onder bepaalde omstandigheden
zal <tt>ipchains</tt> er in stilte niet in slagen de pakkettellers te wissen.
Sommige routes die door de init-scripts van <tt>netbase</tt> zijn gebouwd, leveren
onschuldige waarschuwingsberichten op.
<p>
Al deze problemen worden opgelost in de <a
href="https://www.debian.org/Packages/frozen/base/netbase.html">
potato</a>-versie. Indien u niet wenst op te waarderen naar potato, werden Debian
2.1-compatibele pakketten <a
href="https://www.debian.org/~rcw/2.2/netbase/">beschikbaar gesteld</a>.

	<dt><a href="https://packages.debian.org/pcmcia-source">pcmcia-source</a>
	<dd>
De versie van <tt>pcmcia-source</tt> in slink kan niet gecompileerd worden met
de 2.2 kernel. Opgelost in de versie van
<a href="https://www.debian.org/Packages/frozen/admin/pcmcia-source.html">
potato</a>.

	<dt><a href="https://packages.debian.org/dhcpcd">dhcpcd</a>
	<dd>
Wordt onbruikbaar onder Linux 2.2; gebruik de versie uit <a
href="https://www.debian.org/Packages/frozen/net/dhcpcd.html">
potato</a>.

	<dt><a href="https://packages.debian.org/dhcp-client-beta">dhcp-client-beta</a>
	<dd>
Het <tt>/etc/dhclient-script</tt> werkt niet met 2.2. De versie uit
<a href="https://www.debian.org/Packages/frozen/net/dhcp-client.html">
potato</a> lost dit op; merk op dat het pakket hernoemd werd naar gewoonweg
<code>dhcp-client</code>.

	<dt><a href="https://packages.debian.org/wanpipe">wanpipe</a>
	<dd>
Versie 2.0.1, in slink, is incompatibel met de 2.2. kernels.
Versie 2.0.4 en hoger, die u kunt halen uit
<a href="https://www.debian.org/Packages/frozen/net/wanpipe.html">
potato</a>, zal werken voor de 2.2 kernels, maar niet met de 2.0
kernels (een kernel-patch voor 2.0 is echter ingesloten in de versie van potato).

	<dt><a href="https://packages.debian.org/netstd">netstd</a>
	<dd>
<tt>bootpc</tt> zal geen antwoord ontvangen tenzij de interface al is
geconfigureerd. Dit is opgelost in het nu aparte <a
href="https://packages.debian.org/bootpc">pakket bootpc</a> in potato.

	<dt><a href="https://packages.debian.org/lsof">lsof</a>
	<dd>
<tt>lsof</tt> moet opnieuw gecompileerd worden om met Linux 2.2 te werken.
Waardeer op naar het pakket <tt>lsof</tt> dat beschikbaar is in potato.

	<dt><a href="https://packages.debian.org/acct">acct</a>
	<dd>
In de 2.2-kernel is de administratiestructuur veranderd. Dus indien u
<tt>acct</tt> gebruikt en Linux 2.2, zult u de versie van het pakket uit potato
moeten hebben (dat incompatibel is met de kernellijn 2.0).

	<dt><a href="https://packages.debian.org/isdnutils">isdnutils</a>
	<dd>
<tt>isdnutils</tt> 3.0 of hoger in Debian zou moeten werken met zowel de 2.0
als de 2.2 kernellijn.  Dit is alleen het geval omdat de pakketbeheerder van
Debian speciale moeite heeft gedaan om er zeker van te zijn dat dit het geval is.
Andere distributies hebben misschien niet zoveel geluk.

	<dt><a href="https://packages.debian.org/diald">diald</a>
	<dd>
Het pakket <tt>diald</tt> uit slink ondervindt problemen met het dynamisch
creëren van routes in Linux 2.2. Waardeer op naar de versie die in
potato beschikbaar is.

        <dt><a href="https://packages.debian.org/xosview">xosview</a>
	<dd>
<tt>xosview</tt> geeft een eindeloze lus met Linux 2.2.2 en hoger.
Waardeer op naar de versie die in potato beschikbaar is.



</dl>


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
