#use wml::debian::template title="Debian 12 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ba68acca0765fe2fd2a7fc056b6c929651151770"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Bekende problemen</toc-add-entry>
<toc-add-entry name="security">Beveiligingsproblemen</toc-add-entry>

<p>Het Debian beveiligingsteam brengt updates uit voor pakketten uit de stabiele
release waarin problemen in verband met beveiliging vastgesteld werden.
Raadpleeg de <a href="$(HOME)/security/">beveiligingspagina's</a> voor
informatie over eventuele beveiligingsproblemen die in <q>bookworm</q> ontdekt
werden.</p>

<p>Als u APT gebruikt, voeg dan de volgende regel toe aan
<tt>/etc/apt/sources.list</tt> om toegang te hebben tot de laatste
beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free non-free-firmware
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Tussenreleases</toc-add-entry>

<p>Soms, in het geval van diverse kritieke problemen of beveiligingsupdates,
wordt de uitgebrachte distributie bijgewerkt. Een
dergelijke bijwerking wordt gewoonlijk aangeduid als een tussenrelease.</p>

<ul>
  <li>De eerste tussenrelease, 12.1, werd uitgebracht op
      <a href="$(HOME)/News/2023/20230722">22 juli 2023</a>.</li>
  <li>De tweede tussenrelease, 12.2, werd uitgebracht op
      <a href="$(HOME)/News/2023/20231007">7 oktober 2023</a>.</li>
  <li>De derde tussenrelease, 12.3, werd uitgesteld op
      <a href="$(HOME)/News/2023/2023120902">9 december 2023</a>.</li>
  <li>De vierde tussenrelease 12.4, werd uitgebracht op
     <a href="$(HOME)/News/2023/20231210">10 december 2023</a>.</li>
  <li>De vijfde tussenrelease 12.5, werd uitgebracht op
     <a href="$(HOME)/News/2024/20240210">10 februari 2024</a>.</li>
  <li>De zesde tussenrelease 12.6, werd uitgebracht op
     <a href="$(HOME)/News/2024/20240629">29 juni 2024</a>.</li>
  <li>De zevende tussenrelease 12.7, werd uitgebracht op
     <a href="$(HOME)/News/2024/20240831">31 augustus 2024</a>.</li>
  <li>De achtste tussenrelease 12.8, werd uitgebracht op
     <a href="$(HOME)/News/2024/20241109">9 november 2024</a>.</li>
</ul>


<ifeq <current_release_bookworm> 12.0 "

<p>Er zijn nog geen tussenreleases voor Debian 12.</p>" "

<p>Zie de <a
href="http://deb.debian.org/debian/dists/bookworm/ChangeLog">\
ChangeLog</a>
voor details over wijzigingen tussen 12 en <current_release_bookworm/>.</p>"/>


<p>Verbeteringen voor de uitgebrachte stabiele distributie gaan dikwijls door een
uitgebreide testperiode voordat ze in het archief worden aanvaard.
Deze verbeteringen zijn echter wel beschikbaar in de map
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> van elke Debian archief-spiegelserver.</p>

<p>Als u APT gebruikt om uw pakketten bij te werken, dan kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# voorgestelde toevoegingen aan een tussenrelease van 12
  deb https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>Voer daarna <kbd>apt update</kbd> uit, gevolgd door
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installatiesysteem</toc-add-entry>

<p>
Raadpleeg voor informatie over errata en updates van het installatiesysteem
de pagina met <a href="debian-installer/">installatie-informatie</a>.
</p>
