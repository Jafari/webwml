#use wml::debian::template title="Debian &ldquo;bookworm&rdquo; Installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741"

<h1>Debian installeren <current_release_bookworm></h1>

<if-stable-release release="trixie">
<p><strong>Debian 12 is vervangen door
<a href="../../trixie/">Debian 13 (<q>trixie</q>)</a>. Sommige van de
installatie-images hieronder zijn mogelijk niet langer beschikbaar of werken
niet meer. Het wordt aanbevolen om in de plaats daarvan trixie te installeren.
</strong></p>
</if-stable-release>

<p>
<strong>Voor het installeren van Debian</strong> <current_release_bookworm>
(<em>bookworm</em>), kunt u een van de volgende images downloaden (alle i386 en amd64
cd/dvd-images zijn ook bruikbaar op USB-stick):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst cd-image</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>volledige cd-sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>volledige dvd-sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere images (netboot, flexibele usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>



<p>
<strong>Opmerkingen</strong>
</p>
<ul>
    <li>
	Voor het downloaden van volledige cd- of dvd-images wordt het gebruik van
	BitTorrent of jigdo aanbevolen.
    </li><li>
	Voor de minder gebruikelijke architecturen is enkel een beperkt aantal
	images uit de cd- of dvd-set beschikbaar als ISO-bestand of via BitTorrent.
	De volledige sets zijn enkel via jigdo beschikbaar.
    </li><li>
	Voor de installatie-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze <a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:
</p>

<ul>
<li><a href="../installmanual">Bookworm Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met gekende problemen in het installatiesysteem van
Debian <current_release_bookworm>. Indien u bij het installeren van Debian op
een probleem gestoten bent en dit probleem hier niet vermeld vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installatierapport</a>
waarin u het probleem beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata voor release 12.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Voor de volgende release van Debian wordt gewerkt aan een verbeterde versie
van het installatiesysteem, die u ook kunt gebruiken om bookworm te installeren.
Raadpleeg voor de concrete informatie
<a href="$(HOME)/devel/debian-installer/">de pagina van het Debian-Installer project</a>.
</p>

