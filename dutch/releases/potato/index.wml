#use wml::debian::template title="Debian GNU/Linux 2.2 ('potato') release-informatie" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="dc28c67a4a9013b56f1b40512f11f8af875d6a6c"

<p>Debian GNU/Linux 2.2 (ook bekend als Potato) werd uitgebracht op
<:=spokendate ("2000-08-14"):>.  De laatste tussenrelease van Debian 2.2 werd
<current_release_potato> uitgebracht op <a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>

<p><strong>Debian GNU/Linux 2.2 werd vervangen door
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
Er worden geen beveiligingsupdates meer uitgebracht sinds 30 juni 2003.</strong>
raadpleeg voor meer informatie
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
de resultaten van de enquête van het beveiligingsteam</a>.</p>

<p>Raadpleeg voor informatie over de belangrijkste wijzigingen in deze release
de <a href="releasenotes">Notities bij de release</a> en het officiële
<a href="$(HOME)/News/2000/20000815">persbericht</a>.</p>

<p>Debian GNU/Linux 2.2 werd opgedragen aan de nagedachtenis van Joel "Espy" Klecker, een ontwikkelaar van Debian, die zonder dat het grootste deel van het Debian Project er van op de hoogte was, tijdens het grootste deel van zijn betrokkenheid bij Debian bedlegerig was en vocht tegen een ziekte die bekend staat als de spierdystrofie van Duchenne. Pas nu realiseert het Debian Project zich de omvang van zijn toewijding en de vriendschap die hij ons heeft geschonken. Dus als blijk van waardering en ter nagedachtenis aan zijn inspirerende leven, is deze release van Debian GNU/Linux aan hem opgedragen.</p>

<p>Debian GNU/Linux 2.2 is is verkrijgbaar via het internet of via cd-verkopers.
Raadpleeg de <a href="$(HOME)/distrib/">pagina over het verdelen van de distributie</a>
voor bijkomende informatie over het verkrijgen van Debian.</p>

<p>De volgende architecturen werden in deze release ondersteund:</p>

<ul>
<li><a href="/ports/alpha/">alpha</a>
<li><a href="/ports/arm/">arm</a>
<li><a href="/ports/i386/">i386</a>
<li><a href="/ports/m68k/">m68k</a>
<li><a href="/ports/powerpc/">powerpc</a>
<li><a href="/ports/sparc/">sparc</a>
</ul>

<p>Lees eerst de installatiehandleiding voordat u Debian installeert. De Installatiehandleiding voor uw doelarchitectuur bevat instructies en links voor alle bestanden die u moet installeren. Mogelijk bent u ook geïnteresseerd in de installatiegids voor Debian 2.2, een online tutorial.</p>

<p>Als u APT gebruikt, kunt u de volgende regels gebruiken in uw bestand <code>/etc/apt/sources.list</code> om toegang te krijgen tot de pakketten van potato:</p>

<pre>
  deb http://archive.debian.org potato main contrib non-free
  deb http://non-us.debian.org/debian-non-US potato/non-US main non-free
</pre>

<p>Lees voor bijkomende informatie de man-pagina's <code>apt-get</code>(8) en <code>sources.list</code>(5).</p>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release van woody, ondanks dat deze <em>stabiel</em> werd verklaard. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a> gemaakt
en u kunt ons altijd andere problemen rapporteren.</p>
