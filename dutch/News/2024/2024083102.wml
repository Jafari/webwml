#use wml::debian::translation-check translation="f93b89c74962ae015feaa58f013cef8f7ec08dd9"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.11 werd uitgebracht</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de elfde en laatste update aan van oldstable,
zijn voorgaande stabiele distributie, Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>
Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update is van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Secure Boot (veilig opstarten) en andere besturingssystemen</h2>

<p>
Gebruikers die andere besturingssystemen opstarten op dezelfde hardware, en die
Secure Boot ingeschakeld hebben, moeten zich ervan bewust zijn dat shim 15.8
(ingesloten in Debian <revision>) handtekeningen in oudere versies van shim in
de UEFI-firmware intrekt. Hierdoor kunnen andere besturingssystemen die een shim
versie gebruiken vóór 15,8, niet opstarten.</p>

<p>Getroffen gebruikers kunnen tijdelijk Secure Boot uitschakelen voordat
andere besturingssystemen worden bijgewerkt.</p>

<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van oldstable, de voorgaande stabiele distributie, worden
een paar belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction amd64-microcode "Nieuwe bovenstroomse release; veiligheidsreparaties [CVE-2023-31315]; reparaties aan SEV firmware [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "Nieuwe bovenstroomse stabiele release; oplossen van probleem met sjabloon-injectie [CVE-2021-3583], probleem met openbaarmaking van informatie [CVE-2021-3620], probleem van bestandsoverschrijving [CVE-2023-5115], probleem met sjabloon-injectie [CVE-2023-5764], problemen met openbaarmaking van informatie [CVE-2024-0690 CVE-2022-3697]; tijdelijke oplossing in document voor een lek van een ec2 private sleutel [CVE-2023-4237]">
<correction apache2 "Nieuwe bovenstroomse stabiele release; oplossen van probleem met openbaarmaking van informatie [CVE-2024-40725]">
<correction base-files "Update voor de tussenrelease">
<correction bind9 "Toestaan dat de grenswaarden die zijn geïntroduceerd om CVE-2024-1737 te repareren, worden geconfigureerd">
<correction calibre "Problemen met cross-site scripting oplossen [CVE-2024-7008], probleem met SQL-injectie [CVE-2024-7009]">
<correction choose-mirror "Lijst van beschikbare spiegelservers bijwerken">
<correction cjson "NULL-controles toevoegen aan cJSON_SetValuestring en cJSON_InsertItemInArray [CVE-2023-50472 CVE-2023-50471 CVE-2024-31755]">
<correction cups "Problemen met domeinsocketverwerking oplossen [CVE-2024-35235]; regressie oplossen wanneer alleen domeinsockets worden gebruikt">
<correction curl "Repareren van probleem met overlezen van de ASN.1 datumontleder [CVE-2024-7264]">
<correction debian-installer "Linux kernel ABI verhogen naar 5.10.0-32; herbouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Herbouwen tegen proposed-updates">
<correction dropbear "Reparatie voor <q>noremotetcp</q>-gedrag van keepalive-pakketten in combinatie met de restrictie <q>no-port-forwarding</q> authorized_keys(5)">
<correction fusiondirectory "Compatibiliteit met php-cas-versie-adressering toepassen CVE 2022-39369; probleem met onjuiste sessieverwerking oplossen [CVE-2022-36179]; probleem met cross-site scripting oplossen [CVE-2022-36180]">
<correction gettext.js "Oplossen probleem met vervalsing van aanvragen aan de serverkant [CVE-2024-43370]">
<correction glewlwyd "Bufferoverloop oplossen tijdens webauthn-handtekeningbevestiging [CVE-2022-27240]; mapoverschrijding voorkomen in static_compressed_inmemory_website_callback.c [CVE-2022-29967]; bootstrap, jquery, fork-awesome kopiëren in plaats van er een koppeling naar te maken; bufferoverloop tijdens validering van FIDO2 handtekening [CVE-2023-49208]">
<correction glibc "Oplossing voor ffsll()-prestatieprobleem afhankelijk van de code-uitlijning; prestatieverbeteringen voor memcpy() op arm64; oplossing voor y2038-regressie in nscd als gevolg van oplossing voor CVE-2024-33601 en CVE-2024-33602">
<correction graphviz "Fouten bij schalen oplossen">
<correction gtk+2.0 "Zoeken naar modules in de huidige werkmap vermijden [CVE-2024-6655]">
<correction gtk+3.0 "Zoeken naar modules in de huidige werkmap vermijden [CVE-2024-6655]">
<correction healpix-java "Reparatie voor bouwfout">
<correction imagemagick "Problemen met delen door nul oplossen [CVE-2021-20312 CVE-2021-20313]; oplossing voor onvolledige oplossing voor CVE-2023-34151">
<correction indent "ROUND_UP macro herstellen en de initiële buffergrootte aanpassen om geheugenverwerkingsproblemen op te lossen; oplossen van probleem van lezen buiten de buffer in search_brace()/lexi(); oplossen van overschrijven van heap buffer in search_brace() [CVE-2023-40305]; te laag aflezen van heap buffer in set_buf_break() [CVE-2024-0911]">
<correction intel-microcode "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction libvirt "Oplossen van sVirt-inperkingsprobleem [CVE-2021-3631], van probleem met gebruik na vrijgave [CVE-2021-3975], van problemen van denial of service [CVE-2021-3667 CVE-2021-4147 CVE-2022-0897 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction midge "Weglaten van examples/covers/* met het oog op overeenstemming met DFSG; toevoegen van bouwdoelen build-arch/build-indep; gebruiken van quilt (3.0) als indeling voor bronpakket">
<correction mlpost "Oplossing voor bouwfout met recentere ImageMagick versies">
<correction net-tools "Verwijderen van libdnet-dev als bouwvereiste">
<correction nfs-utils "Alle geldige exportvlaggen aan nfsd doorgeven">
<correction ntfs-3g "Oplossing voor gebruik na vrijgave in <q>ntfs_uppercase_mbs</q> [CVE-2023-52890]">
<correction nvidia-graphics-drivers-tesla-418 "Oplossing voor mislukkende bouw door het gebruik van GPL-eigen symbolen">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse stabiele release">
<correction nvidia-graphics-drivers-tesla-460 "Nieuwe bovenstroomse stabiele release">
<correction ocsinventory-server "Compatibiliteit inbouwen met benaderen van php-cas versie CVE 2022-39369">
<correction onionshare "Vereiste obfs4proxy afwaarderen tot Recommends om verwijdering van obfs4proxy mogelijk te maken">
<correction php-cas "Oplossing voor probleem van exploitatie van servicehostnaamdetectie [CVE-2022-39369]">
<correction poe.app "Commentaarcellen bewerkbaar maken; tekening repareren wanneer een NSActionCell in de voorkeuren wordt opgevolgd om de status te wijzigen">
<correction putty "Oplossing voor het genereren van een te zwak ECDSA nonce waardoor de geheime sleutel kan gevonden worden [CVE-2024-31497]">
<correction riemann-c-client "Voorkomen van misvormde lading in GnuTLS-bewerkingen verzenden/ontvangen">
<correction runc "Repareren van url van busybox tarball; bufferoverloop voorkomen bij schrijven van netlink-berichten [CVE-2021-43784]; reparatie voor tests op recentere kernels; schrijftoegang tot gebruikerscgrouphiërarchie <q>/sys/fs/cgroup/user.slice/...</q> voorkomen [CVE-2023-25809]; regressie van toegangscontrole oplossen [CVE-2023-27561 CVE-2023-28642]">
<correction rustc-web "Nieuwe bovenstroomse stabiele release ter ondersteuning van het bouwen van nieuwe versies van chromium en firefox-esr">
<correction shim "Nieuwe bovenstroomse release">
<correction shim-helpers-amd64-signed "Opnieuw bouwen tegen shim 15.8.1">
<correction shim-helpers-arm64-signed "Opnieuw bouwen tegen shim 15.8.1">
<correction shim-helpers-i386-signed "Opnieuw bouwen tegen shim 15.8.1">
<correction shim-signed "Nieuwe bovenstroomse stabiele release">
<correction symfony "Oplossing voor automatisch laden van HttpClient">
<correction trinity "Oplossing voor mislukken van bouw wanneer ondersteuning voor DECNET weggelaten wordt">
<correction usb.ids "Update van de ingesloten gegevenslijst">
<correction xmedcon "Oplossing voor heapoverloop [CVE-2024-29421]">
</table>


<h2>Beveiligingsupdate</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
release oldstable. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies ID</th>  <th>Pakket</th></tr>
<dsa 2024 5718 org-mode>
<dsa 2024 5719 emacs>
<dsa 2024 5721 ffmpeg>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5730 linux-signed-amd64>
<dsa 2024 5730 linux-signed-arm64>
<dsa 2024 5730 linux-signed-i386>
<dsa 2024 5730 linux>
<dsa 2024 5734 bind9>
<dsa 2024 5736 openjdk-11>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5742 odoo>
<dsa 2024 5743 roundcube>
<dsa 2024 5746 postgresql-13>
<dsa 2024 5747 linux-signed-amd64>
<dsa 2024 5747 linux-signed-arm64>
<dsa 2024 5747 linux-signed-i386>
<dsa 2024 5747 linux>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten zijn verwijderd door omstandigheden waar wij geen invloed op hadden:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction bcachefs-tools "Vertoont problemen, is verouderd">
<correction dnprogs "Vertoont problemen, is verouderd">
<correction iotjs "Niet langer onderhouden, veiligheidsbekommernisssen">
<correction obfs4proxy "Beveiligingsproblemen">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in oldstable, de vorige stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


