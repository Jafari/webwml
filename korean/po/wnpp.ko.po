# wnpp.ko.po
#
# Sebul <sebuls@gmail.com>, 2022.
# Changwoo Ryu <cwryu@debian.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-10-11 23:41+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "입양 요청 없음"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "고아 패키지 없음"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "입양을 기다리는 패키지 없음"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "캐키지 되길 기다리는 패키지 없음"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "패키지 요청 없음"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "도움 요청 없음"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "오늘 이래로 입양."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "어제부터 입양."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "입양 %s일."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "입양 %s일, 최근 활동 오늘."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "입양 %s일, 최근 활동 어제."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "입양 %s일, 최근 활동 %s일 전."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "오늘부터 준비."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "어제부터 준비."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "준비 %s일."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "준비 %s일, 최근 활동 오늘."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "준비 %s일, 최근 활동 어제."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "준비 %s일, 최근 활동 %s일 전."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "오늘부터 입양 요청."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "어제부터 입양 요청."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "%s일 전부터 입양 요청."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "오늘부터 고아."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "어제부터 고아."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "%s일 전부터 고아."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "오늘 요청."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "어제 요청."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "%s일 전 요청."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "패키지 정보"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "순위:"
