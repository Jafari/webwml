#use wml::debian::template title="사람들: 우리는 누구, 우리는 무엇을 하는가" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Sebul" 

# translators: some text is taken from /intro/about.wml
# Sebul, 2021.11.5.
# Hyun-gwan Seo <westporch@gmail.com>, 2024. 8. 14.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">모든 것은 어떻게 시작되었나요</a>
    <li><a href="#devcont">개발자와 기여자</a>
    <li><a href="#supporters">데비안을 지원하는 개인 및 조직</a>
    <li><a href="#users">데비안 사용자</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 많은 사람이 묻습니다: 데비안 발음은 <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span> 데비안 개발자, Ian Murdock, 그리고 그 아내, Debra에서 이름 지었습니다.</p>
</aside>

<h2><a id="history">모든 것은 어떻게 시작되었나요</a></h2>

<p>1993년, 이안 머독이 리눅스와 GNU 정신으로 공개적으로 만들 새 운영체제 작업을 시작했습니다.
그는 공개초청장을 다른 소프트웨어 개발자에게 보내서, 상대적으로 새로운 리눅스 커널 기반 소프트웨어 배포에 기여하도록 요청했습니다.
데비안은 자유소프트웨어 커뮤니티 공개 설계, 기여및 지원을 받으면서 
조심스럽고 신중하게 조합하고 유지되고 지원되도록 되었습니다.</p>

<p>작고, 긴밀한 자유 소프트웨어 해커 그룹으로 시작해서 점점 커져서 
개발자, 기여자, 사용자로 잘 구성된 커다란 커뮤니티로 되었습니다.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">완전한 역사 읽기</a></button></p>

<h2><a id="devcont">개발자 및 기여자</a></h2>

<p>데비안은 모두 자원봉사 단체입니다.
1000명 이상 활동적인 개발자가 <a href="$(DEVEL)/developers.loc">전 세계에</a> 퍼져서
여유 시간에 데비안 작업을 합니다. 
실제로 만나는 사람은 거의 없습니다. 
대신, 주로 전자메일(메일링 리스트<a href="https://lists.debian.org/">lists.debian.org</a>) 
및 IRC(channel #debian at irc.debian.org)로 소통합니다.</p>

<p>공식 데비안 멤버 목록은 
<a href="https://nm.debian.org/members">nm.debian.org</a>, 그리고
<a href="https://contributors.debian.org">contributors.debian.org</a>
에서 데비안 배포 작업을 하는 모든 기여자와 팀을 보여줍니다.</p>

<p>데비안 프로젝트는 조심스럽게 <a href="organization">구성된 기관</a>입니다. 
데비안 프로젝트가 내부에서 어떻게 보이는지에 대한 정보는
<a href="$(DEVEL)/">개발자 코너</a>를 보십시오.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">우리 철학</a></button></p>

<h2><a id="supporters">데비안을 지원하는 개인 및 조직</a></h2>

<p>개발자와 기여자 외에도, 많은 다른 개인 및 조직이 데비안 커뮤니티의 일부입니다:
</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">호스팅 및 하드웨어 후원자</a></li>
  <li><a href="../mirror/sponsors">미러 후원자</a></li>
  <li><a href="../partners/">개발 및 서비스 파트너</a></li>
  <li><a href="../consultants">컨설턴트</a></li>
  <li><a href="../CD/vendors">데비안 설치 미디어 공급업체</a></li>
  <li><a href="../distrib/pre-installed">데비안 설치한 컴퓨터 공급업체</a></li>
  <li><a href="../events/merchandise">상품 공급업체</a></li>
</ul>

<h2><a id="users">데비안 사용자</a></h2>

<p>
데비안은 1000명 넘는 개인은 물론, 크고 작은 다양한 조직에서 씁니다.
어떻게 왜 데비안을 쓰는지 간단한 설명을 제출한 교육, 상업, 비영리 단체 목록은 
<a href="../users/">누가 데비안을 쓰나요?</a> 페이지를 보십시오.
</p>
