#use wml::debian::template title="Documentación" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="212af30808c438c1827b5edf929ea56c7f379f79" maintainer="Laura Arjona Reina"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Comienzo rápido</a></li>
  <li><a href="#manuals">Manuales</a></li>
  <li><a href="#other">Otros documentos (más cortos)</a></li>
</ul>

<p>Crear un sistema operativo libre de gran calidad también incluye escribir
manuales técnicos que describen la operación y uso de los programas.
El Proyecto Debian está esforzándose en proporcionar a todos sus
usuarios documentación adecuada y accesible de manera sencilla.
Esta página contiene una colección de enlaces que llevan a guías de instalación, CÓMOs,
preguntas frecuentes, notas de publicación, nuestra Wiki y más.
</p>


<h2><a id="quick_start">Comienzo rápido</a></h2>

<p>
Si es la primera vez que trata con Debian le recomendamos que comience con
estas dos guías:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Guía de instalación</a></li>
  <li><a href="manuals/debian-faq/">Preguntas frecuentes sobre Debian GNU/Linux</a></li>
</ul>

<p>
Téngalas a mano cuando haga su primera instalación de Debian, probablemente
contesten a muchas preguntas y le ayuden a trabajar en su nuevo sistema Debian.
</p>

<p>
Más tarde puede que quiera mirar los siguientes documentos:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Referencia de Debian</a>: una
  guía de usuario enfocada a la línea de órdenes.</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Notas de publicación</a>:
      publicadas habitualmente con las actualizaciones de Debian, para usuarios que actualizan su distribución.</li>
  <li><a href="https://wiki.debian.org/">La wiki de Debian</a>: la wiki oficial de Debian y una buena fuente
      de información para principiantes.</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">Imprima la tarjeta de referencia de Debian GNU/Linux</a></button></p>

<h2><a id="manuals">Manuales</a></h2>

<p>La mayoría de la documentación incluida en Debian ha sido escrita para
GNU/Linux en general, pero también hay algo de documentación escrita
específicamente para Debian. Báicamente, los documentos se ordenan en estas
categorías:</p>

<ul>
  <li>Manuales: 
estas guías son como libros, ya que describen de manera exhaustiva
grandes temas.
Muchos de los manuales listados más abajo están disponibles tanto en línea
como en forma de paquetes de Debian. De hecho, la mayoría de los manuales
en el sitio web están extraídos de sus respectivos paquetes Debian. Elija
un manual de la lista para ver el nombre de su paquete y/o 
enlaces a las versiones en línea.
</li>
  <li>CÓMOs: como su nombre indica, los <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">documentos
COMO</a>, tal como indica su nombre, describen <em>cómo</em> hacer algo en particular, es decir,
ofrecen consejo detallado y práctico sobre cómo realizar una tarea específica.
</li>
  <li>Preguntas frecuentes: hemos compilado varios documentos respondiendo <em>preguntas frecuentes</em>.
Las cuestiones específicas a Debian se contestan en las
<a href="manuals/debian-faq/">Preguntas frecuentes de Debian</a>.
También hay aparte unas <a href="../CD/faq/">preguntas frecuentes sobre las imágenes de DVD
de Debian</a>, respondiendo todo tipo de preguntas sobre medios de instalación.</li>
  <li>Otros documentos (más cortos): consulte también la <a href="#other">lista</a> de instrucciones más cortas.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Puede encontrar la lista completa de manuales y otra 
documentación de Debian en las páginas web del <a href="ddp">Proyecto de
Documentación de Debian</a>. También hay varios manuales orientados al usuario escritos para Debian
GNU/Linux, disponibles como <a href="books">libros impresos</a>.</p>
</aside>

Muchos de los manuales listados más abajo están disponibles tanto en línea
como en forma de paquetes de Debian; de hecho, la mayoría de los manuales
en el sitio web están extraídos de sus respectivos paquetes Debian. Elija
un manual de la lista para ver el nombre de su paquete y/o 
enlaces a las versiones en línea.

<h3>Manuales específicos de Debian</h3>

<div class="line">
  <div class="item col50">

    <h4><a href="user-manuals">Manuales para usuarios</a></h4>
    <ul>
      <li><a href="https://debian-beginners-handbook.tuxfamily.org/index-en.html">El libro de quien empieza con Debian Bookworm (en inglés)</a></li>
      <li><a href="user-manuals#faq">Preguntas frecuentes de Debian GNU/Linux</a>.</li>
      <li><a href="user-manuals#install">Guía de instalación de Debian</a>.</li>
      <li><a href="user-manuals#relnotes">Notas de publicación de Debian</a>.</li>
      <li><a href="user-manuals#refcard">Tarjeta de referencia de Debian</a></li>
      <li><a href="user-manuals#debian-handbook">El libro del administrador de Debian</a></li>
      <li><a href="user-manuals#quick-reference">Referencia de Debian</a>.</li>
      <li><a href="user-manuals#securing">Manual de seguridad de Debian</a>.</li>
      <li><a href="user-manuals#aptitude">Manual de usuario de aptitude</a></li>
      <li><a href="user-manuals#apt-guide">Manual de usuario de APT</a></li>
      <li><a href="user-manuals#java-faq">Preguntas frecuentes de DebianGNU/Linux y Java</a>.</li>
      <li><a href="user-manuals#hamradio-maintguide">Manual de mantenedores de Debian Hamradio</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manuales para desarrolladores</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Manual de normas de Debian</a>.</li>
      <li><a href="devel-manuals#devref">Referencia del desarrollador de Debian</a>.</li>
      <li><a href="devel-manuals#debmake-doc">Guía para Mantenedores de Debian</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introducción al empaquetamiento en Debian</a></li>
      <li><a href="devel-manuals#menu">El sistema de menús de Debian</a>.</li>
      <li><a href="devel-manuals#d-i-internals">El instalador de Debian a fondo</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guía para mantenedores de paquetes que usan bases de datos</a></li>
      <li><a href="devel-manuals#dbapp-policy">Normativa para paquetes que usan bases de datos</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">Lea la Historia del proyecto Debian</a></button></p>

<h2><a id="other">Otros documentos (más cortos)</a></h2>

<p>Los siguientes documentos incluyen instrucciones más rápidas y más cortas:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Si ha comprobado los recursos arriba mencionados y aún no encuentra
respuestas a sus preguntas o soluciones a sus problemas al respecto de
Debian, consulte nuestra <a href="../support">página de
soporte</a>.</p>
</aside>

  <dt><strong>Páginas de manual</strong></dt>
    <dd>Tradicionalmente, todos los programas de Unix están documentados
        en <em>páginas de manual</em>, manuales de referencia disponibles
	a través de la orden <tt>man</tt>. Normalmente no están pensados
	para principiantes, pero documentan todas las características y funciones de una orden. 
El repositorio completo de todas las páginas de manual disponibles en Debian está en línea: <a 
href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>.
        Las páginas de manual en español están disponibles tras la instalación del paquete
	<a href="https://packages.debian.org/stable/doc/manpages-es">manpages-es</a>.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">Archivos
info</a></strong></dt>
    <dd>Muchos programas de GNU se documentan a través de <em>archivos 
        info </em> en lugar de las páginas de manual. Estos archivos incluyen información
        detallada del programa mismo, opciones y ejemplos útiles y están disponibles a
        través de la orden <tt>info</tt>.
    </dd>

  <dt><strong>Documentos README</strong></dt>
    <dd>También son bastante comunes los archivos <q><em>read me</em></q> (léame).
        Son ficheros de texto simple que describen un solo objeto,
	normalmente un paquete. Podrá encontrar montones de ellos en los
	subdirectorios de <tt>/usr/share/doc/</tt> de su sistema Debian.
        Cada paquete de software tiene un subdirectorio en él con sus propios archivos
        leáme, y también puede contener ejemplos de configuración. Tenga en
        cuenta que, para programas grandes, la documentación se proporciona
        normalmente en un paquete separado (del mismo nombre que el paquete
        original,pero terminado en <em>-doc</em>).
    </dd>
</dl>
</p>
