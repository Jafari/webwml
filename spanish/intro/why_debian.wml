#use wml::debian::template title="Razones para escoger Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Laura Arjona Reina"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian para usuarios</a></li>
    <li><a href="#devel">Debian para desarrolladores</a></li>
    <li><a href="#enterprise">Debian para entornos corporativos</a></li>
  </ul>
</div>

<p>
Hay muchas razones por las que los usuarios eligen Debian como su sistema operativo –
ya sea como usuario/a, para desarrollar, o incluso en entornos corporativos. La mayoría
de los usuarios aprecian la estabilidad y los procesos de actualización de paquetes o la distribución entera
sin complicaciones. Debian también se usa ampliamente
en el sector de desarrollo de software y hardware porque funciona en numerosas
arquitecturas y dispositivos, y ofrece un sistema de seguimiento de incidencias público y otras herramientas
para el desarrollo. Si planea usar Debian en un entorno profesional,
hay beneficios adicionales como las versiones de soporte a largo plazo («Long Term Support», LTS) e imágenes para la nube.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Para mí, es el nivel perfecto de facilidad de uso y estabilidad. He usado varias distribuciones diferentes a lo largo de los años pero Debian es la única que simplemente funciona. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest en Reddit</a></p>
</aside>

<h2><a id="users">Debian para usuarios</a></h2>

<dl>
  <dt><strong>Debian es software libre.</strong></dt>
  <dd>
    Debian está hecho de software libre y de código abierto y siempre será 100%
    <a href="free">libre</a>. Cualquiera es libre de usarlo, modificarlo y
    distribuirlo. Esta es nuestra principal promesa a <a href="../users">nuestros usuarios</a>. Además, es gratuito.
  </dd>
</dl>

<dl>
  <dt><strong>Debian es estable y seguro.</strong></dt>
  <dd>
    Debian es un sistema operativo basado en Linux adecuado para un amplio rango de dispositivos incluyendo
    portátiles, ordenadores de escritorio y servidores. Proporcionamos una configuración predeterminada razonable
    para cada paquete así como actualizaciones de seguridad con regularidad durante su ciclo de vida.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tiene un soporte de hardware extenso.</strong></dt>
  <dd>
    La mayoría del hardware ya está soportado por el núcleo Linux, lo que significa que
    está soportado en Debian también. Hay disponibles controladores no libres
    para el hardware si fueran necesarios.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ofrece un instalador flexible.</strong></dt>
  <dd>
    Nuestro <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">CD «en vivo»</a>
    es para cualquier persona que quiera probar Debian antes de
    instalarlo. También incluye el instalador Calamares que facilita instalar
     Debian desde el sistema «en vivo». Usuarios con más experiencia
    pueden usar el instalador de Debian, que dispone de más opciones de personalización,
    incluyendo la posibilidad de usar una herramienta automatizada de instalación en red.
  </dd>
</dl>

<dl>
  <dt><strong>Debian proporciona actualizaciones de versión sin complicaciones.</strong></dt>
  <dd>
     Es fácil mantener nuestro sistema operativo actualizado, ya sea
    una actualización completa a una versión superior o una actualización de un solo paquete.
  </dd>
</dl>

<dl>
  <dt><strong>Debian es la base de muchas otras distribuciones.</strong></dt>
  <dd>
    Muchas de las distribuciones Linux más populares como Ubuntu, Knoppix, PureOS
    o Tails están basadas en Debian. Proporcionamos todas las herramientas
    por lo que todo el mundo puede extender los paquetes de software del archivo
    de Debian con sus propios paquetes si lo necesitan.
  </dd>
</dl>

<dl>
  <dt><strong>El proyecto Debian es una comunidad.</strong></dt>
  <dd>
    Cualquiera puede ser parte de nuestra comunidad; no necesita
    ser una persona que programe o administre sistemas. Debian tiene una
    <a href="../devel/constitution">estructura de gobierno democrático</a>.
    Al tener todos los miembros de Debian iguales derechos, no puede ser controlada por 
    una sola empresa. Tenemos desarrolladores en más de 60 países y Debian en sí
    se traduce a más de 80 idiomas.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> La razón tras el estatus de Debian como un sistema operativo para desarrolladores es el gran número de paquetes y soporte de software, que es algo importante para los desarrolladores. Está altamente recomendado para programadores avanzados y administradores de sistemas. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma en Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian para desarrolladores</a></h2>

<dl>
  <dt><strong>Múltiples arquitecturas de hardware.</strong></dt>
  <dd>
    Debian soporta una <a href="../ports">larga lista</a> de arquitecturas de CPU
    incluyendo amd64, i386, múltiples versiones de ARM y MIPS, POWER7, POWER8,
    IBM System z y RISC-V. Debian también está disponible para arquitecturas de nicho específico.
  </dd>
</dl>

<dl>
  <dt><strong>Internet de las cosas y dispositivos empotrados.</strong></dt>
  <dd>
    Debian funciona en un amplio rango de dispositivos como Raspberry Pi,
    variantes de QNAP, dispositivos móviles, enrutadores domésticos y muchos ordenadores de placa 
    reducida («Single Board Computer», SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Gran número de paquetes de software disponible.</strong></dt>
  <dd>
    Debian tiene un gran número de <a
    href="$(DISTRIB)/packages">paquetes</a> (actualmente en
    la versión estable: <packages_in_stable> paquetes) que usan el <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">formato deb</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Diferentes versiones.</strong></dt>
  <dd>
    Además de nuestra versión «estable», puede obtener las últimas versiones del software 
    usando las versiones «en pruebas» o «inestable».
  </dd>
</dl>

<dl>
  <dt><strong>Sistema de seguimiento de fallos disponible públicamente.</strong></dt>
  <dd>
    El <a href="../Bugs">sistema de seguimiento de fallos</a> («Bug Tracking System», BTS) de Debian está
    disponible públicamente para cualquiera a través de un navegador web. No escondemos los fallos
    de nuestro software y puede enviar nuevos informes de fallos de manera sencilla o unirse a la discusión.
  </dd>
</dl>

<dl>
  <dt><strong>Normativa de Debian y herramientas de desarrollo.</strong></dt>
  <dd>
    Debian ofrece software de alta calidad. Para aprender más sobre nuestros
    estándares, lea la <a href="../doc/debian-policy/">normativa</a>
    que define los requisitos técnicos de cada paquete incluido en la
    distribución. Nuestra estrategia de integración continua incluye 
    Autopkgtest (ejecuta pruebas sobre paquetes), Piuparts (prueba la instalación, actualización y desinstalación)
    y Lintian (comprueba los paquetes buscando inconsistencias o fallos).
  </dd>
</dl> 

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Estabilidad es sinónimo de Debian. [...]  La seguridad es una de las características de Debian más importantes. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis en pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian para entornos corporativos</a></h2>

<dl>
  <dt><strong>Debian es confiable.</strong></dt>
  <dd>
    Debian prueba su fiabilidad cada día en miles de escenarios del mundo real
    que van desde portátiles de un único usuario a super colisionadores, bolsa de valores
    y la industria automovilística. También es popular en el mundo académico,
    científico y en el sector público.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tiene muchos expertos.</strong></dt>
  <dd>
    Nuestros mantenedores de paquetes no solo se encargan del empaquetado de Debian y
    de incorporar nuevas versiones del software. A menudo son personas expertas en el software original
    y contribuyen al desarrollo en origen directamente.
  </dd>
</dl>

<dl>
  <dt><strong>Debian es seguro.</strong></dt>
  <dd>
    Debian tiene soporte de seguridad para sus versiones estables. Muchas otras
    distribuciones e investigadores de seguridad se apoyan en el gestor de seguridad de Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Soporte a largo plazo.</strong></dt>
  <dd>
    La versión sin coste de Debian con <a href="https://wiki.debian.org/LTS">soporte a largo plazo</a>
    («Long Term Support», LTS) extiende la vida de todas las versiones estables de Debian
    a cinco años como mínimo. Además, la iniciativa comercial de
    <a href="https://wiki.debian.org/LTS/Extended">soporte «LTS» extendido</a> amplía el soporte para un conjunto limitado de paquetes a más de 5 años.
  </dd>
</dl>

<dl>
  <dt><strong>Imágenes para la nube.</strong></dt>
  <dd>
    Hay disponibles imágenes oficiales para las principales plataformas de nube. También
    proporcionamos las herramientas y configuración para que pueda construir su propia imagen personalizada 
    para la nube. También puede usar Debian en máquinas virtuales en el escritorio o en un contenedor.
  </dd>
</dl>
