#use wml::debian::translation-check translation="5fc831c6a77ce47c0c4e44da8a3b594b8280e3dc"
<define-tag pagetitle>Publicado Debian 12 <q>bookworm</q></define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>Después de un año, nueve meses y veintiocho días de desarrollo, el proyecto
Debian se complace en presentar la nueva versión «estable» 12 (nombre en clave <q>bookworm</q>).</p>

<p>Se dará soporte a <q>bookworm</q> durante los próximos cinco años gracias al
trabajo combinado de los equipos de <a href="https://security-team.debian.org/">seguridad</a> 
y de <a href="https://wiki.debian.org/LTS">soporte a largo plazo</a> de Debian.</p>

<p>Conforme a la <a href="$(HOME)/vote/2022/vote_003">resolución general de 2022 sobre firmware no libre</a>, 
hemos creado una nueva área en el archivo que posibilita la separación del firmware
no libre del resto de paquetes no libres:</p>
<ul>
<li>non-free-firmware</li>
</ul>

<p>
La mayoría de los paquetes de firmware no libre han sido movidos de <b>non-free</b> a
<b>non-free-firmware</b>. Esta separación hace posible generar una variedad
de imágenes de instalación oficiales.
</p>

<p>Debian 12 <q>bookworm</q> se publica con varios entornos de escritorio, tales como:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>Esta versión contiene un total de <b>64&nbsp;419</b> paquetes de los que más de <b>11&nbsp;089</b> son
nuevos, mientras que más de <b>6296</b> paquetes se han eliminado por <q>obsoletos</q>. En esta versión
se han actualizado <b>43&nbsp;254</b> paquetes.

La ocupación total en disco de <q>bookworm</q> es de <b>365&nbsp;016&nbsp;420 kB (365 GB)</b>, y contiene
<b>1&nbsp;341&nbsp;564&nbsp;204</b> líneas de código.</p> 

<p><q>bookworm</q> tiene más páginas de manual traducidas que nunca gracias a nuestros
traductores y traductoras, que han hecho que haya páginas <b>man</b> disponibles en varios idiomas,
como: checo, danés, griego, finés, indonesio, macedonio, 
noruego (bokmål), ruso, serbio, sueco, ucraniano y vietnamita. 
Ahora todas las páginas de manual de <b>systemd</b> estan disponibles íntegramente en alemán.</p>

<p>Debian Med Blend incorpora un paquete nuevo: <b>shiny-server</b>, que 
simplifica las aplicaciones web científicas que usan <b>R</b>. Hemos mantenido nuestros
esfuerzos para proporcionar soporte de integración continua para los paquetes del equipo
Debian Med. Instale los metapaquetes versión 3.8.x para Debian bookworm.</p>

<p>Debian Astro Blend sigue proporcionando una solución integral para
astrónomos profesionales, entusiastas y aficionados con actualizaciones de casi
todas las versiones de los paquetes de software incluidos en el «blend». <b>astap</b> y 
<b>planetary-system-stacker</b> ayudan con el apilamiento de imágenes y con la resolución de
astrometría. Ahora está incluido <b> openvlbi</b>, el correlador de código
abierto.</p>

<p>De nuevo se soporta el arranque seguro en ARM64: los usuarios y usuarias de hardware
ARM64 compatible con UEFI pueden arrancar con el arranque seguro habilitado para aprovechar
esta funcionalidad de seguridad.</p>

<p>Más del 67% de los paquetes han sido actualizados en Debian 12 <q>bookworm</q>,
que incluye nuevas versiones de programas como:
</p>

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (servidor de correo electrónico por omisión) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>The GNU C Library 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Linux kernel 6.1 series</li>
<li>LLVM/Clang toolchain 13.0.1, 14.0 (por omisión), and 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>

<p>
Con esta extensa selección de software y su tradicional amplio
soporte de arquitecturas, Debian se mantiene fiel a su objetivo de ser
<q>el sistema operativo universal</q>. Es una elección apropiada para muchos
casos de uso: desde sistemas de escritorio hasta netbooks; desde entornos de desarrollo hasta
sistemas en cluster; y para servidores web, de almacenamiento y de bases de datos. De igual
forma, el trabajo de control de calidad que incluye instalación automática
y pruebas de actualización para todos los paquetes del archivo de Debian ayuda a que <q>bookworm</q>
satisfaga las altas expectativas que los usuarios y usuarias tienen de una versión «estable» de Debian.
</p>

<p>
En total, se proporciona soporte oficial de <q>bookworm</q> en nueve arquitecturas:
</p>
<ul>
<li>PC de 32 (i386) y de 64 bits (amd64),</li>
<li>ARM de 64 bits (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>MIPS little-endian (mipsel),</li>
<li>MIPS little-endian de 64 bits (mips64el),</li>
<li>PowerPC little-endian de 64 bits (ppc64el),</li>
<li>IBM System z (s390x)</li> 
</ul>

<p>
La arquitectura PC de 32 bits (i386) ya no cubre ningún procesador i586; el nuevo requisito mínimo
de procesador es i686. <i>Si su máquina no cumple este requisito
le recomendamos que siga con bullseye mientras dure su ciclo de
soporte.</i>
</p>

<p>El equipo de Debian para la nube («Debian Cloud team») publica <q>bookworm</q> para varios servicios de computación
en la nube: 
</p>
<ul>
<li>Amazon EC2 (amd64 y arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (generic) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amd64, arm64, ppc64el)</li>
</ul>
<p>
La imagen genericcloud debería poder ejecutarse en cualquier entorno virtualizado,
y hay también una imagen no de nube que es útil para probar el proceso de compilación.
</p>

<p>Los paquetes de GRUB <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#grub-os-prober">ya no ejecutan
por omisión os-prober para localizar otros sistemas operativos.</a></p>

<p>Entre versiones, el comité técnico decidió que Debian <q>bookworm</q> 
debería <a href="https://wiki.debian.org/UsrMerge">soportar solo el sistema de ficheros raíz con /usr fusionado</a>, abandonando el soporte 
de /usr no fusionado. El sistema de ficheros de los sistemas instalados como
buster o bullseye no será modificado; sin embargo, los sistemas que utilicen el esquema
antiguo serán convertidos durante la actualización.</p>


<h3>¿Quiere probarlo?</h3>
<p>
Si simplemente quiere probar Debian 12 <q>bookworm</q> sin instalarlo,
puede utilizar una de las <a href="$(HOME)/CD/live/">imágenes en vivo («live»)</a> que cargan y ejecutan
el sistema operativo en modo de solo lectura en la memoria del ordenador.
</p>

<p>
Estas imágenes se ofrecen para las arquitecturas <code>amd64</code> e
<code>i386</code> para su instalación en DVD, memorias USB
e instalaciones netboot. Los usuarios pueden elegir qué entornos
de escritorio probar: GNOME, KDE Plasma, LXDE, LXQt, MATE y Xfce.
Debian Live <q>bookworm</q> contiene también una imagen «live» estándar, que permite
probar un sistema Debian sin interfaz gráfica.
</p>

<p>
Si le satisface el sistema operativo, tiene la opción de instalarlo
en el disco duro de su ordenador desde la imagen en vivo. Esta incluye
el instalador independiente Calamares además del instalador estándar de Debian.
Hay más información disponible en las secciones
<a href="$(HOME)/releases/bookworm/releasenotes">notas de publicación</a> e
<a href="$(HOME)/CD/live/">imágenes de instalación en vivo</a>
del sitio web de Debian.
</p>

<p>
Para instalar Debian 12 <q>bookworm</q> directamente en el dispositivo de almacenamiento
de la computadora puede elegir entre varios tipos de medios de instalación para <a href="https://www.debian.org/download">descargar</a>,
tales como discos Blu-ray, DVD, CD, memorias USB o a través de la red.

Vea la <a href="$(HOME)/releases/bookworm/installmanual">Guía de instalación</a> para más detalles.
</p>

# Translators: some text taken from: 

<p>
Debian se puede instalar en 78 idiomas, la mayoría de ellos disponibles
tanto en interfaces de texto como gráficas.
</p>

<p>
Las imágenes de instalación se pueden descargar a través de:
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (recomendado),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> o
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vea
<a href="$(HOME)/CD/">Debian en CD</a> para más información. <q>bookworm</q>
estará también disponible físicamente en DVD, CD-ROM y discos Blu-ray
a través de numerosos <a href="$(HOME)/CD/vendors">vendedores</a>.
</p>


<h3>Actualización de Debian</h3>
<p>
Para la mayoría de las configuraciones, la herramienta de gestión de paquetes
APT maneja automáticamente las actualizaciones a Debian 12 <q>bookworm</q>
desde la versión anterior, Debian 11 <q>bullseye</q>.
</p>

<p>Antes de actualizar el sistema, recomendamos encarecidamente que haga una
copia de seguridad total, o, por lo menos, de los datos y de la información de configuración que
no pueda permitirse perder. Las herramientas y los procesos de actualización son bastante fiables, pero
un fallo de hardware en mitad de una actualización podría dejar el sistema
seriamente dañado.

Los principales elementos de los que le conviene hacer copia de seguridad son el contenido de /etc y 
de /var/lib/dpkg, el fichero /var/lib/apt/extended_states y la salida de:

<code>$ dpkg --get-selections '*' # (los apóstrofes son importantes)</code>
      
<p>Cualquier información procedente de usuarios y usuarias relacionada con la actualización de <q>bullseye</q> a <q>bookworm</q>
es bienvenida. Por favor, comparta esta información remitiendo un informe de fallo
con sus resultados al <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.en.html#upgrade-reports">sistema
de seguimiento de fallos de Debian</a>, haciendo referencia al paquete <b>upgrade-reports</b>.
</p>

<p>
Ha habido mucho desarrollo en el instalador de Debian, lo que ha resultado en un
mejor soporte de hardware y en otras mejoras como correcciones del soporte
gráfico en UTM, correcciones en el cargador de tipos de letra de GRUB, eliminación de la larga espera al
final del proceso de instalación y correcciones en la detección de sistemas
susceptibles de ser arrancados por el BIOS. Esta versión del instalador de Debian puede habilitar non-free-firmware
donde sea necesario.
</p>



<p>
El paquete <b>ntp</b> ha sido sustituido por el paquete <b>ntpsec</b>,
siendo ahora <b>systemd-timesyncd</b> el servicio de reloj del sistema por omisión; también
están soportados <b>chrony</b> y <b>openntpd</b>.
</p>


<p>Como <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split">el firmware <b>non-free</b> 
se ha movido a su propia sección en el archivo</a>, si tiene firmware
no libre instalado le recomendamos que añada <b>non-free-firmware</b> a su
sources-list de APT.</p>

<p>Es aconsejable eliminar las entradas relativas a bullseye-backports de los ficheros source-list 
de APT antes de la actualización; tras la actualización considere añadir <b>bookworm-backports</b>.

<p>
La distribución de actualizaciones de seguridad para <q>bookworm</q> se llama <b>bookworm-security</b>; los usuarios y usuarias 
deberían adaptar los ficheros source-list de APT al actualizar a Debian 12.

Si la configuración de APT incluye bloqueos («pinning») o <code>APT::Default-Release</code>, 
es probable que también necesite ajustes para permitir la actualización de paquetes a la nueva
versión «estable». Considere <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#disable-apt-pinning">desactivar el bloqueo de APT</a>.
</p>

<p>La actualización a OpenLDAP 2.5 incluye algunos <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#openldap-2.5">cambios incompatibles
que pueden requerir intervención manual</a>. Dependiendo de la configuración, el
servicio <b>slapd</b> puede permanecer parado tras la actualización hasta que se realicen
actualizaciones adicionales en la configuración.</p>


<p>El servicio <b>systemd-resolved</b> no se instala automáticamente
en las actualizaciones puesto que <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved">ahora se encuentra
en un paquete separado</a>. Si utiliza el servicio de sistema systemd-resolved, instale manualmente el nuevo paquete tras
la actualización y tenga en cuenta que, hasta que esté instalado, la resolución DNS puede no 
funcionar puesto que el servicio no estará presente en el sistema.</p>



<p>Hay algunos <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#changes-to-system-logging">cambios en la escritura de logs del sistema</a>;
el paquete <b>rsyslog</b> ya no es necesario en la mayoría de los sistemas, y no se instala
por omisión. Los usuarios pueden cambiarse a <b>journalctl</b> o utilizar las nuevas
<q>marcas de tiempo ("timestamps") de alta precisión</q> que usa ahora <b>rsyslog</b>.
</p>


<p>Los <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble">posibles problemas durante la actualización</a> incluyen
bucles en Conflictos o Pre-Dependencias que pueden resolverse eliminando
algunos paquetes o forzando la reinstalación de otros. Fuentes de preocupación
adicionales son los errores <q>No se pudo realizar la configuración inmediata...</q> para los que
será necesario mantener <b>tanto</b> <q>bullseye</q> (que acaba de eliminarse) como 
<q>bookworm</q> (que acaba de añadirse) en el fichero source-list de APT, y 
los Conflictos de archivo que pueden requerir la eliminación forzada de paquetes.
Como hemos mencionado antes, hacer un respaldo del sistema es la clave para una actualización tranquila en caso de que
se produzcan errores difíciles de resolver.</p> 


<p>Hay algunos paquetes para los que Debian no puede comprometerse a proporcionar versiones 
actualizadas que resuelvan problemas de seguridad. Consulte las <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.en.html#limited-security-support">Limitaciones
en el soporte de seguridad</a>.</p>


<p>
Como siempre, los sistemas Debian pueden actualizarse in situ, sin problemas
o tiempo de inactividad, pero se recomienda encarecidamente leer
las <a href="$(HOME)/releases/bookworm/releasenotes">Notas de publicación</a>
y la <a href="$(HOME)/releases/bookworm/installmanual">Guía de
instalación</a> para informarse de posibles problemas y obtener instrucciones detalladas para
instalar y actualizar Debian. En las próximas semanas, las Notas de publicación
continuarán mejorándose y traduciéndose a más idiomas.
</p>


<h2>Acerca de Debian</h2>

<p>
Debian es un sistema operativo libre desarrollado por
miles de voluntarios de todo el mundo que colaboran a través de
Internet. Los puntos fuertes del proyecto Debian son su base de voluntarios, su
dedicación al contrato social de Debian y al software libre, y su
compromiso por ofrecer el mejor sistema operativo posible. Esta nueva
versión es otro paso importante en esa dirección.
</p>


<h2>Información de contacto</h2>

<p>
Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;press@debian.org&gt;.
</p>

