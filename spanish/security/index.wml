#use wml::debian::template title="Información sobre seguridad" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="452896bf2af61f852728d258a34c0609dd632373"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Mantener seguro su sistema Debian</a></li>
<li><a href="#DSAS">Avisos de seguridad recientes</a></li>
<li><a href="#infos">Fuentes de información sobre seguridad</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian se toma la seguridad muy en serio. Nos hacemos cargo de todos los problemas de seguridad que reclaman nuestra atención y los corregimos en un plazo razonable.</p>
</aside>

<p>
La experiencia ha demostrado que <q>la seguridad por medio de la oscuridad</q> nunca funciona. Por tanto, la revelación pública permite encontrar soluciones mejores y en un menor plazo de tiempo a los problemas de seguridad. Sobre este tema, esta página muestra la situación de Debian respecto a diversos agujeros de seguridad conocidos que podrían, potencialmente, afectar al sistema operativo Debian.
</p>

<p>
El proyecto Debian coordina muchos avisos de seguridad con otros proveedores de software libre, con el resultado de que estos avisos se publican el mismo día que se hace pública la vulnerabilidad.
Para recibir los últimos avisos de seguridad de Debian, suscríbase
a la lista de correo <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Debian también participa en los esfuerzos de estandarización de seguridad:
</p>

<ul>
  <li>Los <a href="#DSAS">avisos de seguridad de Debian</a> son <a href="cve-compatibility">compatibles con CVE</a>.</li>
  <li>Debian <a href="oval/">publica</a> su información de seguridad usando el lenguaje <a href="https://github.com/CISecurity/OVALRepo">Open Vulnerability Assessment Language (OVAL)</a>
# Nota: se trata de un lenguaje de programación, no de un idioma
(n.t. Lenguaje abierto de detección de vulnerabilidades).</li>
</ul>

<h2><a id="keeping-secure">Cómo mantener seguro su sistema Debian</a></h2>



<p>

El paquete <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a> puede instalarse para mantener el ordenador al día con las últimas
actualizaciones de seguridad (y otras) de manera automática.

La <a href="https://wiki.debian.org/UnattendedUpgrades">página wiki</a> tiene
información más detallada sobre cómo instalar manualmente <tt>unattended-upgrades</tt>.

<p>
Para más información sobre cuestiones de seguridad en Debian, diríjase a nuestras FAQ y a nuestra documentación:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Preguntas frecuentes
sobre la seguridad</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Seguridad en Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Avisos recientes</a> <a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>

<p>Estos son los avisos de seguridad de Debian (Debian Security Advisories, DSA) recientemente enviados a la lista <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> es el enlace a la información en el <a href="https://security-tracker.debian.org/tracker">Rastreador de Seguridad de Debian (Debian Security Tracker)</a>, el número DSA enlaza al correo electrónico del anuncio.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (solo títulos)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Avisos de seguridad de Debian (resúmenes)" href="dsa-long">
:#rss#}

<h2><a id="infos">Fuentes de información sobre seguridad</a></h2>
#include "security-sources.inc"
