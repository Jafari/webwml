#use wml::debian::template title="Descarga de Debian"
#use wml::debian::translation-check translation="96a44b00371cb1e75df59d859a900aab253ee412"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Esta página contiene opciones para la descarga e instalación de Debian «estable».


<ul>
<li> <a href="../CD/http-ftp/#mirrors">Réplicas de descarga</a> de imágenes de instalación
<li> <a href="../releases/stable/installmanual">Manual de instalación</a> con instrucciones de instalación detalladas.
<li> <a href="../releases/stable/releasenotes">Notas de publicación</a>.
<li> <a href="../devel/debian-installer/">Imágenes ISO de Debian «en pruebas» («testing»)</a>
<li> <a href="../CD/verify">Verificar la autenticidad de las imágenes de Debian</a>
</ul>

<div class="line">
  <div class="item col50">
    <h2>Descargar una imagen de instalación</h2>
    <ul>
      <li>Una <a href="netinst"><strong>imagen de instalación pequeña</strong></a>:
	    se puede descargar rápidamente y debe guardarse en un disco extraíble.
	    Para utilizar esta opción debe tener una máquina con conexión a Internet
	    
	<ul class="quicklist downlist">
	  <li><a title="Descargar el instalador para PC de 64 bits Intel y AMD"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">iso netinst para
	      PC de 64 bits</a></li>
	  <li><a title="Descargar el instalador para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">iso netinst para
	      PC de 32 bits</a></li>
	  <li><a title="Descargar torrents del CD para PC de 64 bits Intel y AMD"
	         href="<stable-images-url/>/amd64/bt-cd/">torrents para PC de 64 bits (netinst)</a></li>
	  <li><a title="Descargar torrents del CD para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/bt-cd/">torrents para PC de 32 bits (netinst)</a></li>
	</ul>
      </li>
      <li>Una <a href="../CD/"><strong>imagen de instalación
      completa</strong></a>: contiene más paquetes, haciendo más fácil la instalación en máquinas
	sin conexión a Internet.
	<ul class="quicklist downlist">
	  <li><a title="Descargar instalador para PC de 64 bits Intel y AMD"
	         href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">iso de DVD-1 para PC de 64bits</a></li>
	  <li><a title="Descargar instalador para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">iso de DVD-1 para PC de 32 bits</a></li>
	  <li><a title="Descargar torrent del DVD para PC de 64 bits Intel y AMD"
	         href="<stable-images-url/>/amd64/bt-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso.torrent">torrent para PC de 64 bits (DVD)</a></li>
	  <li><a title="Descargar torrents del DVD para PC de 32 bits Intel y AMD"
		 href="<stable-images-url/>/i386/bt-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso.torrent">torrents para PC de 32 bits (DVD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
     <h2>Pruebe Debian live antes de instalar</h2>
     <p>
      Puede probar Debian arrancando un sistema «en vivo» desde un CD, DVD o USB sin 
      instalar ningún fichero en la máquina. Cuando esté listo puede ejecutar el 
      <a href="https://calamares.io">instalador Calamares</a> incluido. Disponibe solamente para PC de 64 bits.
      Lea más <a href="../CD/live#choose_live">información sobre este método</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Descarga la ISO de live con Gnome para PC de 64 bits Intel y AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live con Gnome</a></li>
      <li><a title="Descargar la ISO de live con Xfce para PC de 64 bits Intel y AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live con Xfce</a></li>
      <li><a title="Descargar la ISO de live con KDE para PC de 64 bits Intel y AMD"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live con KDE</a></li>
      <li><a title="Descargar otras ISO de live para para PC de 64 bits Intel y AMD"
            href="<live-images-url/>/amd64/iso-hybrid/">Otras imágenes ISO live</a></li>
      <li><a  title="Descargar torrents de la versión live para PC de 64 bits Intel y AMD"
       href="<live-images-url/>/amd64/bt-hybrid/">torrents de live</a></li>
     </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Adquirir un juego de CD o DVD de uno de los vendedores
    de CD de Debian</a></h2>

   <p>
      Muchos de los vendedores ofrecen la distribución por menos de 5 US$
      más gastos de envío (aunque no siempre realizan envíos internacionales).
   </p>

   <p>Las ventajas básicas de los CD son:</p>

   <ul>
     <li>Puede instalarse en máquinas sin conexión a Internet.</li>
     <li>Puede instalar Debian sin descargar 
     todos los paquetes por sí mismo.</li>
   </ul>

    <h2><a href="pre-installed">Compre una máquina con Debian
      preinstalado</a></h2>
   <p>Esto tiene una serie de ventajas:</p>
   <ul>
    <li>No tiene que instalar Debian.</li>
    <li>La instalación está preconfigurada para ajustarse al
    hardware.</li>
    <li>Puede que el fabricante proporcione soporte técnico.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2>Use una imagen de Debian para la nube</h2>
    <p>Una <a href="https://cloud.debian.org/images/cloud/"><strong>imagen oficial para la nube</strong></a>, creada por el equipo de Debian para la nube («Debian Cloud Team»), puede usarse en:</p>
    <ul>
     <li>Su proveedor de OpenStack, en formato qcow2 o raw.
        <ul class="quicklist downlist">
	   <li>AMD o Intel de 64 bits (<a title="Imagen qcow2 de OpenStack para AMD o Intel de 64 bits" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="Imagen raw de OpenStack para AMD o Intel de 64 bits" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>ARM de 64 bits (<a title="Imagen qcow2 de OpenStack para ARM de 64 bits" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="Imagen raw de OpenStack para ARM de 64 bits" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
	   <li>PowerPC de 64 bits Little Endian (<a title="Imagen qcow2 de OpenStack para PowerPC de 64 bits Little Endian" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="Imagen raw de OpenStack para PowerPC de 64 bits Little Endian" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
        </ul>
      </li>
      <li>una máquina virtual local QEMU, en formatos qcow2 o raw.
      <ul class="quicklist downlist">
	   <li>AMD o Intel de 64 bits (<a title="Imagen qcow2 de QEMU para  AMD o Intel de 64 bits" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="Imagen raw de QEMU para AMD o Intel de 64 bits" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
       <li>ARM de 64 bits (<a title="Imaen qcow2 de QEMU para ARM de 64 bits" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="Imagen raw de QEMU para ARM de 64 bits" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
	   <li>PowerPC de 64 bits Little Endian  (<a title="Imagen qcow2 de QEMU para PowerPC de 64 bits Little Endian" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="Imagen raw de QEMU para PowerPC de 64 bits Little Endian" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, ya sea como imagen de máquina o a través del «AWS Marketplace».
	   <ul class="quicklist downlist">
	    <li><a title="Imágenes de máquina de Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Imágenes de máquina de Amazon</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	     </ul>
      </li>
      <li>Microsoft Azure, en el «Azure Marketplace».
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 en Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 («Bookworm»)</a></li>
	    <li><a title="Debian 11 en Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 («Bullseye»)</a></li>
	   </ul>
      </li>
    </ul>
  </div>
</div>
