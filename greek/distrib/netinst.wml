#use wml::debian::template title="Εγκαθιστώντας το Debian από το Διαδίκτυο" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83" maintainer="galaxico"




<p>Αυτή η μέθοδος εγκατάστασης του Debian απαιτεί μια λειτουργική σύνδεση στο 
Διαδίκτυο 
<em>στη διάρκεια</em> της εγκατάστασης. Σε σύγκριση με άλλες μεθόδους, 
καταλήγετε να μεταφορτώνετε λιγότερα δεδομένα καθώς η διαδικασία θα είναι στα 
μέτρα των δικών σας απαιτήσεων. Υποστηρίζονται ασύρματες συνδέσεις και 
συνδέσεις με Ethernet. Εσωτερικές κάρτες ISDN δυστυχώς
<em>δεν</em> υποστηρίζονται.</p>
<p>Υπάρχουν τρεις επιλογές για εγκαταστάσεις μέσω δικτύου:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">Μικρά CD ή κλειδιά USB</toc-add-entry>

<p>Τα ακόλουθα είναι αρχεία εικόνων. Επιλέξτε την αρχιτεκτονική του επεξεργαστή 
σας.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Για λεπτομέρειες, παρακαλούμε δείτε: <a 
href="../CD/netinst/">Δικτυακή εγκατάσταση από ένα CD ελάχιστου μεγέθους</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">Μικρά CD, ευέλικτα κλειδιά USB 
κλπ.</toc-add-entry>

<p>Μπορείτε να μεταφορτώσετε ένα-δύο αρχεία εικόνων μικρού μεγέθους, κατάλληλα 
για κλειδιά USB και παρόμοιες συσκευές, να τα γράψετε στο αντίστοιχο μέσο και 
στη συνέχεια να ξεκινήσετε την εγκατάσταση εκκινώντας από αυτό.</p>

<p>Υπάρχει κάποια διαφοροποίηση στην υποστήριξη της εγκατάστασης από διάφορες 
πολύ μικρές εικόνες μεταξύ των αρχιτεκτονικών.
</p>

<p>Για λεπτομέρειες, παρακαλούμε κοιτάξτε το
<a href="$(HOME)/releases/stable/installmanual">εγχειρίδιο εγκατάστασης για την 
αρχιτεκτονική σας</a>, ιδιαίτερα το κεφάλαιο
<q>Αποκτώντας τα Μέσα Εγκατάστασης του Συστήματος</q>.</p>

<p>
Εδώ είναι οι σύνδεσμοι στα διαθέσιμα αρχεία εικόνων (κοιτάξτε το αρχείο  
MANIFEST για πληροφορίες):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Εκκίνηση από το δίκτυο</toc-add-entry>

<p>Ρυθμίζετε έναν εξυπηρετητή TFTP και έναν εξυπηρετητή DHCP (ή BOOTP, ή 
RARP) ο οποίος θα χρησιμεύσει για την παροχή των μέσων εγκατάστασης στα 
μηχανήματα του τοπικού σας δικτύου. Αν το BIOS του μηχανήματος-πελάτη 
το υποστηρίζει, τότε μπορείτε να εκκινήσετε το σύστημα εγκατάστασης του Debian 
από το δίκτυο (με χρήση PXE και TFTP), και να συνεχίσετε με την εγκατάσταση του 
υπόλοιπου συστήματος του Debian από το δίκτυο.</p>

<p>Δεν υποστηρίζουν όλα τα μηχανήματα την εκκίνηση από το δίκτυο. Εξαιτίας της 
επιπλέον δουλειάς που απαιτείται, αυτή η μέθοδος εγκατάστασης του Debian δεν 
ενδείκνυται για αρχάριους χρήστες.</p>

<p>Για λεπτομέρειες, παρακαλούμε δείτε το
<a href="$(HOME)/releases/stable/installmanual">εγχειρίδιο εγκατάστασης για την 
αρχιτεκτονική σας</a>, ιδιαίτερα το κεφάλαιο
<q>Προετοιμάζοντας τα αρχεία για δικτυακή εκκίνηση με TFTP</q>.</p>
<p>Εδώ είναι οι σύνδεσμοι για τα αρχεία εικόνων (κοιτάξτε το αρχείο MANIFEST 
για πληροφορίες):</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Αν οποιοδήποτε υλικό στο σύστημά σας <strong>απαιτεί την φόρτωση μη ελεύθερου υλισμικού</strong>
με τους οδηγούς συσκευών, μπορείτε να χρησιμοποιήσετε ένα από τα
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
συμπιεσμένα αρχεία πακέτων υλισμικού</a> ή κατεβάστε μια <strong>ανεπίσημη</strong> εικόνα
που περιλαμβάνει αυτό το <strong>μη ελεύθερο</strong> υλισμικό. Οδηγίες για το πώς να
χρησιμοποιήσετε αυτά τα συμπιεσμένα αρχεία και γενικές πληροφορίες σχετικά με τη φόρτωση υλισμικού
σε μια εγκατάσταση μπορούν να βρεθούν στον <a href="../releases/stable/amd64/ch06s04">Οδηγό Εγκατάστασης</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">ανεπίσημες εικόνες
εγκατάστασης για την  <q>σταθερή</q> έκδοση που περιλαμβάνουν υλισμικό</a>
</p>
</div>
