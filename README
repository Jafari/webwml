
                               Debian "webwml"
                               ---------------


    If you are interested in working on the Debian web pages, please see:

                    https://www.debian.org/devel/website/

Build requirements
------------------

There are a range of tools needed to build even the basic bits of the
website, and then some extra/optional packages that you'll need for
some of the translations. Here's a (probably incomplete!) list...

Basics:

 * debiandoc-sgml       (for writing and formatting documentation in DebianDoc SGML,
                         a simple SGML, DTD  used by the Debian project for creating
                         documentation)
 * gettext              (to handle the .po templates)
 * git                  (you probably have this already if you're reading this!)
 * isoquery             (for language names in the l10n status)
 * libintl-perl         (for building Debian Weekly News RDF files)
 * liblocale-codes-perl (for set of codes and their corresponding names for
                         various international standards)
 * libxml-feedpp-perl   (for parsing and creating syndication feeds, such as RSS
                         and Atom)
 * libxml-rss-perl      (for parsing and creating RSS "Rich Site Summary")
 * libyaml-tiny-perl    (for lightweight and minimalist YAML "YAML Ain't Markup
                         Language" parser and emitter)
 * make                 (to build the site)
 * python3-lxml         (for the oval security advisories)
 * wget                 (the build will use wget to grab some files as it runs)
 * wml                  (the templating engine we use)


Translations:

 * opencc          (Chinese translation conversions)

 
You can find a comprehensive list of packages that needs to be installed 
to complete build the Debian website in the recipe used for www-master.debian.org:

https://salsa.debian.org/dsa-team/mirror/debian.org/blob/master/debian/control

(Note: that list includes the packages needed by the other scripts that are called
by cron and perform other tasks needed by the website but not contained in the webwml
repo (e.g. the manuals showed under www.debian.org/docs, release notes, 
tidy and URL checks...). See https://www.debian.org/devel/website/ for details

------------------

When building, you can pass STRICT_ERROR_CHECKS=1 to make, if you want the build
to stop when errors due to missing dependencies or missing data are found.

For a local build you probably need to use the USE_SAMPLE_FILES=1 setting, to 
use (small) sample files for data that is usually generated from cronjobs on the
official website. Examples include:

    $(DATADIR)/Maintainers
    $(ENGLISHSRCDIR)/$(CUR_DIR)/data/%
    $(L10N_DIR)/data/%
    $(ENGLISHDIR)/mirror/Mirrors.masterlist

See README_CI.gitlab-ci.yml for an example of the use of these settings.

------------------

To see the results of a build before pushing it out to the mirror network 
you can build in www-master.debian.org and then have a look at:

https://www-staging.debian.org/

------------------
